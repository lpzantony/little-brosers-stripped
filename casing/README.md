# Drops's casing 3D model

Here are 3D files for the Drop's casing.

The native format is the .f3d file. It is supposed to be oppened with [AutoCAD Fusion 360](https://www.autodesk.com/products/fusion-360/overview) (only available on Windows and MacOS).

Other files are just there for compatibility if we need to open this project in another software.

Actually, I just exported my .f3d design using as many formats as I could.