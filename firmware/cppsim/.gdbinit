#target ext :2331
#mon endian little
#mon halt

# User interface with asm, regs and cmd windows
define split
	layout split
	layout src
#	layout regs
	focus cmd
end

define criterion
	target remote localhost:1234
end

define flash
	mon halt
	load
	mon reset
end
split
b main
run


