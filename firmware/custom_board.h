#ifndef BOARD_H
#define BOARD_H

#ifdef __cplusplus
extern "C" {
#endif

#include "nrf_gpio.h"

// LEDs definitions for PCA10040
#define LEDS_NUMBER 1

#define LED_START 6
#define LED_1 6
#define LED_STOP 6

#define LEDS_ACTIVE_STATE 1

#define LEDS_INV_MASK LEDS_MASK

#define LEDS_LIST                                                              \
    { LED_1 }

#define BSP_LED_0 LED_1
#define BSP_LED_1 LED_1

#define BUTTONS_NUMBER 0

#define BUTTONS_ACTIVE_STATE 0

#define BUTTONS_LIST                                                           \
    {}

#define FLASH_SCK_PIN 14
#define FLASH_MOSI_PIN 12
#define FLASH_MISO_PIN 19
#define FLASH_SS_PIN 10
#define FLASH_WP_PIN 20
#define FLASH_RESET_PIN 16

// Low frequency clock source to be used by the SoftDevice
#define NRF_CLOCK_LFCLKSRC                                                     \
    {                                                                          \
        .source = NRF_CLOCK_LF_SRC_XTAL, .rc_ctiv = 0, .rc_temp_ctiv = 0,      \
        .accuracy = NRF_CLOCK_LF_ACCURACY_20_PPM                               \
    }

#ifdef __cplusplus
}
#endif

#endif // BOARD_H
