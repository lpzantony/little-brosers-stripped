#include "diff_service.h"
#include "ble_gatts.h"
#include "ble_stack.h"
#include "drop.h"
#include "nrf_delay.h"
#include "power_management.h"
#include "services.h"
#include "task_stack.h"

#define BLE_DIFF_STATUS_START_ACK 4
#define BLE_DIFF_STATUS_MID_ACK 5
#define BLE_DIFF_STATUS_END_ACK 6
#define BLE_MERGE_STATUS_WRITTEN 7
#define BLE_MERGE_STATUS_NONE 8
#define BLE_MERGE_STATUS_END 9

#define BLE_DIFF_START_FLAG 0
#define BLE_DIFF_MID_FLAG 1
#define BLE_DIFF_END_FLAG 2

#define BLE_UUID_NOTIFICATION_TOGGLE 0x2902

static ble_diffs_t *running_diff_service;
static volatile int transaction_state = 0;
static volatile int merge_next_step = 0;

static void merge_handle_msg_in();
static void merge_handle_msg_out();

static void set_transaction_state(int state) { transaction_state = state; }

static void handle_status_write() {
    uint8_t status_value;
    ble_gatts_value_t status;
    status.len = 1;
    status.offset = 0;
    status.p_value = &status_value;

    sd_ble_gatts_value_get(running_diff_service->conn_handle,
                           running_diff_service->status_handles.value_handle,
                           &status);

    switch (status_value) {
    case BLE_DIFF_STATUS_START_ACK:
        set_transaction_state(BLE_DIFF_START_FLAG);
        break;
    case BLE_DIFF_STATUS_MID_ACK:
        set_transaction_state(BLE_DIFF_MID_FLAG);
        break;
    case BLE_DIFF_STATUS_END_ACK:
        set_transaction_state(BLE_DIFF_END_FLAG);
        break;
    case BLE_MERGE_STATUS_WRITTEN:
        set_transaction_state(BLE_MERGE_STATUS_WRITTEN);
        merge_next_step = 1;
        LBLOG("Received merge status written");
        break;
    case BLE_MERGE_STATUS_NONE:
        set_transaction_state(BLE_MERGE_STATUS_NONE);
        merge_next_step = 1;
        LBLOG("Received merge status none");
        break;
    case BLE_MERGE_STATUS_END:
        set_transaction_state(BLE_MERGE_STATUS_END);
        merge_next_step = 1;
        LBLOG("Received merge status end (should not happen)");
        merge_pending = 0;
        // TODO: End merge and clean up service for next usage
        break;
    }
}

static void write_handler(ble_evt_t const *p_ble_evt) {
    uint16_t uuid = p_ble_evt->evt.gatts_evt.params.write.uuid.uuid;
    // Make long writes trigger the same callbacks for normal writes
    bool write_long = 0;
    do {
        write_long = 0;
        switch (uuid) {
        case BLE_UUID_DIFF_STATUS_CHAR:
            handle_status_write();
            break;
        case BLE_UUID_DIFF_PAYLOAD_CHAR:
            // No implementation needed
            LBLOG("Diff payload write");
            break;
        case BLE_UUID_NOTIFICATION_TOGGLE:
            // No implementation needed
            break;
        default:
            if (p_ble_evt->evt.gatts_evt.params.write.op ==
                BLE_GATTS_OP_EXEC_WRITE_REQ_NOW) {
                uint16_t value_handle = *(uint16_t *)mem_block.p_mem;
                write_long = 1;
                if (value_handle ==
                    running_diff_service->payload_handles.value_handle) {
                    uuid = BLE_UUID_DIFF_PAYLOAD_CHAR;
                } else {
                    write_long = 0;
                }
            }
            NRF_LOG_WARNING("Unsupported UUID: %x",
                            p_ble_evt->evt.gatts_evt.params.write.uuid.uuid);
            break;
        }
    } while (write_long);
}

static void diff_service_reset() {
    set_transaction_state(BLE_DIFF_END_FLAG);
    merge_pending = 1;
}

static void add_characteristics(ble_diffs_t *p_diff_service) {
    ret_code_t err_code;
    // UUID
    ble_uuid_t ble_uuid;
    ble_uuid.type = p_diff_service->uuid_type;

    // Client Characteristic Configuration Descriptor Metadata
    ble_gatts_attr_md_t cccd_md = {0};
    // Set GAP connection security to open for read and write permissions
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.write_perm);
    // Storing the CCCD value in stack (softdevice) memory
    cccd_md.vloc = BLE_GATTS_VLOC_STACK;

    ble_gatts_char_md_t char_md = {0};
    char_md.p_char_user_desc = NULL;
    char_md.p_char_pf = NULL;
    char_md.p_user_desc_md = NULL;
    char_md.p_cccd_md = &cccd_md;
    char_md.p_sccd_md = NULL;

    // Attribute Metadata
    ble_gatts_attr_md_t attr_md = {0};
    // Set GAP connection security to open for read and write permissions
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.write_perm);
    // Storing the attribute value in stack (softdevice) memory
    attr_md.vloc = BLE_GATTS_VLOC_STACK;
    attr_md.rd_auth = 0;
    attr_md.wr_auth = 0;
    attr_md.vlen = 0;

    ble_gatts_attr_t attr_char_value = {0};
    attr_char_value.p_uuid = &ble_uuid;
    attr_char_value.p_attr_md = &attr_md;
    attr_char_value.init_offs = 0;

    // Status characteristic
    // UUID
    ble_uuid.uuid = BLE_UUID_DIFF_STATUS_CHAR;

    // Characteristic Metadata
    char_md.char_props.write_wo_resp = 1;
    char_md.char_props.read = 1;
    char_md.char_props.notify = 1;

    // Characteristic Value Attribute
    attr_char_value.init_len = BLE_DIFFS_STATUS_CHAR_LEN;
    attr_char_value.max_len = BLE_DIFFS_STATUS_CHAR_LEN;

    attr_md.vlen = 0;

    err_code = sd_ble_gatts_characteristic_add(p_diff_service->service_handle,
                                               &char_md, &attr_char_value,
                                               &p_diff_service->status_handles);
    APP_ERROR_CHECK(err_code);

    // Diff payload characteristic
    // UUID
    ble_uuid.uuid = BLE_UUID_DIFF_PAYLOAD_CHAR;

    // Characteristic Metadata
    char_md.char_ext_props.reliable_wr = 0;
    char_md.char_props.write_wo_resp = 0;
    char_md.char_props.write = 1;
    char_md.char_props.read = 1;
    char_md.char_props.notify = 0;

    // Characteristic Value Attribute
    attr_char_value.init_len = BLE_DIFFS_PAYLOAD_CHAR_LEN;
    attr_char_value.max_len = BLE_DIFFS_PAYLOAD_CHAR_LEN;

    attr_md.vlen = 1;

    err_code = sd_ble_gatts_characteristic_add(
        p_diff_service->service_handle, &char_md, &attr_char_value,
        &p_diff_service->payload_handles);
    APP_ERROR_CHECK(err_code);
}

void diff_service_init(ble_diffs_t *p_diff_service) {
    ble_uuid128_t diffs_base_uuid = BLE_UUID_BASE;
    update_base_uuid(&diffs_base_uuid);

    // Initialize the service structure
    p_diff_service->conn_handle = BLE_CONN_HANDLE_INVALID;

    // Add a new vendor specific UUID to the softdevice table
    ble_uuid_t diffs_uuid;
    ret_code_t err_code =
        sd_ble_uuid_vs_add(&diffs_base_uuid, &p_diff_service->uuid_type);
    APP_ERROR_CHECK(err_code);
    diffs_uuid.type = p_diff_service->uuid_type;
    diffs_uuid.uuid = BLE_UUID_DIFF_SERVICE;

    // Add the service to the GATT
    err_code =
        sd_ble_gatts_service_add(BLE_GATTS_SRVC_TYPE_PRIMARY, &diffs_uuid,
                                 &p_diff_service->service_handle);
    APP_ERROR_CHECK(err_code);

    add_characteristics(p_diff_service);

    running_diff_service = p_diff_service;
}

void ble_diffs_on_ble_evt(ble_evt_t const *p_ble_evt, void *p_context) {
    if (p_ble_evt == NULL) {
        return;
    }

    ble_diffs_t *p_diff_service = (ble_diffs_t *)p_context;

    switch (p_ble_evt->header.evt_id) {
    case BLE_GAP_EVT_CONNECTED:
        NRF_LOG_INFO("Gap evt connected");
        p_diff_service->conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
        diff_service_reset();
        break;
    case BLE_GAP_EVT_DISCONNECTED:
        NRF_LOG_INFO("Gap evt disconnected");
        p_diff_service->conn_handle = BLE_CONN_HANDLE_INVALID;
        diff_service_reset();
        break;
    case BLE_GATTS_EVT_WRITE:
        if (auth_service_check_status()) {
            LBLOG("Gatt evt write diff/merge");
            write_handler(p_ble_evt);
        }
        break;
    default:
        // No implementation needed
        break;
    }
}

err_t lb_send(lb_task_stack *p_st, void *arg) {
    ble_gatts_value_t diff_message;
    diff_message.len = p_st->buff.used;
    diff_message.offset = 0;
    diff_message.p_value = p_st->buff.payload;

    // Wait for start ack
    while (transaction_state != BLE_DIFF_START_FLAG) {
        power_manage();
    }
    // LBLOG("Received start ack");

    // Write payload to payload char
    ret_code_t err_code = sd_ble_gatts_value_set(
        running_diff_service->conn_handle,
        running_diff_service->payload_handles.value_handle, &diff_message);
    APP_ERROR_CHECK(err_code);

    // Write mid ack
    set_transaction_state(BLE_DIFF_MID_FLAG);
    uint8_t status = BLE_DIFF_STATUS_MID_ACK;
    ble_gatts_hvx_params_t hvx_params = {0};
    hvx_params.handle = running_diff_service->status_handles.value_handle;
    hvx_params.type = BLE_GATT_HVX_NOTIFICATION;
    uint16_t len = 1;
    hvx_params.p_len = &len;
    hvx_params.p_data = &status;
    err_code = sd_ble_gatts_hvx(running_diff_service->conn_handle, &hvx_params);
    APP_ERROR_CHECK(err_code);

    // LBLOG("Sent mid ack");

    // Wait end ack
    while (transaction_state == BLE_DIFF_MID_FLAG) {
        power_manage();
    }

    return LB_OK;
}

err_t lb_recv(lb_task_stack *p_st, void *arg) {
    if (running_diff_service->conn_handle == BLE_CONN_HANDLE_INVALID) {
        return LB_ERR;
    }

    // Write start ack
    uint8_t status = BLE_DIFF_STATUS_START_ACK;
    ble_gatts_hvx_params_t hvx_params = {0};
    hvx_params.handle = running_diff_service->status_handles.value_handle;
    hvx_params.type = BLE_GATT_HVX_NOTIFICATION;
    uint16_t len = 1;
    hvx_params.p_len = &len;
    hvx_params.p_data = &status;

    set_transaction_state(BLE_DIFF_START_FLAG);
    ret_code_t err_code;
    do {
        err_code =
            sd_ble_gatts_hvx(running_diff_service->conn_handle, &hvx_params);
    } while (err_code == NRF_ERROR_INVALID_STATE);
    APP_ERROR_CHECK(err_code);

    // Wait mid ack
    while (transaction_state != BLE_DIFF_MID_FLAG) {
        power_manage();
    }

    // Read payload value
    ble_gatts_value_t diff_message;
    diff_message.len = LB_STACK_PLD_SIZE;
    diff_message.offset = 0;
    diff_message.p_value = p_st->buff.payload;
    sd_ble_gatts_value_get(running_diff_service->conn_handle,
                           running_diff_service->payload_handles.value_handle,
                           &diff_message);

    // Write end ack
    set_transaction_state(BLE_DIFF_END_FLAG);
    status = BLE_DIFF_STATUS_END_ACK;
    err_code = sd_ble_gatts_hvx(running_diff_service->conn_handle, &hvx_params);
    APP_ERROR_CHECK(err_code);

    return LB_OK;
}

// A message was written, attach it to the tree
static void merge_handle_msg_in() {
    // Get the message from the GATT payload char
    uint8_t buff[MESSAGE_MAX_SIZE];
    ble_gatts_value_t recv_message = {0};
    recv_message.len = MESSAGE_MAX_SIZE;
    recv_message.p_value = buff;
    sd_ble_gatts_value_get(running_diff_service->conn_handle,
                           running_diff_service->payload_handles.value_handle,
                           &recv_message);
    // Attach the message to the merkle tree
    message_t tmp_message;
    message_from_raw(buff, &tmp_message);
    message_pool_t tmp_pool = {&tmp_message, 1};

    LBLOG("Received message");
    for (int i = 0; i < 6; i++) {
        LBLOG("%d", tmp_message.signature[i]);
    }

    lb_attach_msgs_to_tree(&root, &tmp_pool, &slot_pool, store_msg, &spi_ctx);
}

// If there is still a message to send, write it to the payload
// Set the status flag accordingly
static void merge_handle_msg_out() {
    uint8_t buff[MESSAGE_MAX_SIZE];
    // Check if there is a message to send
    size_t written;
    err_t ret =
        lb_next_msg_to_send(load_msg, &spi_ctx, &slot_pool, buff, &written);

    if (ret != LB_OK) {
        LBLOG("Error getting next message to send");
        // TODO Handle emergency disconnect
        return;
    }

    if (written != 0) {
        // There is a message to send, write it to the GATT payload
        LBLOG("Sending message");
        ble_gatts_value_t send_message = {0};
        send_message.len = MESSAGE_MAX_SIZE;
        send_message.p_value = buff;
        sd_ble_gatts_value_set(
            running_diff_service->conn_handle,
            running_diff_service->payload_handles.value_handle, &send_message);

        // Write the WRITTEN flag in the status char
        LBLOG("Sending written");
        uint8_t status = BLE_MERGE_STATUS_WRITTEN;
        ble_gatts_hvx_params_t hvx_params = {0};
        hvx_params.handle = running_diff_service->status_handles.value_handle;
        hvx_params.type = BLE_GATT_HVX_NOTIFICATION;
        uint16_t len = BLE_DIFFS_STATUS_CHAR_LEN;
        hvx_params.p_len = &len;
        hvx_params.p_data = &status;
        sd_ble_gatts_hvx(running_diff_service->conn_handle, &hvx_params);
    } else {
        // There is no message to send from this device
        uint8_t status;
        if (transaction_state == BLE_MERGE_STATUS_WRITTEN) {
            // The other device just sent a message and may have more, do not
            // end yet
            LBLOG("Sending none");
            status = BLE_MERGE_STATUS_NONE;
        } else {
            // The other device has no more message either, end the process
            LBLOG("Sending end");
            status = BLE_MERGE_STATUS_END;
            merge_pending = 0;
        }
        ble_gatts_hvx_params_t hvx_params = {0};
        hvx_params.handle = running_diff_service->status_handles.value_handle;
        hvx_params.type = BLE_GATT_HVX_NOTIFICATION;
        uint16_t len = BLE_DIFFS_STATUS_CHAR_LEN;
        hvx_params.p_len = &len;
        hvx_params.p_data = &status;
        sd_ble_gatts_hvx(running_diff_service->conn_handle, &hvx_params);
    }
}

void merge_process() {
    while (merge_pending) {
        // Wait for the next status write
        while (!merge_next_step) {
            power_manage();
        }
        merge_next_step = 0;
        LBLOG("In merge step");

        switch (transaction_state) {
        case BLE_MERGE_STATUS_WRITTEN:
            merge_handle_msg_in();
        case BLE_MERGE_STATUS_NONE:
            merge_handle_msg_out();
        }
    }
}
