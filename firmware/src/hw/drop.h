#ifndef WAKE_DROP_H
#define WAKE_DROP_H

#include "flash.h"
#include "merkle_tree.h"
#include "slot_pool.h"
#include "task_stack.h"

extern lb_merkle_root_md root_md;
extern lb_merkle_tree root;
extern lb_slot_pool slot_pool;
extern lb_task_stack task_stack;
extern lb_spi_ctx spi_ctx;
extern volatile char merge_pending;
extern volatile uint32_t counter;

err_t lb_init_drop(void);

#endif