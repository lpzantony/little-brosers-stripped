#ifndef FLASH_H
#define FLASH_H

#include "extflash_info.h"
#include "message.h"
#include "slot_pool.h"
#include "utils.h"
#include <stdint.h>
#include <stdlib.h>

#define BUFF_LENGTH 255 /**< Transfer length. */
#define MAX_SPI_PAYLOAD 251

typedef struct {
    uint8_t rx_buffer[BUFF_LENGTH];
    uint8_t tx_buffer[BUFF_LENGTH];
    int spi_busy;
    uint16_t slot_pool_addr;
    uint16_t merkle_tree_addr;
} lb_spi_ctx;

// Init the flash
err_t extflash_init(lb_spi_ctx *ctx);

// Request DevID to the flash and check it
err_t extflash_check_edi(lb_spi_ctx *ctx);

// Print the status register content of the flash
err_t extflash_print_status(lb_spi_ctx *ctx);

// Return 1 if the flash is ready to receive cmds
int extflash_is_ready(lb_spi_ctx *ctx);

// Return 1 if an error occured during the last write/erase cmd
int extflash_prgm_err(lb_spi_ctx *ctx);

// Erase a specifig page
// Return LB_MASS_STORAGE_PAGE_ERROR the flash could not erase in
// the asked page
err_t extflash_page_erase(lb_spi_ctx *ctx, const uint16_t pg_addr);

// Erase the whole chip
err_t extflash_chip_erase(lb_spi_ctx *ctx);

err_t extflash_go_to_ultra_sleep(lb_spi_ctx *ctx);

err_t extflash_get_out_ultra_sleep(lb_spi_ctx *ctx);

// Read bytes from a page up to 528 bytes
err_t extflash_read_raw(lb_spi_ctx *ctx, const uint16_t pg_addr,
                        uint8_t *p_data, const size_t size);

err_t extflash_buff_write_raw(lb_spi_ctx *ctx, const uint16_t buff_addr,
                              uint8_t *p_data, const size_t size);

// Write bytes to a page up to 528 bytes
// Return LB_MASS_STORAGE_PAGE_ERROR the flash could not write in
// the asked page
err_t extflash_write_raw(lb_spi_ctx *ctx, const uint16_t pg_addr,
                         uint8_t *p_data, const size_t size);

// Read only the two first bytes a of page to get a message's size
err_t load_msg_size(lb_spi_ctx *ctx, const uint16_t pg_addr, size_t *msg_size);

// Fill a message_t struct from flash
err_t load_msg(void *p_ctx, const uint16_t pg_addr, message_t *p_msg);

// Store a full message_t struct into the flash as a byte array
// Return LB_MASS_STORAGE_PAGE_ERROR the flash could not write in
// the asked page
err_t store_msg(void *p_ctx, const uint16_t pg_addr, message_t *p_msg);

err_t lb_store_slot_pool(lb_slot_pool *p_pool, void *_ctx);
err_t lb_load_slot_pool(lb_slot_pool *p_pool, void *_ctx);

#endif // FLASH_H
