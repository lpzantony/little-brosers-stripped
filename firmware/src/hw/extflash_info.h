#ifndef EXTFLASH_INFO_H
#define EXTFLASH_INFO_H

/* SPI Commands
 * *********************************************************************/

/* Read commands */

#define EXTFLASH_RDMN 0xd2       /* Main Memory Page Read */
#define EXTFLASH_RDARRAYLP 0x01  /* Continuous Array Rd (Low Power Mode) */
#define EXTFLASH_RDARRAYLF 0x03  /* Continuous Array Rd (Low Frequency) */
#define EXTFLASH_RDARRAYHF 0x0b  /* Continuous Array Rd (High Frequency) */
#define EXTFLASH_RDARRAYHF2 0x1b /* Continuous Array Rd 2 (High Frequency) */
#define EXTFLASH_RDARRY 0xe8     /* Continuous Array Rd (Legacy, Obsolete) */
#define EXTFLASH_RDBF1LF 0xd1    /* Buffer 1 Read (Low Frequency) */
#define EXTFLASH_RDBF2LF 0xd3    /* Buffer 2 Read (Low Frequency) */
#define EXTFLASH_RDBF1 0xd4      /* Buffer 1 Read */
#define EXTFLASH_RDBF2 0xd6      /* Buffer 2 Read */

/* Program and Erase Commands */

#define EXTFLASH_WRBF1 0x84         /* Buffer 1 Write */
#define EXTFLASH_WRBF2 0x87         /* Buffer 2 Write */
#define EXTFLASH_BF2TOMNE 0x86      /* BF2->MainMem Page Prgm Built-in Erase */
#define EXTFLASH_BF1TOMNE 0x83      /* BF1->MainMem Page Prgm Built-in Erase */
#define EXTFLASH_BF1TOMN 0x88       /* BF1->MainMem Page Prgm */
#define EXTFLASH_BF2TOMN 0x89       /* BF2->MainMem Page Prgm */
#define EXTFLASH_MNTHRUBF1 0x82     /* MainMem Page Program Through Buffer 1 */
#define EXTFLASH_MNTHRUBF2 0x85     /* MainMem Page Program Through Buffer 2 */
#define EXTFLASH_MNTHRUBF1BP 0x02   /* MainMem Byte/Page Program Through BF1 */
#define EXTFLASH_PGERASE 0x81       /* Page Erase */
#define EXTFLASH_BLKERASE 0x50      /* Block Erase */
#define EXTFLASH_SECTERASE 0x7c     /* Sector Erase */
#define EXTFLASH_CHIPERASE1 0xc7    /* Chip Erase - byte 1 */
#define EXTFLASH_CHIPERASE2 0x94    /* Chip Erase - byte 2 */
#define EXTFLASH_CHIPERASE3 0x80    /* Chip Erase - byte 3 */
#define EXTFLASH_CHIPERASE4 0x9a    /* Chip Erase - byte 4 */
#define EXTFLASH_SUSPEND 0xb0       /* Program/Erase Suspend */
#define EXTFLASH_RESUME 0xd0        /* Program/Erase Resume */
#define EXTFLASH_RDMDWRTHRUBF1 0x58 /* Read-Modify-Write through Buffer 1 */
#define EXTFLASH_RDMDWRTHRUBF2 0x59 /* Read-Modify-Write through Buffer 2 */

/* Protection and Security Commands */

#define EXTFLASH_ENABPROT1 0x3d  /* Enable Sector Protection - byte 1 */
#define EXTFLASH_ENABPROT2 0x2a  /* Enable Sector Protection - byte 2 */
#define EXTFLASH_ENABPROT3 0x7f  /* Enable Sector Protection - byte 3 */
#define EXTFLASH_ENABPROT4 0xa9  /* Enable Sector Protection - byte 4 */
#define EXTFLASH_DISABPROT1 0x3d /* Disable Sector Protection - byte 1 */
#define EXTFLASH_DISABPROT2 0x2a /* Disable Sector Protection - byte 2 */
#define EXTFLASH_DISABPROT3 0x7f /* Disable Sector Protection - byte 3 */
#define EXTFLASH_DISABPROT4 0x9a /* Disable Sector Protection - byte 4 */
#define EXTFLASH_ERASEPROT1 0x3d /* Erase Sector Protection Reg - byte 1 */
#define EXTFLASH_ERASEPROT2 0x2a /* Erase Sector Protection Reg - byte 2 */
#define EXTFLASH_ERASEPROT3 0x7f /* Erase Sector Protection Reg - byte 3 */
#define EXTFLASH_ERASEPROT4 0xcf /* Erase Sector Protection Reg - byte 4 */
#define EXTFLASH_PROGPROT1 0x3d  /* Prgm Sector Protection Reg - byte 1 */
#define EXTFLASH_PROGPROT2 0x2a  /* Prgm Sector Protection Reg - byte 2 */
#define EXTFLASH_PROGPROT3 0x7f  /* Prgm Sector Protection Reg - byte 3 */
#define EXTFLASH_PROGPROT4 0xfc  /* Prgm Sector Protection Reg - byte 4 */
#define EXTFLASH_RDPROT 0x32     /* Read Sector Protection Reg */
#define EXTFLASH_LOCKDOWN1 0x3d  /* Sector Lockdown - byte 1 */
#define EXTFLASH_LOCKDOWN2 0x2a  /* Sector Lockdown - byte 2 */
#define EXTFLASH_LOCKDOWN3 0x7f  /* Sector Lockdown - byte 3 */
#define EXTFLASH_LOCKDOWN4 0x30  /* Sector Lockdown - byte 4 */
#define EXTFLASH_RDLOCKDOWN 0x35 /* Read Sector Lockdown Register  */
#define EXTFLASH_FREEZE_LOCKDOWN1 0x34 /* Freeze Sector Lockdown - byte 1 */
#define EXTFLASH_FREEZE_LOCKDOWN2 0x55 /* Freeze Sector Lockdown - byte 2 */
#define EXTFLASH_FREEZE_LOCKDOWN3 0xaa /* Freeze Sector Lockdown - byte 3 */
#define EXTFLASH_FREEZE_LOCKDOWN4 0x40 /* Freeze Sector Lockdown - byte 4 */
#define EXTFLASH_PROGSEC1 0x9b         /* Program Security Register - byte 1 */
#define EXTFLASH_PROGSEC2 0x00         /* Program Security Register - byte 2 */
#define EXTFLASH_PROGSEC3 0x00         /* Program Security Register - byte 3 */
#define EXTFLASH_PROGSEC4 0x00         /* Program Security Register - byte 4 */
#define EXTFLASH_RDSEC 0x77            /* Read Security Register */

/* Additional commands */

#define EXTFLASH_MNTOBF1XFR 0x53 /* Main Memory Page to Buffer 1 Transfer */
#define EXTFLASH_MNTOBF2XFR 0x55 /* Main Memory Page to Buffer 2 Transfer */
#define EXTFLASH_MNBF1CMP 0x60   /* Main Memory Page to Buffer 1 Compare  */
#define EXTFLASH_MNBF2CMP 0x61   /* Main Memory Page to Buffer 2 Compare */
#define EXTFLASH_AUTOWRBF1 0x58  /* Auto Page Rewrite through Buffer 1 */
#define EXTFLASH_AUTOWRBF2 0x59  /* Auto Page Rewrite through Buffer 2 */
#define EXTFLASH_DPD 0xb9        /* Deep Power-down */
#define EXTFLASH_RESUMEDPD 0xab  /* Resume from Deep Power-down */
#define EXTFLASH_UDPD 0x79       /* Ultra-Deep Power-Down */
#define EXTFLASH_RESUMEUDPD 0xd7 /* Resume from U-Deep Power-Down (dummy) */
#define EXTFLASH_RDSR 0xd7       /* Status Register Read */
#define EXTFLASH_RDDEVID 0x9f    /* Manufacturer and Device ID Read */
#define EXTFLASH_CFGPOW2SZ1 0x3d /* Config Pow of 2 (Binary) Page Size(1) */
#define EXTFLASH_CFGPOW2SZ2 0x2a /* Config Pow of 2 (Binary) Page Size(2) */
#define EXTFLASH_CFGPOW2SZ3 0x80 /* Config Pow of 2 (Binary) Page Size(3) */
#define EXTFLASH_CFGPOW2SZ4 0xa6 /* Config Pow of 2 (Binary) Page Size(4) */
#define EXTFLASH_CFGDATFSZ1 0x3d /* Config Std DataFlash Page Size(1) */
#define EXTFLASH_CFGDATFSZ2 0x2a /* Config Std DataFlash Page Size(2) */
#define EXTFLASH_CFGDATFSZ3 0x80 /* Config Std DataFlash Page Size(3) */
#define EXTFLASH_CFGDATFSZ4 0xA7 /* Config Std DataFlash Page Size(4) */
#define EXTFLASH_SOFTRST1 0xf0   /* Software Reset(1) */
#define EXTFLASH_SOFTRST2 0x00   /* Software Reset(2) */
#define EXTFLASH_SOFTRST3 0x00   /* Software Reset(3) */
#define EXTFLASH_SOFTRST4 0x00   /* Software Reset(4) */

/* 1 Block = 8 pages; 1 sector = 128 pages */

#define PG_PER_BLOCK (8)
#define PG_PER_SECTOR (128)
#define PG_PER_DEVICE (8192)

#define FLASH_DENSITY 0xD
#define ST_REG_BUSY_BIT 7
#define ST_REG_PROTECT_BIT 1
#define ST_REG_PG_SZ_BIT 0
#define ST_REG_PRG_ERR_BIT 5

#define MAX_PG_ADDR 8191
#define MAX_BF_BYTE_ADDR 527

#define MAX_DELAY_PAGE_ERASE_MS 35
#define MAX_DELAY_GOTO_UD_PWR_DOWN_US 4
#define MAX_DELAY_GETOUT_UD_PWR_DOWN_US 180

#define NB_PG (MAX_PG_ADDR + 1)
#define PG_SIZE 528
#define SCTR_SIZE 128
#define SLOT_POOL_PG_START_ADDR (NB_PG - (SCTR_SIZE * 2))
#define SLOT_POOL_PG_END_ADDR (NB_PG - 1)
#define MT_PG_START_ADDR (SLOT_POOL_PG_START_ADDR - (SCTR_SIZE * 2))
#define MT_PG_END_ADDR (SLOT_POOL_PG_START_ADDR - 1)
#endif /* EXTFLASH_INFO_H */