#ifndef AUTHENTICATION_SERVICE_H
#define AUTHENTICATION_SERVICE_H

#include <stdint.h>
#include <string.h>

#include "ble.h"
#include "ble_hci.h"
#include "ble_srv_common.h"
#include "cert.h"
#include "gatt.h"
#include "lb_crypto.h"
#include "nrf_drv_rng.h"
#include "nrf_log.h"
#include "nrf_sdh_ble.h"
#include "sdk_config.h"

#define STATUS_LENGTH 1
#define USER_CHALLENGE_LENGTH 64
#define DROP_CHALLENGE_LENGTH 64
#define CHALLENGE_RESPONSE_LENGTH 64

#define BLE_UUID_AUTH_SERVICE 0x1000
#define BLE_AUTHS_BLE_OBSERVER_PRIO 2

#define BLE_UUID_AUTH_STATUS_CHAR (BLE_UUID_AUTH_SERVICE + 0x1)
#define BLE_AUTHS_STATUS_CHAR_LEN STATUS_LENGTH
#define BLE_UUID_AUTH_TIME_CERTIFICATE_CHAR (BLE_UUID_AUTH_SERVICE + 0x2)
#define BLE_AUTHS_TIME_CERTIFICATE_CHAR_LEN TIME_CERTIFICATE_LENGTH
#define BLE_UUID_AUTH_USER_CERTIFICATE_CHAR (BLE_UUID_AUTH_SERVICE + 0x3)
#define BLE_AUTHS_USER_CERTIFICATE_CHAR_LEN USER_CERTIFICATE_MAX_LENGTH
#define BLE_UUID_AUTH_DROP_CHALLENGE_CHAR (BLE_UUID_AUTH_SERVICE + 0x4)
#define BLE_AUTHS_DROP_CHALLENGE_CHAR_LEN CHALLENGE_RESPONSE_LENGTH
#define BLE_UUID_AUTH_DROP_RESPONSE_CHAR BLE_UUID_AUTH_DROP_CHALLENGE_CHAR
#define BLE_AUTHS_DROP_RESPONSE_CHAR_LEN CHALLENGE_RESPONSE_LENGTH
#define BLE_UUID_AUTH_USER_CHALLENGE_CHAR (BLE_UUID_AUTH_SERVICE + 0x5)
#define BLE_AUTHS_USER_CHALLENGE_CHAR_LEN CHALLENGE_RESPONSE_LENGTH
#define BLE_UUID_AUTH_USER_RESPONSE_CHAR BLE_UUID_AUTH_USER_CHALLENGE_CHAR
#define BLE_AUTHS_USER_RESPONSE_CHAR_LEN CHALLENGE_RESPONSE_LENGTH

#define BLE_AUTHS_DEF(_name)                                                   \
    ble_auths_t _name;                                                         \
    NRF_SDH_BLE_OBSERVER(_name##_obs, BLE_AUTHS_BLE_OBSERVER_PRIO,             \
                         ble_auths_on_ble_evt, &_name)

typedef struct {
    uint8_t uuid_type;
    uint16_t service_handle;
    uint16_t conn_handle;
    ble_gatts_char_handles_t status_handles;
    ble_gatts_char_handles_t time_certificate_handles;
    ble_gatts_char_handles_t user_certificate_handles;
    ble_gatts_char_handles_t drop_challenge_handles;
    ble_gatts_char_handles_t user_challenge_handles;
} ble_auths_t;
extern uint32_t time_reference;

void auth_service_init(ble_auths_t *p_auth_service);
void ble_auths_on_ble_evt(ble_evt_t const *p_ble_evt, void *p_context);
bool auth_service_check_status();

#endif /* AUTHENTICATION_SERVICE_H */
