#include "authentication_service.h"
#include "authentication_service_backend.h"
#include "ble_stack.h"
#include "connection_timer.h"
#include "services.h"
#include "test_hw.h"

static ble_auths_t *running_auth_service;

// User certificate verification status
static bool certificate_status;
// Time certificate verification status
static bool time_certificate_status;
// User challenge verification status
static bool challenge_status;
// User challenge thrown status
static bool challenge_thrown;
// Drop authentication challenge received
static bool drop_authentication;

static uint8_t service_status;

uint32_t time_reference;

static void disconnect() {
    if (running_auth_service->conn_handle == BLE_CONN_HANDLE_INVALID) {
        return;
    }
    ret_code_t err_code =
        sd_ble_gap_disconnect(running_auth_service->conn_handle,
                              BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
    APP_ERROR_CHECK(err_code);
    running_auth_service->conn_handle = BLE_CONN_HANDLE_INVALID;
}

static void authentication_service_reset() {
    certificate_status = 0;
    time_certificate_status = 0;
    challenge_status = 0;
    challenge_thrown = 0;
    drop_authentication = 0;
    service_status = 0;

    ble_gatts_value_t status;
    status.len = STATUS_LENGTH;
    status.offset = 0;
    status.p_value = &service_status;
    ret_code_t err_code = sd_ble_gatts_value_set(
        running_auth_service->conn_handle,
        running_auth_service->status_handles.value_handle, &status);

    APP_ERROR_CHECK(err_code);
}

bool auth_service_check_status() {
    return (time_certificate_status && certificate_status && challenge_status);
}

static void auth_service_update_service_status(bool update) {
    if (running_auth_service->conn_handle == BLE_CONN_HANDLE_INVALID) {
        return;
    }

    if (auth_service_check_status()) {
        service_status = 0xFF;
        NRF_LOG_INFO("Authentication successful");
    } else {
        if (!update) {
            return;
        }
        service_status++;
    }

    ble_gatts_hvx_params_t hvx_params = {0};
    hvx_params.handle = running_auth_service->status_handles.value_handle;
    hvx_params.type = BLE_GATT_HVX_NOTIFICATION;
    hvx_params.offset = 0;
    uint16_t len = STATUS_LENGTH;
    hvx_params.p_len = &len;
    hvx_params.p_data = &service_status;

    ret_code_t err_code =
        sd_ble_gatts_hvx(running_auth_service->conn_handle, &hvx_params);

    APP_ERROR_CHECK(err_code);
}

static void auth_service_time_certificate_to_read() { service_status = 0xF0; }

static void write_handler(ble_evt_t const *p_ble_evt) {
    uint16_t uuid = p_ble_evt->evt.gatts_evt.params.write.uuid.uuid;
    // Make long writes trigger the same callbacks for normal writes
    bool write_long = 0;
    do {
        write_long = 0;
        switch (uuid) {
        case BLE_UUID_AUTH_TIME_CERTIFICATE_CHAR:
            NRF_LOG_INFO("Gatt write time certificate");
            // Get time certificate
            uint32_t prev = time_reference;
            time_certificate_status =
                auth_service_read_time_certificate(&time_reference);
            if (!time_certificate_status) {
                disconnect();
            }
            // If the time reference wasn't changed, the time certificate was
            // updated
            if (prev == time_reference) {
                auth_service_time_certificate_to_read();
            }
            // Update status
            auth_service_update_service_status(1);
            NRF_LOG_INFO("Time certificate result: %u, reference: 0x%x",
                         time_certificate_status, time_reference);
            break;
        case BLE_UUID_AUTH_USER_CERTIFICATE_CHAR:
            NRF_LOG_INFO("Gatt write user certificate");
            // Generate the challenge after receiving the certificate
            challenge_thrown = 1;
            auth_service_generate_challenge();
            // Update status
            auth_service_update_service_status(1);
            // Verify user certificate
            certificate_status = auth_service_verify_certificate();
            if (!certificate_status) {
                disconnect();
            }
            // Update status
            auth_service_update_service_status(0);
            NRF_LOG_INFO("User certificate result: %u", certificate_status);
            break;
        case BLE_UUID_AUTH_DROP_CHALLENGE_CHAR:
            NRF_LOG_INFO("Gatt write drop challenge");
            if (drop_authentication) {
                break;
            }
            // Respond to challenge
            drop_authentication = 1;
            auth_service_generate_response();
            NRF_LOG_INFO("Drop generate response");
            // Update status
            auth_service_update_service_status(1);
            break;
        case BLE_UUID_AUTH_USER_RESPONSE_CHAR:
            NRF_LOG_INFO("Gatt write user response");
            // Check if the challenge was thrown first
            if (!challenge_thrown) {
                break;
            }
            // Verify user response
            challenge_status = auth_service_verify_response();
            NRF_LOG_INFO("User response result: %u", challenge_status);
            if (!challenge_status) {
                disconnect();
            }
            // Update status
            auth_service_update_service_status(0);
            break;
        default:
            if (p_ble_evt->evt.gatts_evt.params.write.op ==
                BLE_GATTS_OP_EXEC_WRITE_REQ_NOW) {
                uint16_t value_handle = *(uint16_t *)mem_block.p_mem;
                write_long = 1;
                if (value_handle ==
                    running_auth_service->time_certificate_handles
                        .value_handle) {
                    uuid = BLE_UUID_AUTH_TIME_CERTIFICATE_CHAR;
                    continue;
                } else if (value_handle ==
                           running_auth_service->user_certificate_handles
                               .value_handle) {
                    uuid = BLE_UUID_AUTH_USER_CERTIFICATE_CHAR;
                    continue;
                } else if (value_handle ==
                           running_auth_service->drop_challenge_handles
                               .value_handle) {
                    uuid = BLE_UUID_AUTH_DROP_CHALLENGE_CHAR;
                    continue;
                } else if (value_handle ==
                           running_auth_service->user_challenge_handles
                               .value_handle) {
                    uuid = BLE_UUID_AUTH_USER_RESPONSE_CHAR;
                    continue;
                } else {
                    write_long = 0;
                }
            }
            NRF_LOG_WARNING("Unsupported UUID: %x",
                            p_ble_evt->evt.gatts_evt.params.write.uuid.uuid);
            break;
        }
    } while (write_long);
}

static void add_characteristics(ble_auths_t *p_auth_service) {
    ret_code_t err_code;
    // UUID
    ble_uuid_t ble_uuid;
    ble_uuid.type = p_auth_service->uuid_type;

    // Client Characteristic Configuration Descriptor Metadata
    ble_gatts_attr_md_t cccd_md = {0};
    // Set GAP connection security to open for read and write permissions
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.write_perm);
    // Storing the CCCD value in stack (softdevice) memory
    cccd_md.vloc = BLE_GATTS_VLOC_STACK;

    ble_gatts_char_md_t char_md = {0};
    char_md.p_char_user_desc = NULL;
    char_md.p_char_pf = NULL;
    char_md.p_user_desc_md = NULL;
    char_md.p_cccd_md = &cccd_md;
    char_md.p_sccd_md = NULL;

    // Attribute Metadata
    ble_gatts_attr_md_t attr_md = {0};
    // Set GAP connection security to open for read and write permissions
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.write_perm);
    // Storing the attribute value in stack (softdevice) memory
    attr_md.vloc = BLE_GATTS_VLOC_STACK;
    attr_md.rd_auth = 0;
    attr_md.wr_auth = 0;
    attr_md.vlen = 0;

    ble_gatts_attr_t attr_char_value = {0};
    attr_char_value.p_uuid = &ble_uuid;
    attr_char_value.p_attr_md = &attr_md;
    attr_char_value.init_offs = 0;

    // Status characteristic
    // UUID
    ble_uuid.uuid = BLE_UUID_AUTH_STATUS_CHAR;

    // Characteristic Metadata
    char_md.char_props.write = 0;
    char_md.char_ext_props.reliable_wr = 0;
    char_md.char_props.read = 1;
    char_md.char_props.notify = 1;

    // Characteristic Value Attribute
    attr_char_value.init_len = BLE_AUTHS_STATUS_CHAR_LEN;
    attr_char_value.max_len = BLE_AUTHS_STATUS_CHAR_LEN;

    attr_md.vlen = 0;

    err_code = sd_ble_gatts_characteristic_add(p_auth_service->service_handle,
                                               &char_md, &attr_char_value,
                                               &p_auth_service->status_handles);
    APP_ERROR_CHECK(err_code);

    // Time certificate characteristic
    // UUID
    ble_uuid.uuid = BLE_UUID_AUTH_TIME_CERTIFICATE_CHAR;

    // Characteristic Metadata
    char_md.char_props.write = 1;
    char_md.char_props.read = 1;
    char_md.char_props.notify = 0;

    // Characteristic Value Attribute
    attr_char_value.init_len = BLE_AUTHS_TIME_CERTIFICATE_CHAR_LEN;
    attr_char_value.max_len = BLE_AUTHS_TIME_CERTIFICATE_CHAR_LEN;

    attr_md.vlen = 1;

    err_code = sd_ble_gatts_characteristic_add(
        p_auth_service->service_handle, &char_md, &attr_char_value,
        &p_auth_service->time_certificate_handles);
    APP_ERROR_CHECK(err_code);

    // User certificate characteristic
    // UUID
    ble_uuid.uuid = BLE_UUID_AUTH_USER_CERTIFICATE_CHAR;

    // Characteristic Metadata
    char_md.char_ext_props.reliable_wr = 0;
    char_md.char_props.write = 1;
    char_md.char_props.read = 0;
    char_md.char_props.notify = 0;

    // Characteristic Value Attribute
    attr_char_value.init_len = BLE_AUTHS_USER_CERTIFICATE_CHAR_LEN;
    attr_char_value.max_len = BLE_AUTHS_USER_CERTIFICATE_CHAR_LEN;

    attr_md.vlen = 1;

    err_code = sd_ble_gatts_characteristic_add(
        p_auth_service->service_handle, &char_md, &attr_char_value,
        &p_auth_service->user_certificate_handles);
    APP_ERROR_CHECK(err_code);

    attr_md.vlen = 0;

    // Drop challenge characteristic
    // UUID
    ble_uuid.uuid = BLE_UUID_AUTH_DROP_CHALLENGE_CHAR;

    // Characteristic Metadata
    char_md.char_ext_props.reliable_wr = 0;
    char_md.char_props.write = 1;
    char_md.char_props.read = 1;
    char_md.char_props.notify = 0;

    // Characteristic Value Attribute
    attr_char_value.init_len = BLE_AUTHS_DROP_CHALLENGE_CHAR_LEN;
    attr_char_value.max_len = BLE_AUTHS_DROP_CHALLENGE_CHAR_LEN;

    err_code = sd_ble_gatts_characteristic_add(
        p_auth_service->service_handle, &char_md, &attr_char_value,
        &p_auth_service->drop_challenge_handles);
    APP_ERROR_CHECK(err_code);

    // User challenge characteristic
    // UUID
    ble_uuid.uuid = BLE_UUID_AUTH_USER_CHALLENGE_CHAR;

    // Characteristic Metadata
    char_md.char_props.write = 1;
    char_md.char_props.read = 1;
    char_md.char_props.notify = 0;

    // Characteristic Value Attribute
    attr_char_value.init_len = BLE_AUTHS_USER_CHALLENGE_CHAR_LEN;
    attr_char_value.max_len = BLE_AUTHS_USER_CHALLENGE_CHAR_LEN;

    err_code = sd_ble_gatts_characteristic_add(
        p_auth_service->service_handle, &char_md, &attr_char_value,
        &p_auth_service->user_challenge_handles);
    APP_ERROR_CHECK(err_code);
}

void auth_service_init(ble_auths_t *p_auth_service) {
    ble_uuid128_t auths_base_uuid = BLE_UUID_BASE;
    update_base_uuid(&auths_base_uuid);

    // Initialize the service structure
    p_auth_service->conn_handle = BLE_CONN_HANDLE_INVALID;

    // Add a new vendor specific UUID to the softdevice table
    ble_uuid_t auths_uuid;
    ret_code_t err_code =
        sd_ble_uuid_vs_add(&auths_base_uuid, &p_auth_service->uuid_type);
    APP_ERROR_CHECK(err_code);
    auths_uuid.type = p_auth_service->uuid_type;
    auths_uuid.uuid = BLE_UUID_AUTH_SERVICE;

    // Add the service to the GATT
    err_code =
        sd_ble_gatts_service_add(BLE_GATTS_SRVC_TYPE_PRIMARY, &auths_uuid,
                                 &p_auth_service->service_handle);
    APP_ERROR_CHECK(err_code);

    add_characteristics(p_auth_service);

    running_auth_service = p_auth_service;
    authentication_service_backend_init(p_auth_service);
}

void ble_auths_on_ble_evt(ble_evt_t const *p_ble_evt, void *p_context) {
    if (p_ble_evt == NULL) {
        return;
    }

    ble_auths_t *p_auth_service = (ble_auths_t *)p_context;

    switch (p_ble_evt->header.evt_id) {
    case BLE_GAP_EVT_CONNECTED:
        NRF_LOG_INFO("Gap evt connected");
        p_auth_service->conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
        // Reset authentication status
        authentication_service_reset();
        break;
    case BLE_GAP_EVT_DISCONNECTED:
        NRF_LOG_INFO("Gap evt disconnected");
        p_auth_service->conn_handle = BLE_CONN_HANDLE_INVALID;
        authentication_service_reset();
        break;
    case BLE_GATTS_EVT_WRITE:
        // As long as authentication isn't complete, handle writes
        if (!drop_authentication || !auth_service_check_status()) {
            lb_connection_timer_cancel();
            NRF_LOG_INFO("Gatt evt write authentication");
            write_handler(p_ble_evt);
        }
        break;
    default:
        // No implementation needed
        break;
    }
}
