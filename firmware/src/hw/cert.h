#ifndef CERT_H
#define CERT_H

#include "nrf_crypto_types.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#define USER_CERTIFICATE_MIN_LENGTH 297
#define USER_CERTIFICATE_MAX_LENGTH 380

#define TIME_CERTIFICATE_PAYLOAD_LENGTH 4
#define TIME_CERTIFICATE_LENGTH (72 + TIME_CERTIFICATE_PAYLOAD_LENGTH)

/**
 * \brief Check that the certificate was signed by the CA
 *
 * \param user_certificate: the certificate buffer to verify
 * \param public_key: return a pointer to the public key of the certificate
 */
bool certificate_valid(uint8_t *user_certificate, size_t length,
                       nrf_value_length_t *user_public);

bool time_certificate_valid(uint8_t *time_certificate, size_t length,
                            uint32_t *time_reference);

size_t get_time_certificate(uint8_t *time_certificate);

#endif // CERT_H
