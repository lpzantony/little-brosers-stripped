#ifndef BSP_TIMERS_H
#define BSP_TIMERS_H

#include <stdbool.h>

void timers_init(void);
void buttons_leds_init(bool *p_erase_bounds);

#endif
