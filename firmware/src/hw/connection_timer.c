#include "connection_timer.h"

APP_TIMER_DEF(connection_timer_id);

static void connection_timeout_handler(void *on_timemout) {
    on_timemout_func _to_call = (on_timemout_func)on_timemout;
    _to_call();
}
void lb_connection_timer_start(uint32_t timeout_ms,
                               on_timemout_func on_timemout) {
    ret_code_t err_code = app_timer_start(
        connection_timer_id, APP_TIMER_TICKS(timeout_ms), on_timemout);
    APP_ERROR_CHECK(err_code);
}

void lb_connection_timer_cancel() {
    ret_code_t err_code = app_timer_stop(connection_timer_id);
    APP_ERROR_CHECK(err_code);
}

void lb_connection_timer_init() {

    // Create timers.
    ret_code_t err_code =
        app_timer_create(&connection_timer_id, APP_TIMER_MODE_SINGLE_SHOT,
                         connection_timeout_handler);
    APP_ERROR_CHECK(err_code);
}
