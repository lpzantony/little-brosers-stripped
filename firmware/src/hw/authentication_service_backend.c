#include "authentication_service_backend.h"
#include "lb_crypto.h"
#include "test_hw.h"

static ble_auths_t *running_auth_service;
NRF_CRYPTO_ECC_PRIVATE_KEY_CREATE(drop_private, SECP256R1);
static uint8_t user_challenge[CHALLENGE_RESPONSE_LENGTH];
NRF_CRYPTO_ECDSA_SIGNATURE_CREATE(signature, SECP256R1);

static __ALIGN(4) uint8_t
    user_public_buffer[NRF_CRYPTO_ECC_PUBLIC_KEY_SIZE(SECP256R1)];

nrf_value_length_t user_public = {
    .p_value = user_public_buffer,
    .length = NRF_CRYPTO_ECC_PUBLIC_KEY_SIZE(SECP256R1)};

void authentication_service_backend_init(ble_auths_t *auth_service) {
    running_auth_service = auth_service;
    lb_crypto_init();
    lb_get_drop_private_key(&drop_private);
}

uint8_t generate_random(uint8_t *buffer, uint8_t length) {
    ret_code_t err_code = nrf_drv_rng_rand(buffer, length);
    APP_ERROR_CHECK(err_code);
    return length;
}

void auth_service_generate_challenge(void) {
    if (running_auth_service->conn_handle == BLE_CONN_HANDLE_INVALID) {
        return;
    }
    // Generate challenge
    uint8_t challenge_length =
        generate_random(user_challenge, CHALLENGE_RESPONSE_LENGTH);

    // Update characteristic
    ble_gatts_value_t challenge;
    challenge.len = challenge_length;
    challenge.offset = 0;
    challenge.p_value = user_challenge;
    ret_code_t err_code = sd_ble_gatts_value_set(
        running_auth_service->conn_handle,
        running_auth_service->user_challenge_handles.value_handle, &challenge);

    APP_ERROR_CHECK(err_code);
}

bool auth_service_verify_response(void) {
    if (running_auth_service->conn_handle == BLE_CONN_HANDLE_INVALID) {
        return 0;
    }
    // Read user reponse and put it in signature variable
    ble_gatts_value_t response;
    response.len = CHALLENGE_RESPONSE_LENGTH;
    response.offset = 0;
    response.p_value = signature.p_value;

    sd_ble_gatts_value_get(
        running_auth_service->conn_handle,
        running_auth_service->user_challenge_handles.value_handle, &response);

    return lb_crypto_verify_signature(user_challenge, CHALLENGE_RESPONSE_LENGTH,
                                      &signature, &user_public);
}

void auth_service_generate_response(void) {
    if (running_auth_service->conn_handle == BLE_CONN_HANDLE_INVALID) {
        return;
    }
    uint8_t drop_challenge[CHALLENGE_RESPONSE_LENGTH];
    uint8_t length;

    // Read the challenge
    ble_gatts_value_t challenge;
    challenge.len = CHALLENGE_RESPONSE_LENGTH;
    challenge.offset = 0;
    challenge.p_value = drop_challenge;

    ret_code_t err_code = sd_ble_gatts_value_get(
        running_auth_service->conn_handle,
        running_auth_service->drop_challenge_handles.value_handle, &challenge);

    APP_ERROR_CHECK(err_code);

    length = challenge.len;

    // Sign the challenge
    lb_crypto_sign(drop_challenge, length, &signature, &drop_private);
    // Update characteristic
    ble_gatts_value_t response;
    response.len = signature.length;
    response.offset = 0;
    response.p_value = signature.p_value;
    err_code = sd_ble_gatts_value_set(
        running_auth_service->conn_handle,
        running_auth_service->drop_challenge_handles.value_handle, &response);

    APP_ERROR_CHECK(err_code);
}

bool auth_service_read_time_certificate(uint32_t *time_reference) {
    if (running_auth_service->conn_handle == BLE_CONN_HANDLE_INVALID) {
        return 0;
    }
    uint8_t time_certificate[TIME_CERTIFICATE_LENGTH];

    // Read the certificate
    ble_gatts_value_t certificate;
    certificate.len = TIME_CERTIFICATE_LENGTH;
    certificate.offset = 0;
    certificate.p_value = time_certificate;

    ret_code_t err_code = sd_ble_gatts_value_get(
        running_auth_service->conn_handle,
        running_auth_service->time_certificate_handles.value_handle,
        &certificate);

    APP_ERROR_CHECK(err_code);

    uint32_t ref = *time_reference;

    bool res = time_certificate_valid(certificate.p_value, certificate.len,
                                      time_reference);

    if (ref == *time_reference) {
        certificate.len = get_time_certificate(certificate.p_value);
        ret_code_t err_code = sd_ble_gatts_value_set(
            running_auth_service->conn_handle,
            running_auth_service->time_certificate_handles.value_handle,
            &certificate);

        APP_ERROR_CHECK(err_code);
    }

    return res;
}

bool auth_service_verify_certificate(void) {
    if (running_auth_service->conn_handle == BLE_CONN_HANDLE_INVALID) {
        return 0;
    }
    uint8_t user_certificate[USER_CERTIFICATE_MAX_LENGTH];

    // Read the certificate
    ble_gatts_value_t certificate;
    certificate.offset = 0;
    certificate.len = USER_CERTIFICATE_MAX_LENGTH;
    certificate.p_value = user_certificate;

    ret_code_t err_code = sd_ble_gatts_value_get(
        running_auth_service->conn_handle,
        running_auth_service->user_certificate_handles.value_handle,
        &certificate);

    APP_ERROR_CHECK(err_code);

    // Length less than minimum certificate length
    if (certificate.len < USER_CERTIFICATE_MIN_LENGTH) {
        return 0;
    }
    return certificate_valid(user_certificate, certificate.len, &user_public);
}

bool test_write_response(nrf_value_length_t *signature) {
    ble_gatts_value_t response;
    response.len = signature->length;
    response.offset = 0;
    response.p_value = signature->p_value;

    ret_code_t err_code = sd_ble_gatts_value_set(
        running_auth_service->conn_handle,
        running_auth_service->user_challenge_handles.value_handle, &response);

    APP_ERROR_CHECK(err_code);

    return auth_service_verify_response();
}

bool test_write_cert(nrf_value_length_t *cert) {
    ble_gatts_value_t response;
    response.len = cert->length;
    response.offset = 0;
    response.p_value = cert->p_value;

    ret_code_t err_code = sd_ble_gatts_value_set(
        running_auth_service->conn_handle,
        running_auth_service->user_certificate_handles.value_handle, &response);

    APP_ERROR_CHECK(err_code);

    return auth_service_verify_certificate();
}
