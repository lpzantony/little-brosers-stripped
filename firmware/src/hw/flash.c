#include "flash.h"
#include "app_error.h"
#include "boards.h"
#include "nrf_delay.h"
#include "nrf_drv_gpiote.h"
#include "nrf_drv_spi.h"
#include "nrf_log.h"
#include "power_management.h"
#include <stdint.h>
#include <string.h>

//< SPI instance index
#define SPI_INSTANCE 0
#define OPCODE_INDEX 0
#define PG_ADDR_INDEX 1
#define DATA_INDEX 4

#define OPCODE tx_buffer[OPCODE_INDEX]
#define TX_DATA tx_buffer[DATA_INDEX]
#define RX_DATA rx_buffer[DATA_INDEX]

#ifdef BOARD_CUSTOM

//< SPI instance
static uint8_t tmp_rawmsg[MESSAGE_MAX_SIZE];
static const nrf_drv_spi_t spi = NRF_DRV_SPI_INSTANCE(SPI_INSTANCE);

/**
 * @brief SPI user event handler.
 * @param event
 */
void spi_event_handler(const nrf_drv_spi_evt_t *p_event, void *p_context) {
    lb_spi_ctx *ctx = (lb_spi_ctx *)p_context;
    if (p_event->type == NRF_DRV_SPI_EVENT_DONE) {
        ctx->spi_busy = 0;
    } else {
        // Should not happen
    }
}
#endif
// Init the flash
err_t extflash_init(lb_spi_ctx *ctx) {
#ifdef BOARD_CUSTOM
    if (ctx == NULL)
        return LB_WRONG_ARGS_VALUE;

    // Putting flash reset to false
    uint32_t err_code;
    if (!nrf_drv_gpiote_is_init()) {
        err_code = nrf_drv_gpiote_init();
    }
    (void)err_code;
    // Init the RESET pin to high
    nrf_drv_gpiote_out_config_t config = GPIOTE_CONFIG_OUT_SIMPLE(1);
    err_code = nrf_drv_gpiote_out_init(FLASH_RESET_PIN, &config);

    // SPI config
    nrf_drv_spi_config_t spi_config = NRF_DRV_SPI_DEFAULT_CONFIG;

    // CS is handled by the driver
    spi_config.ss_pin = FLASH_SS_PIN;
    spi_config.miso_pin = FLASH_MISO_PIN;
    spi_config.mosi_pin = FLASH_MOSI_PIN;
    spi_config.sck_pin = FLASH_SCK_PIN;
    APP_ERROR_CHECK(
        nrf_drv_spi_init(&spi, &spi_config, spi_event_handler, ctx));
    ctx->slot_pool_addr = SLOT_POOL_PG_START_ADDR;
    ctx->merkle_tree_addr = MT_PG_START_ADDR;
#else
    (void)ctx;
#endif
    return LB_OK;
}
#ifdef BOARD_CUSTOM
static inline err_t extflash_get_status(lb_spi_ctx *ctx) {
    ctx->OPCODE = EXTFLASH_RDSR;
    ctx->spi_busy = 1;
    err_t retval =
        nrf_drv_spi_transfer(&spi, &ctx->OPCODE, 1, ctx->rx_buffer, 3);
    if (retval != NRF_SUCCESS) {
        ctx->spi_busy = 0;
        return retval;
    }
    while (ctx->spi_busy)
        power_manage(); // wait for event
    return LB_OK;
}

static inline err_t compute_page_addr_byte(uint8_t *p_dest,
                                           const uint16_t page_addr,
                                           const uint16_t buffer_byte_addr) {
    if (p_dest == NULL || page_addr > MAX_PG_ADDR ||
        buffer_byte_addr > MAX_BF_BYTE_ADDR)
        return LB_WRONG_ARGS_VALUE;
    // we will take only the bits 6 to 12 of page_addr for dest[0]
    // bit 7 of dest[0] is ignored
    *(p_dest + 0) = (page_addr >> 6) & 0x7F;
    *(p_dest + 1) =
        ((page_addr & 0x3F) << 2) + ((buffer_byte_addr >> 8) & 0x03);
    *(p_dest + 2) = buffer_byte_addr & 0xFF;
    return LB_OK;
}

static inline err_t compute_wr_buff_addr_byte(uint8_t *p_dest,
                                              const uint16_t buffer_byte_addr) {
    if (p_dest == NULL || buffer_byte_addr > MAX_BF_BYTE_ADDR)
        return LB_WRONG_ARGS_VALUE;
    *(p_dest + 0) = 0;
    *(p_dest + 1) = ((buffer_byte_addr >> 8) & 0x01);
    *(p_dest + 2) = buffer_byte_addr & 0xFF;
    return LB_OK;
}

// Request DevID to the flash and check it
err_t extflash_check_edi(lb_spi_ctx *ctx) {
    // LBLOG("In check_edi");
    ctx->OPCODE = EXTFLASH_RDDEVID;
    ctx->spi_busy = 1;
    err_t retval =
        nrf_drv_spi_transfer(&spi, &ctx->OPCODE, 4, ctx->rx_buffer, 6);
    if (retval != NRF_SUCCESS) {
        ctx->spi_busy = 0;
        return retval;
    }
    while (ctx->spi_busy)
        power_manage(); // wait for event

    static const uint8_t expected[] = {0x00, 0x1f, 0x27, 0x01, 0x01, 0x00};
    if (!memcmp(ctx->rx_buffer, expected, 6)) {
        LBLOG("Flash answered correctly !");
    } else {
        return LB_ERR;
    }
    return LB_OK;
}

// Return 1 if the flash is ready to receive cmds
int extflash_is_ready(lb_spi_ctx *ctx) {
    extflash_get_status(ctx);
    return LB_GET_BIT(ctx->rx_buffer[1], ST_REG_BUSY_BIT) &
           LB_GET_BIT(ctx->rx_buffer[2], ST_REG_BUSY_BIT);
}

// Return 1 if an error occured during the last write/erase cmd
int extflash_prgm_err(lb_spi_ctx *ctx) {
    extflash_get_status(ctx);
    return LB_GET_BIT(ctx->rx_buffer[2], ST_REG_PRG_ERR_BIT);
}

// Print the status register content of the flash
err_t extflash_print_status(lb_spi_ctx *ctx) {
    err_t retval = extflash_get_status(ctx);
    if (retval != LB_OK)
        return retval;
    LBLOG("Ext flash is : %s\n",
          LB_GET_BIT(ctx->rx_buffer[1], ST_REG_BUSY_BIT) ? "ready" : "busy");
    LBLOG("Ext flash density is : %s\n",
          (LB_GET_BITS(ctx->rx_buffer[1], 2, 0xF) == FLASH_DENSITY) ? "32 Mb"
                                                                    : "unknow");
    LBLOG("Ext flash sector protect is : %s\n",
          LB_GET_BIT(ctx->rx_buffer[1], ST_REG_PROTECT_BIT) ? "enabled"
                                                            : "disabled");
    LBLOG("Ext flash page size is : %s\n",
          LB_GET_BIT(ctx->rx_buffer[1], ST_REG_PG_SZ_BIT) ? "512 Mbits"
                                                          : "528 Mbits");
    LBLOG("Ext flash last prgm : %s\n",
          LB_GET_BIT(ctx->rx_buffer[2], ST_REG_PRG_ERR_BIT) ? "fail"
                                                            : "succes");
    return LB_OK;
}

err_t extflash_page_erase(lb_spi_ctx *ctx, const uint16_t pg_addr) {
    if (ctx == NULL || pg_addr > MAX_PG_ADDR)
        return LB_WRONG_ARGS_VALUE;
    ctx->OPCODE = EXTFLASH_PGERASE;
    compute_page_addr_byte(&ctx->tx_buffer[PG_ADDR_INDEX], pg_addr, 0);
    err_t retval = nrf_drv_spi_transfer(&spi, ctx->tx_buffer, 4, NULL, 0);
    if (retval != NRF_SUCCESS) {
        ctx->spi_busy = 0;
        return retval;
    }
    while (ctx->spi_busy)
        power_manage(); // wait for event

    // We use a delay here because during debugging
    // sessiosn we saw that the status registrer
    // indeed takes some time to set to "ready"
    // but each time, the flash is not
    // ready yet when it does so.

    // We detected this by erasing a page and reading
    // it immediately after. The read values are always
    // zeros if you read immediately after status reg
    // sets to "ready".

    // So here we wait the maximum amount of time
    // taken by the flash to erase a page, according
    // to its datasheet
    nrf_delay_ms(MAX_DELAY_PAGE_ERASE_MS);
    while (extflash_is_ready(ctx) == 0)
        ;
    if (extflash_prgm_err(ctx))
        return LB_MASS_STORAGE_PAGE_ERROR;
    return LB_OK;
}

err_t extflash_chip_erase(lb_spi_ctx *ctx) {
    if (ctx == NULL)
        return LB_WRONG_ARGS_VALUE;
    const uint8_t chip_erase[4] = {EXTFLASH_CHIPERASE1, EXTFLASH_CHIPERASE2,
                                   EXTFLASH_CHIPERASE3, EXTFLASH_CHIPERASE4};
    ctx->spi_busy = 1;
    memcpy(ctx->tx_buffer, chip_erase, 4);
    err_t retval = nrf_drv_spi_transfer(&spi, ctx->tx_buffer, 4, NULL, 0);
    if (retval != NRF_SUCCESS) {
        ctx->spi_busy = 0;
        return retval;
    }
    while (ctx->spi_busy)
        power_manage(); // wait for event
    while (extflash_is_ready(ctx) == 0)
        ;
    return LB_OK;
}

static inline err_t extflash_disable_lock_down(lb_spi_ctx *ctx) {
    if (ctx == NULL)
        return LB_WRONG_ARGS_VALUE;
    const uint8_t free_ld[4] = {
        EXTFLASH_FREEZE_LOCKDOWN1, EXTFLASH_FREEZE_LOCKDOWN2,
        EXTFLASH_FREEZE_LOCKDOWN3, EXTFLASH_FREEZE_LOCKDOWN4};
    ctx->spi_busy = 1;
    memcpy(ctx->tx_buffer, free_ld, 4);
    err_t retval = nrf_drv_spi_transfer(&spi, ctx->tx_buffer, 4, NULL, 0);
    if (retval != NRF_SUCCESS) {
        ctx->spi_busy = 0;
        return retval;
    }
    while (ctx->spi_busy)
        power_manage(); // wait for event
    while (extflash_is_ready(ctx) == 0)
        ;
    return LB_OK;
}

err_t extflash_go_to_ultra_sleep(lb_spi_ctx *ctx) {
    if (ctx == NULL)
        return LB_WRONG_ARGS_VALUE;
    // Wait for the flash to be ready
    while (extflash_is_ready(ctx) == 0)
        ;
    // Sending the go-to-sleep command
    ctx->spi_busy = 1;
    ctx->OPCODE = EXTFLASH_UDPD;
    err_t retval = nrf_drv_spi_transfer(&spi, ctx->tx_buffer, 2, NULL, 0);
    if (retval != NRF_SUCCESS) {
        ctx->spi_busy = 0;
        return retval;
    }
    while (ctx->spi_busy)
        power_manage(); // wait for event
    // Delay before allowing anyone assert SPI's CS
    // See the datasheet for more details
    nrf_delay_us(MAX_DELAY_GOTO_UD_PWR_DOWN_US);
    return LB_OK;
}

err_t extflash_get_out_ultra_sleep(lb_spi_ctx *ctx) {
    if (ctx == NULL)
        return LB_WRONG_ARGS_VALUE;
    ctx->spi_busy = 1;
    ctx->OPCODE = EXTFLASH_RESUMEUDPD;
    err_t retval = nrf_drv_spi_transfer(&spi, ctx->tx_buffer, 1, NULL, 0);
    if (retval != NRF_SUCCESS) {
        ctx->spi_busy = 0;
        return retval;
    }
    while (ctx->spi_busy)
        power_manage(); // wait for event
    // Delay before allowing anyone assert SPI's CS
    // See the datasheet for more details
    nrf_delay_us(MAX_DELAY_GETOUT_UD_PWR_DOWN_US);

    // Wait for the flash to be ready
    while (extflash_is_ready(ctx) == 0)
        ;
    return LB_OK;
}
err_t extflash_push_buff(lb_spi_ctx *ctx, const uint16_t pg_addr) {
    if (ctx == NULL || pg_addr > MAX_PG_ADDR)
        return LB_WRONG_ARGS_VALUE;
    // Flush buffer to MainMemory page
    ctx->spi_busy = 1;
    ctx->OPCODE = EXTFLASH_BF1TOMNE;
    compute_page_addr_byte(&ctx->tx_buffer[PG_ADDR_INDEX], pg_addr, 0);
    err_t retval = nrf_drv_spi_transfer(&spi, ctx->tx_buffer, 4, NULL, 0);
    if (retval != NRF_SUCCESS) {
        ctx->spi_busy = 0;
        return LB_ERR;
    }
    while (ctx->spi_busy)
        power_manage(); // wait for event
    while (extflash_is_ready(ctx) == 0)
        ;
    if (extflash_prgm_err(ctx))
        return LB_MASS_STORAGE_PAGE_ERROR;
    return LB_OK;
}

err_t extflash_buff_write_raw(lb_spi_ctx *ctx, const uint16_t buff_addr,
                              uint8_t *p_data, const size_t size) {
    if (ctx == NULL || size > MAX_SPI_PAYLOAD || buff_addr > MAX_BF_BYTE_ADDR)
        return LB_WRONG_ARGS_VALUE;

    ctx->spi_busy = 1;
    ctx->OPCODE = EXTFLASH_WRBF1;
    compute_wr_buff_addr_byte(&ctx->tx_buffer[PG_ADDR_INDEX], buff_addr);
    // If user provided data, copy it, otherwise, we trust him to have
    // writtent in tx_buffer
    if (p_data != NULL) {
        memcpy(&ctx->tx_buffer[DATA_INDEX], p_data, size);
    }
    err_t retval =
        nrf_drv_spi_transfer(&spi, ctx->tx_buffer, size + 4, NULL, 0);
    if (retval != NRF_SUCCESS) {
        ctx->spi_busy = 0;
        return LB_ERR;
    }
    while (ctx->spi_busy)
        power_manage(); // wait for event
    while (extflash_is_ready(ctx) == 0)
        ;
    return LB_OK;
}

// Write bytes to a page up to 528 bytes
err_t extflash_write_raw(lb_spi_ctx *ctx, const uint16_t pg_addr,
                         uint8_t *p_data, const size_t size) {
    if (ctx == NULL || size > MAX_BF_BYTE_ADDR + 1 || pg_addr > MAX_PG_ADDR)
        return LB_WRONG_ARGS_VALUE;

    err_t retval;
    int written = 0;
    // Fill the buffer up to MAX_BF_BYTE_ADDR + 1
    while (written < size) {
        int to_write = 0;
        to_write = (size - written < MAX_SPI_PAYLOAD) ? size - written
                                                      : MAX_SPI_PAYLOAD;

        ctx->spi_busy = 1;
        ctx->OPCODE = EXTFLASH_WRBF1;
        compute_wr_buff_addr_byte(&ctx->tx_buffer[PG_ADDR_INDEX], written);
        // If user provided data, copy it, otherwise, we trust him to have
        // writtent in tx_buffer
        if (p_data != NULL) {
            memcpy(&ctx->tx_buffer[DATA_INDEX], &p_data[written], to_write);
        }
        retval =
            nrf_drv_spi_transfer(&spi, ctx->tx_buffer, to_write + 4, NULL, 0);
        if (retval != NRF_SUCCESS)
            goto error;
        while (ctx->spi_busy)
            power_manage(); // wait for event
        while (extflash_is_ready(ctx) == 0)
            ;
        written += to_write;
    }

    // Flush buffer to MainMemory page
    ctx->spi_busy = 1;
    ctx->OPCODE = EXTFLASH_BF1TOMNE;
    compute_page_addr_byte(&ctx->tx_buffer[PG_ADDR_INDEX], pg_addr, 0);
    retval = nrf_drv_spi_transfer(&spi, ctx->tx_buffer, 4, NULL, 0);
    if (retval != NRF_SUCCESS)
        goto error;
    while (ctx->spi_busy)
        power_manage(); // wait for event
    while (extflash_is_ready(ctx) == 0)
        ;
    if (extflash_prgm_err(ctx))
        return LB_MASS_STORAGE_PAGE_ERROR;
    return LB_OK;

error:
    ctx->spi_busy = 0;
    return LB_ERR;
}
#endif

// Store a full message_t struct into the flash as a byte array
err_t store_msg(void *p_ctx, const uint16_t pg_addr, message_t *p_msg) {
#ifdef BOARD_CUSTOM
    if (p_msg == NULL)
        return LB_WRONG_ARGS_VALUE;
    lb_spi_ctx *ctx = (lb_spi_ctx *)p_ctx;

    // Convert msg to raw byte array
    message_to_raw(tmp_rawmsg, p_msg);

    // Send over SPI to the external flash
    return extflash_write_raw(ctx, pg_addr, tmp_rawmsg, p_msg->size);
#else
    return LB_OK;
#endif
}

err_t load_msg_size(lb_spi_ctx *ctx, const uint16_t pg_addr, size_t *msg_size) {
#ifdef BOARD_CUSTOM
    if (ctx == NULL)
        return LB_WRONG_ARGS_VALUE;

    // Read the size
    err_t retval;
    ctx->spi_busy = 1;
    ctx->OPCODE = EXTFLASH_RDARRAYLP;
    compute_page_addr_byte(&ctx->tx_buffer[PG_ADDR_INDEX], pg_addr, 0);
    retval =
        nrf_drv_spi_transfer(&spi, ctx->tx_buffer, 4, ctx->rx_buffer, 2 + 4);
    if (retval != NRF_SUCCESS)
        goto error;
    while (ctx->spi_busy)
        power_manage(); // wait for event
    *msg_size = bytes_to_uint16(&ctx->rx_buffer[4]);
    while (extflash_is_ready(ctx) == 0)
        ;
    return LB_OK;
error:
    ctx->spi_busy = 0;
    return LB_ERR;
#else
    return LB_OK;
#endif
}

err_t extflash_read_to_spibuff(lb_spi_ctx *ctx, const uint16_t pg_addr,
                               uint8_t *p_data, const size_t size,
                               const size_t in_page_addr) {
    if (ctx == NULL || size > MAX_SPI_PAYLOAD || pg_addr > MAX_PG_ADDR ||
        in_page_addr > MAX_BF_BYTE_ADDR ||
        in_page_addr + size > MAX_BF_BYTE_ADDR) {
        return LB_WRONG_ARGS_VALUE;
    }
    // READ
    ctx->spi_busy = 1;
    ctx->OPCODE = EXTFLASH_RDARRAYLP;
    compute_page_addr_byte(&ctx->tx_buffer[PG_ADDR_INDEX], pg_addr,
                           in_page_addr);
    err_t retval =
        nrf_drv_spi_transfer(&spi, ctx->tx_buffer, 4, ctx->rx_buffer, size + 4);
    if (retval != NRF_SUCCESS) {
        ctx->spi_busy = 0;
        return LB_ERR;
    }
    while (ctx->spi_busy)
        power_manage(); // wait for event
    if (p_data != NULL) {
        memcpy(p_data, &ctx->rx_buffer[DATA_INDEX], size);
    }
    while (extflash_is_ready(ctx) == 0)
        ;
    return LB_OK;
}

// Read bytes from a page up to 528 bytes
err_t extflash_read_raw(lb_spi_ctx *ctx, const uint16_t pg_addr,
                        uint8_t *p_data, const size_t size) {
#ifdef BOARD_CUSTOM
    if (ctx == NULL || size > MAX_BF_BYTE_ADDR + 1 || pg_addr > MAX_PG_ADDR) {
        return LB_WRONG_ARGS_VALUE;
    }

    err_t retval;
    int read = 0;
    while (read < size) {
        int to_read = 0;
        to_read =
            (size - read < MAX_SPI_PAYLOAD) ? size - read : MAX_SPI_PAYLOAD;
        // READ
        ctx->spi_busy = 1;
        ctx->OPCODE = EXTFLASH_RDARRAYLP;
        compute_page_addr_byte(&ctx->tx_buffer[PG_ADDR_INDEX], pg_addr, read);
        retval = nrf_drv_spi_transfer(&spi, ctx->tx_buffer, 4, ctx->rx_buffer,
                                      to_read + 4);
        if (retval != NRF_SUCCESS) {
            goto error;
        }
        while (ctx->spi_busy)
            power_manage(); // wait for event
        if (p_data != NULL) {
            memcpy(&p_data[read], &ctx->rx_buffer[DATA_INDEX], to_read);
        }
        while (extflash_is_ready(ctx) == 0)
            ;
        read += to_read;
    }
    return LB_OK;

error:
    ctx->spi_busy = 0;
    return LB_ERR;
#else
    return LB_OK;
#endif
}

// Return a message_t struct filled from flash
err_t load_msg(void *p_ctx, const uint16_t pg_addr, message_t *p_msg) {
#ifdef BOARD_CUSTOM
    if (p_msg == NULL) {
        return LB_WRONG_ARGS_VALUE;
    }
    lb_spi_ctx *ctx = (lb_spi_ctx *)p_ctx;

    // Read the size fo the message
    err_t retval;
    size_t msg_size;
    retval = load_msg_size(ctx, pg_addr, &msg_size);
    if (retval != LB_OK) {
        return retval;
    }

    // Check that message size is coherent
    if (msg_size < MESSAGE_HEADER_SIZE + MESSAGE_SIGNATURE_SIZE + 2 ||
        msg_size > MESSAGE_MAX_SIZE) {
        return LB_ERR;
    }

    // actually read message over SPI from external flash memory
    retval = extflash_read_raw(ctx, pg_addr, tmp_rawmsg, msg_size);

    // Build message struct from raw byte array
    message_from_raw(tmp_rawmsg, p_msg);
    return retval;
#else
    return LB_OK;
#endif
}

err_t lb_store_slot_pool(lb_slot_pool *p_pool, void *_ctx) {
    if (_ctx == NULL || p_pool == NULL) {
        return LB_ERR;
    }
    (void)p_pool;
#ifdef BOARD_CUSTOM
    // FLASH
    lb_spi_ctx *ctx = (lb_spi_ctx *)_ctx;

    // Write in spi buffer the slot_pool meta data
    int to_write = lb_slot_md_to_raw(&ctx->TX_DATA, p_pool);
    extflash_write_raw(ctx, SLOT_POOL_PG_START_ADDR, NULL, to_write);

    uint32_t slot_written = 0;

    err_t retval = LB_OK;
    // Fill the pages dedicated to slot pool
    for (int i_page = SLOT_POOL_PG_START_ADDR + 1;
         i_page < SLOT_POOL_PG_END_ADDR; i_page++) {
        // one page filled per loop

        if (slot_written >= LB_NB_MAX_MSGS + 1) {
            // We are done writting all slots
            break;
        }
        int slot_written_in_fl_buff = 0;
        // Fill the extflash buffer
        while (slot_written_in_fl_buff < PG_SIZE / LB_SLOT_SIZE_BYTE &&
               slot_written < LB_NB_MAX_MSGS + 1) {
            // at each loop we write in extflash MAX_SPI_PAYLOAD /
            // LB_SLOT_SIZE_BYTE slots and we move cursor consequently
            // (slot_written_in_fl_buff)

            // fill spi Buffer
            // number of slots written in the spi buff
            int written_slot_in_spi_buff = 0;
            while (written_slot_in_spi_buff <
                       MAX_SPI_PAYLOAD / LB_SLOT_SIZE_BYTE &&
                   slot_written_in_fl_buff + written_slot_in_spi_buff <
                       PG_SIZE / LB_SLOT_SIZE_BYTE &&
                   slot_written < LB_NB_MAX_MSGS + 1) {
                // one slot is written in spi buff per loop
                lb_slot_to_raw(
                    &ctx->tx_buffer[DATA_INDEX + written_slot_in_spi_buff *
                                                     LB_SLOT_SIZE_BYTE],
                    &p_pool->slots[slot_written]);
                written_slot_in_spi_buff++;
                slot_written++;
            }
            // The spi buffer is filled, push to extflash buffer at position
            // 'slot_written_in_fl_buff * LB_SLOT_SIZE_BYTE'
            retval = extflash_buff_write_raw(
                ctx, slot_written_in_fl_buff * LB_SLOT_SIZE_BYTE, NULL,
                written_slot_in_spi_buff * LB_SLOT_SIZE_BYTE);
            if (retval != LB_OK) {
                return LB_ERR;
            }
            slot_written_in_fl_buff += written_slot_in_spi_buff;
            written_slot_in_spi_buff = 0;
        }

        // Extflash buff is filled up, push to page
        retval = extflash_push_buff(ctx, i_page);
        if (retval != LB_OK) {
            return LB_ERR;
        }
        slot_written_in_fl_buff = 0;
    }
        // FLASH

#endif
    return LB_OK;
}

err_t lb_load_slot_pool(lb_slot_pool *p_pool, void *_ctx) {
    if (_ctx == NULL || p_pool == NULL) {
        return LB_WRONG_ARGS_VALUE;
    }

    lb_spi_ctx *ctx = (lb_spi_ctx *)_ctx;
    err_t retval;

    // Get slot_pool meta data
    retval = extflash_read_raw(ctx, SLOT_POOL_PG_START_ADDR, &ctx->RX_DATA,
                               LB_SLOT_POOL_MD_SIZE_BYTE);
    lb_slot_md_from_raw(&ctx->RX_DATA, p_pool);

    // READ FLASH

    int slot_read = 0;

    retval = LB_OK;
    // Read the slots from pages dedicated to slot pool
    for (int i_page = SLOT_POOL_PG_START_ADDR + 1;
         i_page < SLOT_POOL_PG_END_ADDR; i_page++) {
        // one page read per loop

        if (slot_read >= LB_NB_MAX_MSGS + 1) {
            // We are done reading all slots
            break;
        }

        int slot_read_from_page = 0;
        int to_read = 0;
        while (slot_read_from_page < PG_SIZE / LB_SLOT_SIZE_BYTE &&
               slot_read < LB_NB_MAX_MSGS + 1) {

            to_read = (slot_read < (LB_NB_MAX_MSGS + 1) -
                                       (MAX_SPI_PAYLOAD / LB_SLOT_SIZE_BYTE))
                          ? MAX_SPI_PAYLOAD / LB_SLOT_SIZE_BYTE
                          : (LB_NB_MAX_MSGS + 1) - slot_read;

            // actually read message over SPI from external flash memory
            retval = extflash_read_to_spibuff(ctx, i_page, NULL,
                                              to_read * LB_SLOT_SIZE_BYTE,
                                              slot_read_from_page);
            if (retval != LB_OK) {
                return LB_ERR;
            }

            // Fill slot poll from spi Buffer
            for (int k = 0; k < to_read; k++) {
                lb_slot_from_raw(
                    &ctx->rx_buffer[DATA_INDEX + k * LB_SLOT_SIZE_BYTE],
                    &p_pool->slots[slot_read]);
                slot_read++;
            }
        }
        // READ FLASH
        slot_read_from_page = 0;
    }
    return LB_OK;
}
