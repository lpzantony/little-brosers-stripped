#ifndef GATT_H
#define GATT_H

#include "utils.h"
#include <ble_types.h>

#define BLE_UUID_BASE                                                          \
    {                                                                          \
        {                                                                      \
            0x58, 0x43, 0x41, 0x47, 0x00, 0x00, 0x00, 0x00, 0xEF, 0x4F, 0x52,  \
                0xC6, 0x00, 0x00, 0x00, 0x00                                   \
        }                                                                      \
    }

void gatt_init(void);
void update_base_uuid(ble_uuid128_t *const base);

#endif
