#ifndef DIFF_SERVICE_H
#define DIFF_SERVICE_H

#include "ble.h"
#include "ble_hci.h"
#include "ble_srv_common.h"
#include "gatt.h"
#include "lb_crypto.h"
#include "merkle_merge.h"
#include "nrf_drv_rng.h"
#include "nrf_log.h"
#include "nrf_sdh_ble.h"
#include "sdk_config.h"

#define BLE_UUID_DIFF_SERVICE 0x2000
#define BLE_DIFF_BLE_OBSERVER_PRIO 2

#define STATUS_LENGTH 1
#define CHILDREN_NUMBER 8
#define HASH_SIZE 4
#define OVERHEAD 1
#define PAYLOAD_MAX_LENGTH MESSAGE_MAX_SIZE

#define BLE_UUID_DIFF_STATUS_CHAR (BLE_UUID_DIFF_SERVICE + 0x1)
#define BLE_DIFFS_STATUS_CHAR_LEN STATUS_LENGTH
#define BLE_UUID_DIFF_PAYLOAD_CHAR (BLE_UUID_DIFF_SERVICE + 0x2)
#define BLE_DIFFS_PAYLOAD_CHAR_LEN PAYLOAD_MAX_LENGTH

#define BLE_DIFFS_DEF(_name)                                                   \
    ble_diffs_t _name;                                                         \
    NRF_SDH_BLE_OBSERVER(_name##_obs, BLE_AUTHS_BLE_OBSERVER_PRIO,             \
                         ble_diffs_on_ble_evt, &_name)

typedef struct {
    uint8_t uuid_type;
    uint16_t service_handle;
    uint16_t conn_handle;
    ble_gatts_char_handles_t status_handles;
    ble_gatts_char_handles_t payload_handles;
} ble_diffs_t;

void diff_service_init(ble_diffs_t *p_diff_service);
void ble_diffs_on_ble_evt(ble_evt_t const *p_ble_evt, void *p_context);
void merge_process();
err_t lb_send(lb_task_stack *p_st, void *arg);
err_t lb_recv(lb_task_stack *p_st, void *arg);

extern volatile int diff_ready;

#endif /* DIFF_SERVICE_H */
