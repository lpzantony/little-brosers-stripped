#ifndef CONNECTION_TIMER_H
#define CONNECTION_TIMER_H
#include "app_timer.h"
#include "drop.h"
#include "utils.h"

#define LB_CONNECTION_TIMEOUT_MS 3000
typedef void (*on_timemout_func)(void);

// Need to be call after any function which inits the app_timer API
void lb_connection_timer_init();

void lb_connection_timer_start(uint32_t timeout_ms,
                               on_timemout_func on_timemout);

void lb_connection_timer_cancel();

#endif // CONNECTION_TIMER_H