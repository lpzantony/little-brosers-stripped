#ifndef LB_CRYPTO_H
#define LB_CRYPTO_H

#include "app_error.h"
#include "nrf_crypto.h"
#include "nrf_crypto_sw_hash.h"
#include <stdbool.h>
#include <string.h>

#define LB_LITTLE_ENDIAN 1

extern nrf_value_length_t server_public;

extern nrf_value_length_t drop_private_inner;

void lb_crypto_init();

// SHA256 big endian
void lb_crypto_hash(const uint8_t *const data, uint32_t len,
                    nrf_value_length_t *const hash);

// SECP256R1 with SHA256 big endian
int lb_crypto_sign(const uint8_t *const data, uint32_t len,
                   nrf_value_length_t *const signature,
                   const nrf_value_length_t *const private_key);

bool lb_crypto_verify_signature(const uint8_t *const data, uint32_t len,
                                const nrf_value_length_t *const signature,
                                const nrf_value_length_t *const public_key);

bool lb_crypto_verify_signature_from_hash(
    const nrf_value_length_t *const hash,
    const nrf_value_length_t *const signature,
    const nrf_value_length_t *const public_key);

void lb_get_drop_private_key(nrf_value_length_t *private_key);

void lb_get_drop_public_key(nrf_value_length_t *public_key);

void lb_get_server_public_key(nrf_value_length_t *server_public_key);

void lb_crypto_invert_private_key(nrf_value_length_t *private_key);

void lb_crypto_invert_public_key(nrf_value_length_t *public_key);

bool lb_check_root_certificate(nrf_value_length_t *certificate);

#endif /* LB_CRYPTO_H */
