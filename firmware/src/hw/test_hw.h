#ifndef TEST_HW_H
#define TEST_HW_H

#include "authentication_service_backend.h"
#include "flash.h"
#include "lb_crypto.h"
#include "message.h"

extern nrf_value_length_t user_public;

void test_init_keys(void);
void test_message(void);
void test_auth_user(uint8_t *data, uint32_t len);
void test_auth_drop_challenge(ble_auths_t *running_auth_service);
void test_auth_drop_verify(ble_auths_t *running_auth_service);
void test_certificate_validity(void);
void test_auth_certificate(void);
void test_time_certificate(void);
int merkle_tree_test_routine();
void extflash_test_routine(lb_spi_ctx *ctx);
void msg_in_flash(lb_spi_ctx *ctx);
void merge_test(lb_spi_ctx *ctx);
void slot_bckp(lb_spi_ctx *ctx);
void pool_bckp(lb_spi_ctx *ctx);
void pool_store(lb_spi_ctx *ctx);
void pool_wake(lb_spi_ctx *ctx);
#endif /* TEST_HW_H */
