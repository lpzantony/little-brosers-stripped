#include "SEGGER_RTT.h"
#include "advertising.h"
#include "ble_stack.h"
#include "bsp_timers.h"
#include "conn_params.h"
#include "drop.h"
#include "gap.h"
#include "gatt.h"
#include "lb_crypto.h"
#include "log.h"
#include "message.h"
#include "nrf_delay.h"
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "pm.h"
#include "power_management.h"
#include "services.h"
#include "utils.h"

#include "test_hw.h"

#include "connection_timer.h"

#define DUMMY_MSG_SIZE MESSAGE_SIGNATURE_SIZE + MESSAGE_HEADER_SIZE + 1 + 2

/**@brief Function for application main entry.
 */
int main(void) {

    bool erase_bonds;

    // Initialize.
    log_init();
    timers_init();

    buttons_leds_init(&erase_bonds);

    ble_stack_init();
    gap_params_init();
    gatt_init();
    services_init();
    advertising_init();
    conn_params_init();
    peer_manager_init();

    // Init crypto functions
    lb_crypto_init();
    lb_init_drop();
    extflash_go_to_ultra_sleep(&spi_ctx);

    advertising_start(erase_bonds);
    lb_connection_timer_init();
    // Enter main loop.
    for (;;) {
        uint8_t old_root_hash[LB_SLOT_SIGNATURE_SIZE];
        memcpy(old_root_hash, root.p_hash, LB_SLOT_SIGNATURE_SIZE);

        // Wait for authentication
        while (!auth_service_check_status()) {
            power_manage();
        }
        LBLOG("Auth ended\n");

        // AWAKE FLASH
        // Awake up extflash
        extflash_get_out_ultra_sleep(&spi_ctx);

        // SHIFT
        // Shift the tree. If not needed, this func will return with an error,
        // that's all
        lb_shift_tree(&root, &slot_pool, time_reference, lb_store_slot_pool,
                      &spi_ctx);

        // DIFF
        // Process the diff
        LBLOG("Starting diff\n");
        lb_diff(&task_stack, &root, 0, lb_send, NULL, lb_recv, NULL);
        LBLOG("Out of diff\n");

        // MERGE (WIP)
        LBLOG("Starting merge");
        merge_process();
        LBLOG("Out of merge");

        // UPDATE COUNTER
        if (lb_compare_hash(old_root_hash, root.p_hash)) {
            LBLOG("Updating counter");
            counter++;
            update_counter(counter);
            // Saving slot_pool
            err_t ret = lb_store_slot_pool(&slot_pool, &spi_ctx);
            if (ret != LB_OK) {
                LBLOG("Error saving slot pool");
            }
        }

        // ASLEEP FLASH
        // Send extflash to sleep
        extflash_go_to_ultra_sleep(&spi_ctx);

        // DISCONNECT
        ble_disconnect();
        nrf_delay_ms(1000);
    }
}
