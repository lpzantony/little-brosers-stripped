#include "advertising.h"
#include "app_error.h"
#include "app_params.h"
#include "ble_srv_common.h"
#include "bsp_btn_ble.h"
#include "drop_data.h"
#include "nrf_log.h"
#include "nrf_sdh_ble.h"
#include "nrf_sdh_soc.h"
#include "peer_manager.h"
#include "power_management.h"

// Adapting the SDK macro BLE_ADVERTISING_DEF to a non static declaration
ble_advertising_t m_advertising;
static ble_advdata_manuf_data_t manuf_data;
static ble_advdata_t advdata;

// Manufacturer specific advertising data
typedef struct __attribute__((__packed__)) {
    uint32_t lb_id;
    uint16_t company_id;
    uint16_t network_id;
    uint16_t drop_id;
    uint32_t update_counter;
} data_format_t;

NRF_SDH_BLE_OBSERVER(m_advertising_ble_obs, BLE_ADV_BLE_OBSERVER_PRIO,
                     ble_advertising_on_ble_evt, &m_advertising);
NRF_SDH_SOC_OBSERVER(m_advertising_soc_obs, BLE_ADV_SOC_OBSERVER_PRIO,
                     ble_advertising_on_sys_evt, &m_advertising);

// Update counter and advertising data
void update_counter(uint32_t counter) {
    data_format_t data = {swap_uint32(get_lb_id()),
                          swap_uint16(get_company_id()),
                          swap_uint16(get_network_id()),
                          swap_uint16(get_drop_id()), swap_uint32(counter)};

    manuf_data.data.p_data = (uint8_t *)&data;

    ret_code_t err_code;
    err_code = ble_advdata_set(&advdata, NULL);
    APP_ERROR_CHECK(err_code);
}

/**@brief Function for handling advertising events.
 *
 * @details This function will be called for advertising events which are passed
 * to the application.
 *
 * @param[in] ble_adv_evt  Advertising event.
 */
static void on_adv_evt(ble_adv_evt_t ble_adv_evt) {
    ret_code_t err_code;

    switch (ble_adv_evt) {
    case BLE_ADV_EVT_FAST:
        NRF_LOG_INFO("Fast advertising.");
        err_code = bsp_indication_set(BSP_INDICATE_ADVERTISING);
        APP_ERROR_CHECK(err_code);
        break;

    case BLE_ADV_EVT_IDLE:
        sleep_mode_enter();
        break;

    default:
        break;
    }
}

/**@brief Clear bond information from persistent storage.
 */
static void delete_bonds(void) {
    ret_code_t err_code;

    NRF_LOG_INFO("Erase bonds!");

    err_code = pm_peers_delete();
    APP_ERROR_CHECK(err_code);
}

/**@brief Function for starting advertising.
 */
void advertising_start(bool erase_bonds) {
    if (erase_bonds == true) {
        delete_bonds();
        // Advertising is started by PM_EVT_PEERS_DELETED_SUCEEDED evetnt
    } else {
        ret_code_t err_code =
            ble_advertising_start(&m_advertising, BLE_ADV_MODE_FAST);

        APP_ERROR_CHECK(err_code);
    }
}

/**@brief Function for initializing the Advertising functionality.
 */
void advertising_init() {
    ret_code_t err_code;
    ble_advertising_init_t init = {{0}};

    // Transform fields to big-endian for transmission
    data_format_t data = {swap_uint32(get_lb_id()),
                          swap_uint16(get_company_id()),
                          swap_uint16(get_network_id()),
                          swap_uint16(get_drop_id()), swap_uint32(0)};

    // 0xFFFF identifier for development
    manuf_data.company_identifier = 0xFFFF;
    manuf_data.data.p_data = (uint8_t *)&data;
    manuf_data.data.size = sizeof(data);

    /* For easy detection in nordic app */
    advdata.name_type = BLE_ADVDATA_FULL_NAME;
    /* */

    advdata.include_appearance = false;
    advdata.flags = BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE;
    advdata.p_manuf_specific_data = &manuf_data;

    init.advdata = advdata;

    init.config.ble_adv_fast_enabled = true;
    init.config.ble_adv_fast_interval = APP_ADV_INTERVAL;

    init.evt_handler = on_adv_evt;

    err_code = ble_advertising_init(&m_advertising, &init);
    APP_ERROR_CHECK(err_code);

    ble_advertising_conn_cfg_tag_set(&m_advertising, APP_BLE_CONN_CFG_TAG);
}
