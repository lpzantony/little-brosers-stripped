#include "lb_crypto.h"
#include "drop_data.h"
#include "nrf_log.h"

static nrf_crypto_signature_info_t my_signature_info = {
    NRF_CRYPTO_CURVE_SECP256R1, NRF_CRYPTO_HASH_TYPE_SHA256,
    NRF_CRYPTO_ENDIAN_LE};

NRF_CRYPTO_HASH_CREATE(my_hash, SHA256);

nrf_value_length_t drop_private_inner = {
    .p_value = drop_info.private_key,
    .length = NRF_CRYPTO_ECC_PRIVATE_KEY_SIZE(SECP256R1)};

nrf_value_length_t server_public = {
    .p_value = drop_info.ca_public_key,
    .length = NRF_CRYPTO_ECC_PUBLIC_KEY_SIZE(SECP256R1)};

void lb_crypto_init() {
    static bool active = 0;
    if (active) {
        return;
    }

    active = 1;

    ret_code_t err_code = nrf_crypto_init();
    APP_ERROR_CHECK(err_code);
}

void lb_crypto_hash(const uint8_t *const data, uint32_t len,
                    nrf_value_length_t *const hash) {
    static sha256_context_t ctx;
    sha256_init(&ctx);
    sha256_update(&ctx, data, len);
    sha256_final(&ctx, hash->p_value, LB_LITTLE_ENDIAN);
}

int lb_crypto_sign(const uint8_t *const data, uint32_t len,
                   nrf_value_length_t *const signature,
                   const nrf_value_length_t *const private_key) {
    // Hash
    lb_crypto_hash(data, len, &my_hash);

    // Sign
    return nrf_crypto_ecdsa_sign_hash(my_signature_info, private_key, &my_hash,
                                      signature);
}

bool lb_crypto_verify_signature(const uint8_t *const data, uint32_t len,
                                const nrf_value_length_t *const signature,
                                const nrf_value_length_t *const public_key) {

    // Hash
    lb_crypto_hash(data, len, &my_hash);

    // Verify signature and return result
    uint32_t res = nrf_crypto_ecdsa_verify_hash(my_signature_info, public_key,
                                                &my_hash, signature);

    return (res == NRF_SUCCESS);
}

bool lb_crypto_verify_signature_from_hash(
    const nrf_value_length_t *const hash,
    const nrf_value_length_t *const signature,
    const nrf_value_length_t *const public_key) {

    // Verify signature and return result
    int res = nrf_crypto_ecdsa_verify_hash(my_signature_info, public_key, hash,
                                           signature);
    return (res == NRF_SUCCESS);
}

void lb_get_drop_private_key(nrf_value_length_t *private_key) {
    memcpy(private_key->p_value, drop_private_inner.p_value,
           private_key->length);
}

void lb_get_drop_public_key(nrf_value_length_t *public_key) {
    nrf_crypto_curve_info_t my_curve_info = {NRF_CRYPTO_CURVE_SECP256R1,
                                             NRF_CRYPTO_ENDIAN_LE};

    nrf_crypto_ecc_public_key_calculate(my_curve_info, &drop_private_inner,
                                        public_key);
}

void lb_get_server_public_key(nrf_value_length_t *public_key) {
    memcpy(public_key->p_value, server_public.p_value, public_key->length);
}

void lb_crypto_invert_private_key(nrf_value_length_t *private_key) {
    for (uint8_t i = 0; i < private_key->length / 2; i++) {
        uint8_t temp = private_key->p_value[i];
        private_key->p_value[i] =
            private_key->p_value[private_key->length - 1 - i];
        private_key->p_value[private_key->length - 1 - i] = temp;
    }
}

void lb_crypto_invert_public_key(nrf_value_length_t *public_key) {
    uint8_t half_length = public_key->length / 2;
    uint8_t temp;

    // Invert each half by itself
    for (uint8_t i = 0; i < half_length / 2; i++) {
        temp = public_key->p_value[i];
        public_key->p_value[i] = public_key->p_value[half_length - 1 - i];
        public_key->p_value[half_length - 1 - i] = temp;

        temp = public_key->p_value[i + half_length];
        public_key->p_value[i + half_length] =
            public_key->p_value[half_length - 1 - i + half_length];
        public_key->p_value[half_length - 1 - i + half_length] = temp;
    }
}
