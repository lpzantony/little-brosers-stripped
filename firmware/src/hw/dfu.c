#include "dfu.h"
#include "nrf_log.h"

void ble_dfu_evt_handler(ble_dfu_buttonless_evt_type_t event) {
    switch (event) {
    case BLE_DFU_EVT_BOOTLOADER_ENTER_PREPARE:
        NRF_LOG_INFO("Device is preparing to enter bootloader mode.");
        // YOUR_JOB: Disconnect all bonded devices that currently are connected.
        //           This is required to receive a service changed indication
        //           on bootup after a successful (or aborted) Device Firmware
        //           Update.
        break;

    case BLE_DFU_EVT_BOOTLOADER_ENTER:
        // YOUR_JOB: Write app-specific unwritten data to FLASH, control
        // finalization of this
        //           by delaying reset by reporting false in
        //           app_shutdown_handler
        NRF_LOG_INFO("Device will enter bootloader mode.");
        break;

    case BLE_DFU_EVT_BOOTLOADER_ENTER_FAILED:
        NRF_LOG_ERROR(
            "Request to enter bootloader mode failed asynchroneously.");
        // YOUR_JOB: Take corrective measures to resolve the issue
        //           like calling APP_ERROR_CHECK to reset the device.
        break;

    case BLE_DFU_EVT_RESPONSE_SEND_ERROR:
        NRF_LOG_ERROR("Request to send a response to client failed.");
        // YOUR_JOB: Take corrective measures to resolve the issue
        //           like calling APP_ERROR_CHECK to reset the device.
        APP_ERROR_CHECK(false);
        break;

    default:
        NRF_LOG_ERROR("Unknown event from ble_dfu_buttonless.");
        break;
    }
}
