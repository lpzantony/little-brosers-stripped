#include "gatt.h"
#include "app_error.h"
#include "drop_data.h"
#include "nrf_ble_gatt.h"
#include "nrf_sdh_ble.h"

NRF_BLE_GATT_DEF(m_gatt); /**< GATT module instance. */

/**@brief Function for handling the YYY Service events.
 * YOUR_JOB implement a service handler function depending on the event the
service you are using can generate
 *
 * @details This function will be called for all YY Service events which are
passed to
 *          the application.
 *
 * @param[in]   p_yy_service   YY Service structure.
 * @param[in]   p_evt          Event received from the YY Service.
 *
 *
static void on_yys_evt(ble_yy_service_t     * p_yy_service,
                       ble_yy_service_evt_t * p_evt)
{
    switch (p_evt->evt_type)
    {
        case BLE_YY_NAME_EVT_WRITE:
            APPL_LOG("[APPL]: charact written with value %s. ",
p_evt->params.char_xx.value.p_str); break;

        default:
            // No implementation needed.
            break;
    }
}
*/

/**@brief Function for initializing the GATT module.
 */
void gatt_init(void) {
    ret_code_t err_code = nrf_ble_gatt_init(&m_gatt, NULL);
    APP_ERROR_CHECK(err_code);
    // err_code = nrf_ble_gatt_att_mtu_periph_set(&m_gatt, 217);
    // APP_ERROR_CHECK(err_code);
}

void update_base_uuid(ble_uuid128_t *const base) {
    uint16_to_bytes(swap_uint16(get_company_id()), base->uuid128 + 4);
    uint16_to_bytes(swap_uint16(get_network_id()), base->uuid128 + 6);
}
