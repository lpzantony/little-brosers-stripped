#ifndef ADVERTISING_H
#define ADVERTISING_H

#include "ble_advertising.h"
#include "utils.h"
#include <stdbool.h>

void advertising_start(bool erase_bonds);
void advertising_init(void);
void update_counter(uint32_t counter);

extern ble_advertising_t m_advertising;

uint32_t get_update_counter(void);

#endif
