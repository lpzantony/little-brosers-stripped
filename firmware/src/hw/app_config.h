#ifndef APP_CONFIG_H
#define APP_CONFIG_H

// Uncomment if yout want the NRF_LOG to be able to handle large printings
//#define LARGE_PRINT_BUFFER

// Uncomment if yout want the NRF_LOG macros to be executed during idle
// #define ENABLE_DEFERRED_LOG

#endif
