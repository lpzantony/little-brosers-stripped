#ifndef CONN_PARAMS_H
#define CONN_PARAMS_H

#include <stdint.h>

extern uint16_t m_conn_handle;

void conn_params_init(void);

#endif
