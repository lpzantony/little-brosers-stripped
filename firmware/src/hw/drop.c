#include "drop.h"

#define MAGIC_NUMBER "GACX"
#define MAGIC_NUMBER_SIZE 4
#define MAGIC_NUMBER_ADDR 0

LB_ALLOC_ROOT_MD(root_md);
LB_ALLOC_MERKLE_TREE(root, slot_pool);
lb_task_stack task_stack;
lb_spi_ctx spi_ctx;
volatile char merge_pending;
volatile uint32_t counter;

err_t lb_init_drop(void) {
    lb_task_stack st;
    counter = 0;
    (void)st;
    LB_INIT_ROOT_MD(root_md);
    LB_INIT_MERKLE_TREE(root);
    lb_stack_init(&task_stack, &slot_pool);
    lb_init_merkle_tree_only(&root, &root_md);
    extflash_init(&spi_ctx);
    uint8_t buff[MAGIC_NUMBER_SIZE];
    uint8_t magic_buff[MAGIC_NUMBER_SIZE] = MAGIC_NUMBER;
    err_t retval =
        extflash_read_raw(&spi_ctx, MAGIC_NUMBER_ADDR, buff, MAGIC_NUMBER_SIZE);
    if (retval != LB_OK) {
        return retval;
    }
    if (memcmp(buff, magic_buff, MAGIC_NUMBER_SIZE)) {
        lb_init_slots(&slot_pool, LB_MT_OFFSET);
        retval = extflash_write_raw(&spi_ctx, MAGIC_NUMBER_ADDR, magic_buff,
                                    MAGIC_NUMBER_SIZE);
        if (retval != LB_OK) {
            return retval;
        }
        retval = lb_store_slot_pool(&slot_pool, &spi_ctx);
        if (retval != LB_OK) {
            return retval;
        }
    } else {
        // Try to restore from flash
        retval = lb_load_slot_pool(&slot_pool, &spi_ctx);
        if (retval != LB_OK) {
            return retval;
        }
        if (slot_pool.nb_slots != LB_NB_MAX_MSGS + 1) {
            // Slot that has been sotred is not the same size, reset it
            lb_init_slots(&slot_pool, LB_MT_OFFSET);
            retval = lb_store_slot_pool(&slot_pool, &spi_ctx);
            if (retval != LB_OK) {
                return retval;
            }
        }
        lb_attach_leafs_to_slots(&root, &slot_pool);

        char counter = 0;
        ssize_t i_slot = slot_pool.i_begin_busy;
        while (i_slot != -1) {
            lb_slot *slot = &slot_pool.slots[i_slot];
            LBLOG("prev = %2d, next = %2d, ts = %2u, addr = %3u\n",
                  slot->i_previous, slot->i_next, slot->timestamp, slot->addr);
            counter++;
            i_slot = slot_pool.slots[i_slot].i_next;
        }
        LBLOG("bb = %2d, bf = %2d, nbfree = %3d, cd = %6d\n",
              slot_pool.i_begin_busy, slot_pool.i_begin_free, slot_pool.nb_free,
              slot_pool.certif_date);
    }

    // Diff testing code, should not be merged in master
    // message_t message = {{1517221285, 0, 0, 0}, 0, {0}, {0x01}};

    return LB_OK;
}
