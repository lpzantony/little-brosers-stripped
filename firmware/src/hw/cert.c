#include "cert.h"
#include "lb_crypto.h"
#include "nrf_crypto.h"
#include "nrf_log.h"
#include "utils.h"

#define SIGNATURE_LENGTH 64
#define MIN_ASN1_SIGNATURE_LENGTH (SIGNATURE_LENGTH + 6)
#define DATE_LENGTH 13
#define ISSUER_INDEX 45
#define INNER_LENGTH_INDEX 5
#define USERNAME_PREQUEL 12
#define USERNAME_MAX_LENGTH 20
#define USER_ID_PREQUEL 17
#define USER_ID_MAX_LENGTH 10
#define PUB_KEY_PREQUEL1 3
#define PUB_KEY_PREQUEL2 4

// Payload size + signature header
#define TIME_SIGNATURE_INDEX (TIME_CERTIFICATE_PAYLOAD_LENGTH + 3)

NRF_CRYPTO_ECDSA_SIGNATURE_CREATE(signature, SECP256R1);

static uint8_t latest_time_certificate[TIME_CERTIFICATE_LENGTH];
static uint8_t time_certificate_length;

bool certificate_valid(uint8_t *user_certificate, size_t length,
                       nrf_value_length_t *user_public) {
    /*
     * Parse signature
     */
    uint8_t *ptr = user_certificate + (length - MIN_ASN1_SIGNATURE_LENGTH);

    // 5 signature header and 12 signature algorithm
    uint8_t sig_length = 5 + 12;
    // Find out where we are in the signature
    switch (*ptr) {
    case 0x02:
        sig_length += *(ptr - 1);
        ptr += 1;
        break;
    case 0x30:
        sig_length += *(ptr + 1);
        ptr += 3;
        break;
    default:
        sig_length += *ptr;
        ptr += 2;
        break;
    }

    // Check for leading 0
    if (*(ptr++) != SIGNATURE_LENGTH / 2) {
        ptr++;
    }

    // Copy the first half of the signature
    memcpy(signature.p_value, ptr, SIGNATURE_LENGTH / 2);

    ptr += SIGNATURE_LENGTH / 2 + 1;

    // Check for leading 0
    if (*(ptr++) != SIGNATURE_LENGTH / 2) {
        ptr++;
    }

    // Copy the second half of the signature
    memcpy(signature.p_value + SIGNATURE_LENGTH / 2, ptr, SIGNATURE_LENGTH / 2);
    // I used this function because it does what we want. Signature inversion
    // and public key inversion consist of the same operation
    lb_crypto_invert_public_key(&signature);

    // Verify signature
    bool res = lb_crypto_verify_signature(user_certificate + 4,
                                          length - sig_length - 4, &signature,
                                          &server_public);
    if (!res) {
        return false;
    }

    // Adjust offset based on certificate length
    uint8_t offset = 0;
    if (user_certificate[INNER_LENGTH_INDEX] > 0x80) {
        offset = user_certificate[INNER_LENGTH_INDEX] - 0x80;
    }

    /*
     * Parse validity dates
     */
    char start_date[DATE_LENGTH + 1];
    start_date[DATE_LENGTH] = 0;
    char end_date[DATE_LENGTH + 1];
    end_date[DATE_LENGTH] = 0;
    ptr = user_certificate + ISSUER_INDEX + offset;

    // Detect issuer sequence
    if (*(ptr++) != 0x30) {
        return false;
    }

    // Skip the issuer sequence length + 1
    uint8_t to_skip = *ptr;
    ptr += to_skip + 1;

    // Find beginning of date
    ptr += 4;
    // Parse start date
    memcpy(start_date, ptr, DATE_LENGTH);
    // Parse end date
    ptr += DATE_LENGTH + 2;
    memcpy(end_date, ptr, DATE_LENGTH);
    ptr += DATE_LENGTH;

    // NRF_LOG_INFO("Start date: %s", nrf_log_push(start_date));
    // NRF_LOG_INFO("End date: %s", nrf_log_push(end_date));

    /*
     * Parse username and user ID
     */
    char username[USERNAME_MAX_LENGTH];
    ptr += USERNAME_PREQUEL;
    uint8_t username_length = *ptr;
    if (username_length > USERNAME_MAX_LENGTH) {
        return false;
    }
    memcpy(username, ++ptr, username_length);
    username[username_length] = 0;
    ptr += username_length;

    // NRF_LOG_INFO("Username: %s", nrf_log_push(username));

    char user_id_string[USER_ID_MAX_LENGTH];
    ptr += USER_ID_PREQUEL;
    uint8_t user_id_length = *ptr;
    if (user_id_length > USER_ID_MAX_LENGTH) {
        return false;
    }
    memcpy(user_id_string, ++ptr, user_id_length);
    user_id_string[user_id_length] = 0;
    ptr += user_id_length;
    // Uncomment when used later
    // uint32_t user_id = atoi(user_id_string);

    // NRF_LOG_INFO("User ID: %u", user_id);

    /*
     * Parse public key
     */
    ptr += PUB_KEY_PREQUEL1;
    to_skip = *ptr;
    ptr += to_skip + 1 + PUB_KEY_PREQUEL2;
    memcpy(user_public->p_value, ptr, user_public->length);
    lb_crypto_invert_public_key(user_public);

    // All tests have passed, return true
    return true;
}

bool time_certificate_valid(uint8_t *time_certificate, size_t length,
                            uint32_t *time_reference) {
    uint8_t *ptr = time_certificate;
    uint32_t temp_time = bytes_to_uint32(ptr);
    /*
     * Parse signature
     */
    ptr = time_certificate + TIME_SIGNATURE_INDEX;

    // Check for leading 0
    if (*(ptr++) != SIGNATURE_LENGTH / 2) {
        ptr++;
    }

    // Copy the first half of the signature
    memcpy(signature.p_value, ptr, SIGNATURE_LENGTH / 2);

    ptr += SIGNATURE_LENGTH / 2 + 1;

    // Check for leading 0
    if (*(ptr++) != SIGNATURE_LENGTH / 2) {
        ptr++;
    }

    // Copy the second half of the signature
    memcpy(signature.p_value + SIGNATURE_LENGTH / 2, ptr, SIGNATURE_LENGTH / 2);
    // I used this function because it does what we want. Signature inversion
    // and public key inversion consist of the same operation
    lb_crypto_invert_public_key(&signature);

    // Verify signature
    bool res = lb_crypto_verify_signature(time_certificate,
                                          TIME_CERTIFICATE_PAYLOAD_LENGTH,
                                          &signature, &server_public);

    if (res && temp_time >= *time_reference) {
        *time_reference = temp_time;
        memcpy(latest_time_certificate, time_certificate, length);
        time_certificate_length = length;
    }
    return res;
}

size_t get_time_certificate(uint8_t *time_certificate) {
    memcpy(time_certificate, latest_time_certificate, time_certificate_length);
    return time_certificate_length;
}
