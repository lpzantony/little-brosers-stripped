#ifndef LOG_H
#define LOG_H

#include <stdint.h>

void assert_nrf_callback(uint16_t line_num, const uint8_t *p_file_name);
void log_init(void);

#endif
