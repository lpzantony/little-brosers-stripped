#ifndef BLE_STACK_H
#define BLE_STACK_H

#include "ble.h"
#include "cert.h"
#include "message.h"
#include <stdint.h>

#define QUEUED_BUFFER_LENGTH                                                   \
    (MAX(USER_CERTIFICATE_MAX_LENGTH, MESSAGE_MAX_SIZE) + 8)

void ble_stack_init(void);
void ble_disconnect(void);

extern uint8_t queued_write_buffer[];
extern ble_user_mem_block_t mem_block;
extern uint16_t m_conn_handle;

#endif
