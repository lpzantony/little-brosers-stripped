#ifndef SLEEP_MODE_H
#define SLEEP_MODE_H

void sleep_mode_enter(void);
void power_manage(void);

#endif
