#include "test_hw.h"
#include "diff_service.h"
#include "merkle_merge.h"
#include "merkle_tree.h"
#include "task_stack.h"

NRF_CRYPTO_ECC_PUBLIC_KEY_CREATE(drop_public, SECP256R1);
NRF_CRYPTO_ECC_PRIVATE_KEY_CREATE(user_private, SECP256R1);
NRF_CRYPTO_ECDSA_SIGNATURE_CREATE(my_signature, SECP256R1);

static uint8_t drop_challenge[DROP_CHALLENGE_LENGTH];

void test_init_keys(void) {
    lb_get_drop_public_key(&drop_public);
    uint8_t priv[] = {0x6e, 0x76, 0x02, 0x8f, 0x37, 0xbb, 0xba, 0x56,
                      0x26, 0x17, 0x12, 0x14, 0x7e, 0x85, 0x61, 0x57,
                      0x91, 0xa7, 0x3f, 0xc2, 0x23, 0x17, 0x1d, 0x89,
                      0xeb, 0x3e, 0x2e, 0x98, 0xce, 0xc7, 0xf6, 0xc9};

    memcpy(user_private.p_value, priv, user_private.length);
    lb_crypto_invert_private_key(&user_private);
    nrf_crypto_curve_info_t my_curve_info = {NRF_CRYPTO_CURVE_SECP256R1,
                                             NRF_CRYPTO_ENDIAN_LE};

    nrf_crypto_ecc_public_key_calculate(my_curve_info, &user_private,
                                        &user_public);
}

void test_message(void) {
    // Test message functions
    message_t msg;

    // Payload
    uint8_t payload[] = {0x00, 0x01, 0x02, 0x03, 0x04};

    msg.header.timestamp = 0xABCDEF12;
    msg.header.from_id = 0xDEADBEEF;
    msg.header.to_id = 0x00000000;
    msg.header.type = 0x0001;
    msg.size = MESSAGE_HEADER_SIZE + sizeof(payload) + sizeof(msg.size) +
               MESSAGE_SIGNATURE_SIZE;
    memcpy(msg.payload, payload, sizeof(payload));

    uint8_t raw[msg.size];
    message_to_raw(raw, &msg);

    //*********************************//
    //       Authentication test       //
    //*********************************//

    // Sign
    lb_crypto_sign(raw + sizeof(msg.size),
                   msg.size - sizeof(msg.size) - MESSAGE_SIGNATURE_SIZE,
                   &my_signature, &user_private);

    // Append signature to message
    memcpy(msg.signature, my_signature.p_value, MESSAGE_SIGNATURE_SIZE);

    // Verify signature with the proper public key
    int sig = message_verify_signature(&msg, &user_public);
    NRF_LOG_INFO("Signature verification before -> raw: %d", sig);

    //*********************************//
    //   Passing through raw format    //
    //*********************************//
    message_to_raw(raw, &msg);
    message_t new_msg = {{0}};

    message_from_raw(raw, &new_msg);

    sig = message_verify_signature(&new_msg, &user_public);
    NRF_LOG_INFO("Signature verification after -> raw -> message_t: %d\n", sig);

    message_to_raw(raw, &new_msg);
//#define DUMP
#ifdef DUMP
    NRF_LOG_INFO("raw message, %d: ", size);
    NRF_LOG_HEXDUMP_INFO(raw, size);
#endif
}

void test_auth_user(uint8_t *data, uint32_t len) {
    lb_crypto_sign(data, len, &my_signature, &user_private);
    bool res = test_write_response(&my_signature);
    NRF_LOG_INFO("Auth user test: %d", res);
}

void test_auth_drop_challenge(ble_auths_t *running_auth_service) {
    // Generate challenge
    uint8_t challenge_length =
        generate_random(drop_challenge, DROP_CHALLENGE_LENGTH);

    // Update characteristic
    ble_gatts_value_t challenge;
    challenge.len = challenge_length;
    challenge.offset = 0;
    challenge.p_value = drop_challenge;
    ret_code_t err_code = sd_ble_gatts_value_set(
        running_auth_service->conn_handle,
        running_auth_service->drop_challenge_handles.value_handle, &challenge);

    APP_ERROR_CHECK(err_code);

    auth_service_generate_response();
    test_auth_drop_verify(running_auth_service);
}

void test_auth_drop_verify(ble_auths_t *running_auth_service) {
    // Read drop reponse and put it in signature variable
    ble_gatts_value_t response;
    response.len = CHALLENGE_RESPONSE_LENGTH;
    response.offset = 0;
    response.p_value = my_signature.p_value;

    sd_ble_gatts_value_get(
        running_auth_service->conn_handle,
        running_auth_service->drop_challenge_handles.value_handle, &response);

    bool res = lb_crypto_verify_signature(
        drop_challenge, CHALLENGE_RESPONSE_LENGTH, &my_signature, &drop_public);

    NRF_LOG_INFO("Auth drop test: %d", res);
}

void test_time_certificate(void) {
    static uint8_t time_cert[] = {
        0x5a, 0x66, 0x7b, 0x00, 0x30, 0x44, 0x02, 0x20, 0x1e, 0xb6, 0xeb,
        0xef, 0x73, 0x41, 0x2a, 0x49, 0x34, 0xe9, 0xce, 0x40, 0x26, 0xf3,
        0x46, 0x7a, 0xec, 0xd3, 0x88, 0x4a, 0xd3, 0x96, 0x25, 0x0e, 0xc9,
        0x65, 0x18, 0x98, 0xb6, 0xe3, 0x68, 0xde, 0x02, 0x20, 0x33, 0x07,
        0x4a, 0x21, 0x02, 0x5f, 0x2b, 0xde, 0x1d, 0xa3, 0x46, 0x2d, 0x1e,
        0x88, 0xb7, 0x2b, 0xcd, 0xb2, 0x53, 0x7f, 0x2c, 0xe2, 0x7b, 0x1d,
        0x07, 0xe6, 0x58, 0xbd, 0x9f, 0xe7, 0x51, 0x50};

    uint32_t ref = 0;

    bool res = time_certificate_valid(time_cert, sizeof(time_cert), &ref);
    NRF_LOG_INFO("Cert time validity test: %d, ref: %x", res, ref);
}

void test_certificate_validity(void) {
    static uint8_t raw[] = {
        0x30, 0x82, 0x01, 0x64, 0x30, 0x82, 0x01, 0x0a, 0xa0, 0x03, 0x02, 0x01,
        0x02, 0x02, 0x14, 0x31, 0x27, 0xd2, 0xcf, 0x4f, 0xcd, 0xa1, 0xc2, 0x68,
        0xbf, 0x64, 0x00, 0x5e, 0x7a, 0x2c, 0xbe, 0x72, 0xb9, 0x88, 0xee, 0x30,
        0x0a, 0x06, 0x08, 0x2a, 0x86, 0x48, 0xce, 0x3d, 0x04, 0x03, 0x02, 0x30,
        0x29, 0x31, 0x13, 0x30, 0x11, 0x06, 0x03, 0x55, 0x04, 0x03, 0x0c, 0x0a,
        0x4c, 0x42, 0x5f, 0x4e, 0x45, 0x54, 0x57, 0x4f, 0x52, 0x4b, 0x31, 0x12,
        0x30, 0x10, 0x06, 0x0a, 0x09, 0x92, 0x26, 0x89, 0x93, 0xf2, 0x2c, 0x64,
        0x01, 0x01, 0x0c, 0x02, 0x2d, 0x31, 0x30, 0x1e, 0x17, 0x0d, 0x31, 0x38,
        0x30, 0x31, 0x31, 0x39, 0x30, 0x38, 0x35, 0x33, 0x34, 0x32, 0x5a, 0x17,
        0x0d, 0x31, 0x38, 0x30, 0x34, 0x32, 0x39, 0x30, 0x38, 0x35, 0x33, 0x34,
        0x32, 0x5a, 0x30, 0x29, 0x31, 0x14, 0x30, 0x12, 0x06, 0x03, 0x55, 0x04,
        0x03, 0x0c, 0x0b, 0x63, 0x68, 0x72, 0x69, 0x73, 0x5f, 0x74, 0x65, 0x73,
        0x74, 0x31, 0x31, 0x11, 0x30, 0x0f, 0x06, 0x0a, 0x09, 0x92, 0x26, 0x89,
        0x93, 0xf2, 0x2c, 0x64, 0x01, 0x01, 0x0c, 0x01, 0x33, 0x30, 0x59, 0x30,
        0x13, 0x06, 0x07, 0x2a, 0x86, 0x48, 0xce, 0x3d, 0x02, 0x01, 0x06, 0x08,
        0x2a, 0x86, 0x48, 0xce, 0x3d, 0x03, 0x01, 0x07, 0x03, 0x42, 0x00, 0x04,
        0xbf, 0xcf, 0x51, 0xd4, 0x33, 0xec, 0x5c, 0x27, 0xac, 0x63, 0xf9, 0xf6,
        0x57, 0x3c, 0xee, 0x43, 0x36, 0x82, 0xb5, 0x9c, 0x51, 0x3a, 0x80, 0xe2,
        0xc7, 0xdf, 0x40, 0x17, 0xc4, 0x0f, 0x52, 0x4a, 0x42, 0x10, 0x97, 0x7c,
        0x7c, 0xb2, 0x9a, 0xda, 0x81, 0x8c, 0xf5, 0x31, 0x7c, 0xc3, 0x73, 0x76,
        0xe9, 0xf2, 0xae, 0x67, 0x5c, 0x4e, 0x70, 0xf5, 0x12, 0xeb, 0x4f, 0x72,
        0x43, 0x50, 0xc0, 0x93, 0xa3, 0x10, 0x30, 0x0e, 0x30, 0x0c, 0x06, 0x03,
        0x55, 0x1d, 0x13, 0x01, 0x01, 0xff, 0x04, 0x02, 0x30, 0x00, 0x30, 0x0a,
        0x06, 0x08, 0x2a, 0x86, 0x48, 0xce, 0x3d, 0x04, 0x03, 0x02, 0x03, 0x48,
        0x00, 0x30, 0x45, 0x02, 0x21, 0x00, 0xc9, 0x49, 0xef, 0x25, 0x1b, 0xca,
        0x80, 0x89, 0xf2, 0x5b, 0xff, 0x4f, 0xc3, 0xf4, 0xee, 0x5e, 0x5d, 0xbc,
        0x0e, 0x73, 0x77, 0x8b, 0x0b, 0x4b, 0x6b, 0xb0, 0x5a, 0x80, 0x96, 0x16,
        0xb4, 0x4d, 0x02, 0x20, 0x14, 0x76, 0x90, 0xbe, 0x96, 0xc3, 0x70, 0x41,
        0x4b, 0x8f, 0xe3, 0xfc, 0x73, 0x97, 0x44, 0x98, 0xe9, 0x58, 0x29, 0x27,
        0x19, 0x85, 0xd7, 0xe8, 0x28, 0xdb, 0x16, 0x79, 0x11, 0x98, 0xc5, 0x7a};

    NRF_CRYPTO_ECC_PUBLIC_KEY_CREATE(my_public, SECP256R1);

    bool res = certificate_valid(raw, sizeof(raw), &my_public);
    NRF_LOG_INFO("Cert user validity test: %d", res);
}

#define NB_MSG 8

int merkle_tree_test_routine() {
    message_t p_messages1[NB_MSG] = {
        //  header, payload, signature
        {{160, 19, 21, 0}, 0, {0}, {10}}, {{512, 21, 12, 0}, 0, {1}, {11}},
        {{100, 41, 72, 0}, 0, {2}, {12}}, {{189, 41, 21, 0}, 0, {3}, {13}},
        {{124, 72, 19, 0}, 0, {4}, {14}}, {{415, 19, 72, 0}, 0, {5}, {15}},
        {{378, 1, 41, 0}, 0, {6}, {16}},  {{101, 1, 41, 0}, 0, {7}, {17}}};
    message_pool_t pool1 = {p_messages1, NB_MSG};

    message_t p_messages2[NB_MSG] = {
        //  header, payload, signature
        {{382, 19, 21, 0}, 0, {26}, {10}}, {{388, 21, 12, 0}, 0, {1}, {11}},
        {{301, 41, 72, 0}, 0, {2}, {12}},  {{339, 41, 21, 0}, 0, {3}, {13}},
        {{378, 72, 19, 0}, 0, {4}, {14}},  {{398, 19, 72, 0}, 0, {5}, {15}},
        {{324, 1, 41, 0}, 0, {6}, {16}},   {{110, 1, 41, 0}, 0, {7}, {17}}};
    message_pool_t pool2 = {p_messages2, NB_MSG};

    message_t p_messages3[NB_MSG] = {
        //  header, payload, signature
        {{382, 19, 21, 0}, 0, {0}, {10}}, {{382, 21, 12, 0}, 0, {1}, {11}},
        {{381, 41, 72, 0}, 0, {2}, {12}}, {{383, 41, 21, 0}, 0, {3}, {13}},
        {{381, 72, 19, 0}, 0, {4}, {14}}, {{381, 19, 72, 0}, 0, {9}, {15}},
        {{381, 1, 41, 0}, 0, {6}, {16}},  {{225, 1, 41, 0}, 0, {7}, {17}}};
    message_pool_t pool3 = {p_messages3, NB_MSG};

    message_t p_messages4[NB_MSG] = {
        //  header, payload, signature
        {{381, 19, 21, 0}, 0, {52}, {10}}, {{381, 21, 12, 0}, 0, {50}, {11}},
        {{381, 41, 72, 0}, 0, {40}, {12}}, {{381, 41, 21, 0}, 0, {45}, {13}},
        {{381, 72, 19, 0}, 0, {29}, {14}}, {{381, 19, 72, 0}, 0, {5}, {15}},
        {{382, 1, 41, 0}, 0, {6}, {16}},   {{382, 1, 41, 0}, 0, {92}, {17}}};
    message_pool_t pool4 = {p_messages4, NB_MSG};

    err_t retval = LB_OK;
    (void)retval;

    LBLOG("HELLO");

    LBLOG("Before build_merkle_tree()\n");
    LB_ALLOC_ROOT_MD(root_md);
    LB_INIT_ROOT_MD(root_md);
    LB_ALLOC_MERKLE_TREE(root, slot_pool);
    LB_INIT_MERKLE_TREE(root);
    retval = lb_init_merkle_tree(&root, &root_md, &slot_pool, LB_MT_OFFSET);

    if (retval != LB_OK)
        LBLOG("An error occured : %i\n", retval);

    LBLOG("BEGIN : Control timestamp nodes from main\n");
    lb_merkle_tree *tmp1, *tmp2;
    for (unsigned int i = 0; i < NB_MSG; i++) {
        tmp1 =
            lb_get_leaf_for_timestamp(&root, p_messages1[i].header.timestamp);
        tmp2 = lb_get_sub_tree_root(tmp1);
        LBLOG("timestamp %u sits into node %u, from root child "
              "going from %u to %u \n",
              p_messages1[i].header.timestamp, tmp1->array_id, LU(tmp2->start),
              LU(tmp2->end));
    }
    LBLOG("END : Control timestamp nodes from main\n");

    retval = lb_attach_msgs_to_tree(&root, &pool1, &slot_pool, NULL, NULL);
    if (retval != LB_OK) {
        LBLOG("Could not attach msg to tree\n");
        return LB_ERR;
    }
    LBLOG("###############################################\n");
    LBLOG("###############################################\n");

    ssize_t i_slot = slot_pool.i_begin_busy;
    if (i_slot == -1) {
        LBLOG("ERROR, i_begin_slot is -1\n");
        return LB_ERR;
    }

    for (unsigned int i = 0; i < LB_NB_MAX_MSGS; i++) {
        lb_slot *p_slot = &slot_pool.slots[i_slot];
        LBLOG("timestamp %d, message address = %d\n", LU(p_slot->timestamp),
              p_slot->addr);
        if (p_slot->i_next == -1)
            break;
        i_slot = p_slot->i_next;
    }

    LBLOG("###############################################\n");
    LBLOG("###############################################\n");
    // Second pass
    retval = lb_attach_msgs_to_tree(&root, &pool2, &slot_pool, NULL, NULL);
    if (retval != LB_OK) {
        LBLOG("Could not attach msg to tree\n");
        return LB_ERR;
    }

    // Third pass
    retval = lb_attach_msgs_to_tree(&root, &pool3, &slot_pool, NULL, NULL);
    if (retval != LB_OK) {
        LBLOG("Could not attach msg to tree\n");
        return LB_ERR;
    }

    // Fourth pass
    retval = lb_attach_msgs_to_tree(&root, &pool4, &slot_pool, NULL, NULL);
    if (retval != LB_OK) {
        LBLOG("Could not attach msg to tree\n");
        return LB_ERR;
    }

    i_slot = slot_pool.i_begin_busy;
    if (i_slot == -1) {
        LBLOG("ERROR, i_begin_slot is -1\n");
        return LB_ERR;
    }

    for (unsigned int i = 0; i < LB_NB_MAX_MSGS; i++) {
        lb_slot *p_slot = &slot_pool.slots[i_slot];
        LBLOG("timestamp %d, message address = %d\n", LU(p_slot->timestamp),
              p_slot->addr);
        if (p_slot->i_next == -1)
            break;
        i_slot = p_slot->i_next;
    }
    return LB_OK;
}
#define DUMMY_MSG_SIZE MESSAGE_SIGNATURE_SIZE + MESSAGE_HEADER_SIZE + 1 + 2
#define NB_TESTS 5
void extflash_test_routine(lb_spi_ctx *ctx) {

    static message_t msg = {
        {0x01020364, 0xFFEEAADD, 0xDDEEAADD, 0x5678},
        MESSAGE_HEADER_SIZE + MESSAGE_SIGNATURE_SIZE + 16 + 32 + 2,
        {2, 4, 6, 8, 6, 6, 6, 2, 4, 6, 8, 1, 1, 6, 0, 0,
         8, 8, 8, 8, 1, 2, 3, 4, 8, 8, 8, 8, 8, 8, 8, 8,
         8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 1},
        {5, 7, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
         2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
         2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 9, 9, 9, 2, 6, 7, 3}};

    message_t p_messages1[NB_MSG] = {

        //  header, payload, signature
        {{160, 19, 21, 0}, DUMMY_MSG_SIZE, {0}, {0}},
        {{512, 21, 12, 0}, DUMMY_MSG_SIZE, {1}, {1}},
        {{100, 41, 72, 0}, DUMMY_MSG_SIZE, {2}, {2}},
        {{189, 41, 21, 0}, DUMMY_MSG_SIZE, {3}, {3}},
        {{124, 72, 19, 0}, DUMMY_MSG_SIZE, {4}, {4}},
        {{415, 19, 72, 0}, DUMMY_MSG_SIZE, {5}, {5}},
        {{378, 1, 41, 0}, DUMMY_MSG_SIZE, {6}, {6}},
        {{101, 1, 41, 0}, DUMMY_MSG_SIZE, {7}, {7}}};
    message_pool_t pool1 = {p_messages1, NB_MSG};
    (void)pool1;
    lb_task_stack st;
    (void)st;
    LB_ALLOC_ROOT_MD(root_md);
    LB_INIT_ROOT_MD(root_md);
    LB_ALLOC_MERKLE_TREE(root, slot_pool);
    LB_INIT_MERKLE_TREE(root);
    (void)root;
    (void)slot_pool;
    (void)root_md;
    err_t retval;
    retval = lb_init_merkle_tree(&root, &root_md, &slot_pool, LB_MT_OFFSET);
    retval = store_msg(ctx, 128, &p_messages1[0]);
    LBLOG("TEST msg has been written\n");
    retval = lb_attach_msgs_to_tree(&root, &pool1, &slot_pool, store_msg, ctx);
    (void)retval;

    int page = 226;
    for (int i = 0; i < NB_TESTS; i++) {
        retval = store_msg(ctx, page, &msg);
        if (retval != LB_OK) {
            if (retval == LB_MASS_STORAGE_PAGE_ERROR) {
                LBLOG("Page : %d : PAGE WRITE ERROR\n", page);
            } else {
                LBLOG("Page : %d : Could not store msg\n", page);
            }
        } else {
            LBLOG("Page : %d : DONE WRITING\n", page);
        }
        page++;
    }

    page = 229;
    retval = extflash_page_erase(ctx, page);
    if (retval != LB_OK) {
        if (retval == LB_MASS_STORAGE_PAGE_ERROR) {
            LBLOG("Page : %d : PAGE ERASE ERROR\n", page);
        } else {
            LBLOG("Page : %d : Could not erase msg\n", page);
        }
    } else {
        LBLOG("Page : %d : DONE ERASING\n", page);
    }

    page = 226;
    static message_t msgs[NB_TESTS];
    for (int i = 0; i < NB_TESTS; i++) {
        retval = load_msg(ctx, page, &msgs[i]);
        if (retval != LB_OK) {
            LBLOG("Page : %d : Could not load msg -> %d\n", page, retval);
        } else {
            LBLOG("Page : %d : DONE READING\n", page);
        }
        page++;
    }
}

void printf_slots_from_flash(lb_spi_ctx *ctx, lb_slot_pool *p_pool) {
    int counter = 0;
    message_t msg;
    ssize_t i_slot = p_pool->i_begin_busy;
    err_t retval = LB_OK;
    LBLOG(">>> In printf_slots_from_flash\n");
    while (i_slot != -1) {
        retval = load_msg(ctx, p_pool->slots[i_slot].addr, &msg);
        if (retval != LB_OK) {
            LBLOG("Could not read msg, abort.\n");
            return;
        }
        LBLOG("%2d : timestamp %4u, addr = %4u, sign = %4u", counter,
              LU(msg.header.timestamp), p_pool->slots[i_slot].addr,
              msg.signature[0]);
#ifdef DUMP
        for (int k = 0; k < MESSAGE_SIGNATURE_SIZE; k++) {
            LBLOG(" %02x", msg.signature[k]);
        }

        LBLOG(" )\n");
#endif
        counter++;
        i_slot = p_pool->slots[i_slot].i_next;
    }
}

void msg_in_flash(lb_spi_ctx *ctx) {
    LBLOG("In msg_in_flash\n");
    message_t p_messages1[NB_MSG] = {
        //  header, payload, signature
        {{160, 19, 21, 0}, DUMMY_MSG_SIZE, {0}, {0}},
        {{512, 21, 12, 0}, DUMMY_MSG_SIZE, {1}, {1}},
        {{100, 41, 72, 0}, DUMMY_MSG_SIZE, {2}, {2}},
        {{189, 41, 21, 0}, DUMMY_MSG_SIZE, {3}, {3}},
        {{124, 72, 19, 0}, DUMMY_MSG_SIZE, {4}, {4}},
        {{415, 19, 72, 0}, DUMMY_MSG_SIZE, {5}, {5}},
        {{378, 1, 41, 0}, DUMMY_MSG_SIZE, {6}, {6}},
        {{101, 1, 41, 0}, DUMMY_MSG_SIZE, {7}, {7}}};
    message_pool_t pool1 = {p_messages1, NB_MSG};
    (void)pool1;
    lb_task_stack st;
    (void)st;
    LB_ALLOC_ROOT_MD(root_md);
    LB_INIT_ROOT_MD(root_md);
    LB_ALLOC_MERKLE_TREE(root, slot_pool);
    LB_INIT_MERKLE_TREE(root);
    err_t retval =
        lb_init_merkle_tree(&root, &root_md, &slot_pool, LB_MT_OFFSET);
    extflash_print_status(ctx);

    retval = lb_attach_msgs_to_tree(&root, &pool1, &slot_pool, store_msg, ctx);
    (void)retval;
    LBLOG("Attached msgs to tree\n");
    printf_slots_from_flash(ctx, &slot_pool);
    LBLOG("Shifting...\n");
    lb_shift_tree(&root, &slot_pool, slot_pool.certif_date + 102,
                  lb_store_slot_pool, ctx);
    LBLOG("Shifted !\n");
    printf_slots_from_flash(ctx, &slot_pool);
}

#define MSG_POOL_SIZE 16
void merge_test(lb_spi_ctx *ctx) {
    message_t messagesG[MSG_POOL_SIZE];
    message_pool_t pool1 = {messagesG, MSG_POOL_SIZE};

    LB_ALLOC_ROOT_MD(root_md);
    LB_INIT_ROOT_MD(root_md);

    lb_task_stack st;
    (void)st;

    for (unsigned int i = 0; i < MSG_POOL_SIZE; i++) {
        message_t message = {
            {LB_MT_START + i, 0, 0, 0}, DUMMY_MSG_SIZE, {0}, {(uint8_t)i}};
        memcpy(message.signature, &i, sizeof(i));
        messagesG[i] = message;
    }
    (void)pool1;

    LB_ALLOC_MERKLE_TREE(root1, slot_pool1);
    LB_INIT_MERKLE_TREE(root1);
    lb_init_merkle_tree(&root1, &root_md, &slot_pool1, LB_MT_OFFSET);
    lb_attach_msgs_to_tree(&root1, &pool1, &slot_pool1, store_msg, ctx);

    char counter = 0;
    ssize_t i_slot = slot_pool1.i_begin_busy;
    uint32_t timestamps[LB_NB_MAX_MSGS];
    int i = 0;
    while (i_slot != -1) {
        if (counter % 2 == 0) {
            LBLOG("%d : timestamp %u marked not shared\n", i,
                  slot_pool1.slots[i_slot].timestamp);
            timestamps[i++] = slot_pool1.slots[i_slot].timestamp;
            slot_pool1.slots[i_slot].shared = 0;
        }
        counter++;
        i_slot = slot_pool1.slots[i_slot].i_next;
    }
    uint8_t buff[MESSAGE_MAX_SIZE];
    size_t written = 1;
    i = 0;
    message_t msg;
    while (written != 0) {
        lb_next_msg_to_send(load_msg, ctx, &slot_pool1, buff, &written);
        if (written == 0)
            continue;
        message_from_raw(buff, &msg);
        LBLOG("%d : Got msg timestamp %d\n", i, msg.header.timestamp);
        if (msg.header.timestamp != timestamps[i]) {
            LBLOG("ERROR MERGE\n");
        }
        i++;
    }
}

void slot_bckp(lb_spi_ctx *ctx) {
    (void)ctx;
    lb_slot src_slot = {
        -1, 45, 99888, 129, {0xDE, 0xAD, 0xBE, 0xEF, 0xCA, 0xFE}};
    uint8_t buff[LB_SLOT_SIZE_BYTE];

    lb_slot dest_slot;
    LBLOG("prev = %5d, next = %5d, ts = %5u, addr = %5u \n",
          src_slot.i_previous, src_slot.i_next, src_slot.timestamp,
          src_slot.addr);
    for (int i = 0; i < LB_SLOT_SIGNATURE_SIZE; i++) {
        LBLOG("%02X ", src_slot.p_hash[i]);
    }

    lb_slot_to_raw(buff, &src_slot);
    lb_slot_from_raw(buff, &dest_slot);
    LBLOG("prev = %5d, next = %5d, ts = %5u, addr = %5u\n",
          dest_slot.i_previous, dest_slot.i_next, dest_slot.timestamp,
          dest_slot.addr);
    for (int i = 0; i < LB_SLOT_SIGNATURE_SIZE; i++) {
        LBLOG("%02X ", dest_slot.p_hash[i]);
    }
}

void pool_bckp(lb_spi_ctx *ctx) {
    (void)ctx;
    lb_slot_pool pool = {
        {
            {3, 7, 2, 129, {0xDE, 0xAD, 0xBE, 0xEF, 0xCA, 0xFE}},  // 0
            {7, 2, 4, 129, {0xDE, 0xAD, 0xBE, 0xEF, 0xCA, 0xFE}},  // 1
            {1, 4, 5, 130, {0xDE, 0xAD, 0xBE, 0xEF, 0xCA, 0xFE}},  // 2
            {-1, 0, 1, 131, {0xDE, 0xAD, 0xBE, 0xEF, 0xCA, 0xFE}}, // 3
            {2, 6, 6, 132, {0xDE, 0xAD, 0xBE, 0xEF, 0xCA, 0xFE}},  // 4
            {6, -1, 8, 133, {0xDE, 0xAD, 0xBE, 0xEF, 0xCA, 0xFE}}, // 5
            {4, 5, 7, 134, {0xDE, 0xAD, 0xBE, 0xEF, 0xCA, 0xFE}},  // 6
            {0, 1, 3, 135, {0xDE, 0xAD, 0xBE, 0xEF, 0xCA, 0xFE}}   // 7
        },
        3,
        8,
        LB_NB_MAX_MSGS - 8,
        LB_MT_OFFSET};

    char counter = 0;
    ssize_t i_slot = pool.i_begin_busy;
    while (i_slot != -1) {
        lb_slot *slot = &pool.slots[i_slot];
        LBLOG("prev = %2d, next = %2d, ts = %2u, addr = %3u\n",
              slot->i_previous, slot->i_next, slot->timestamp, slot->addr);
        counter++;
        i_slot = pool.slots[i_slot].i_next;
    }
    LBLOG("bb = %2d, bf = %2d, nbfree = %3d, cd = %6d\n", pool.i_begin_busy,
          pool.i_begin_free, pool.nb_free, pool.certif_date);

    LBLOG(" max_spi_pyld = %d, slot_size = %d, nb_slot_per_spi = %d\n",
          MAX_SPI_PAYLOAD, LB_SLOT_SIZE_BYTE,
          (MAX_SPI_PAYLOAD / LB_SLOT_SIZE_BYTE));
    lb_slot_pool pool2;

    err_t retval;
    retval = lb_store_slot_pool(&pool, ctx);
    if (retval != LB_OK) {
        LBLOG("ERROR lb_store_slot_pool\n");
        return;
    }
    LBLOG("store succeed !\n");
    retval = lb_load_slot_pool(&pool2, ctx);
    if (retval != LB_OK) {
        LBLOG("ERROR lb_load_slot_pool\n");
        return;
    }

    counter = 0;
    i_slot = pool2.i_begin_busy;
    while (i_slot != -1) {
        lb_slot *slot = &pool2.slots[i_slot];
        LBLOG("prev = %2d, next = %2d, ts = %2u, addr = %3u\n",
              slot->i_previous, slot->i_next, slot->timestamp, slot->addr);
        counter++;
        i_slot = pool.slots[i_slot].i_next;
    }
    LBLOG("bb = %2d, bf = %2d, nbfree = %3d, cd = %6d\n", pool2.i_begin_busy,
          pool2.i_begin_free, pool2.nb_free, pool2.certif_date);
}

void pool_store(lb_spi_ctx *ctx) {
    LBLOG("In pool_store\n");
    message_t p_messages1[NB_MSG] = {
        //  header, payload, signature
        {{160, 19, 21, 0}, DUMMY_MSG_SIZE, {0}, {0}},
        {{512, 21, 12, 0}, DUMMY_MSG_SIZE, {1}, {1}},
        {{100, 41, 72, 0}, DUMMY_MSG_SIZE, {2}, {2}},
        {{189, 41, 21, 0}, DUMMY_MSG_SIZE, {3}, {3}},
        {{124, 72, 19, 0}, DUMMY_MSG_SIZE, {4}, {4}},
        {{415, 19, 72, 0}, DUMMY_MSG_SIZE, {5}, {5}},
        {{378, 1, 41, 0}, DUMMY_MSG_SIZE, {6}, {6}},
        {{101, 1, 41, 0}, DUMMY_MSG_SIZE, {7}, {7}}};
    message_pool_t pool1 = {p_messages1, NB_MSG};
    (void)pool1;
    lb_task_stack st;
    (void)st;
    LB_ALLOC_ROOT_MD(root_md);
    LB_INIT_ROOT_MD(root_md);
    LB_ALLOC_MERKLE_TREE(root, slot_pool);
    LB_INIT_MERKLE_TREE(root);
    err_t retval =
        lb_init_merkle_tree(&root, &root_md, &slot_pool, LB_MT_OFFSET);
    retval = lb_attach_msgs_to_tree(&root, &pool1, &slot_pool, store_msg, ctx);

    retval = lb_store_slot_pool(&slot_pool, ctx);
    if (retval != LB_OK) {
        LBLOG("ERROR lb_store_slot_pool\n");
        return;
    }
    LBLOG("store succeed !\n");

    (void)retval;
}

void pool_wake(lb_spi_ctx *ctx) {
    LB_ALLOC_ROOT_MD(root_md);
    LB_INIT_ROOT_MD(root_md);
    LB_ALLOC_MERKLE_TREE_ONLY(root);
    LB_INIT_MERKLE_TREE(root);
    err_t retval = lb_init_merkle_tree_only(&root, &root_md);
    LB_ALLOC_SLOT_POOL(slot_pool);
    retval = lb_load_slot_pool(&slot_pool, ctx);
    if (retval != LB_OK) {
        LBLOG("ERROR lb_load_slot_pool\n");
        return;
    }

    char counter = 0;
    ssize_t i_slot = slot_pool.i_begin_busy;

    while (i_slot != -1) {
        lb_slot *slot = &slot_pool.slots[i_slot];
        LBLOG("prev = %2d, next = %2d, ts = %2u, addr = %3u\n",
              slot->i_previous, slot->i_next, slot->timestamp, slot->addr);
        counter++;
        i_slot = slot_pool.slots[i_slot].i_next;
    }
    LBLOG("bb = %2d, bf = %2d, nbfree = %3d, cd = %6d\n",
          slot_pool.i_begin_busy, slot_pool.i_begin_free, slot_pool.nb_free,
          slot_pool.certif_date);
    lb_attach_leafs_to_slots(&root, &slot_pool);
    lb_merkle_tree *fleaf = lb_get_leaf_for_timestamp(&root, 100);
    if (fleaf == NULL)
        return;
    if (fleaf->i_bucket_slot == -1) {
        LBLOG("No msg attagched to 100's leaf\n");
    } else {
        LBLOG("100's leaf has a msg attached. timestamp = %d\n",
              slot_pool.slots[fleaf->i_bucket_slot].timestamp);
    }
}

volatile int diff_clear = 0;

void test_diff() {
    message_t p_messages1[8] = {//  header, payload, signature
                                {{1, 41, 72, 0}, DUMMY_MSG_SIZE, {2}, {0x01}},
                                {{2, 41, 72, 0}, DUMMY_MSG_SIZE, {2}, {0x02}},
                                {{3, 41, 72, 0}, DUMMY_MSG_SIZE, {2}, {0x03}},
                                {{4, 10, 41, 0}, DUMMY_MSG_SIZE, {7}, {0x04}},
                                {{5, 10, 41, 0}, DUMMY_MSG_SIZE, {7}, {0x05}},
                                {{6, 10, 41, 0}, DUMMY_MSG_SIZE, {7}, {0x06}},
                                {{7, 19, 21, 0}, DUMMY_MSG_SIZE, {0}, {0x07}},
                                {{8, 19, 21, 0}, DUMMY_MSG_SIZE, {0}, {0x08}}};
    message_pool_t pool1 = {p_messages1, 8};

    LB_ALLOC_ROOT_MD(root_md);
    LB_INIT_ROOT_MD(root_md);
    LB_ALLOC_MERKLE_TREE(root, pool);
    LB_INIT_MERKLE_TREE(root);
    lb_task_stack stack;
    lb_init_merkle_tree(&root, &root_md, &pool, LB_MT_OFFSET);
    lb_attach_msgs_to_tree(&root, &pool1, &pool, NULL, NULL);
    lb_stack_init(&stack, &pool);

    LBLOG("DIFF STARTING");
    LBLOG("Root hash: %d", *root.p_hash);
    lb_diff(&stack, &root, 0, lb_send, NULL, lb_recv, NULL);
}
