#ifndef SERVICES_H
#define SERVICES_H

#include "authentication_service.h"
#include "ble.h"
#include "diff_service.h"
#include "message.h"
#include "utils.h"

void services_init(void);

extern ble_auths_t m_auths;
extern ble_diffs_t m_diff;

#endif /* SERVICES_H */
