#ifndef DROP_H
#define DROP_H

#include <stdint.h>

#define LB_ID 0x47414358

typedef struct {
    uint8_t private_key[0x20];
    uint8_t ca_public_key[0x40];
    uint16_t company_id;
    uint16_t network_id;
    uint16_t id;
} DropInfo;

extern DropInfo drop_info;

uint32_t get_lb_id();

uint16_t get_company_id();

uint16_t get_network_id();

uint16_t get_drop_id();

#endif // DROP_H
