#ifndef DFU_H
#define DFU_H

#include "ble_dfu.h"

void ble_dfu_evt_handler(ble_dfu_buttonless_evt_type_t event);

#endif
