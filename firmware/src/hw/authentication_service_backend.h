#ifndef AUTHENTICATION_SERVICE_BACKEND_H
#define AUTHENTICATION_SERVICE_BACKEND_H

#include "authentication_service.h"
#include <stdbool.h>
#include <stdint.h>

void authentication_service_backend_init(ble_auths_t *running_auth_service);

void auth_service_generate_challenge(void);
bool auth_service_verify_response(void);
void auth_service_generate_response(void);
bool auth_service_verify_certificate(void);
bool auth_service_read_time_certificate(uint32_t *time_reference);

uint8_t generate_random(uint8_t *buffer, uint8_t length);
bool test_write_response(nrf_value_length_t *signature);
bool test_write_cert(nrf_value_length_t *cert);

#endif /* AUTHENTICATION_SERVICE_BACKEND_H */
