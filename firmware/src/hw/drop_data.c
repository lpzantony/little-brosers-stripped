#include "drop_data.h"

DropInfo drop_info __attribute__((section(".drop_data"))) __attribute__((used));

uint32_t get_lb_id() { return LB_ID; }

uint16_t get_company_id() { return drop_info.company_id; }

uint16_t get_network_id() { return drop_info.network_id; }

uint16_t get_drop_id() { return drop_info.id; }
