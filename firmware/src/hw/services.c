#include "services.h"
#include "authentication_service.h"
#include "ble_dfu.h"
#include "dfu.h"
#include <stdint.h>

/* YOUR_JOB: Declare all services structure your application is using
 *  BLE_XYZ_DEF(m_xyz);
 */

BLE_AUTHS_DEF(m_auths);
BLE_DIFFS_DEF(m_diff);

/**@brief Function for initializing services that will be used by the
 * application.
 */
void services_init() {
    ret_code_t err_code;

    // Init the DFU service
    ble_dfu_buttonless_init_t dfus_init = {.evt_handler = ble_dfu_evt_handler};

    // Initialize the async SVCI interface to bootloader.
    err_code = ble_dfu_buttonless_async_svci_init();
    APP_ERROR_CHECK(err_code);

    err_code = ble_dfu_buttonless_init(&dfus_init);
    APP_ERROR_CHECK(err_code);

    auth_service_init(&m_auths);
    diff_service_init(&m_diff);
}
