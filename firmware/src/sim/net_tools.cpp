#include <net_tools.h>

#include <arpa/inet.h> // for inet_ntoa
#include <cstring>     //for strncpy
#include <fcntl.h>     //set blocking
#include <net/if.h>    // for struct ifreq
#include <netdb.h>     //gethostbyname
#include <sys/statvfs.h>
#include <sys/wait.h> // wait()
#include <unistd.h>   // for close

bool NetTools::validateIpAddress(const std::string &ipAddress) {
    struct sockaddr_in sa;
    int result = inet_pton(AF_INET, ipAddress.c_str(), &(sa.sin_addr));
    return result != 0;
}

int NetTools::getInterfaceProperty(int flag, std::string &buf) {
    int fd;
    struct ifreq ifr;
    char iface[] = "eth0";

    fd = socket(AF_INET, SOCK_DGRAM, 0);
    if (fd == ERR) {
        buf.clear();
        return ERR;
    }

    // Type of address to retrieve - IPv4 IP address
    ifr.ifr_addr.sa_family = AF_INET;

    // Copy the interface name in the ifreq structure
    strncpy(ifr.ifr_name, iface, IFNAMSIZ - 1);

    ioctl(fd, flag, &ifr);

    close(fd);

    // save result
    buf =
        std::string(inet_ntoa(((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr));
    return 0;
}

bool NetTools::isFSReadOnly() {
    struct statvfs buffer;
    int ret = statvfs("/", &buffer);
    if (ret == -1)
        return ERR;
    return (buffer.f_flag & ST_RDONLY);
}

std::string NetTools::exec(char *cmd) {
    FILE *pipe;
    pipe = popen(cmd, "r");
    if (!pipe)
        return "ERROR";
    char buffer[128];
    std::string result = "";
    while (!feof(pipe)) {
        if (fgets(buffer, 128, pipe) != NULL)
            result += buffer;
    }
    pclose(pipe);
    return result;
}

int NetTools::setReuseAddr(int fd) {
    // allowing reusing bind
    int yes = 1;
    if (setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, (char *)&yes, sizeof(yes)) ==
        -1) {
        return ERR;
    }
    return 0;
}

int NetTools::setFdBlocking(int fd, int blocking) {
    int flags = fcntl(fd, F_GETFL, 0); // R�cup�re le flag du file descriptor
    if (flags == -1)                   // Si echec
        return 0;                      // return echec (0)
    if (blocking)                      // Si demande pour bloquant
        flags &= ~O_NONBLOCK;          // Passage en bloquant
    else                               // Sinon si d�bloquant demand�
        flags |= O_NONBLOCK;           // Passage en d�bloquant
    return fcntl(fd, F_SETFL, flags) !=
           -1; // renvoi la r�ussite (1) ou l'echec (0) du changement
}

int NetTools::setRcvTimeout(int fd, long timeout_s, long timeout_us) {
    struct timeval timeout_t;
    timeout_t.tv_sec = timeout_s;
    timeout_t.tv_usec = timeout_us;
    if (setsockopt(fd, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout_t,
                   sizeof(timeout_t)) < 0) {
        perror("setsockopt failed");
        return ERR;
    }
    return 0;
}

// for INET4 sockets
int NetTools::setAddress(struct sockaddr_in &addr, const std::string &host,
                         int port) {
    struct hostent *hostinfo = NULL;
    // gethostbyname() is obsolete!
    if (host.empty() || !(hostinfo = ::gethostbyname(host.c_str())))
        return -1; // host not found
    ::memset(&addr, 0, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    // creer l'adresse du remote host a partir de son nom
    ::memcpy((void *)&addr.sin_addr, hostinfo->h_addr_list[0],
             hostinfo->h_length);
    return 0;
}
