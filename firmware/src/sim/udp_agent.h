
#ifndef UDP_AGENT_H_
#define UDP_AGENT_H_
#include <iostream>

#include "event_speaker.h"
#include "udp_exception.h"

#include <errno.h>
#include <mutex>
#include <queue>
#include <sys/socket.h>
#include <thread>
#include <vector>

#include <chrono> // std::chrono::seconds
#include <functional>
#include <string.h>
#include <string>

#include <unistd.h>

#include <arpa/inet.h>

#define INVALID_SOCKET -1
#define SOCKET_ERROR -1
typedef int SOCKET;
typedef struct sockaddr_in SOCKADDR_IN;
typedef struct sockaddr SOCKADDR;
typedef struct in_addr IN_ADDR;
#define CRLF "\r\n"

using UdpSpeaker = EventSpeaker<std::vector<uint8_t>>;
using UdpListener = EventListener<std::vector<uint8_t>>;

class UdpAgent : public UdpSpeaker {
  public:
    //========== DEBUG
    const bool debug = false;

    static const unsigned short DEFAULT_PORT = 54000;
    static const uint32_t BUF_SIZE = 210000;
    static const uint32_t UDP_MEM_MAX =
        35000; // 35724;		// /proc/sys/net/ipv4/udp_mem
    static const uint32_t SIZE_BEFORE_TEMPO = 50000;

    UdpAgent(uint32_t srcPort, std::string ipAddr, uint32_t destPort) {
        setRunning(false);
        // asking for a socket
        listener = socket(AF_INET, SOCK_DGRAM, 0);
        if (listener == INVALID_SOCKET) {
            perror("socket()");
            exit(errno);
        }
        // timeout
        struct timeval tv;
        tv.tv_sec = 2;  /* 2 Secs Timeout */
        tv.tv_usec = 0; // Not init'ing this can cause strange errors
        setsockopt(listener, SOL_SOCKET, SO_RCVTIMEO, (char *)&tv,
                   sizeof(struct timeval));

        srcinfo.sin_addr.s_addr = htonl(INADDR_ANY);
        srcinfo.sin_family = AF_INET;
        // store given srcport in srcinfo:
        srcinfo.sin_port = htons(srcPort);

        destinfo.sin_addr.s_addr = inet_addr(ipAddr.c_str());
        // destinfo.sin_addr.s_addr =  inet_addr("192.168.1.49");
        destinfo.sin_family = AF_INET;
        destinfo.sin_port = htons(destPort);
        // destinfo.sin_port = htons(25565);
    }

    virtual ~UdpAgent() {}

    //***** Function to call after creating UdpAgent object
    void start() {
        auto status = bind(listener, (SOCKADDR *)&srcinfo, sizeof srcinfo);
        if (status != 0) {
            throw UDPException(status);
        }
        setRunning(true);
        the_thread = std::thread(&UdpAgent::AgentRoutine, this);
    }

    //***** Function to call in order to stop the thread and destroy the socket
    void stop() {
        close(listener);
        setRunning(false);
        the_thread.join();
    }

    bool send(std::vector<uint8_t> data) {
        if (getDestPort() == 0)
            return false;
        socklen_t destinfosize = sizeof destinfo;
        if (data.size() >= BUF_SIZE)
            return false; // string too long for default buffer size

        static char buf[BUF_SIZE] = "";
        memcpy(buf, data.data(), data.size());

        int ret = sendto(listener, buf, data.size(), 0, (sockaddr *)&destinfo,
                         destinfosize);
        switch (ret) {
        case -1:
            std::cout << "ERROR : data not sent!" << std::endl;
            perror("UDP: ");
            return false;
        default:
            return true;
        }
    }

    void lockMutex() { the_mutex.lock(); }
    void unlockMutex() { the_mutex.unlock(); }

    bool sendChar(unsigned char *c, int32_t size) {
        if (getDestPort() == 0)
            return false;
        socklen_t destinfosize = sizeof destinfo;

        int ret =
            sendto(listener, c, size, 0, (sockaddr *)&destinfo, destinfosize);
        if (ret == -1)
            return false;
        return true;
    }

    bool sendShort(short *value) {
        if (getDestPort() == 0)
            return false;
        socklen_t destinfosize = sizeof destinfo;

        int ret = sendto(listener, value, sizeof(value), 0,
                         (sockaddr *)&destinfo, destinfosize);
        if (ret == -1)
            return false;
        return true;
    }

    std::string getDestAddr() const { return inet_ntoa(destinfo.sin_addr); }

    unsigned short int getSrcPort() const { return ntohs(srcinfo.sin_port); }
    unsigned short int getDestPort() const { return ntohs(destinfo.sin_port); }

  private:
    SOCKADDR_IN destinfo, srcinfo;
    SOCKET listener;
    std::thread the_thread;
    std::mutex the_mutex;
    bool running;
    std::queue<vector<std::string>> net_fifo;

    void agentPrintLn(std::string str) {
        std::cout << "UdpAgent : " << str << std::endl;
    }

    void agentPrint(std::string str) { std::cout << "UdpAgent : " << str; }

    void setRunning(bool run) {
        lockMutex();
        running = run;
        unlockMutex();
    }

    bool getRunning() {
        bool tmp;
        lockMutex();
        tmp = running;
        unlockMutex();
        return tmp;
    }

    //-----------------------------------------------------
    // UdpAgent Functions
    //-----------------------------------------------------

    //***** The function runned by the_thread
    void AgentRoutine() {
        uint8_t buf[BUF_SIZE] = "";
        int ret = -1;
        socklen_t destinfosize = sizeof destinfo;

        while (getRunning()) {
            buf[0] = 0;
            // Below we set a timeout for recv in order to be abble to catch
            // SIGINT signal
            struct timeval tv;
            tv.tv_sec = 0;
            tv.tv_usec = 100000;
            if (setsockopt(listener, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv)) <
                0) {
                perror("Error");
            }
            ret = recvfrom(listener, buf, sizeof(buf), 0, (sockaddr *)&destinfo,
                           &destinfosize);
            if (ret == -1) {
                switch (errno) {
                case EAGAIN:
                case EINTR:
                    continue;
                default:
                    // if(debug)
                    agentPrintLn("Socket receiving error!");
                    perror("UDP: recvfrom() : ");
                    return;
                }
            } else {
                std::vector<uint8_t> absbuf;
                absbuf.assign(buf, buf + ret);
                notifyObservers(0, absbuf);
            }
        }
    }
};

#endif /* UDP_AGENT_H_ */
