#ifndef EVENT_LISTENER_H
#define EVENT_LISTENER_H

#include <mutex>
#include <queue> // std::queue
#include <string>

// This class as well as the EventSpeaker class are based
// on a tutorial available at this URL:
// http://www.dreamincode.net/forums/topic/197421-the-listener-pattern/

// MAYBE_UNUSED Allows a function argument not to be used. it is usefull when
// -Wextra gcc flag is used
#define MAYBE_UNUSED __attribute__((unused))

/*! \class EventListener
    \brief Class intended to be notified to any EventSpeaker instance's event.

    This abstract class forces its children classes to implement the notify()
   function. An EventListener should add itself to a EventSpeaker's list of
   observers using the EventSpeaker::addObserver() function.

    Whenever an event will occur from the EventSpeaker, its thread will call the
   notify function.
*/

template <typename T> class EventListener {
  public:
    /*!
     * \brief	Empty destructor
     */
    virtual ~EventListener() {} // Destructor

    /*!
     * \brief	Function called by the EventSpeaker this EventListener is
     * listening to.
     *
     * \param	eventID			int allowing EventSpeaker to give info
     * about the type of event. The Meaning of this value depends on the
     * EventListener child implementation
     * \param	event			The actual content of the event
     */
    virtual void notify(MAYBE_UNUSED const int eventID,
                        MAYBE_UNUSED const T event) = 0;

  protected:
    // constructor is protected because this class is abstract, it's only meant
    // to be inherited!
    /*!
     * \brief	Empty constructor
     */
    EventListener() {}

  private:
    // -------------------------
    // Disabling default copy constructor and default
    // assignment operator.
    // -------------------------
    EventListener(const EventListener &yRef);
    EventListener &operator=(const EventListener &yRef);
};

using StrListener = EventListener<std::string>;

#endif /* EVENTLISTENER_H_ */
