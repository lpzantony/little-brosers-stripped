
#ifndef CUSTOM_EXCEPTIONS_H_
#define CUSTOM_EXCEPTIONS_H_

#include <exception>
#include <string>

/*
 * \brief Base class for exception which are returned to the client
 */
class RequestException : public std::exception {
    // Empty body, only used for RAII/RTTI
    virtual const char *what() const throw() = 0;
};

/*
 * \brief Warn that the command sent was incorrectly used
 */
class BadCommandUsageException : public std::exception {

  public:
    /*
     * \brief Construct the exception with command's name and usage
     * \param name the command's name
     * \param usage the command's usage
     */
    BadCommandUsageException(const std::string &name,
                             const std::string &usage) {
        m_msg = "Bad usage for <" + name + ">\n" + "Usage : " + usage;
    }

    /*
     * \brief return the error message
     */
    virtual const char *what() const noexcept override { return m_msg.c_str(); }

  private:
    std::string m_msg;
};

#endif /* CORE_CUSTOMEXCEPTIONS_H_ */
