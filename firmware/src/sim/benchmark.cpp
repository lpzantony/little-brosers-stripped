#include <benchmark.h>
#include <string> //stoi

//=====> includes et defines pour DropsRoutine
// pour strcmp, strcpy...
#include <cstring>
#include <iomanip> // std::setw std::setfill
#include <signal.h>

#define NB_MSG 8
#define DUMMY_MSG_SIZE MESSAGE_SIGNATURE_SIZE + MESSAGE_HEADER_SIZE + 2 + 1

//=====> Fin includes et defines pour ZyraRoutune
void lb_print_msgs(lb_merkle_tree *p_root, message_t *p_msgs,
                   uint32_t certif_date) {
    printf("BEGIN : Control timestamp nodes from main\n");
    lb_merkle_tree *tmp1, *tmp2;
    for (unsigned int i = 0; i < NB_MSG; i++) {
        tmp1 = lb_get_leaf_for_timestamp(
            p_root, lb_convert_absolute_to_relative_timestamp(
                        p_msgs[i].header.timestamp, certif_date, LB_MT_OFFSET));
        tmp2 = lb_get_sub_tree_root(tmp1);
        printf("timestamp %u sits into node %u, from root child "
               "going from %u to %u \n",
               p_msgs[i].header.timestamp, tmp1->array_id, LU(tmp2->start),
               LU(tmp2->end));
    }
    printf("END : Control timestamp nodes from main\n");
}
Benchmark::Benchmark(std::mutex *mx, int main_pid) {
    if (mx == nullptr)
        throw runtime_error("Cant build benchmark without allocated mutex!");
    this->run = mx;
    run->lock();
    mainPID = main_pid;
    cmd.clear();
}

message_t p_messagesG1[NB_MSG] = {
    //  header, payload, signature
    {{1, 41, 72, 0}, DUMMY_MSG_SIZE, {2}, {0x01}},
    {{2, 41, 72, 0}, DUMMY_MSG_SIZE, {2}, {0x02}},
    {{3, 41, 72, 0}, DUMMY_MSG_SIZE, {2}, {0x03}},
    {{4, 10, 41, 0}, DUMMY_MSG_SIZE, {7}, {0x04}},
    {{5, 10, 41, 0}, DUMMY_MSG_SIZE, {7}, {0x05}},
    {{6, 10, 41, 0}, DUMMY_MSG_SIZE, {7}, {0x06}},
    {{7, 19, 21, 0}, DUMMY_MSG_SIZE, {0}, {0x07}},
    {{8, 19, 21, 0}, DUMMY_MSG_SIZE, {0}, {0x08}}};
message_pool_t poolG1 = {p_messagesG1, NB_MSG};

message_t p_messagesG2[13] = {
    //  header, payload, signature
    {{382, 19, 21, 0}, DUMMY_MSG_SIZE, {26}, {1, 2, 3, 4, 5}},
    {{398, 19, 72, 0}, DUMMY_MSG_SIZE, {5}, {15}},
    {{1, 41, 72, 0}, DUMMY_MSG_SIZE, {2}, {0x01}},
    {{2, 41, 72, 0}, DUMMY_MSG_SIZE, {2}, {0x02}},
    {{3, 41, 72, 0}, DUMMY_MSG_SIZE, {2}, {0x03}},
    {{4, 10, 41, 0}, DUMMY_MSG_SIZE, {7}, {0x04}},
    {{1382, 19, 21, 0}, DUMMY_MSG_SIZE, {26}, {21, 2, 3, 4, 5}},
    {{1388, 21, 12, 0}, DUMMY_MSG_SIZE, {1}, {111}},
    {{130001, 41, 72, 0}, DUMMY_MSG_SIZE, {2}, {12}},
    {{1339, 41, 21, 0}, DUMMY_MSG_SIZE, {3}, {243}},
    {{13978, 72, 19, 0}, DUMMY_MSG_SIZE, {4}, {14}},
    {{1398, 19, 72, 0}, DUMMY_MSG_SIZE, {5}, {15}},
    {{51110, 1, 41, 0}, DUMMY_MSG_SIZE, {7}, {17}}};
message_pool_t poolG2 = {p_messagesG2, 13};

message_t p_messages1[NB_MSG] = {
    //  header, payload, signature
    {{160, 19, 21, 0}, DUMMY_MSG_SIZE, {0}, {90}},
    {{512, 21, 12, 0}, DUMMY_MSG_SIZE, {1}, {91}},
    {{100, 41, 72, 0}, DUMMY_MSG_SIZE, {2}, {92}},
    {{189, 41, 21, 0}, DUMMY_MSG_SIZE, {3}, {93}},
    {{124, 72, 19, 0}, DUMMY_MSG_SIZE, {4}, {94}},
    {{415, 19, 72, 0}, DUMMY_MSG_SIZE, {5}, {95}},
    {{378, 1, 41, 0}, DUMMY_MSG_SIZE, {6}, {96}},
    {{101, 1, 41, 0}, DUMMY_MSG_SIZE, {7}, {97}}};
message_pool_t pool1 = {p_messages1, NB_MSG};

message_t p_messages11[NB_MSG - 1] = {
    //  header, payload, signature
    {{160, 19, 21, 0}, DUMMY_MSG_SIZE, {0}, {21, 22, 23, 24, 25}},
    {{512, 21, 12, 0}, DUMMY_MSG_SIZE, {1}, {11, 51}},
    {{100, 41, 72, 0}, DUMMY_MSG_SIZE, {2}, {11, 12, 13, 14, 15}},
    {{189, 41, 21, 0}, DUMMY_MSG_SIZE, {3}, {13, 52}},
    {{124, 72, 19, 0}, DUMMY_MSG_SIZE, {4}, {14, 53}},
    {{415, 19, 72, 0}, DUMMY_MSG_SIZE, {5}, {15, 54}},
    {{378, 1, 41, 0}, DUMMY_MSG_SIZE, {6}, {16, 55}}};
message_pool_t pool11 = {p_messages11, NB_MSG - 1};

message_t p_messages2[NB_MSG] = {
    //  header, payload, signature
    {{382, 19, 21, 0}, DUMMY_MSG_SIZE, {26}, {8}},
    {{388, 21, 12, 0}, DUMMY_MSG_SIZE, {1}, {9}},
    {{301, 41, 72, 0}, DUMMY_MSG_SIZE, {2}, {10}},
    {{339, 41, 21, 0}, DUMMY_MSG_SIZE, {3}, {11}},
    {{378, 72, 19, 0}, DUMMY_MSG_SIZE, {4}, {12}},
    {{398, 19, 72, 0}, DUMMY_MSG_SIZE, {5}, {13}},
    {{324, 1, 41, 0}, DUMMY_MSG_SIZE, {6}, {14}},
    {{110, 1, 41, 0}, DUMMY_MSG_SIZE, {7}, {15}}};
message_pool_t pool2 = {p_messages2, NB_MSG};

message_t p_messages3[NB_MSG] = {
    //  header, payload, signature
    {{382, 19, 21, 0}, DUMMY_MSG_SIZE, {0}, {16}},
    {{382, 21, 12, 0}, DUMMY_MSG_SIZE, {1}, {17}},
    {{381, 41, 72, 0}, DUMMY_MSG_SIZE, {2}, {18}},
    {{383, 41, 21, 0}, DUMMY_MSG_SIZE, {3}, {19}},
    {{381, 72, 19, 0}, DUMMY_MSG_SIZE, {4}, {20}},
    {{381, 19, 72, 0}, DUMMY_MSG_SIZE, {9}, {21}},
    {{381, 1, 41, 0}, DUMMY_MSG_SIZE, {6}, {22}},
    {{225, 1, 41, 0}, DUMMY_MSG_SIZE, {7}, {23}}};
message_pool_t pool3 = {p_messages3, NB_MSG};

message_t p_messages4[NB_MSG] = {
    //  header, payload, signature
    {{381, 19, 21, 0}, DUMMY_MSG_SIZE, {52}, {24}},
    {{381, 21, 12, 0}, DUMMY_MSG_SIZE, {50}, {25}},
    {{381, 41, 72, 0}, DUMMY_MSG_SIZE, {40}, {26}},
    {{381, 41, 21, 0}, DUMMY_MSG_SIZE, {45}, {27}},
    {{381, 72, 19, 0}, DUMMY_MSG_SIZE, {29}, {28}},
    {{381, 19, 72, 0}, DUMMY_MSG_SIZE, {5}, {29}},
    {{382, 1, 41, 0}, DUMMY_MSG_SIZE, {6}, {30}},
    {{382, 1, 41, 0}, DUMMY_MSG_SIZE, {92}, {31}}};
message_pool_t pool4 = {p_messages4, NB_MSG};

// "main" like function
int Benchmark::DropsRoutine(MAYBE_UNUSED std::vector<std::string> args) {
    shell.setCmd("exit", member(&Benchmark::exit_from_shell, this));
    shell.setCmd("ping", member(&Benchmark::pingTCP, this));
    shell.setCmd("sd", member(&Benchmark::startDiff, this));
    shell.setCmd("wd", member(&Benchmark::waitDiff, this));
    shell.setCmd("d", member(&Benchmark::deleteTimestamp, this));
    shell.setCmd("pm", member(&Benchmark::printMsg, this));
    shell.setCmd("am", member(&Benchmark::attachMsg, this));
    shell.setCmd("pcd", member(&Benchmark::printCertifDate, this));
    shell.setCmd("ccd", member(&Benchmark::changeCertifDate, this));
    shell.setCmd("sm", member(&Benchmark::startMerge, this));
    shell.setCmd("wm", member(&Benchmark::waitMerge, this));
    printf("#######################################################\n");
    printf("################> Begining of the main cpp routine\n");
    printf("#######################################################\n");
    (void)pool1;
    (void)pool11;
    (void)pool2;
    (void)pool3;
    (void)pool4;
    (void)poolG1;
    (void)poolG2;
    if (is_LE()) {
        printf("Little Endian\n");
    } else {
        printf("Big Endian\n");
    }

    err_t retval = LB_OK;
    (void)retval;

    LB_ALLOC_ROOT_MD(root_md);
    LB_INIT_ROOT_MD(root_md);
    printf("Before build_merkle_tree()\n");
    LB_ALLOC_MERKLE_TREE(root, slot_pool);
    LB_INIT_MERKLE_TREE(root);
    lb_task_stack st1;
    dev1 = new Device(root, root_md, slot_pool, st1);

    LB_ALLOC_MERKLE_TREE(root2, slot_pool2);
    LB_INIT_MERKLE_TREE(root2);
    if (args.size() < 2) {
        lb_task_stack st2;
        dev2 = new Device(root2, root_md, slot_pool2, st2);
    }

    int srcport = UdpAgent::DEFAULT_PORT;
    int distport = UdpAgent::DEFAULT_PORT + 1;
    if (args.size() >= 2) {
        srcport = stoi(args[0]);
        distport = stoi(args[1]);
    }
    dev1->run("127.0.0.1", srcport, distport);
    if (args.size() < 2) {
        dev2->run("127.0.0.1", UdpAgent::DEFAULT_PORT + 1,
                  UdpAgent::DEFAULT_PORT);
    }

    printf("###############################################\n");
    printf("###############################################\n");

    dev1->setRequest(INIT_MK_TREE, NULL);

    if (args.size() < 2) {
        dev2->setRequest(INIT_MK_TREE, NULL);
    }

    if (args.size() < 2) {
        dev1->setRequest(ATTACH_MSGS, &pool1);
        dev2->setRequest(ATTACH_MSGS, &pool1);
    }

    printf("\n\n");
    printf("###############################################\n");
    printf("###############################################\n");
    lb_print_merkle_tree(root.p_children[0], &slot_pool);
    lb_print_merkle_tree(root.p_children[1], &slot_pool);
    lb_print_merkle_tree(root.p_children[2], &slot_pool);
    lb_print_merkle_tree(root.p_children[3], &slot_pool);
    printf("###############################################\n");
    printf("###############################################\n");
//#define SECOND_PASS
#ifdef SECOND_PASS
    // Second pass
    dev1->setRequest(ATTACH_MSGS, &pool1);

    printf("------>Attaching dev2 for the second time! \n");
    dev2->setRequest(ATTACH_MSGS, &pool2);

    printf("------>Attaching dev2 for the third time! \n");
    dev2->setRequest(ATTACH_MSGS, &pool3);

    printf("------>Attaching dev2 for the fourth time! \n");
    dev2->setRequest(ATTACH_MSGS, &pool4);

    printf("###############################################\n");
    printf("###############################################\n");
    if (args.size() < 2) {
        lb_print_merkle_tree(root2.p_children[0], &slot_pool2);
    }
    printf("###############################################\n");
    printf("###############################################\n");

    printf("dev1 msgs1 : \n");
    dev1->setRequest(PRINT_MSGS, NULL);

    printf("dev2 msgs2 : \n");
    dev2->setRequest(PRINT_MSGS, NULL);
#endif

    printf("\n");

    std::string line;
    run_bool = true;
    while (run_bool) {
        try {
            printing = true;
            std::cout << "> " << std::flush;
            std::getline(std::cin, line);
            if (line.size() == 0)
                continue;
            shell.parse(line, std::cout);

            printing = false;
        } catch (BadCommandUsageException &e) {
            std::cout << "Erreur: " << e.what() << std::endl;
        }
    }
    // Use the following command to wait for a ctrl+c
    // wait_signal();

    std::cout << "Stop signal received. Leaving Benchmark... " << std::endl;
    //====> cleaning up memory
    dev1->stop();
    if (args.size() < 2) {
        dev2->stop();

        if (dev2)
            delete dev2;
    }
    if (dev1)
        delete dev1;
    return 0;
}

//=================================>| Shell functions

SHELL_FUNC(Benchmark::exit_from_shell) {
    willPrint = true;
    output << "goodbye from Benchmark member function!" << endl;
    kill(mainPID, SIGINT);
}

SHELL_FUNC(Benchmark::pingTCP) {
    willPrint = true;
    if (args.size() < 2) {
        dev1->setRequest(PING, NULL);
    } else if (args[1] == "1") {
        dev1->setRequest(PING, NULL);
    } else if (args[1] == "2") {
        dev2->setRequest(PING, NULL);
    }
}

SHELL_FUNC(Benchmark::startDiff) {
    willPrint = true;
    if (args.size() < 2) {
        dev1->setRequest(START_DIFF, NULL);
    } else if (args[1] == "1") {
        dev1->setRequest(START_DIFF, NULL);
    } else if (args[1] == "2") {
        dev2->setRequest(START_DIFF, NULL);
    }
}

SHELL_FUNC(Benchmark::waitDiff) {
    willPrint = true;
    if (args.size() < 2) {
        dev1->setRequest(WAIT_DIFF, NULL);
    } else if (args[1] == "1") {
        dev1->setRequestNoBlock(WAIT_DIFF, NULL);
    } else if (args[1] == "2") {
        dev2->setRequestNoBlock(WAIT_DIFF, NULL);
    }
}

SHELL_FUNC(Benchmark::deleteTimestamp) {
    willPrint = true;
    if (args.size() < 2) {
        cout << "Argument needed!" << endl;
        return;
    }
    uint32_t timestamp;
    if (args.size() < 3) {
        timestamp = stoi(args[1]);
        dev1->setRequest(DEL_SLOT, &timestamp);
    } else if (args[1] == "1") {
        timestamp = stoi(args[2]);
        dev1->setRequest(DEL_SLOT, &timestamp);
    } else if (args[1] == "2") {
        timestamp = stoi(args[2]);
        dev2->setRequest(DEL_SLOT, &timestamp);
    }
}

SHELL_FUNC(Benchmark::printMsg) {
    willPrint = true;
    if (args.size() < 2) {
        dev1->setRequest(PRINT_MSGS, NULL);
    } else if (args[1] == "1") {
        dev1->setRequest(PRINT_MSGS, NULL);
    } else if (args[1] == "2") {
        dev2->setRequest(PRINT_MSGS, NULL);
    }
}

SHELL_FUNC(Benchmark::printCertifDate) {
    willPrint = true;
    if (args.size() < 2) {
        dev1->setRequest(PRINT_CD, NULL);
    } else if (args[1] == "1") {
        dev1->setRequest(PRINT_CD, NULL);
    } else if (args[1] == "2") {
        dev2->setRequest(PRINT_CD, NULL);
    }
}

SHELL_FUNC(Benchmark::changeCertifDate) {
    uint32_t timestamp;
    if (args.size() < 2) {
        cout << "Error, need new CertifDate !" << endl;
        return;
    } else if (args.size() < 3) {
        if (stoi(args[1]) < 0) {
            cout << "Error, need positive CertifDate !" << endl;
            return;
        }
        timestamp = stoi(args[1]);
        dev1->setRequest(SET_CD, &timestamp);
    } else if (args[1] == "1") {
        if (stoi(args[2]) < 0) {
            cout << "Error, need positive CertifDate !" << endl;
            return;
        }
        timestamp = stoi(args[2]);
        dev1->setRequest(SET_CD, &timestamp);
    } else if (args[1] == "2") {
        if (stoi(args[2]) < 0) {
            cout << "Error, need positive CertifDate !" << endl;
            return;
        }
        timestamp = stoi(args[2]);
        dev2->setRequest(SET_CD, &timestamp);
    }
}

SHELL_FUNC(Benchmark::attachMsg) {
    willPrint = true;
    if (args.size() < 2) {
        cout << "ERROR :Wrong params" << endl;
    } else if (args[1] == "1") {
        dev1->setRequest(ATTACH_MSGS, &pool1);
    } else if (args[1] == "11") {
        dev1->setRequest(ATTACH_MSGS, &pool11);
    } else if (args[1] == "2") {
        dev1->setRequest(ATTACH_MSGS, &pool2);
    } else if (args[1] == "3") {
        dev1->setRequest(ATTACH_MSGS, &pool3);
    } else if (args[1] == "4") {
        dev1->setRequest(ATTACH_MSGS, &pool4);
    } else if (args[1] == "g1") {
        dev1->setRequest(ATTACH_MSGS, &poolG1);
    } else if (args[1] == "g2") {
        dev1->setRequest(ATTACH_MSGS, &poolG2);
    } else if (args[1] == "all") {
        dev1->setRequest(ATTACH_MSGS, &pool1);
        dev1->setRequest(ATTACH_MSGS, &pool2);
        dev1->setRequest(ATTACH_MSGS, &pool3);
        dev1->setRequest(ATTACH_MSGS, &pool4);
    }
}

SHELL_FUNC(Benchmark::startMerge) {
    willPrint = true;
    if (args.size() < 2) {
        dev1->setRequest(START_MERGE, NULL);
    } else if (args[1] == "1") {
        dev1->setRequest(START_MERGE, NULL);
    } else if (args[1] == "2") {
        dev2->setRequest(START_MERGE, NULL);
    }
}

SHELL_FUNC(Benchmark::waitMerge) {
    willPrint = true;
    if (args.size() < 2) {
        dev1->setRequest(WAIT_MERGE, NULL);
    } else if (args[1] == "1") {
        dev1->setRequestNoBlock(WAIT_MERGE, NULL);
    } else if (args[1] == "2") {
        dev2->setRequestNoBlock(WAIT_MERGE, NULL);
    }
}

//================================>| notify functions

void Benchmark::notify(MAYBE_UNUSED const int eventID,
                       MAYBE_UNUSED const std::string str) {}

void Benchmark::wait_signal() {
    std::thread([](std::mutex *mx) { mx->lock(); }, run).join();
}
