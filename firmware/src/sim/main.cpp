#include <cmath>
#include <iostream>
#include <signal.h>
#include <unistd.h>

#include "benchmark.h"
#include <mutex>
// end Rubi

Benchmark *bench;
std::mutex mx;

void handle_sig(int sig) {
    std::cerr << std::endl
              << "Received signal : " << std::to_string(sig) << std::endl;
    if (sig == 11) {
        exit(-1);
    }
    bench->shutdown();
    std::cout << "Press Enter to exit..." << std::endl;
}

int main(int argc, char *argv[]) {
    bench = new Benchmark(&mx, getpid());
    signal(SIGINT, handle_sig);
    signal(SIGABRT, handle_sig);
    signal(SIGSEGV, handle_sig);

    std::vector<string> args;
    for (int i = 1; i < argc; i++) {
        args.push_back(argv[i]);
    }
    (void)args;
    bench->DropsRoutine(args);
    std::cout << std::endl << "EXIT: Ending Drops simulation\n\n";
    return 0;
}
