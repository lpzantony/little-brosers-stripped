/*********************************************************************
 * Filename:   sha256.h
 * Author:     Brad Conte (brad AT bradconte.com)
 * Copyright:
 * Disclaimer: This code is presented "as is" without any guarantees.
 * Details:
 *********************************************************************/

#ifndef SHA256_H
#define SHA256_H

#include "stddef.h"
#include "utils.h"
#ifdef __cplusplus
extern "C" {
#endif

/**@brief Current state of a hash operation.
 */
typedef struct {
    uint8_t data[64];
    uint32_t datalen;
    uint64_t bitlen;
    uint32_t state[8];
} sha256_context_t;

/**@brief Function for initializing a @ref sha256_context_t instance.
 *
 * @param[out] ctx  Context instance to be initialized.
 *
 * @retval NRF_SUCCESS     If the instance was successfully initialized.
 * @retval NRF_ERROR_NULL  If the parameter was NULL.
 */
err_t sha256_init(sha256_context_t *ctx);

/**@brief Function for calculating the hash of an array of uint8_t data.
 *
 * @details This function can be called multiple times in sequence. This is
 * equivalent to calling
 *          the function once on a concatenation of the data from the different
 * calls.
 *
 * @param[in,out] ctx   Hash instance.
 * @param[in]     data  Data to be hashed.
 * @param[in]     len   Length of the data to be hashed.
 *
 * @retval NRF_SUCCESS     If the data was successfully hashed.
 * @retval NRF_ERROR_NULL  If the ctx parameter was NULL or the data parameter
 * was NULL,  while the len parameter was not zero.
 */
err_t sha256_update(sha256_context_t *ctx, const uint8_t *data,
                    const size_t len);

/**@brief Function for extracting the hash value from a hash instance.
 *
 * @details This function should be called after all data to be hashed has been
 * passed to the hash
 *          instance (by one or more calls to @ref sha256_update).
 *
 * Do not call @ref sha256_update again after @ref sha256_final has been called.
 *
 * @param[in,out] ctx   Hash instance.
 * @param[out]    hash  Array to hold the extracted hash value (assumed to be 32
 * bytes long).
 * @param[in]     le   Store the hash in little-endian.
 *
 * @retval NRF_SUCCESS     If the has value was successfully extracted.
 * @retval NRF_ERROR_NULL  If a parameter was NULL.
 */
err_t sha256_final(sha256_context_t *ctx, uint8_t *hash, uint8_t le);

#ifdef __cplusplus
}
#endif

#endif // SHA256_H
