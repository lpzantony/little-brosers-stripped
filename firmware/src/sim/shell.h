#ifndef SHELL_H
#define SHELL_H

#include "custom_exceptions.h"
#include <functional>
#include <map>
#include <string>
#include <vector>
#define ACK_TOKEN "ACK"
// Macro usefull for some calls of setCmd
#define MAYBE_UNUSED __attribute__((unused))

#define member(member, memberOf)                                               \
    std::bind(member, memberOf, std::placeholders::_1, std::placeholders::_2)

// Macro used to declare shell functions
#define SHELL_FUNC(x)                                                          \
    void x(MAYBE_UNUSED const std::vector<std::string> &args,                  \
           MAYBE_UNUSED std::ostream &output)

/*! \class Shell
    \brief Very basic shell allowing interraction with other thread or a
   keyboard
*/

class Shell {
    // use void instead of bool, exceptions are handling the error management
    // stuff
    using FuncShell =
        std::function<void(const std::vector<std::string> &, std::ostream &)>;

  public:
    /*!
     * \brief Shell constructor. It will add at least one command to the shell :
     * "help"
     */
    Shell() { setCmd("help", member(&Shell::CommandList, this)); }

    using Params = std::vector<std::string>;

    /*!
     * \brief	setCmd registers a function to the command list.
     *
     * \param 	name		name of the command to add
     * \param 	function	Pointer to the function which has to be called
     *whenever "name" is thrown to the shell
     *
     * \details	You can use Shell::setCmd this way with non member functions (c
     *functions): \code{.cpp} setCmd("foo", fooFunction); \endcode
     *
     * \details	considering fooFunction declared this way:
     *			\code{.cpp}
     *     			void fooFunction(const std::vector<std::string>&
     *args, std::ostream& output) \endcode
     *
     * \details	You can use it with anonymous functions this way:
     *			\code{.cpp}
     *     			setCmd("ping", [](const
     *std::vector<std::string>&, std::ostream& output) { output << "pong";
     *     			});
     *     		\endcode
     *
     *
     * \details	You also may add a member function to the command list using
     *the member() macro.
     *
     * \details	Example below assumes that class Benchmark has a
     *Benchmark::exit_from_shell method and that "shell" object has been created
     *in a scope belonging to a Benchmark object. exit_from_shell must
     * 			return void and expect two arguments (const
     *std::vector<std::string>& and std::ostream&*): \code{.cpp}
     * 				shell.setCmd("exit",
     *member(&Benchmark::exit_from_shell, this)); \endcode
     */
    void setCmd(const std::string &name, FuncShell function);

    /*!
     * \brief 	Parses a given string which should contain a command, get its
     * name and tries to find and call the corresponding command
     *
     * \param	cmd		The string to be parsed
     * \param 	output	ostream to which commands will be abble to print
     * messages if needed \return 	True if a command has been found, false
     * otherwise. If during the command execution an error occurs, it will throw
     * a BadCommandUsageException.
     */
    bool parse(const std::string &cmd, std::ostream &output);

    /*!
     * \brief 	Tries to find the command name cmd[0] in the list of registered
     * commmands
     *
     * \param	cmd		A array containing the command name in the first
     * cell and the command arguments int the other cells. \param 	output
     * ostream to which commands will be abble to print messages if needed
     * \return True if a command has been found, false otherwise. If during the
     * command execution an error occurs, it will throw a
     * BadCommandUsageException.
     */
    bool callCmd(std::vector<std::string> cmd, std::ostream &output);

  private:
    SHELL_FUNC(CommandList);
    std::map<std::string, FuncShell> m_cmds;
};

#endif /* CORE_SHELL_H_ */
