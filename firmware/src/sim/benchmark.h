#ifndef BENCHMARK_H
#define BENCHMARK_H

#include <mutex>
#include <thread>
// Pour le shell
#include "command.h"
#include "device.h"
#include "event_listener.h"
#include "shell.h"

extern "C" {
#include "merkle_tree.h"
}

using namespace std;

class Benchmark : public StrListener {
  public:
    static const int ERR = -1;
    Benchmark(std::mutex *mx, int main_pid);
    virtual ~Benchmark() { shutdown(); }
    void shutdown() {
        run->unlock();
        run_bool = false;
    }
    void notify(MAYBE_UNUSED const int eventID,
                MAYBE_UNUSED const std::string str) override;

    int DropsRoutine(MAYBE_UNUSED std::vector<std::string> args);

    SHELL_FUNC(exit_from_shell);
    SHELL_FUNC(pingTCP);
    SHELL_FUNC(startDiff);
    SHELL_FUNC(waitDiff);
    SHELL_FUNC(deleteTimestamp);
    SHELL_FUNC(printMsg);
    SHELL_FUNC(attachMsg);
    SHELL_FUNC(printCertifDate);
    SHELL_FUNC(changeCertifDate);
    SHELL_FUNC(startMerge);
    SHELL_FUNC(waitMerge);

  private:
    Shell shell;
    std::mutex *run;
    bool run_bool = false;
    bool printing = false;
    bool willPrint = false;
    int mainPID = 0;
    Command cmd;
    void wait_signal();
    Device *dev1;
    Device *dev2;
};

#endif /* CORE_BENCHMARK_H_ */
