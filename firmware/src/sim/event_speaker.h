#ifndef EVENT_SPEAKER_H
#define EVENT_SPEAKER_H

//-----------------------------------------------------
// EventSpeaker Class
// This class as well as the EventListener class are based
// on a tutorial available at this URL:
// http://www.dreamincode.net/forums/topic/197421-the-listener-pattern/
//-----------------------------------------------------

#include "event_listener.h"
#include <algorithm>

template <typename T> class EventListener;

/*! \class EventSpeaker
    \brief Class intended to be inherited which will give access to its child to
   its event notification feature.

        Any class inherating from this class has the ability to register
   Observers (EvenetListeners) and notify them when needed.
*/

template <typename T> class EventSpeaker {
  public:
    /*!
     * \brief	Empty destructor
     */
    virtual ~EventSpeaker() {}

    /*!
     * \brief	Function called by the EventSpeaker in order to register at the
     * observers list of this EventSpeaker
     *
     * \param	eventListener	A pointer to the EventListener wanting to
     * register itself to the observers list of this EventSpeaker \return
     * True if the registration has been successfull. False if the EventListener
     * is already registered.
     */
    bool addObserver(EventListener<T> *eventListener) {
        typename std::vector<EventListener<T> *>::iterator temp =
            find(m_ObserverVec.begin(), m_ObserverVec.end(), eventListener);
        // Return false if the observer is already in the vector. This is not
        // expected. But there is nothing really wrong either
        if (temp != m_ObserverVec.end())
            return false;
        m_ObserverVec.push_back(eventListener);

        return true;
    }

    /*!
     * \brief	Function called by the EventSpeaker in order to unregister from
     * the observers list of this EventSpeaker
     *
     * \param	eventListener	A pointer to the EventListener wanting to
     * unregister itself from the observers list of this EventSpeaker \return
     * True if the unregistration has been successfull. False if the
     * EventListener has never been registered.
     */
    bool removeObserver(const EventListener<T> *eventListener) {
        typename std::vector<EventListener<T> *>::iterator temp =
            find(m_ObserverVec.begin(), m_ObserverVec.end(), eventListener);
        // Return false if the observer could not be found (and evidently can’t
        // be removed.
        if (temp == m_ObserverVec.end())
            return false;
        else
            m_ObserverVec.erase(remove(m_ObserverVec.begin(),
                                       m_ObserverVec.end(), eventListener));
        return true;
    }

    /*!
     * \brief	Function called to notify all the registered observers
     *
     * \param	eventID			int allowing EventSpeaker to give info
     * about the type of event. The Meaning of this value depends on the
     * EventListener child implementation
     * \param	event			The actual content of the event
     * \return	false if no observer has been notified, true otherwise
     */
    bool notifyObservers(const int eventID, const T event) {
        for (auto const &eventListener : m_ObserverVec) {
            eventListener->notify(eventID, event);
        }

        // Return false if there are no observers in the vector
        return (m_ObserverVec.size() > 0);
    }

    /*!
     * \brief	Gives the number of observers registered
     *
     * \return	The number of observers registered
     */
    uint32_t ObserversCout() { return m_ObserverVec.size(); }

  protected:
    // constructor is protected because this class is abstract, it’s only meant
    // to be inherited!
    /*!
     * \brief	Empty constructor
     */
    EventSpeaker() {}

  private:
    std::vector<EventListener<T> *> m_ObserverVec;
    // -------------------------
    // Disabling default copy constructor and default
    // assignment operator.
    // -------------------------
    EventSpeaker(const EventSpeaker &yRef);
    EventSpeaker &operator=(const EventSpeaker &yRef);
};

#endif /* EVENTSPEAKER_H_ */
