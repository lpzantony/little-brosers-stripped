#ifndef DEVICE_H
#define DEVICE_H

extern "C" {
#include "merkle_merge.h"
#include "merkle_tree.h"
#include "utils.h"
}
#include "command.h"
#include "udp_agent.h"
#include <mutex>
#include <thread>
#include <unistd.h> //usleep()
#include <vector>

typedef struct {
    uint8_t raw[LB_NB_MAX_MSGS][MESSAGE_MAX_SIZE];
} vflash_t;

typedef enum {
    NONE,
    TREE_COMP,
    INIT_MK_TREE,
    ATTACH_MSGS,
    PRINT_MSGS,
    MUTE_TCP,
    PING,
    START_DIFF,
    WAIT_DIFF,
    DEL_SLOT,
    PRINT_CD,
    SET_CD,
    START_MERGE,
    WAIT_MERGE
} Request;

class Device : public UdpListener {
  public:
    Device(lb_merkle_tree &p_root, lb_merkle_root_md &p_root_md,
           lb_slot_pool &p_slots, lb_task_stack &p_st);
    virtual ~Device();
    int run(const string &destIp,
            const uint32_t destPort = UdpAgent::DEFAULT_PORT + 1,
            const uint32_t srcPort = UdpAgent::DEFAULT_PORT);
    void stop();
    void threadRoutine();
    err_t init_merkle_tree();
    err_t attach_msgs_to_tree(message_pool_t *p_msg_pool);
    void print_msgs();
    void notify(const int eventID, const std::vector<uint8_t> data) override;
    void waitForUdpRecv();
    void merge(int start);
    inline void setRequest(Request req, void *data) {

        req_mx.lock();
        this->req = req;
        p_req_data = data;
        req_mx.unlock();
        while (getRequest() != NONE)
            usleep(1000);
    }
    inline void setRequestNoBlock(Request req, void *data) {
        req_mx.lock();
        this->req = req;
        p_req_data = data;
        req_mx.unlock();
    }

  protected:
    std::thread the_thread;
    std::mutex the_mutex;
    bool running, mute_TCP;
    lb_merkle_tree *p_root;
    lb_merkle_root_md *p_root_md;
    lb_slot_pool *p_slots;
    lb_task_stack *p_st;
    Request req;
    vflash_t msgs;
    queue<vector<uint8_t>> net_fifo;
    mutex req_mx, net_fifo_mx;
    void *p_req_data;
    UdpAgent *net;

    inline void setRunning(bool run) {
        the_mutex.lock();
        running = run;
        the_mutex.unlock();
    }

    inline bool getRunning() {
        bool tmp;
        the_mutex.lock();
        tmp = running;
        the_mutex.unlock();
        return tmp;
    }

    inline Request getRequest() {
        Request tmp;
        req_mx.lock();
        tmp = req;
        req_mx.unlock();
        return tmp;
    }
};

#endif
