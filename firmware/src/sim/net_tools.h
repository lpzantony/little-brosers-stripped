#ifndef NET_TOOLS_H
#define NET_TOOLS_H

#include <string>
#include <sys/ioctl.h> // for ioctl() flags
#include <sys/socket.h>

/*! \class NetTools
    \brief Abstract class providing tools to manage network operations
*/

class NetTools {
  public:
    static const int ERR =
        -1; /*!< Most used error code among the class methods */

    /*!
     * \brief	Tells if the given string is a correct IPV4 address
     *
     * \param	ipAddress	string to be tested as an IPV4 address
     * \return	True if the string contains a valid IPV4 address, false
     * otherwise.
     */
    static bool validateIpAddress(const std::string &ipAddress);

    /*!
     * \brief	Tells if the current filesystem is read-Only or not
     *
     * \return	True if the filesystem is in read-only mode, false
     * otherwise.
     */
    static bool isFSReadOnly();

    /*!
     * \brief	Obtain a property from eth0
     *
     * \param	flag		Asked property
     * \param	buf			buffer where to put the
     * extracted
     * property
     * \return	0 in case of success, NetTools::ERR otherwise.
     */
    static int getInterfaceProperty(int flag, std::string &buf);

    /*!
     * \brief	Obtain the eth0 IP address
     *
     * \return	the eth0 IP address in case of success, an empty string
     * otherwise
     */
    static inline std::string getIPAddress() {
        std::string ip = "";
        getInterfaceProperty(SIOCGIFADDR, ip);
        return ip;
    }

    /*!
     * \brief	Obtain the eth0 netmask
     *
     * \return	the eth0 netmask in case of success, an empty string
     * otherwise
     */
    static inline std::string getNetmask() {
        std::string net = "";
        getInterfaceProperty(SIOCGIFNETMASK, net);
        return net;
    }

    /*!
     * \brief Lanuch a command through sh (C++)
     *
     * \param	cmd	Command to be executed in a shell
     */
    static inline void syscall(const std::string cmd) { system(cmd.c_str()); }

    /*!
     * \brief Lanuch a command through sh (C)
     *
     * \param	cmd	Command to be executed in a shell
     * \return 	The text result of the cmd, "ERROR" otherwise
     */
    static std::string exec(char *cmd);

    /*!
     * \brief Reboots the computer
     */
    static inline void reboot() { system("reboot -i"); }

    /*!
     * \brief Gives the size of a socket buffer
     */
    static inline int getSocketRcvBufSize() {
        int n;
        unsigned int m = sizeof(n);
        int fdsocket = socket(AF_INET, SOCK_STREAM, 0);
        getsockopt(fdsocket, SOL_SOCKET, SO_RCVBUF, (void *)&n, &m);
        return n;
    }

    /*!
     * \brief Gives thte size of a socket buffer
     */
    static inline int getSocketSendBufSize() {
        int n;
        unsigned int m = sizeof(n);
        int fdsocket = socket(AF_INET, SOCK_STREAM, 0);
        getsockopt(fdsocket, SOL_SOCKET, SO_SNDBUF, (void *)&n, &m);
        return n;
    }

    static int setReuseAddr(int fd);

    static int setFdBlocking(int fd, int blocking);
    static int setRcvTimeout(int fd, long timeout_s, long timeout_us);
    static int setAddress(struct sockaddr_in &addr, const std::string &host,
                          int port);
};

#endif /* NETWORKAGENT_H_ */
