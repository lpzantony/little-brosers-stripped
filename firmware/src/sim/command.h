#ifndef COMMAND_H
#define COMMAND_H

#include <initializer_list>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string>
#include <vector>

using namespace std;

/*! \class Command
    \brief Class used to send formated messages to TCP ou UDP socket

        This class is nothing more than a std::vector with a name.
        It also features some supplementary methods to ease its usage.
        It keeps the flexibility of a std::vector but can be converted at any
   time to a dully formatted version made to be as portable and robust as
   possible.

        Formated version of a command always starts with a Command::startChar
   ('$' by default) followed by Command::NB_DIGIT_CMD_SIZE (4 by default)
   zeros-filled digits number representing the size of the rest of the command.
        After the Command::NB_DIGIT_CMD_SIZE number comes the command name
   followed by eventual params which will be all following a Command::separator
   (';' by default).


        Example 1: "$0010myCommand1"

        Example 2: "$0024myComman2;param1;param2,param3"

*/

class Command : public vector<string> {
  public:
    static const char NB_DIGIT_CMD_SIZE =
        4; /*!< Number of digits dedicated to the command size */
    static const char startChar = '$'; /*!< Default start character */
    static const char separator =
        ';'; /*!< Default separator character. There is one before each param */

    /*!
     * \brief	0,1 or 2 arguments constructor.
     *
     * 			Calling Command() with no arguments	will generate a
     * command which will use "startChar" as start character and "separator" as
     * parameter delimiter.
     *
     * 			You can specify your own startChar or separator by
     * giving them as arguments.
     *
     * 			The Command made by this constructor will have a name
     * set to "noname" and will be empty. You'll have to set a name with the
     * Command::setName() or Command::reset() member function.
     *
     * \param 	startChar optionnal custom start character. Default is
     * Command::startChar \param 	separator optionnal custom separator
     * character. Default is Command::separator
     */
    Command(char startChar = Command::startChar,
            char separator = Command::separator)
        : vector<string>(), name("noname"), _startChar(startChar),
          _separator(separator) {}

    /*!
     * \brief	Build a command by parsing a valid formated command string
     *
     * 			This constructor is expected to be used only from a
     * valid command stored in a string.
     *
     * 			You still can specify custom startChar and separator to
     * match those of the given string if they are not the default ones.
     *
     * \param 	req 			Formated command as a string, i. e.
     * "$0023myCommand;param1;param2"
     * \param 	startChar 		optionnal custom start character.
     * Default
     * is Command::startChar \param 	separator 		optionnal custom
     * separator character. Default is Command::separator
     */
    Command(const string &req, char startChar = Command::startChar,
            char separator = Command::separator)
        : Command(startChar, separator) {
        try {
            buildFromFormatedCmd(req);
        } catch (runtime_error &e) {
            throw e;
        }
    }

    /*!
     * \brief	Build a command from a name and a list of params
     *
     * 			This constructor is expected to be used when the
     * developper already knows the command name and its params even before
     * creating it.
     *
     * 			You still can specify custom startChar and separator.
     *
     * 			Usage is : Command cmd = new Command("myCommandName",
     * {"arg1", "arg2", "arg3"});
     *
     * 			Or       : Command cmd = new Command("myCommandName",
     * {});
     *
     * \param 	name 			Command name
     * \param 	params			List of params. All params must be
     * std::string. \param 	startChar 		optionnal custom start
     * character. Default is Command::startChar \param 	separator
     * optionnal custom separator character. Default is Command::separator
     */
    Command(const string &name, const initializer_list<std::string> params,
            char startChar = Command::startChar,
            char separator = Command::separator)
        : Command(startChar, separator) {
        buildFromName(name);
        for (auto param : params)
            push_back(param);
    }

    /*!
     * \brief	Prints a nice text representation of the command
     * 			to the given output stream.
     * \param	ostream			Output stream to which this function
     * will print the text.
     */
    void print(ostream &stream) const;

    /*!
     * \brief 	Converts a std::stringstream to a std::string
     *
     * \param 	stream 			Reference to a std::stringstream from
     * which we will extract the data. \return 	std::string filled with the
     * content of the std::stringstream
     */
    string streamToString(stringstream &stream) const;

    /*!
     * \brief 	Returns the command's name
     *
     * \return	A string filled with the command's name
     */
    inline string getName() const { return name; }

    /*!
     * \brief 	Convert a string to a bool
     * \return 	A bool interpretation of the string. Anything different from the
     * ASCCI "0" character is considered true.
     */
    static bool to_bool(std::string const &s) { return s != "0"; }

    /*!
     * \brief 	Changes the name of the command
     * \param 	name 			new command name
     */
    inline void setName(const string &name) { this->name = name; }

    /*!
     * \brief 	Changes the name of the command and clears its content
     * \param 	newName 		new command name
     */
    inline void reset(const string &newName) {
        clear();
        name = newName;
    }

    /*!
     * \brief 	Changes the name of the command and clears its content
     * \param 	newName 		new command name
     * \param 	args 			Optionnal list of params we want to put
     * into the cmd during reset
     *
     * \details Usage may be cmd.reset("MyNewName", "p1", "p2", "p3")
     */
    template <typename... Args>
    inline void reset(const string &newName, Args... args) {
        reset(newName);
        _reset(args...);
    }

    /*!
     * \brief 	Return a dully formated text representation of the command.
     * Ready to be sent.
     *
     * \param 	insertStart 	If this bool is set to true, the full command is
     * outputed, with the startChar and the checksum. If false, you only get the
     * command's name and the params without the startChar
     * and the checksum. \return A string filled with a dully formated
     * representation of the command
     */
    string formatCmd(bool insertStart = true) const;

    /*!
     * \brief This function need to be fixed.
     * 			It is supposed to do the same as formatCmd()
     * 			but with a custom separator and startChar
     */
    string customFormatCmd(std::string newSeparator, bool insertStart = true);

    /*!
     * \brief replace all instances of a "from" string by a "to" string in the
     * "str" string
     *
     * \param	str				std::string from which we will
     * replace parts \param	from			token we will delete
     * \param	to				token we will use to replace the
     * "from" token
     */
    static void ReplaceAll(std::string &str, const std::string &from,
                           const std::string &to);

    /*!
     * \brief 	Returns the command's separator
     * \return The char used to separate params
     */
    inline char getSeparator() const { return _separator; }

    /*!
     * \brief 	Return an integer value of the command size.
     *        	This includes the name size and each param size with a +1 for
     * the separator for each param. \return Return : name size + ((1 + param
     * size)
     * * nb_params)
     */
    inline int getSum() const {
        int sum = name.size();
        for (auto param : *this)
            sum += param.size() + 1;
        return sum;
    }

    /*!
     * \brief 	Returns a string representation of an int on
     * Command::NB_DIGIT_CMD_SIZE digits filled up with zeros \param 	sumValue
     * The to be converted int \return	A string representation of sumValue
     * filled with zeros on non-used digits.
     */
    static inline string getSumStr(int sumValue) {
        const char format[5] = {'%', '0', NB_DIGIT_CMD_SIZE + '0', 'u', 0};
        char sum[NB_DIGIT_CMD_SIZE + 1];
        sprintf(sum, format, sumValue);
        return sum;
    }

    /*!
     * \brief 	Pre-parse a string containing none, one or several cmds.
     *
     * \param 	req 			Reference to string where the raw data
     * is located
     * \param 	firstCmd 		Rerefence to a string where we will
     * write the frist command we found, or and error message in case of error
     *
     * \return 	In case of success, returns the number of chars to retrieve from
     * the 'req' string and write the first command it found into the 'firstCmd'
     * parameter. Return 0 if no cmd is present, -1 for any other error For
     * return value 0 or 1, the error is written into the 'firstCmd'
     * parameter
     */
    static int lookForCmd(const string &req, string &firstCmd);

    /*!
     *  \brief	Convertion operator from Command to string.
     *  		It will return a formated command in a string.
     *  		Avoids calling Command::formatCmd()
     */
    inline operator std::string() const { return formatCmd(); }

  private:
    /*!
     * Template variadic allowing public reset()
     * function to accept a variable count of arguments
     */
    inline void _reset(string t) { push_back(t); }
    template <typename... Args> inline void _reset(string t, Args... args) {
        push_back(t);
        _reset(args...);
    }

    void buildFromFormatedCmd(const string &req);
    void buildFromName(const string &req);

    string name;
    char _startChar;
    char _separator;
};

#endif /* COMMAND_H_ */
