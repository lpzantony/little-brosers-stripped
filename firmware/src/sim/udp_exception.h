#ifndef NETWORK_UDP_EXCEPTION_H_
#define NETWORK_UDP_EXCEPTION_H_
#include <exception>
#include <string>

class UDPException : public std::exception {
  public:
    UDPException() {}

    UDPException(int status) {
        switch (status) {
        /*case sf::Socket::NotReady:
            mMessage = "Not Ready";
            break;
        case sf::Socket::Partial:
            mMessage = "partial status";
            break;
        case sf::Socket::Disconnected:
            mMessage = "Disconnected";
            break;
        case sf::Socket::Error:
            mMessage = "status is error";
            break;*/
        default:
            mMessage = "Unknown Error";
        }
    }
    virtual ~UDPException() {}

    const char *what() const throw() override {
        return std::string("UDPException : " + mMessage)
            .c_str(); // this->exception();
    }

  private:
    std::string mMessage;
};
#endif /* NETWORK_UDP_EXCEPTION_H_ */
