#include <iostream>
#include <shell.h>
#include <stdexcept>

void Shell::setCmd(const std::string &name, FuncShell function) {
    m_cmds[name] = function;
    if (name.find(ACK_TOKEN) == std::string::npos)
        std::cout << "Add function '" << name << "'" << std::endl;
}
void Shell::CommandList(MAYBE_UNUSED const std::vector<std::string> &args,
                        MAYBE_UNUSED std::ostream &output) {
    output << "For help on a specific command, just type the command name"
           << std::endl;
    output << "/!\\ WARNING /!\\: Never use the commands ending by \""
           << ACK_TOKEN << "\"!" << std::endl;
    output << "List of available commands :" << std::endl;
    for (auto func : m_cmds) {
        output << "    " << func.first << std::endl;
    }
}

bool Shell::parse(const std::string &cmd, std::ostream &output) {
    std::size_t i = 0;
    std::vector<std::string> accu;

    bool escape = false, escape_space = false;

    // Explode string
    for (std::size_t j = 0; j < cmd.size(); ++j) {
        if (cmd[j] == ' ' && !escape_space) {
            if (i == j) {
                ++i;
                escape = false;
                continue;
            }
            accu.push_back(cmd.substr(i, j - i));
            i = j + 1;
        } else if (cmd[j] == '\'' && !escape) {
            if (escape_space) {
                accu.push_back(cmd.substr(i + 1, j - i - 1));
                escape_space = false;
                i = j + 1;
            } else
                escape_space = true;
        }

        escape = false;
        if (cmd[j] == '\\')
            escape = true;
    }
    if (i != cmd.size())
        accu.push_back(cmd.substr(i, cmd.size() - i));
    try {
        return callCmd(accu, output);
    } catch (BadCommandUsageException &e) {
        throw e;
    }
}

bool Shell::callCmd(std::vector<std::string> cmd, std::ostream &output) {
    // Parse content
    auto func = m_cmds.find(cmd[0]);
    if (func != m_cmds.end()) {
        try {
            func->second(cmd, output);
        } catch (BadCommandUsageException &e) {
            throw e;
        }
    } else {
        output << "Command not found : '" << cmd[0] << "'" << std::endl;
        return false;
    }
    return true;
}
