#include "device.h"
#include <iostream> //cout

Device::Device(lb_merkle_tree &p_root, lb_merkle_root_md &p_root_md,
               lb_slot_pool &p_slots, lb_task_stack &p_st) {
    this->p_root = &p_root;
    this->p_slots = &p_slots;
    this->p_root_md = &p_root_md;
    this->req = NONE;
    this->mute_TCP = true;
    this->p_st = &p_st;
    lb_stack_init(this->p_st, this->p_slots);
}

int Device::run(const string &destIp, const uint32_t destPort,
                const uint32_t srcPort) {
    setRunning(true);
    net = new UdpAgent(srcPort, destIp, destPort);
    net->addObserver(this);
    net->start();

    the_thread = std::thread(&Device::threadRoutine, this);
    usleep(1000);
    return 0;
}

void Device::stop() {
    if (net) {
        net->stop();
        delete net;
    }
    setRunning(false);
    the_thread.join();
}
err_t lb_lb(lb_task_stack *p_st, lb_merkle_tree *p_root, int start,
            lb_diff_RX_TX lb_send, void *lb_send_args, lb_diff_RX_TX lb_recv,
            void *lb_recv_args) {
    (void)p_root;
    if (start) {
        cout << "Sending..." << endl;
        lb_send(p_st, lb_send_args);
        cout << "Sent" << endl;
    } else {
        cout << "Waiting recv..." << endl;
        lb_recv(p_st, lb_recv_args);
        cout << "Received" << endl;
    }
    return LB_OK;
}

// LAMBDA
using callback_pair = std::pair<err_t (*)(lb_task_stack *, void *), void *>;

extern "C" err_t caller(lb_task_stack *p_st, void *arg) {
    callback_pair *p = static_cast<callback_pair *>(arg);
    return p->first(p_st, p->second);
}
// LAMBDA
extern "C" err_t storeMsg(void *args, const uint16_t addr, message_t *p_msg) {
    if (p_msg == NULL || addr > LB_NB_MAX_MSGS + LB_ADDR_FIRST_STORAGE ||
        args == NULL) {
        return LB_WRONG_ARGS_VALUE;
    }
    // uint8_t** msgs = (uint8_t**) args;
    vflash_t *msgs = (vflash_t *)args;
    message_to_raw(msgs->raw[addr - LB_ADDR_FIRST_STORAGE], p_msg);
    return LB_OK;
}

extern "C" err_t loadMsg(void *args, const uint16_t addr, message_t *p_msg) {
    if (p_msg == NULL || addr > LB_NB_MAX_MSGS + LB_ADDR_FIRST_STORAGE ||
        args == NULL) {
        return LB_WRONG_ARGS_VALUE;
    }
    vflash_t *msgs = (vflash_t *)args;
    message_from_raw(msgs->raw[addr - LB_ADDR_FIRST_STORAGE], p_msg);
    return LB_OK;
}

void Device::waitForUdpRecv() {
    while (net_fifo.empty()) {
        net_fifo_mx.unlock();
        usleep(250);
        net_fifo_mx.lock();
    }
}

void Device::merge(int start) {
    std::vector<uint8_t> data(sizeof(uint32_t));
    uint32_t send_count;
    int recv_count;
    message_t msg;
    message_pool_t msgpool = {&msg, 1};
    size_t written = 1;
    err_t retval;
    lb_get_send_count(this->p_slots, &send_count);
    if (start) {
        uint32_to_bytes(send_count, data.data());
        net->send(data);
        net_fifo_mx.lock();
        waitForUdpRecv();
        recv_count = bytes_to_uint32(net_fifo.front().data());
        net_fifo.pop();
        net_fifo_mx.unlock();

        while (written > 0 || recv_count > 0) {
            if (written > 0) {
                // Sending next
                data.clear();
                data.resize(MESSAGE_MAX_SIZE);
                retval = lb_next_msg_to_send(loadMsg, &this->msgs, p_slots,
                                             data.data(), &written);
                if (retval != LB_OK)
                    return;
                if (written > 0) {
                    data.resize(written);
                    net_fifo_mx.lock();
                    net->send(data);
                    net_fifo_mx.unlock();
                    send_count--;
                }
            }
            if (recv_count > 0) {
                // recv next msg
                data.clear();
                net_fifo_mx.lock();
                waitForUdpRecv();
                message_from_raw(net_fifo.front().data(), &msg);
                net_fifo.pop();
                net_fifo_mx.unlock();
                // Add msg to tree
                lb_attach_msgs_to_tree(this->p_root, &msgpool, p_slots,
                                       storeMsg, &this->msgs);
                recv_count--;
            }
        }

    } else {
        net_fifo_mx.lock();
        waitForUdpRecv();
        recv_count = bytes_to_uint32(net_fifo.front().data());
        net_fifo.pop();
        net_fifo_mx.unlock();
        uint32_to_bytes(send_count, data.data());
        net->send(data);

        while (written > 0 || recv_count > 0) {
            if (recv_count > 0) {
                data.clear();
                net_fifo_mx.lock();
                waitForUdpRecv();
                message_from_raw(net_fifo.front().data(), &msg);
                net_fifo.pop();
                net_fifo_mx.unlock();

                // Add msg to tree
                lb_attach_msgs_to_tree(this->p_root, &msgpool, p_slots,
                                       storeMsg, &this->msgs);
                recv_count--;
            }
            if (written > 0) {
                // Sending next
                data.clear();
                data.resize(MESSAGE_MAX_SIZE);
                retval = lb_next_msg_to_send(loadMsg, &this->msgs, p_slots,
                                             data.data(), &written);
                if (retval != LB_OK)
                    return;
                if (written > 0) {
                    data.resize(written);
                    net_fifo_mx.lock();
                    net->send(data);
                    net_fifo_mx.unlock();
                    send_count--;
                }
            }
        }
    }
    printf("recv_count = %u, send_count = %u\n", recv_count, send_count);
}

void Device::threadRoutine() {
    err_t retval = LB_OK;
    (void)retval;
    Command cmd;
    std::vector<uint8_t> data;

    // Following lambda function is supposed to be the send() that diff uses
    auto diff_send = [&](lb_task_stack *p_st) -> err_t {
        static uint8_t sent = 0;
        (void)p_st;
        data.clear();

        data.assign(p_st->buff.payload, p_st->buff.payload + p_st->buff.used);
        data.insert(data.begin(), sent);
        net->send(data);
        sent++;
        usleep(250);
        return LB_OK;
    };
    // Following lambda function is supposed to be the recv() that diff uses
    auto diff_recv = [&](lb_task_stack *p_st) -> err_t {
        static uint8_t received = 0;
        // static uint8_t old_received = 0;
        (void)p_st;
        net_fifo_mx.lock();
        // old_received = received;
        while (net_fifo.empty()) {
            net_fifo_mx.unlock();
            usleep(250);
            net_fifo_mx.lock();
        }
        if (received != *net_fifo.front().data()) {
            cout << "UDP : Un-ordered stream !" << endl;
            net_fifo.pop();
            net_fifo_mx.unlock();
            exit(LB_ERR);
        }
        received++;

        memcpy(p_st->buff.payload, net_fifo.front().data() + 1,
               net_fifo.front().size());
        net_fifo.pop();

        net_fifo_mx.unlock();
        return LB_OK;
    };

    // This is a caller for send lambda function used by diff
    auto diff_send_caller = [](lb_task_stack *p_st,
                               void *arg) -> err_t { // note this is captureless
        return (*static_cast<decltype(diff_send) *>(arg))(p_st);
    };
    // This is a caller for recv lambda function used by diff
    auto diff_recv_caller = [](lb_task_stack *p_st,
                               void *arg) -> err_t { // note this is captureless
        return (*static_cast<decltype(diff_recv) *>(arg))(p_st);
    };

    // This a pair linking the send lambda function to its caller
    callback_pair diff_send_pair{diff_send_caller, &diff_send};

    // This a pair linking the recv lambda function to its caller
    callback_pair diff_recv_pair{diff_recv_caller, &diff_recv};
    ssize_t i_slot;

    while (getRunning()) {
        req_mx.lock();
        switch (req) {
        case NONE:
            break;
        case TREE_COMP:
            std::cerr << "Main asked to compare the trees\n";
            break;
        case INIT_MK_TREE:
            retval = this->init_merkle_tree();
            if (retval != LB_OK)
                printf("An error occured : %i\n", retval);
            break;
        case ATTACH_MSGS:
            if (this->attach_msgs_to_tree((message_pool_t *)p_req_data) !=
                LB_OK) {
                printf("Could not attach msg to tree\n");
                // return LB_ERR;
            }
            break;
        case PRINT_MSGS:
            this->print_msgs();
            break;
        case MUTE_TCP:
            this->mute_TCP = *((int *)p_req_data);
            break;
        case PING:
            data.push_back(80);
            data.push_back(73);
            data.push_back(78);
            data.push_back(71);
            cout << "sending" << endl;
            net->send(data);
            data.clear();
            break;
        case START_DIFF:
            lb_diff(this->p_st, this->p_root, 1, caller, &diff_send_pair,
                    caller, &diff_recv_pair);
            // lb_lb(this->p_st, this->p_root, 1, caller, &diff_send_pair,
            // caller, &diff_recv_pair);
            lb_print_diff_result(this->p_st);
            break;
        case WAIT_DIFF:
            lb_diff(this->p_st, this->p_root, 0, caller, &diff_send_pair,
                    caller, &diff_recv_pair);
            // lb_lb(this->p_st, this->p_root, 0, caller, &diff_send_pair,
            // caller, &diff_recv_pair);
            lb_print_diff_result(this->p_st);
            break;
        case DEL_SLOT:
            if (lb_get_timestamp_index(this->p_root, *((uint32_t *)p_req_data),
                                       this->p_slots, &i_slot) != LB_OK) {
                cout << "Could not find such a timestamp : "
                     << to_string(*((uint32_t *)p_req_data)) << endl;
            } else if (lb_detach_slot(this->p_root, i_slot, this->p_slots) !=
                       LB_OK) {
                cout << "Could not find such a timestamp : "
                     << to_string(*((uint32_t *)p_req_data)) << endl;
            } else {
                lb_free_a_slot(this->p_slots, i_slot);
                cout << "Freed slot for  timestamp : "
                     << to_string(*((uint32_t *)p_req_data)) << endl;
            }
            break;
        case PRINT_CD:
            cout << "certif_date = " << to_string(this->p_slots->certif_date)
                 << endl;
            break;
        case SET_CD:
            lb_shift_tree(this->p_root, this->p_slots,
                          *((uint32_t *)p_req_data), NULL, NULL);
            break;
        case START_MERGE:
            merge(1);
            break;
        case WAIT_MERGE:
            merge(0);
            break;
        default:
            break;
        }
        req = NONE;
        req_mx.unlock();

        net_fifo_mx.lock();
        while (!net_fifo.empty()) {
            cout << endl << flush;
            cout << "Main loop UDP received : ";
            for (auto str : net_fifo.front())
                cout << str << " ";
            cout << endl;
            net_fifo.pop();
        }
        net_fifo_mx.unlock();

        // Volontary sleep
        usleep(1000);
    }
}

err_t Device::init_merkle_tree() {
    return lb_init_merkle_tree(p_root, p_root_md, p_slots, LB_MT_OFFSET);
}

err_t Device::attach_msgs_to_tree(message_pool_t *p_msg_pool) {
    return lb_attach_msgs_to_tree(p_root, p_msg_pool, p_slots, storeMsg,
                                  &this->msgs);
}

void Device::print_msgs() {
    ssize_t i_tmp = p_slots->i_begin_busy;
    if (i_tmp == -1) {
        printf("No msgs stored\n");
        return;
    }
    lb_slot *slot = &p_slots->slots[i_tmp];

    for (unsigned int i = 0; i < LB_NB_MAX_MSGS; i++) {
        printf("%2d : timestamp %4u, addr = %4u, hash =", i,
               LU(slot->timestamp), slot->addr);
        for (int k = 0; k < LB_SLOT_SIGNATURE_SIZE; k++)
            printf(" %02x", slot->p_hash[k]);
        printf(" )\n");

        if (slot->i_next == -1)
            break;
        slot = &p_slots->slots[slot->i_next];
    }
}

Device::~Device() {}

void Device::notify(const int eventID, const std::vector<uint8_t> data) {
    (void)eventID;
    net_fifo_mx.lock();
    net_fifo.push(data);
    net_fifo_mx.unlock();
}
