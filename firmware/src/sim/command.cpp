
#include <command.h>
#include <stdexcept> //runtime exception

void Command::buildFromName(const string &req) {
    stringstream stream(req);
    string token;
    getline(stream, name, _separator);
}

void Command::buildFromFormatedCmd(const string &req) {

    unsigned int i = 0;

    // Looking for _startChar
    for (; i < req.size(); i++)
        if (req[i] == _startChar)
            break;
    if (i == req.size())
        throw runtime_error("Can't parse command, missing start char.");

    // Make a brand new string starting at _startChar
    string req2 = req.substr(i);

    // Making sure the source string is long enough
    if (req2.size() < 6)
        throw runtime_error("Can't parse command, too short string.");

    // Extracting command size
    unsigned int sum = 0;
    try {
        sum = stoul(req2.substr(1, NB_DIGIT_CMD_SIZE));
    } catch (std::invalid_argument &e) {
        throw runtime_error(
            "Can't parse command, command size character can't be parsed.");
    }

    // Check if size annouced is the actual size
    if (1 + NB_DIGIT_CMD_SIZE + sum != req2.size())
        throw runtime_error("Can't parse command, command size doesn't match.");

    // Extracting name from req2
    name = "";
    for (i = 1 + NB_DIGIT_CMD_SIZE; i < 1 + NB_DIGIT_CMD_SIZE + sum; i++) {
        if (req2[i] == _separator)
            break;
        name += req2[i];
    }
    // We have a name-only cmd
    if (i == 1 + NB_DIGIT_CMD_SIZE + sum)
        return;

    // We will have params
    i++;
    string token = "";
    for (; i < 1 + NB_DIGIT_CMD_SIZE + sum; i++) {
        if (req2[i] == _separator) {
            push_back(token);
            token = "";
        } else
            token += req2[i];
    }
    // Push last param
    if (token.size())
        push_back(token);
}

void Command::print(ostream &stream) const {
    stream << "=====> Command name: " << name << endl;
    int i = 0;
    for (string str : *this) {
        stream << "\t param " << to_string(i++) << " : " << str << endl;
    }
    stream << endl;
}

string Command::formatCmd(bool insertStart) const {
    string str;
    if (insertStart)
        str = str + _startChar + getSumStr(getSum());
    str += this->name;
    for (string param : *this) {
        str += _separator + param;
    }
    return str;
}

string Command::customFormatCmd(std::string newSeparator, bool insertStart) {
    string str = formatCmd(insertStart);
    ReplaceAll(str, std::to_string(_separator), newSeparator);
    return str;
}

string Command::streamToString(stringstream &stream) const {
    string token;
    string ret = "";
    while (getline(stream, token))
        ret += token;
    return ret;
}

void Command::ReplaceAll(std::string &str, const std::string &from,
                         const std::string &to) {
    size_t start_pos = 0;
    while ((start_pos = str.find(from, start_pos)) != std::string::npos) {
        str.replace(start_pos, from.length(), to);
        start_pos +=
            to.length(); // Handles case where 'to' is a substring of 'from'
    }
}

int Command::lookForCmd(const string &req, string &firstCmd) {
    unsigned int i = 0;

    // Looking for _startChar
    for (; i < req.size(); i++)
        if (req[i] == startChar)
            break;
    if (i == req.size()) {
        firstCmd = "Can't parse command, missing start char or empty request.";
        return -1;
    }

    // Make a brand new string starting at _startChar
    string req2 = req.substr(i);

    // Making sure the source string is long enough
    if (req2.size() < 6) {
        firstCmd = "Can't parse command, too short string.";
        return -1;
    }

    // Extracting command size
    unsigned int sum = 0;
    try {
        sum = stoul(req2.substr(1, NB_DIGIT_CMD_SIZE));
    } catch (std::invalid_argument &e) {
        firstCmd =
            "Can't parse command, command size character can't be parsed.";
        return -1;
    }
    // Check if string is at least the size annouced
    if (1 + NB_DIGIT_CMD_SIZE + sum > req2.size()) {
        firstCmd = "Can't parse command, command size doesn't match.";
        return -1;
    }

    // Returning the extracted Cmd
    firstCmd = req2.substr(0, 1 + NB_DIGIT_CMD_SIZE + sum);

    return i;
}
