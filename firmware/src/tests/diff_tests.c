#include "merkle_merge.h"
#include "merkle_tree.h"
#include <criterion/criterion.h>
#include <stdio.h>
#define NB_MSG 8
#define DUMMY_MSG_SIZE MESSAGE_SIGNATURE_SIZE + MESSAGE_HEADER_SIZE + 1

err_t lb_exchange(lb_task_stack *p_st_src, lb_task_stack *p_st_dest) {
    memcpy(p_st_dest->buff.payload, p_st_src->buff.payload,
           p_st_src->buff.used);
    return LB_OK;
}

// This function executes a local diff between two pools, it is usd to simulate
// the diff correct results in criterion
void execute_diff(message_pool_t *p_pool1, message_pool_t *p_pool2,
                  lb_task_stack *stack1, lb_task_stack *stack2) {
    LB_ALLOC_ROOT_MD(root_md);
    LB_INIT_ROOT_MD(root_md);

    LB_ALLOC_MERKLE_TREE(root1, slot_pool1);
    LB_INIT_MERKLE_TREE(root1);
    lb_init_merkle_tree(&root1, &root_md, &slot_pool1, LB_MT_OFFSET);
    lb_attach_msgs_to_tree(&root1, p_pool1, &slot_pool1, NULL, NULL);

    lb_task_stack st1;
    lb_stack_init(&st1, &slot_pool1);

    LB_ALLOC_MERKLE_TREE(root2, slot_pool2);
    LB_INIT_MERKLE_TREE(root2);
    lb_init_merkle_tree(&root2, &root_md, &slot_pool2, LB_MT_OFFSET);
    lb_attach_msgs_to_tree(&root2, p_pool2, &slot_pool2, NULL, NULL);

    // Set shared flags to 1 on both trees
    lb_set_shared_flag(&slot_pool1);
    lb_set_shared_flag(&slot_pool2);

    lb_task_stack st2;
    lb_stack_init(&st2, &slot_pool2);

    lb_task task1;
    lb_task task2;
    st1.to_recv_count = 0;
    st2.to_recv_count = 0;

    lb_stack_push(&st1, TASK_TX | TASK_ROOT, &root1);
    lb_stack_push(&st2, TASK_RX, &root2);

    while (lb_stack_size(&st1) > 0 && lb_stack_size(&st2) > 0) {
        cr_assert(lb_stack_size(&st1) == lb_stack_size(&st2));
        lb_stack_peek(&st1, &task1.type, &task1.p_node);
        lb_stack_pop(&st1);

        lb_stack_peek(&st2, &task2.type, &task2.p_node);
        lb_stack_pop(&st2);

        if (task1.type & TASK_RX) {
            // st2 sends, st1 receives
            // printf("Device 2 -----> Device 1\n");
            cr_assert(task2.type & TASK_TX);
            lb_transmission(&st2, &task2);
            lb_exchange(&st2, &st1);
            lb_reception(&st1, &task1);
            cr_assert(st1.slot_left == st2.slot_left);
            while (st2.slot_left != 0) {
                // printf("Burst 2-> 1\n");
                // printf("Slots_left entering burst: %d\n", st2.slot_left);
                if (task2.type & TASK_BCKT) {
                    lb_burst_TX_B(&st2, task2.p_node);
                    lb_exchange(&st2, &st1);
                    lb_burst_RX_B(&st1, task1.p_node);
                } else if (task2.type & TASK_BCKT_RESP) {
                    lb_burst_TX_BR(&st2);
                    lb_exchange(&st2, &st1);
                    // lb_dump_payoad(&st1);
                    lb_burst_RX_BR(&st1, task1.p_node);
                }
                cr_assert(st1.slot_left == st2.slot_left, "%d, %d",
                          st1.slot_left, st2.slot_left);
            }
        } else if (task1.type & TASK_TX) {
            // st1 sends, st2 receives
            // printf("Device 1 -----> Device 2\n");
            cr_assert(task2.type & TASK_RX);
            lb_transmission(&st1, &task1);
            lb_exchange(&st1, &st2);
            lb_reception(&st2, &task2);
            cr_assert(st1.slot_left == st2.slot_left, "%04x, %04x",
                      st1.slot_left, st2.slot_left);
            while (st1.slot_left != 0) {
                // printf("Burst 1-> 2\n");
                // printf("Slots_left entering burst: %d\n", st1.slot_left);
                if (task1.type & TASK_BCKT) {
                    lb_burst_TX_B(&st1, task1.p_node);
                    lb_exchange(&st1, &st2);
                    // lb_dump_payload(&st1);
                    lb_burst_RX_B(&st2, task2.p_node);
                } else if (task1.type & TASK_BCKT_RESP) {
                    lb_burst_TX_BR(&st1);
                    lb_exchange(&st1, &st2);
                    lb_burst_RX_BR(&st2, task2.p_node);
                }
                cr_assert(st1.slot_left == st2.slot_left);
            }
        }
    }
    *stack1 = st1;
    *stack2 = st2;
}

// Test to verify behaviour of lb_burst_TX_BR function
Test(diff, burst_tx_br) {
    LB_ALLOC_SLOT_POOL(slot_pool1);
    lb_init_slots(&slot_pool1, LB_MT_OFFSET);

    lb_task_stack st1;
    lb_stack_init(&st1, &slot_pool1);

    int32_t indexs[] = {2, 4, 5, 6, 8, 10, 14, 67, 98};
    uint8_t buff[sizeof(indexs) + 1] = {0};
    for (unsigned int i = 0; i < sizeof(indexs) / sizeof(int32_t); i++) {
        lb_istack_push(&st1.to_recv, indexs[i]);
        memcpy(&buff[1 + i * 4], &indexs[i], sizeof(int32_t));
    }

    st1.buff.payload[0] = TASK_BCKT_RESP;
    st1.buff.used = 1;

    // register how many slots we need to require
    st1.slot_left = sizeof(indexs) / sizeof(int32_t);

    st1.i_lb = 0;
    int idx = 0;
    // While we need to send orders
    while (st1.slot_left != 0) {
        ssize_t old_slot_left = st1.slot_left;
        lb_burst_TX_BR(&st1);
        printf("\n");

        // conpare buff for each order
        for (unsigned int i = 0; i < old_slot_left - st1.slot_left; i++) {
            cr_assert(
                !memcmp(&st1.buff.payload[1 + i * sizeof(int32_t)],
                        &buff[1 + idx * sizeof(int32_t) + i * sizeof(int32_t)],
                        sizeof(int32_t)),
                "Incorrect payload!, i = %d, order idx = %d", i, idx);
        }
        idx += old_slot_left - st1.slot_left;
        cr_assert(st1.buff.payload[0] == TASK_BCKT_RESP);
    }
}

// Test to verify behaviour of lb_burst_TX_B function
Test(diff, burst_tx_b) {
    message_t p_messages[NB_MSG] = {
        //  header, payload, signature
        {{160, 19, 21, 0}, DUMMY_MSG_SIZE, {0}, {21, 22, 23, 24, 25}},
        {{512, 21, 12, 0}, DUMMY_MSG_SIZE, {1}, {11}},
        {{100, 41, 72, 0}, DUMMY_MSG_SIZE, {2}, {11, 12, 13, 14, 15}},
        {{189, 41, 21, 0}, DUMMY_MSG_SIZE, {3}, {13}},
        {{124, 72, 19, 0}, DUMMY_MSG_SIZE, {4}, {14}},
        {{415, 19, 72, 0}, DUMMY_MSG_SIZE, {5}, {15}},
        {{378, 1, 41, 0}, DUMMY_MSG_SIZE, {6}, {16}},
        {{101, 1, 41, 0}, DUMMY_MSG_SIZE, {7}, {17}}};
    message_pool_t pool = {p_messages, NB_MSG};

    LB_ALLOC_SLOT_POOL(slot_pool);
    lb_init_slots(&slot_pool, LB_MT_OFFSET);

    lb_merkle_tree leaf;
    leaf.start = LB_MT_START;
    leaf.end = LB_MT_END;
    leaf.array_id = 0;
    leaf.i_bucket_slot = -1;

    lb_update_nodes_hash(&slot_pool, &leaf);
    lb_attach_msgs_to_tree(&leaf, &pool, &slot_pool, NULL, NULL);

    int counter = 0;
    int i_slot = leaf.i_bucket_slot;

    while (i_slot != -1) {
        counter++;
        i_slot = slot_pool.slots[i_slot].i_next;
    }

    cr_assert(counter == 8);
    lb_task_stack stack;
    lb_stack_init(&stack, &slot_pool);

    stack.slot_left = counter;
    stack.i_lb = leaf.i_bucket_slot;
    i_slot = stack.i_lb;
    while (stack.slot_left != 0) {
        ssize_t old_slot_left = stack.slot_left;
        lb_burst_TX_B(&stack, &leaf);
        // old_slot_left - slot_left represent the number of hashes copied in
        // the payload after the previous iteration of lb_burst_TX_B
        for (int i = 0; i < old_slot_left - stack.slot_left; i++) {
            // Assert that we correctly copied the hases of the messages in the
            // payload
            cr_assert(!memcmp(
                &stack.buff.payload[1 + i * LB_SLOT_SIGNATURE_SIZE],
                slot_pool.slots[i_slot].p_hash, LB_SLOT_SIGNATURE_SIZE));
            i_slot = slot_pool.slots[i_slot].i_next;
        }
        // Verify that i_lb progress at the same rythm as i_slot
        cr_assert(i_slot = stack.i_lb);
    }
}

// Test to verify behaviour of the lb_burst_RX_B function with a small payload
Test(diff, burst_rx_b1) {
    message_t p_messages[4] = {
        //  header, payload, signature
        {{100, 41, 72, 0}, DUMMY_MSG_SIZE, {2}, {11, 12, 13, 14, 15, 16, 17}},
        {{101, 1, 41, 0}, DUMMY_MSG_SIZE, {7}, {17}},
        {{160, 19, 21, 0}, DUMMY_MSG_SIZE, {0}, {21, 22, 23, 24, 25}},
        {{512, 21, 12, 0}, DUMMY_MSG_SIZE, {1}, {11}}};
    message_pool_t pool = {p_messages, 4};

    LB_ALLOC_SLOT_POOL(slot_pool);
    lb_init_slots(&slot_pool, LB_MT_OFFSET);

    lb_merkle_tree leaf;
    leaf.start = LB_MT_START;
    leaf.end = LB_MT_END;
    leaf.array_id = 0;
    leaf.i_bucket_slot = -1;

    lb_update_nodes_hash(&slot_pool, &leaf);
    lb_attach_msgs_to_tree(&leaf, &pool, &slot_pool, NULL, NULL);

    int counter = 0;
    ssize_t i_slot = leaf.i_bucket_slot;

    while (i_slot != -1) {
        counter++;
        i_slot = slot_pool.slots[i_slot].i_next;
    }

    cr_assert(counter == 4);
    lb_task_stack stack;
    lb_stack_init(&stack, &slot_pool);

    // Setup as handle_B should have
    // This assume that the size of the payload is larger than 16, the code
    // will have to change otherwise, sorry not sorry
    lb_unset_shared(&leaf, &slot_pool);
    uint8_t new_payload[25] = {0, 11, 12, 13, 14, 15, 16, 17, 0, 0, 0, 0, 0, 21,
                               20, 24, 25, 0, 0, // This message should be
                               // unknown the rest should not
                               11, 0, 0, 0, 0, 0};
    memcpy(stack.buff.payload, new_payload, 25);
    stack.slot_left = 4;
    stack.i_lb = leaf.i_bucket_slot;

    while (stack.slot_left != 0) {
        lb_burst_RX_B(&stack, &leaf);
    }
    cr_expect(stack.to_recv_count == 1, "%d", stack.to_recv_count);
    cr_expect(stack.to_recv.indexs[0] == 2);
    i_slot = leaf.i_bucket_slot;
    for (int i = 0; i < 4; i++) {
        if (i != 2) {
            cr_assert(slot_pool.slots[i_slot].shared, "%d", i);
        } else {
            cr_assert(!slot_pool.slots[i_slot].shared, "%d", i);
        }
        i_slot = slot_pool.slots[i_slot].i_next;
    }
}

// Test to verify behaviour of the lb_burst_RX_B function with payload in
// multiple parts
Test(diff, rx_b2) {
    message_t p_messages[12] = {//  header, payload, signature
                                {{1, 41, 72, 0}, DUMMY_MSG_SIZE, {2}, {0x01}},
                                {{2, 41, 72, 0}, DUMMY_MSG_SIZE, {2}, {0x02}},
                                {{3, 41, 72, 0}, DUMMY_MSG_SIZE, {2}, {0x03}},
                                {{4, 10, 41, 0}, DUMMY_MSG_SIZE, {7}, {0x04}},
                                {{5, 10, 41, 0}, DUMMY_MSG_SIZE, {7}, {0x05}},
                                {{6, 10, 41, 0}, DUMMY_MSG_SIZE, {7}, {0x06}},
                                {{7, 19, 21, 0}, DUMMY_MSG_SIZE, {0}, {0x07}},
                                {{8, 19, 21, 0}, DUMMY_MSG_SIZE, {0}, {0x08}},
                                {{9, 19, 21, 0}, DUMMY_MSG_SIZE, {0}, {0x09}},
                                {{10, 21, 12, 0}, DUMMY_MSG_SIZE, {1}, {0x0a}},
                                {{11, 21, 12, 0}, DUMMY_MSG_SIZE, {1}, {0x0b}},
                                {{12, 21, 12, 0}, DUMMY_MSG_SIZE, {1}, {0x0c}}};
    message_pool_t pool = {p_messages, 12};

    LB_ALLOC_SLOT_POOL(slot_pool);
    lb_init_slots(&slot_pool, LB_MT_OFFSET);

    lb_merkle_tree leaf;
    leaf.start = LB_MT_START;
    leaf.end = LB_MT_END;
    leaf.array_id = 0;
    leaf.i_bucket_slot = -1;

    lb_update_nodes_hash(&slot_pool, &leaf);
    lb_attach_msgs_to_tree(&leaf, &pool, &slot_pool, NULL, NULL);

    int counter = 0;
    ssize_t i_slot = leaf.i_bucket_slot;

    while (i_slot != -1) {
        counter++;
        i_slot = slot_pool.slots[i_slot].i_next;
    }

    cr_assert(counter == MIN(12, LB_NB_MAX_MSGS));
    lb_task_stack stack;
    lb_stack_init(&stack, &slot_pool);

    // Setup as handle_B should have
    // This test assumes that the number of children is 8. It will have to
    // change if the number of children does
    lb_unset_shared(&leaf, &slot_pool);
    uint8_t first_payload[LB_STACK_PLD_SIZE] = {
        0x01, 0x42, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x04, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x05, 0x00, 0x00, 0x00, 0x00, 0x00, 0x06, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00};
    memcpy(stack.buff.payload, first_payload, LB_STACK_SIZE);
    stack.slot_left = 12;
    stack.i_lb = leaf.i_bucket_slot;
    lb_burst_RX_B(&stack, &leaf);
    uint8_t second_payload[LB_STACK_PLD_SIZE] = {
        0x01, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x09, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x1a, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0b, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x0c, 0x00, 0x00, 0x00, 0x00, 0x00,
    };
    cr_expect(stack.slot_left == 5);
    memcpy(stack.buff.payload, second_payload, LB_STACK_SIZE);
    lb_burst_RX_B(&stack, &leaf);
    cr_expect(stack.to_recv_count == 2, " To receive count: %d instead of 2",
              stack.to_recv_count);
    cr_expect(stack.to_recv.indexs[0] == 0);
    cr_expect(stack.to_recv.indexs[1] == 9);
    cr_expect(stack.slot_left == 0);
    i_slot = leaf.i_bucket_slot;
    for (int i = 0; i < counter; i++) {
        if (i == 0 || i == 9) {
            cr_expect(!slot_pool.slots[i_slot].shared,
                      "%d should not be shared", i);
        } else {
            cr_expect(slot_pool.slots[i_slot].shared, "%d should be shared", i);
        }
        i_slot = slot_pool.slots[i_slot].i_next;
    }
    cr_expect(i_slot == -1);
}

Test(diff, diff_pool0) {
    message_t p_messages1[8] = {
        //  header, payload, signature
        {{160, 19, 21, 0}, DUMMY_MSG_SIZE, {0}, {21, 22, 23, 24, 25}},
        {{512, 21, 12, 0}, DUMMY_MSG_SIZE, {1}, {11}},
        {{100, 41, 72, 0}, DUMMY_MSG_SIZE, {2}, {11, 12, 13, 14, 15}},
        {{189, 41, 21, 0}, DUMMY_MSG_SIZE, {3}, {13}},
        {{124, 72, 19, 0}, DUMMY_MSG_SIZE, {4}, {14}},
        {{415, 19, 72, 0}, DUMMY_MSG_SIZE, {5}, {15}},
        {{378, 1, 41, 0}, DUMMY_MSG_SIZE, {6}, {16}},
        {{101, 1, 41, 0}, DUMMY_MSG_SIZE, {7}, {17}}};
    message_pool_t pool1 = {p_messages1, 8};
    (void)pool1;

    message_t p_messages2[8] = {
        //  header, payload, signature
        {{160, 19, 21, 0}, DUMMY_MSG_SIZE, {0}, {21, 22, 23, 24, 25}},
        {{512, 21, 12, 0}, DUMMY_MSG_SIZE, {1}, {11}},
        {{100, 41, 72, 0}, DUMMY_MSG_SIZE, {2}, {11, 12, 13, 14, 15}},
        {{189, 41, 21, 0}, DUMMY_MSG_SIZE, {3}, {13}},
        {{124, 72, 19, 0}, DUMMY_MSG_SIZE, {4}, {14}},
        {{415, 19, 72, 0}, DUMMY_MSG_SIZE, {5}, {15}},
        {{378, 1, 41, 0}, DUMMY_MSG_SIZE, {6}, {16}},
        {{101, 1, 41, 0}, DUMMY_MSG_SIZE, {7}, {17}}};
    message_pool_t pool2 = {p_messages2, 8};
    (void)pool2;

    lb_task_stack stack1;
    lb_task_stack stack2;

    execute_diff(&pool1, &pool2, &stack1, &stack2);

    uint32_t send_count1;
    lb_get_send_count(stack1.p_pool, &send_count1);
    uint32_t send_count2;
    lb_get_send_count(stack2.p_pool, &send_count2);
    cr_assert(send_count1 == 0);
    cr_assert(send_count2 == 0);
}

Test(diff, diff_pool1) {
    message_t p_messages1[8 * 2] = {
        //  header, payload, signature
        {{160, 19, 21, 0}, DUMMY_MSG_SIZE, {0}, {21, 22, 23, 24, 25}},
        {{512, 21, 12, 0}, DUMMY_MSG_SIZE, {1}, {11}},
        {{100, 41, 72, 0}, DUMMY_MSG_SIZE, {2}, {11, 12, 13, 14, 15}},
        {{189, 41, 21, 0}, DUMMY_MSG_SIZE, {3}, {13}},
        {{124, 72, 19, 0}, DUMMY_MSG_SIZE, {4}, {14}},
        {{415, 19, 72, 0}, DUMMY_MSG_SIZE, {5}, {15}},
        {{378, 1, 41, 0}, DUMMY_MSG_SIZE, {6}, {16}},
        {{101, 1, 41, 0}, DUMMY_MSG_SIZE, {7}, {17}},
        {{382, 19, 21, 0}, DUMMY_MSG_SIZE, {26}, {1, 2, 3, 4, 5}},
        {{388, 21, 12, 0}, DUMMY_MSG_SIZE, {1}, {11}},
        {{301, 41, 72, 0}, DUMMY_MSG_SIZE, {2}, {12}},
        {{339, 41, 21, 0}, DUMMY_MSG_SIZE, {3}, {13}},
        {{378, 72, 19, 0}, DUMMY_MSG_SIZE, {4}, {14}},
        {{398, 19, 72, 0}, DUMMY_MSG_SIZE, {5}, {15}},
        {{324, 1, 41, 0}, DUMMY_MSG_SIZE, {6}, {16}},
        {{110, 1, 41, 0}, DUMMY_MSG_SIZE, {7}, {17}}};
    message_pool_t pool1 = {p_messages1, 8 * 2};
    (void)pool1;

    message_t p_messages2[0] = {
        //  header, payload, signature
    };
    message_pool_t pool2 = {p_messages2, 0};
    (void)pool2;

    lb_task_stack stack1;
    lb_task_stack stack2;

    execute_diff(&pool1, &pool2, &stack1, &stack2);

    uint32_t send_count1;
    lb_get_send_count(stack1.p_pool, &send_count1);
    uint32_t send_count2;
    lb_get_send_count(stack2.p_pool, &send_count2);
    cr_assert(send_count1 == 16, "send_count1 = %d", send_count1);
    cr_assert(send_count2 == 0, "send_count2 = %d", send_count2);
}

Test(diff, diff_pool2) {
    message_t p_messages1[7] = {//  header, payload, signature
                                {{1, 41, 72, 0}, DUMMY_MSG_SIZE, {2}, {0x01}},
                                {{3, 41, 72, 0}, DUMMY_MSG_SIZE, {2}, {0x03}},
                                {{4, 10, 41, 0}, DUMMY_MSG_SIZE, {7}, {0x04}},
                                {{5, 10, 41, 0}, DUMMY_MSG_SIZE, {7}, {0x05}},
                                {{6, 10, 41, 0}, DUMMY_MSG_SIZE, {7}, {0x06}},
                                {{7, 19, 21, 0}, DUMMY_MSG_SIZE, {0}, {0x07}},
                                {{8, 19, 21, 0}, DUMMY_MSG_SIZE, {0}, {0x08}}};
    message_pool_t pool1 = {p_messages1, 7};

    message_t p_messages2[8] = {//  header, payload, signature
                                {{1, 41, 72, 0}, DUMMY_MSG_SIZE, {2}, {0x01}},
                                {{2, 41, 72, 0}, DUMMY_MSG_SIZE, {2}, {0x02}},
                                {{3, 41, 72, 0}, DUMMY_MSG_SIZE, {2}, {0x03}},
                                {{4, 10, 41, 0}, DUMMY_MSG_SIZE, {7}, {0x04}},
                                {{5, 10, 41, 0}, DUMMY_MSG_SIZE, {7}, {0x05}},
                                {{6, 10, 41, 0}, DUMMY_MSG_SIZE, {7}, {0x06}},
                                {{7, 19, 21, 0}, DUMMY_MSG_SIZE, {0}, {0x07}},
                                {{8, 19, 21, 0}, DUMMY_MSG_SIZE, {0}, {0x08}}};
    message_pool_t pool2 = {p_messages2, 8};

    lb_task_stack stack1;
    lb_task_stack stack2;

    execute_diff(&pool1, &pool2, &stack1, &stack2);

    uint32_t send_count1;
    lb_get_send_count(stack1.p_pool, &send_count1);
    uint32_t send_count2;
    lb_get_send_count(stack2.p_pool, &send_count2);
    cr_assert(send_count1 == 0);
    cr_assert(send_count2 == 1);
}

Test(diff, diff_pool3) {
    message_t p_messages1[NB_MSG] = {
        //  header, payload, signature
        {{1, 41, 72, 0}, DUMMY_MSG_SIZE, {2}, {0x01}},
        {{2, 41, 72, 0}, DUMMY_MSG_SIZE, {2}, {0x02}},
        {{3, 41, 72, 0}, DUMMY_MSG_SIZE, {2}, {0x03}},
        {{4, 10, 41, 0}, DUMMY_MSG_SIZE, {7}, {0x04}},
        {{5, 10, 41, 0}, DUMMY_MSG_SIZE, {7}, {0x05}},
        {{6, 10, 41, 0}, DUMMY_MSG_SIZE, {7}, {0x06}},
        {{7, 19, 21, 0}, DUMMY_MSG_SIZE, {0}, {0x07}},
        {{8, 19, 21, 0}, DUMMY_MSG_SIZE, {0}, {0x08}}};
    message_pool_t pool1 = {p_messages1, NB_MSG};

    message_t p_messages2[13] = {
        //  header, payload, signature
        {{382, 19, 21, 0}, DUMMY_MSG_SIZE, {26}, {1, 2, 3, 4, 5}},
        {{398, 19, 72, 0}, DUMMY_MSG_SIZE, {5}, {15}},
        {{1, 41, 72, 0}, DUMMY_MSG_SIZE, {2}, {0x01}},
        {{2, 41, 72, 0}, DUMMY_MSG_SIZE, {2}, {0x02}},
        {{3, 41, 72, 0}, DUMMY_MSG_SIZE, {2}, {0x03}},
        {{4, 10, 41, 0}, DUMMY_MSG_SIZE, {7}, {0x04}},
        {{1382, 19, 21, 0}, DUMMY_MSG_SIZE, {26}, {21, 2, 3, 4, 5}},
        {{1388, 21, 12, 0}, DUMMY_MSG_SIZE, {1}, {111}},
        {{130001, 41, 72, 0}, DUMMY_MSG_SIZE, {2}, {12}},
        {{1339, 41, 21, 0}, DUMMY_MSG_SIZE, {3}, {243}},
        {{13978, 72, 19, 0}, DUMMY_MSG_SIZE, {4}, {14}},
        {{1398, 19, 72, 0}, DUMMY_MSG_SIZE, {5}, {15}},
        {{51110, 1, 41, 0}, DUMMY_MSG_SIZE, {7}, {17}}};
    message_pool_t pool2 = {p_messages2, 13};

    lb_task_stack stack1;
    lb_task_stack stack2;

    execute_diff(&pool1, &pool2, &stack1, &stack2);

    uint32_t send_count1;
    lb_get_send_count(stack1.p_pool, &send_count1);
    uint32_t send_count2;
    lb_get_send_count(stack2.p_pool, &send_count2);
    cr_assert(send_count1 == 4);
    cr_assert(send_count2 == 9);
}

Test(diff, diff_pool4) {
    message_t p_messages1[NB_MSG] = {
        //  header, payload, signature
        {{1, 41, 72, 0}, DUMMY_MSG_SIZE, {2}, {0x01}},
        {{2, 41, 72, 0}, DUMMY_MSG_SIZE, {2}, {0x02}},
        {{3, 41, 72, 0}, DUMMY_MSG_SIZE, {2}, {0x03}},
        {{4, 10, 41, 0}, DUMMY_MSG_SIZE, {7}, {0x04}},
        {{5, 10, 41, 0}, DUMMY_MSG_SIZE, {7}, {0x05}},
        {{6, 10, 41, 0}, DUMMY_MSG_SIZE, {7}, {0x06}},
        {{7, 19, 21, 0}, DUMMY_MSG_SIZE, {0}, {0x07}},
        {{8, 19, 21, 0}, DUMMY_MSG_SIZE, {0}, {0x08}}};
    message_pool_t pool1 = {p_messages1, NB_MSG};
    (void)pool1;

    message_t p_messages2[NB_MSG] = {
        //  header, payload, signature
        {{382, 19, 21, 0}, DUMMY_MSG_SIZE, {26}, {1, 2, 3, 4, 5}},
        {{388, 21, 12, 0}, DUMMY_MSG_SIZE, {1}, {11}},
        {{301, 41, 72, 0}, DUMMY_MSG_SIZE, {2}, {12}},
        {{339, 41, 21, 0}, DUMMY_MSG_SIZE, {3}, {13}},
        {{378, 72, 19, 0}, DUMMY_MSG_SIZE, {4}, {14}},
        {{398, 19, 72, 0}, DUMMY_MSG_SIZE, {5}, {15}},
        {{324, 1, 41, 0}, DUMMY_MSG_SIZE, {6}, {16}},
        {{110, 1, 41, 0}, DUMMY_MSG_SIZE, {7}, {17}}};
    message_pool_t pool2 = {p_messages2, NB_MSG};
    (void)pool2;

    lb_task_stack stack1;
    lb_task_stack stack2;

    execute_diff(&pool1, &pool2, &stack1, &stack2);

    uint32_t send_count1;
    lb_get_send_count(stack1.p_pool, &send_count1);
    uint32_t send_count2;
    lb_get_send_count(stack2.p_pool, &send_count2);
    cr_assert(send_count1 == 8);
    cr_assert(send_count2 == 8);
}

Test(diff, diff_pool5) {
    message_t p_messages1[NB_MSG] = {
        //  header, payload, signature
        {{1, 41, 72, 0}, DUMMY_MSG_SIZE, {2}, {0x01}},
        {{2, 41, 72, 0}, DUMMY_MSG_SIZE, {2}, {0x02}},
        {{3, 41, 72, 0}, DUMMY_MSG_SIZE, {2}, {0x03}},
        {{4, 10, 41, 0}, DUMMY_MSG_SIZE, {7}, {0x04}},
        {{5, 10, 41, 0}, DUMMY_MSG_SIZE, {7}, {0x05}},
        {{6, 10, 41, 0}, DUMMY_MSG_SIZE, {7}, {0x06}},
        {{7, 19, 21, 0}, DUMMY_MSG_SIZE, {0}, {0x07}},
        {{8, 19, 21, 0}, DUMMY_MSG_SIZE, {0}, {0x08}}};
    message_pool_t pool1 = {p_messages1, NB_MSG};
    (void)pool1;

    message_t p_messages2[NB_MSG] = {
        //  header, payload, signature
        {{1, 41, 72, 0}, DUMMY_MSG_SIZE, {2}, {0x42}},
        {{2, 41, 72, 0}, DUMMY_MSG_SIZE, {2}, {0x43}},
        {{3, 41, 72, 0}, DUMMY_MSG_SIZE, {2}, {0x03}},
        {{4, 10, 41, 0}, DUMMY_MSG_SIZE, {7}, {0x04}},
        {{5, 10, 41, 0}, DUMMY_MSG_SIZE, {7}, {0x05}},
        {{6, 10, 41, 0}, DUMMY_MSG_SIZE, {7}, {0x06}},
        {{7, 19, 21, 0}, DUMMY_MSG_SIZE, {0}, {0x07}},
        {{8, 19, 21, 0}, DUMMY_MSG_SIZE, {0}, {0x08}}};
    message_pool_t pool2 = {p_messages2, NB_MSG};
    (void)pool2;

    lb_task_stack stack1;
    lb_task_stack stack2;

    execute_diff(&pool1, &pool2, &stack1, &stack2);

    uint32_t send_count1;
    lb_get_send_count(stack1.p_pool, &send_count1);
    uint32_t send_count2;
    lb_get_send_count(stack2.p_pool, &send_count2);
    cr_assert(send_count1 == 2);
    cr_assert(send_count2 == 2);
}

Test(diff, diff_pool6) {
    // Building a huge message pool
    message_t p_messages1[LB_NB_MAX_MSGS];
    for (unsigned int i = 0; i < LB_NB_MAX_MSGS; i++) {
        message_t message = {{LB_MT_START, 0, 0, 0}, DUMMY_MSG_SIZE, {0}, {0}};
        memcpy(message.signature, &i, sizeof(i));
        p_messages1[i] = message;
    }
    message_pool_t pool1 = {p_messages1, LB_NB_MAX_MSGS};

    message_t p_messages2[NB_MSG] = {
        //  header, payload, signature
        {{1, 41, 72, 0}, DUMMY_MSG_SIZE, {2}, {0x42, 0xff, 0xff, 0xf1}},
        {{2, 41, 72, 0}, DUMMY_MSG_SIZE, {2}, {0x43, 0xff, 0xff, 0xf1}},
        {{3, 41, 72, 0}, DUMMY_MSG_SIZE, {2}, {0x03, 0xff, 0xff, 0xf1}},
        {{4, 10, 41, 0}, DUMMY_MSG_SIZE, {7}, {0x04, 0xff, 0xff, 0xf1}},
        {{5, 10, 41, 0}, DUMMY_MSG_SIZE, {7}, {0x05, 0xff, 0xff, 0xf1}},
        {{6, 10, 41, 0}, DUMMY_MSG_SIZE, {7}, {0x06, 0xff, 0xff, 0xf1}},
        {{7, 19, 21, 0}, DUMMY_MSG_SIZE, {0}, {0x07, 0xff, 0xff, 0xf1}},
        {{8, 19, 21, 0}, DUMMY_MSG_SIZE, {0}, {0x08, 0xff, 0xff, 0xf1}}};
    message_pool_t pool2 = {p_messages2, NB_MSG};
    (void)pool2;

    lb_task_stack stack1;
    lb_task_stack stack2;

    execute_diff(&pool1, &pool2, &stack1, &stack2);

    int counter = 0;
    ssize_t i = stack1.p_pool->i_begin_busy;
    while (i != -1) {
        counter++;
        i = stack1.p_pool->slots[i].i_next;
    }

    cr_assert(counter == LB_NB_MAX_MSGS);

    uint32_t send_count1;
    lb_get_send_count(stack1.p_pool, &send_count1);
    uint32_t send_count2;
    lb_get_send_count(stack2.p_pool, &send_count2);
    cr_expect(send_count1 == LB_NB_MAX_MSGS, "Message count: %d instead of %d",
              send_count1, LB_NB_MAX_MSGS);
    cr_expect(send_count2 == 8);
}
