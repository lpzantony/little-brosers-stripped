#include "merkle_tree.h"
#include "slot_pool.h"
#include <criterion/criterion.h>

#define DUMMY_MSG_SIZE MESSAGE_SIGNATURE_SIZE + MESSAGE_HEADER_SIZE + 1

// Returns true if slot1's timestamp is strictly smaller than slot2's or if the
// timestamps are equal but slot1's signature is lexicographically strictly
// smaller than slot2's signature
bool compare_messages(lb_slot *slot1, lb_slot *slot2) {
    if (slot1->timestamp == slot2->timestamp) {
        // Comparing signature hash
        for (int i = 0; i < SHA256_BLOCK_SIZE; i++) {
            if (slot1->p_hash[i] != slot2->p_hash[i]) {
                return slot1->p_hash[i] < slot2->p_hash[i];
            }
        }
        return false;
    } else {
        return slot1->timestamp < slot2->timestamp;
    }
}

// Small message pool with no duplicate

// Verify that after inserting 8 messages in an empty tree, there are 8 messages
// in the slot pool
Test(simple_slot_pool, message_count) {
    message_t p_messages1[8] = {//  header, payload, signature
                                {{382, 19, 21, 0}, DUMMY_MSG_SIZE, {26}, {10}},
                                {{388, 21, 12, 0}, DUMMY_MSG_SIZE, {1}, {11}},
                                {{301, 41, 72, 0}, DUMMY_MSG_SIZE, {2}, {12}},
                                {{339, 41, 21, 0}, DUMMY_MSG_SIZE, {3}, {13}},
                                {{378, 72, 19, 0}, DUMMY_MSG_SIZE, {4}, {14}},
                                {{398, 19, 72, 0}, DUMMY_MSG_SIZE, {5}, {15}},
                                {{324, 1, 41, 0}, DUMMY_MSG_SIZE, {6}, {16}},
                                {{110, 1, 41, 0}, DUMMY_MSG_SIZE, {7}, {17}}};

    message_pool_t message_pool = {p_messages1, 8};

    LB_ALLOC_ROOT_MD(root_md);
    LB_INIT_ROOT_MD(root_md);
    LB_ALLOC_MERKLE_TREE(root, slot_pool);
    LB_INIT_MERKLE_TREE(root);
    lb_init_merkle_tree(&root, &root_md, &slot_pool, LB_MT_OFFSET);
    lb_attach_msgs_to_tree(&root, &message_pool, &slot_pool, NULL, NULL);

    ssize_t i_slot = slot_pool.i_begin_busy;
    int counter = 0;
    cr_expect(i_slot != -1);
    while (i_slot != -1) {
        counter++;
        lb_slot *p_slot = &slot_pool.slots[i_slot];
        i_slot = p_slot->i_next;
    }
    cr_expect(counter == MIN(8, LB_NB_MAX_MSGS));
}

// Verify that after inserting messages in an empty tree, messages are correctly
// ordered in the slot pool
Test(simple_slot_pool, message_order) {
    message_t p_messages[8] = {//  header, payload, signature
                               {{382, 19, 21, 0}, DUMMY_MSG_SIZE, {26}, {10}},
                               {{388, 21, 12, 0}, DUMMY_MSG_SIZE, {1}, {11}},
                               {{301, 41, 72, 0}, DUMMY_MSG_SIZE, {2}, {12}},
                               {{339, 41, 21, 0}, DUMMY_MSG_SIZE, {3}, {13}},
                               {{378, 72, 19, 0}, DUMMY_MSG_SIZE, {4}, {14}},
                               {{398, 19, 72, 0}, DUMMY_MSG_SIZE, {5}, {15}},
                               {{324, 1, 41, 0}, DUMMY_MSG_SIZE, {6}, {16}},
                               {{110, 1, 41, 0}, DUMMY_MSG_SIZE, {7}, {17}}};

    message_pool_t message_pool = {p_messages, 8};

    LB_ALLOC_ROOT_MD(root_md);
    LB_INIT_ROOT_MD(root_md);
    LB_ALLOC_MERKLE_TREE(root, slot_pool);
    LB_INIT_MERKLE_TREE(root);
    lb_init_merkle_tree(&root, &root_md, &slot_pool, LB_MT_OFFSET);
    lb_attach_msgs_to_tree(&root, &message_pool, &slot_pool, NULL, NULL);

    ssize_t i_slot = slot_pool.i_begin_busy;
    cr_expect(i_slot != -1);

    while (slot_pool.slots[i_slot].i_next != -1) {
        lb_slot *p_slot = &slot_pool.slots[i_slot];
        lb_slot *p_next = &slot_pool.slots[p_slot->i_next];
        cr_expect(compare_messages(p_slot, p_next));
        i_slot = p_slot->i_next;
    }
}

// The tests are repeated with other message pools

// Big message pool with no duplicate, the message pool is bigger than the
// internal memory
Test(big_slot_pool, message_count) {
    message_t p_messages[LB_NB_MAX_MSGS + 1];
    for (int i = 0; i < LB_NB_MAX_MSGS + 1; i++) {
        message_t message = {{LB_MT_OFFSET + LB_MT_START + i, 0, 0, 0},
                             DUMMY_MSG_SIZE,
                             {0},
                             {0}};
        p_messages[i] = message;
    }

    message_pool_t message_pool = {p_messages, LB_NB_MAX_MSGS + 1};
    LB_ALLOC_ROOT_MD(root_md);
    LB_INIT_ROOT_MD(root_md);
    LB_ALLOC_MERKLE_TREE(root, slot_pool);
    LB_INIT_MERKLE_TREE(root);
    lb_init_merkle_tree(&root, &root_md, &slot_pool, LB_MT_OFFSET);
    lb_attach_msgs_to_tree(&root, &message_pool, &slot_pool, NULL, NULL);

    ssize_t i_slot = slot_pool.i_begin_busy;
    int counter = 0;
    cr_expect(i_slot != -1);
    while (i_slot != -1) {
        counter++;
        lb_slot *p_slot = &slot_pool.slots[i_slot];
        i_slot = p_slot->i_next;
    }
    cr_expect(counter == LB_NB_MAX_MSGS, "got %d", counter);
}

Test(big_slot_pool, message_order) {
    message_t p_messages[LB_NB_MAX_MSGS + 1];
    for (int i = 0; i < LB_NB_MAX_MSGS + 1; i++) {
        message_t message = {
            {LB_MT_START + i, 0, 0, 0}, DUMMY_MSG_SIZE, {0}, {0}};
        p_messages[i] = message;
    }

    message_pool_t message_pool = {p_messages, LB_NB_MAX_MSGS + 1};
    LB_ALLOC_ROOT_MD(root_md);
    LB_INIT_ROOT_MD(root_md);
    LB_ALLOC_MERKLE_TREE(root, slot_pool);
    LB_INIT_MERKLE_TREE(root);
    lb_init_merkle_tree(&root, &root_md, &slot_pool, LB_MT_OFFSET);
    lb_attach_msgs_to_tree(&root, &message_pool, &slot_pool, NULL, NULL);

    ssize_t i_slot = slot_pool.i_begin_busy;
    cr_expect(i_slot != -1);

    while (slot_pool.slots[i_slot].i_next != -1) {
        lb_slot *p_slot = &slot_pool.slots[i_slot];
        lb_slot *p_next = &slot_pool.slots[p_slot->i_next];
        cr_expect(compare_messages(p_slot, p_next), "%d", i_slot);
        i_slot = p_slot->i_next;
    }
}

// Message pool with duplicates
Test(duplicate_slot_pool, message_count) {
    message_t p_messages1[8] = {//  header, payload, signature
                                {{333, 21, 12, 0}, DUMMY_MSG_SIZE, {1}, {10}},
                                {{333, 21, 12, 0}, DUMMY_MSG_SIZE, {1}, {10}},
                                {{301, 41, 72, 0}, DUMMY_MSG_SIZE, {2}, {12}},
                                {{339, 41, 21, 0}, DUMMY_MSG_SIZE, {3}, {13}},
                                {{378, 72, 19, 0}, DUMMY_MSG_SIZE, {4}, {14}},
                                {{398, 19, 72, 0}, DUMMY_MSG_SIZE, {5}, {15}},
                                {{324, 1, 41, 0}, DUMMY_MSG_SIZE, {6}, {16}},
                                {{110, 1, 41, 0}, DUMMY_MSG_SIZE, {7}, {17}}};

    message_pool_t message_pool = {p_messages1, 8};
    LB_ALLOC_ROOT_MD(root_md);
    LB_INIT_ROOT_MD(root_md);
    LB_ALLOC_MERKLE_TREE(root, slot_pool);
    LB_INIT_MERKLE_TREE(root);
    lb_init_merkle_tree(&root, &root_md, &slot_pool, LB_MT_OFFSET);
    lb_attach_msgs_to_tree(&root, &message_pool, &slot_pool, NULL, NULL);

    ssize_t i_slot = slot_pool.i_begin_busy;
    int counter = 0;
    cr_expect(i_slot != -1);
    while (i_slot != -1) {
        counter++;
        lb_slot *p_slot = &slot_pool.slots[i_slot];
        i_slot = p_slot->i_next;
    }
    cr_expect(counter == MIN(7, LB_NB_MAX_MSGS));
}

// Attaching twice the same pool should not change the number of attached
// messages
Test(duplicate_slot_pool, add_twice_pool) {
    message_t p_messages1[8] = {//  header, payload, signature
                                {{382, 19, 21, 0}, DUMMY_MSG_SIZE, {26}, {10}},
                                {{388, 21, 12, 0}, DUMMY_MSG_SIZE, {1}, {11}},
                                {{301, 41, 72, 0}, DUMMY_MSG_SIZE, {2}, {12}},
                                {{339, 41, 21, 0}, DUMMY_MSG_SIZE, {3}, {13}},
                                {{378, 72, 19, 0}, DUMMY_MSG_SIZE, {4}, {14}},
                                {{398, 19, 72, 0}, DUMMY_MSG_SIZE, {5}, {15}},
                                {{324, 1, 41, 0}, DUMMY_MSG_SIZE, {6}, {16}},
                                {{110, 1, 41, 0}, DUMMY_MSG_SIZE, {7}, {17}}};

    message_pool_t message_pool = {p_messages1, 8};

    LB_ALLOC_ROOT_MD(root_md);
    LB_INIT_ROOT_MD(root_md);
    LB_ALLOC_MERKLE_TREE(root, slot_pool);
    LB_INIT_MERKLE_TREE(root);
    lb_init_merkle_tree(&root, &root_md, &slot_pool, LB_MT_OFFSET);

    lb_attach_msgs_to_tree(&root, &message_pool, &slot_pool, NULL, NULL);
    // Adding message pool a second time
    lb_attach_msgs_to_tree(&root, &message_pool, &slot_pool, NULL, NULL);

    ssize_t i_slot = slot_pool.i_begin_busy;
    int counter = 0;
    cr_expect(i_slot != -1);
    while (i_slot != -1) {
        counter++;
        lb_slot *p_slot = &slot_pool.slots[i_slot];
        i_slot = p_slot->i_next;
    }
    cr_expect(counter == MIN(8, LB_NB_MAX_MSGS));
}
