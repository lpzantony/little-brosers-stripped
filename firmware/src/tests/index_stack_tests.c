#include "index_stack.h"
#include <criterion/criterion.h>
#include <stdio.h>
#define NB_INDEX 3

Test(index_stack, push) {

    err_t retval;
    lb_index_stack st;
    lb_istack_init(&st);

    ssize_t indexs[NB_INDEX] = {185, 45, 34};

    for (int i = 0; i < NB_INDEX; i++) {
        retval = lb_istack_push(&st, indexs[i]);
        cr_assert(retval == LB_OK, "lb_istack_push an error code : %d", retval);
        cr_assert(lb_istack_size(&st) == i + 1,
                  "Stack size incorrect. %d instead of %d", lb_istack_size(&st),
                  i + 1);
    }
    cr_assert(lb_istack_size(&st) == NB_INDEX,
              "Stack size incorrect. %d instead of %d", lb_istack_size(&st),
              NB_INDEX);
}

Test(index_stack, pop) {
    err_t retval;
    lb_index_stack st;
    lb_istack_init(&st);

    ssize_t indexs[NB_INDEX] = {185, 45, 34};

    for (int i = 0; i < NB_INDEX; i++) {
        lb_istack_push(&st, indexs[i]);
    }
    for (int i = 1; i <= NB_INDEX; i++) {
        retval = lb_istack_pop(&st);
        cr_assert(retval == LB_OK, "lb_istack_pop an error code : %d", retval);
        cr_assert(lb_istack_size(&st) == NB_INDEX - i,
                  "Stack size incorrect. %d instead of %d", lb_istack_size(&st),
                  NB_INDEX - i);
    }
}

Test(index_stack, peek) {
    lb_index_stack st;
    lb_istack_init(&st);

    ssize_t indexs[NB_INDEX] = {185, 45, 34};

    for (int i = 0; i < NB_INDEX; i++) {
        lb_istack_push(&st, indexs[i]);
    }
    err_t retval;
    int32_t index;
    for (int i = 1; i <= NB_INDEX; i++) {
        retval = lb_istack_peek(&st, &index);
        cr_assert(retval == LB_OK, "lb_istack_peek an error code : %d", retval);
        cr_assert(index == indexs[NB_INDEX - i],
                  "Task's type peeked is incorrect. %d instead of %d", index,
                  indexs[NB_INDEX - i]);
        lb_istack_pop(&st);
    }
}

Test(index_stack, empty) {
    err_t retval;
    lb_index_stack st;
    lb_istack_init(&st);

    ssize_t indexs[NB_INDEX] = {185, 45, 34};

    for (int i = 0; i < NB_INDEX; i++) {
        lb_istack_push(&st, indexs[i]);
    }
    for (int i = 1; i <= NB_INDEX; i++) {
        lb_istack_pop(&st);
    }
    retval = lb_istack_pop(&st);
    cr_assert(
        retval == LB_STACK_EMPTY,
        "Stack should be empty,return value incorrect. %d instead of %d\n",
        retval, LB_STACK_EMPTY);
}

Test(index_stack, full) {
    err_t retval;
    lb_index_stack st;
    lb_istack_init(&st);

    ssize_t indexs[NB_INDEX] = {185, 45, 34};

    for (int i = 0; i < LB_NB_MAX_MSGS; i++) {
        lb_istack_push(&st, indexs[i % NB_INDEX]);
    }
    retval = lb_istack_push(&st, indexs[0]);
    cr_assert(retval == LB_STACK_FULL,
              "Stack should be full,return value incorrect. %d instead of %d\n",
              retval, LB_STACK_FULL);
}
