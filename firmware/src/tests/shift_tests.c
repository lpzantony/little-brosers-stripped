//#include "merkle_merge.h"
#include "merkle_tree.h"
#include <criterion/criterion.h>
#include <stdio.h>
#define NB_MSG 8
#define DUMMY_MSG_SIZE MESSAGE_SIGNATURE_SIZE + MESSAGE_HEADER_SIZE + 1

Test(shift, shift1) {
    LB_ALLOC_ROOT_MD(root_md);
    LB_INIT_ROOT_MD(root_md);
    message_t p_messages1[8] = {
        //  header, payload, signature
        {{/*LB_MT_OFFSET + */ 160, 19, 21, 0}, DUMMY_MSG_SIZE, {0}, {0}},
        {{/*LB_MT_OFFSET + */ 512, 21, 12, 0}, DUMMY_MSG_SIZE, {1}, {1}},
        {{/*LB_MT_OFFSET + */ 100, 41, 72, 0}, DUMMY_MSG_SIZE, {2}, {2}},
        {{/*LB_MT_OFFSET + */ 189, 41, 21, 0}, DUMMY_MSG_SIZE, {3}, {3}},
        {{/*LB_MT_OFFSET + */ 124, 72, 19, 0}, DUMMY_MSG_SIZE, {4}, {4}},
        {{/*LB_MT_OFFSET + */ 415, 19, 72, 0}, DUMMY_MSG_SIZE, {5}, {5}},
        {{/*LB_MT_OFFSET + */ 378, 1, 41, 0}, DUMMY_MSG_SIZE, {6}, {6}},
        {{/*LB_MT_OFFSET + */ 101, 1, 41, 0}, DUMMY_MSG_SIZE, {7}, {7}}};
    message_pool_t pool1 = {p_messages1, 8};
    (void)pool1;

    LB_ALLOC_MERKLE_TREE(root, slot_pool);
    LB_INIT_MERKLE_TREE(root);
    lb_init_merkle_tree(&root, &root_md, &slot_pool, LB_MT_OFFSET);
    lb_attach_msgs_to_tree(&root, &pool1, &slot_pool, NULL, NULL);

    int counter = 0;
    ssize_t i_slot = slot_pool.i_begin_busy;

    while (i_slot != -1) {
        counter++;
        i_slot = slot_pool.slots[i_slot].i_next;
    }
    cr_assert(counter == MIN(8, LB_NB_MAX_MSGS));

    lb_shift_tree(&root, &slot_pool, slot_pool.certif_date + 102, NULL, NULL);

    counter = 0;
    i_slot = slot_pool.i_begin_busy;
    while (i_slot != -1) {
        counter++;
        i_slot = slot_pool.slots[i_slot].i_next;
    }
    cr_assert(counter == MIN(6, LB_NB_MAX_MSGS));
    // cr_assert(slot_pool.nb_free == 2);
}

Test(shift, shift_to_eject_all_slots) {
    LB_ALLOC_ROOT_MD(root_md);
    LB_INIT_ROOT_MD(root_md);
    message_t p_messages1[8] = {
        //  header, payload, signature
        {{/*LB_MT_OFFSET + */ 160, 19, 21, 0}, DUMMY_MSG_SIZE, {0}, {0}},
        {{/*LB_MT_OFFSET + */ 512, 21, 12, 0}, DUMMY_MSG_SIZE, {1}, {1}},
        {{/*LB_MT_OFFSET + */ 100, 41, 72, 0}, DUMMY_MSG_SIZE, {2}, {2}},
        {{/*LB_MT_OFFSET + */ 189, 41, 21, 0}, DUMMY_MSG_SIZE, {3}, {3}},
        {{/*LB_MT_OFFSET + */ 124, 72, 19, 0}, DUMMY_MSG_SIZE, {4}, {4}},
        {{/*LB_MT_OFFSET + */ 415, 19, 72, 0}, DUMMY_MSG_SIZE, {5}, {5}},
        {{/*LB_MT_OFFSET + */ 378, 1, 41, 0}, DUMMY_MSG_SIZE, {6}, {6}},
        {{/*LB_MT_OFFSET + */ 101, 1, 41, 0}, DUMMY_MSG_SIZE, {7}, {7}}};
    message_pool_t pool1 = {p_messages1, 8};
    (void)pool1;

    LB_ALLOC_MERKLE_TREE(root, slot_pool);
    LB_INIT_MERKLE_TREE(root);
    lb_init_merkle_tree(&root, &root_md, &slot_pool, LB_MT_OFFSET);
    lb_attach_msgs_to_tree(&root, &pool1, &slot_pool, NULL, NULL);

    int counter = 0;
    ssize_t i_slot = slot_pool.i_begin_busy;

    while (i_slot != -1) {
        counter++;
        i_slot = slot_pool.slots[i_slot].i_next;
    }
    cr_assert(counter == MIN(8, LB_NB_MAX_MSGS));

    lb_shift_tree(&root, &slot_pool, slot_pool.certif_date + LB_MT_OFFSET, NULL,
                  NULL);

    counter = 0;
    i_slot = slot_pool.i_begin_busy;
    while (i_slot != -1) {
        counter++;
        i_slot = slot_pool.slots[i_slot].i_next;
    }
    cr_assert(counter == MIN(0, LB_NB_MAX_MSGS));
    // cr_assert(slot_pool.nb_free == 2);
}

Test(shift, insert_after_shift) {
    LB_ALLOC_ROOT_MD(root_md);
    LB_INIT_ROOT_MD(root_md);

    message_t p_messagesfull[LB_NB_MAX_MSGS];
    for (unsigned int i = 0; i < LB_NB_MAX_MSGS; i++) {
        message_t message = {
            {LB_MT_START + i, 0, 0, 0}, DUMMY_MSG_SIZE, {0}, {i}};
        memcpy(message.signature, &i, sizeof(i));
        p_messagesfull[i] = message;
    }
    message_pool_t poolfull = {p_messagesfull, LB_NB_MAX_MSGS};

    message_t p_messages1[8] = {
        //  header, payload, signature
        {{2 * LB_MT_OFFSET + 160, 19, 21, 0}, DUMMY_MSG_SIZE, {0}, {0}},
        {{2 * LB_MT_OFFSET + 512, 21, 12, 0}, DUMMY_MSG_SIZE, {1}, {1}},
        {{2 * LB_MT_OFFSET + 100, 41, 72, 0}, DUMMY_MSG_SIZE, {2}, {2}},
        {{2 * LB_MT_OFFSET + 189, 41, 21, 0}, DUMMY_MSG_SIZE, {3}, {3}},
        {{2 * LB_MT_OFFSET + 124, 72, 19, 0}, DUMMY_MSG_SIZE, {4}, {4}},
        {{2 * LB_MT_OFFSET + 415, 19, 72, 0}, DUMMY_MSG_SIZE, {5}, {5}},
        {{2 * LB_MT_OFFSET + 378, 1, 41, 0}, DUMMY_MSG_SIZE, {6}, {6}},
        {{2 * LB_MT_OFFSET + 101, 1, 41, 0}, DUMMY_MSG_SIZE, {7}, {7}}};
    message_pool_t pool1 = {p_messages1, 8};
    (void)pool1;

    LB_ALLOC_MERKLE_TREE(root, slot_pool);
    LB_INIT_MERKLE_TREE(root);
    lb_init_merkle_tree(&root, &root_md, &slot_pool, LB_MT_OFFSET);
    lb_attach_msgs_to_tree(&root, &poolfull, &slot_pool, NULL, NULL);

    int counter = 0;
    ssize_t i_slot = slot_pool.i_begin_busy;

    while (i_slot != -1) {
        counter++;
        i_slot = slot_pool.slots[i_slot].i_next;
    }
    cr_assert(counter == LB_NB_MAX_MSGS);

    lb_shift_tree(&root, &slot_pool, slot_pool.certif_date + 2 * LB_MT_OFFSET,
                  NULL, NULL);

    counter = 0;
    i_slot = slot_pool.i_begin_busy;
    while (i_slot != -1) {
        counter++;
        i_slot = slot_pool.slots[i_slot].i_next;
    }
    cr_assert(counter == 0);

    lb_attach_msgs_to_tree(&root, &pool1, &slot_pool, NULL, NULL);

    counter = 0;
    i_slot = slot_pool.i_begin_busy;

    while (i_slot != -1) {
        counter++;
        i_slot = slot_pool.slots[i_slot].i_next;
    }
    cr_assert(counter == MIN(8, LB_NB_MAX_MSGS), "got counter = %d", counter);
}