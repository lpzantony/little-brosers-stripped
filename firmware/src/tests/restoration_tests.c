#include "merkle_tree.h"
#include "slot_pool.h"
#include "task_stack.h"
#include <criterion/criterion.h>
#define NB_MSG 8
#define SLOT_POOL_SAVE_PATH "_build/slot_pool"
#define MERKLE_TREE_SAVE_PATH "_build/merkle_tree"

// Build a lb_merkle_tree, attach messages to it, save the slot_pool in a file
// Restore it and verify that the value is the same
Test(restoration, slot_pool) {
    LB_ALLOC_ROOT_MD(root_md);
    LB_INIT_ROOT_MD(root_md);
    LB_ALLOC_MERKLE_TREE(root, slot_pool);
    LB_INIT_MERKLE_TREE(root);

    message_t p_messages1[NB_MSG] = {
        //  header, payload, signature
        {{160, 19, 21, 0}, 0, {0}, {10}}, {{512, 21, 12, 0}, 0, {1}, {11}},
        {{100, 41, 72, 0}, 0, {2}, {12}}, {{189, 41, 21, 0}, 0, {3}, {13}},
        {{124, 72, 19, 0}, 0, {4}, {14}}, {{415, 19, 72, 0}, 0, {5}, {15}},
        {{378, 1, 41, 0}, 0, {6}, {16}},  {{101, 1, 41, 0}, 0, {7}, {17}}};
    message_pool_t pool1 = {p_messages1, NB_MSG};

    // Init tree and attach messages to it
    lb_init_merkle_tree(&root, &root_md, &slot_pool, LB_MT_OFFSET);
    lb_attach_msgs_to_tree(&root, &pool1, &slot_pool, NULL, NULL);
    err_t ret = lb_save_slot_pool(&slot_pool, SLOT_POOL_SAVE_PATH);
    cr_assert(ret == LB_OK);

    LB_ALLOC_SLOT_POOL(slot_pool_restored);
    lb_load_slot_pool(&slot_pool_restored, SLOT_POOL_SAVE_PATH);
    cr_assert(ret == LB_OK);

    // Check the slots values
    for (int i = 0; i < LB_NB_MAX_MSGS + 1; i++) {
        lb_slot slot = slot_pool.slots[i];
        lb_slot slot_restored = slot_pool_restored.slots[i];
        cr_expect(slot.timestamp == slot_restored.timestamp,
                  "Timestamps should be equal: %d %d", slot.timestamp,
                  slot_restored.timestamp);
        cr_expect(slot.i_next == slot_restored.i_next,
                  "i_next should be equal: %d %d", slot.i_next,
                  slot_restored.i_next);
        cr_expect(slot.i_previous == slot_restored.i_previous,
                  "i_previous should be equal: %d %d", slot.i_previous,
                  slot_restored.i_previous);
        cr_expect(slot.addr == slot_restored.addr,
                  "addr should be equal: %d %d", slot.addr, slot_restored.addr);
        for (int k = 0; k < LB_SLOT_SIGNATURE_SIZE; k++) {
            cr_expect(slot.p_hash[k] == slot_restored.p_hash[k],
                      "p_hash[%d] should be equal: %d %d", k, slot.p_hash,
                      slot_restored.p_hash);
        }
    }

    // Check the i_begin_busy, i_begin_free and nb_free values
    cr_expect(slot_pool.i_begin_busy == slot_pool_restored.i_begin_busy,
              "i_begin_busy should be equal: %d %d", slot_pool.i_begin_busy,
              slot_pool_restored.i_begin_busy);
    cr_expect(slot_pool.i_begin_free == slot_pool_restored.i_begin_free,
              "i_begin_free should be equal: %d %d", slot_pool.i_begin_free,
              slot_pool_restored.i_begin_free);
    cr_expect(slot_pool.nb_free == slot_pool_restored.nb_free,
              "nb_free should be equal: %d %d", slot_pool.nb_free,
              slot_pool_restored.nb_free);
    cr_expect(slot_pool.nb_slots == slot_pool_restored.nb_slots,
              "nb_free should be equal: %d %d", slot_pool.nb_slots,
              slot_pool_restored.nb_slots);
    cr_expect(slot_pool.certif_date == slot_pool_restored.certif_date,
              "nb_free should be equal: %d %d", slot_pool.certif_date,
              slot_pool_restored.certif_date);
}

// Build a lb_merkle_tree, attach messages to it, save the tree in a file
// Restore it and verify that the value is the same
Test(restoration, merkle_tree) {
    LB_ALLOC_ROOT_MD(root_md);
    LB_INIT_ROOT_MD(root_md);
    LB_ALLOC_MERKLE_TREE(root, slot_pool);
    LB_INIT_MERKLE_TREE(root);

    message_t p_messages1[NB_MSG] = {
        //  header, payload, signature
        {{160, 19, 21, 0}, 0, {0}, {10}}, {{512, 21, 12, 0}, 0, {1}, {11}},
        {{100, 41, 72, 0}, 0, {2}, {12}}, {{189, 41, 21, 0}, 0, {3}, {13}},
        {{124, 72, 19, 0}, 0, {4}, {14}}, {{415, 19, 72, 0}, 0, {5}, {15}},
        {{378, 1, 41, 0}, 0, {6}, {16}},  {{101, 1, 41, 0}, 0, {7}, {17}}};
    message_pool_t pool1 = {p_messages1, NB_MSG};

    // Init tree and attach messages to it
    lb_init_merkle_tree(&root, &root_md, &slot_pool, LB_MT_OFFSET);
    lb_attach_msgs_to_tree(&root, &pool1, &slot_pool, NULL, NULL);

    err_t ret = lb_save_merkle_tree(&root, &root_md, MERKLE_TREE_SAVE_PATH);
    cr_assert(ret == LB_OK);
    unsigned int tree_size;
    unsigned int nb_nodes;
    unsigned int pool_size;
    lb_merkle_tree_size(&root, &root_md, &tree_size, &nb_nodes);
    lb_slot_pool_size(&slot_pool, &pool_size);
    LBLOG("Merkle tree     : %d nodes for %d bytes\n", nb_nodes, tree_size);
    LBLOG("Slot pool       : %d slots for %d bytes\n", LB_NB_MAX_MSGS + 1,
          pool_size);
    // diff's stack
    lb_task_stack st;
    lb_stack_init(&st, &slot_pool);
    unsigned int stack_size;
    lb_task_size_size(&st, &stack_size);
    LBLOG("Task stask tree : %d bytes\n", stack_size);
    LBLOG("Total RAM usage : %d bytes\n", tree_size + pool_size + stack_size);
    // Allocate new merkle tree and load values previously saved in it
    LB_ALLOC_MERKLE_TREE(root_restored, slot_pool_restored);
    LB_INIT_MERKLE_TREE(root_restored);
    lb_init_merkle_tree(&root_restored, &root_md, &slot_pool_restored,
                        LB_MT_OFFSET);
    lb_load_merkle_tree(&root_restored, &root_md, MERKLE_TREE_SAVE_PATH);

    for (int k = 0; k < LB_MT_NB_CHILDREN; k++) {
        lb_merkle_tree *p_tree = root.p_children[k];
        lb_merkle_tree *p_tree_restored = root_restored.p_children[k];
        size_t node_count = root_md.node_count[k];

        for (unsigned int i = 0; i < node_count; i++) {
            cr_assert(p_tree->array_id == p_tree_restored->array_id);
            cr_assert(p_tree->start == p_tree_restored->start);
            cr_assert(lb_get_node_depth(p_tree) ==
                      lb_get_node_depth(p_tree_restored));
            cr_assert(p_tree->i_bucket_slot == p_tree_restored->i_bucket_slot);

            for (unsigned int j = 0; j < LB_SLOT_SIGNATURE_SIZE; j++) {
                cr_assert(p_tree->p_hash[j] == p_tree_restored->p_hash[j],
                          "%d, %d", p_tree->p_hash[j],
                          p_tree_restored->p_hash[j]);
            }
        }
    }
}
