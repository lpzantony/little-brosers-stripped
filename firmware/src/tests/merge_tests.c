#include "merkle_merge.h"
#include "merkle_tree.h"
#include <criterion/criterion.h>
#include <stdio.h>
#define NB_MSG 8
#define DUMMY_MSG_SIZE MESSAGE_SIGNATURE_SIZE + MESSAGE_HEADER_SIZE + 1 + 2
#define OFFSET 2

static message_t messagesG[16];
static message_pool_t pool1 = {messagesG, 16};

err_t load_msg(void *osef, const uint16_t address, message_t *p_msg) {
    (void)osef;
    (void)address;
    (void)p_msg;
    (void)pool1;
    (void)messagesG;
    if (address < LB_ADDR_FIRST_STORAGE)
        return LB_ERR;
    uint8_t tmpbuff[MESSAGE_MAX_SIZE];
    message_to_raw(tmpbuff, &messagesG[address - LB_ADDR_FIRST_STORAGE]);
    message_from_raw(tmpbuff, p_msg);
    return LB_OK;
}

Test(merge, simple_merge) {
    LB_ALLOC_ROOT_MD(root_md);
    LB_INIT_ROOT_MD(root_md);

    for (unsigned int i = 0; i < 8; i++) {
        message_t message = {
            {LB_MT_START + i, 0, 0, 0}, DUMMY_MSG_SIZE, {0}, {(uint8_t)i}};
        memcpy(message.signature, &i, sizeof(i));
        messagesG[i] = message;
    }
    (void)pool1;

    LB_ALLOC_MERKLE_TREE(root1, slot_pool1);
    LB_INIT_MERKLE_TREE(root1);
    lb_init_merkle_tree(&root1, &root_md, &slot_pool1, LB_MT_OFFSET);
    lb_attach_msgs_to_tree(&root1, &pool1, &slot_pool1, NULL, NULL);

    char counter = 0;
    ssize_t i_slot = slot_pool1.i_begin_busy;
    uint32_t timestamps[LB_NB_MAX_MSGS];
    int i = 0;
    while (i_slot != -1) {
        if (counter % 2 == 0) {
            // printf("%d : timestamp %u marked not shared\n", i,
            // slot_pool1.slots[i_slot].timestamp);
            timestamps[i++] = slot_pool1.slots[i_slot].timestamp;
            slot_pool1.slots[i_slot].shared = 0;
        }
        counter++;
        i_slot = slot_pool1.slots[i_slot].i_next;
    }
    uint8_t buff[MESSAGE_MAX_SIZE];
    size_t written = 1;
    i = 0;
    message_t msg;
    while (written != 0) {
        lb_next_msg_to_send(load_msg, NULL, &slot_pool1, buff, &written);
        if (written == 0)
            continue;
        message_from_raw(buff, &msg);
        // printf("%d : Got msg timestamp %d\n", i, msg.header.timestamp);

        cr_assert(msg.header.timestamp == timestamps[i]);
        i++;
    }
}

Test(merge, double_merge) {
    LB_ALLOC_ROOT_MD(root_md);
    LB_INIT_ROOT_MD(root_md);

    for (unsigned int i = 0; i < 8; i++) {
        message_t message = {
            {LB_MT_START + i, 0, 0, 0}, DUMMY_MSG_SIZE, {0}, {(uint8_t)i}};
        memcpy(message.signature, &i, sizeof(i));
        messagesG[i] = message;
    }
    (void)pool1;

    LB_ALLOC_MERKLE_TREE(root1, slot_pool1);
    LB_INIT_MERKLE_TREE(root1);
    lb_init_merkle_tree(&root1, &root_md, &slot_pool1, LB_MT_OFFSET);
    lb_attach_msgs_to_tree(&root1, &pool1, &slot_pool1, NULL, NULL);

    char counter = 0;
    ssize_t i_slot = slot_pool1.i_begin_busy;
    uint32_t timestamps[LB_NB_MAX_MSGS];
    int i = 0;
    while (i_slot != -1) {
        if (counter % 2 == 0) {
            // printf("%d : timestamp %u marked not shared\n", i,
            // slot_pool1.slots[i_slot].timestamp);
            timestamps[i++] = slot_pool1.slots[i_slot].timestamp;
            slot_pool1.slots[i_slot].shared = 0;
        }
        counter++;
        i_slot = slot_pool1.slots[i_slot].i_next;
    }
    uint8_t buff[MESSAGE_MAX_SIZE];
    size_t written;
    message_t msg;
    written = 1;
    i = 0;
    while (written != 0) {
        lb_next_msg_to_send(load_msg, NULL, &slot_pool1, buff, &written);
        if (written == 0)
            continue;
        message_from_raw(buff, &msg);
        // printf("%d : Got msg timestamp %d\n", i, msg.header.timestamp);

        cr_assert(msg.header.timestamp == timestamps[i]);
        i++;
    }
    // second pass
    written = 1;
    i = 0;
    while (written != 0) {
        lb_next_msg_to_send(load_msg, NULL, &slot_pool1, buff, &written);
        if (written == 0)
            continue;
        message_from_raw(buff, &msg);
        // printf("%d : Got msg timestamp %d\n", i, msg.header.timestamp);

        cr_assert(msg.header.timestamp == timestamps[i]);
        i++;
    }
}

Test(merge, all_shared) {
    LB_ALLOC_ROOT_MD(root_md);
    LB_INIT_ROOT_MD(root_md);

    for (unsigned int i = 0; i < 8; i++) {
        message_t message = {
            {LB_MT_START + i, 0, 0, 0}, DUMMY_MSG_SIZE, {0}, {(uint8_t)i}};
        memcpy(message.signature, &i, sizeof(i));
        messagesG[i] = message;
    }
    (void)pool1;

    LB_ALLOC_MERKLE_TREE(root1, slot_pool1);
    LB_INIT_MERKLE_TREE(root1);
    lb_init_merkle_tree(&root1, &root_md, &slot_pool1, LB_MT_OFFSET);
    lb_attach_msgs_to_tree(&root1, &pool1, &slot_pool1, NULL, NULL);

    int i = 0;
    uint8_t buff[MESSAGE_MAX_SIZE];
    size_t written = 1;
    i = 0;
    while (written != 0) {
        lb_next_msg_to_send(load_msg, NULL, &slot_pool1, buff, &written);
        if (written == 0)
            continue;
        i++;
    }
    cr_assert(i == 0);

    written = 1;
    i = 0;
    while (written != 0) {
        lb_next_msg_to_send(load_msg, NULL, &slot_pool1, buff, &written);
        if (written == 0)
            continue;
        i++;
    }
    cr_assert(i == 0);
}