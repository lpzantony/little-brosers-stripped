#include "task_stack.h"
#include <criterion/criterion.h>
#include <stdio.h>
#define NB_TS 3

Test(task_stack, push) {

    err_t retval;
    lb_task_stack st;
    lb_slot_pool slot_pool;
    lb_stack_init(&st, &slot_pool);
    lb_merkle_tree node;
    lb_merkle_tree node1;

    lb_task tasks[NB_TS] = {
        {TASK_CHILD, &node}, {TASK_EMPTY, &node1}, {TASK_BCKT, &node1}};

    for (int i = 0; i < NB_TS; i++) {
        retval = lb_stack_push(&st, tasks[i].type, tasks[i].p_node);
        cr_assert(retval == LB_OK, "lb_stack_push an error code : %d", retval);
        cr_assert(lb_stack_size(&st) == i + 1,
                  "Stack size incorrect. %d instead of %d", lb_stack_size(&st),
                  i + 1);
    }
    cr_assert(lb_stack_size(&st) == NB_TS,
              "Stack size incorrect. %d instead of %d", lb_stack_size(&st),
              NB_TS);
}

Test(task_stack, pop) {
    err_t retval;
    lb_task_stack st;
    lb_slot_pool slot_pool;
    lb_stack_init(&st, &slot_pool);
    lb_merkle_tree node;
    lb_merkle_tree node1;

    lb_task tasks[NB_TS] = {
        {TASK_CHILD, &node}, {TASK_EMPTY, &node1}, {TASK_BCKT, &node1}};

    for (int i = 0; i < NB_TS; i++) {
        lb_stack_push(&st, tasks[i].type, tasks[i].p_node);
    }
    for (int i = 1; i <= NB_TS; i++) {
        retval = lb_stack_pop(&st);
        cr_assert(retval == LB_OK, "lb_stack_pop an error code : %d", retval);
        cr_assert(lb_stack_size(&st) == NB_TS - i,
                  "Stack size incorrect. %d instead of %d", lb_stack_size(&st),
                  NB_TS - i);
    }
}

Test(task_stack, peek) {
    lb_task_stack st;
    lb_slot_pool slot_pool;
    lb_stack_init(&st, &slot_pool);
    lb_merkle_tree node;
    lb_merkle_tree node1;

    lb_task tasks[NB_TS] = {
        {TASK_CHILD, &node}, {TASK_EMPTY, &node1}, {TASK_BCKT, &node1}};

    for (int i = 0; i < NB_TS; i++) {
        lb_stack_push(&st, tasks[i].type, tasks[i].p_node);
    }
    err_t retval;
    lb_task ts;
    for (int i = 1; i <= NB_TS; i++) {
        retval = lb_stack_peek(&st, &ts.type, &ts.p_node);
        cr_assert(retval == LB_OK, "lb_stack_peek an error code : %d", retval);
        cr_assert(ts.type == tasks[NB_TS - i].type,
                  "Task's type peeked is incorrect. %d instead of %d", ts.type,
                  tasks[NB_TS - i].type);
        cr_assert(ts.p_node == tasks[NB_TS - i].p_node,
                  "Task's node peeked is incorrect. %p instead of %p",
                  ts.p_node, tasks[NB_TS - i].p_node);
        lb_stack_pop(&st);
    }
}

Test(task_stack, empty) {
    err_t retval;
    lb_task_stack st;
    lb_slot_pool slot_pool;
    lb_stack_init(&st, &slot_pool);
    lb_merkle_tree node;
    lb_merkle_tree node1;

    lb_task tasks[NB_TS] = {
        {TASK_CHILD, &node}, {TASK_EMPTY, &node1}, {TASK_BCKT, &node1}};

    for (int i = 0; i < NB_TS; i++) {
        lb_stack_push(&st, tasks[i].type, tasks[i].p_node);
    }
    for (int i = 1; i <= NB_TS; i++) {
        lb_stack_pop(&st);
    }
    retval = lb_stack_pop(&st);
    cr_assert(
        retval == LB_STACK_EMPTY,
        "Stack should be empty,return value incorrect. %d instead of %d\n",
        retval, LB_STACK_EMPTY);
}

Test(task_stack, full) {
    err_t retval;
    lb_task_stack st;
    lb_slot_pool slot_pool;
    lb_stack_init(&st, &slot_pool);
    lb_merkle_tree node;
    lb_merkle_tree node1;

    lb_task tasks[NB_TS] = {
        {TASK_CHILD, &node}, {TASK_EMPTY, &node1}, {TASK_BCKT, &node1}};

    for (int i = 0; i < LB_STACK_SIZE; i++) {
        lb_stack_push(&st, tasks[i % NB_TS].type, tasks[i % NB_TS].p_node);
    }
    retval = lb_stack_push(&st, tasks[0].type, tasks[0].p_node);
    cr_assert(retval == LB_STACK_FULL,
              "Stack should be full,return value incorrect. %d instead of %d\n",
              retval, LB_STACK_FULL);
}
