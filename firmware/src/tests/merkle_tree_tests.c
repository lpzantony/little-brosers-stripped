#include "merkle_tree.h"
#include <criterion/criterion.h>
#include <math.h>
#include <stdio.h>

Test(merkle_tree, array_id) {
    LB_ALLOC_ROOT_MD(root_md);
    LB_INIT_ROOT_MD(root_md);
    LB_ALLOC_MERKLE_TREE(root, slot_pool);
    LB_INIT_MERKLE_TREE(root);
    lb_init_merkle_tree(&root, &root_md, &slot_pool, LB_MT_OFFSET);

    // Checking array_id for each node
    for (int k = 0; k < LB_MT_NB_CHILDREN; k++) {
        int node_count = root_md.node_count[k];
        lb_merkle_tree *tree_array = root.p_children[k];

        for (int i = 0; i < node_count; i++) {
            cr_expect(
                tree_array[i].array_id == i,
                "Tree %d: array_id of index %d is incorrect. %d instead of %d",
                k, i, tree_array[i].array_id, i);
        }
    }
}

// Verify that the nodes in the tree's array are at the good depth
Test(merkle_tree, depth) {
    LB_ALLOC_ROOT_MD(root_md);
    LB_INIT_ROOT_MD(root_md);
    LB_ALLOC_MERKLE_TREE(root, slot_pool);
    LB_INIT_MERKLE_TREE(root);
    lb_init_merkle_tree(&root, &root_md, &slot_pool, LB_MT_OFFSET);

    for (int k = 0; k < LB_MT_NB_CHILDREN; k++) {
        int depth = root_md.depth[k];
        lb_merkle_tree *tree_array = root.p_children[k];
        uint16_t tree_array_depth = lb_get_node_depth(&tree_array[0]);
        cr_expect(tree_array_depth == 0,
                  "Tree %d: wrong depth at index 0. %d instead of 0", k,
                  lb_get_node_depth(&tree_array[0]));

        int j = 1;
        for (int i = 1; i < depth; i++) {
            for (int l = 0; l < pow(LB_MT_NB_CHILDREN, i); l++) {
                tree_array_depth = lb_get_node_depth(&tree_array[j]);
                cr_expect(tree_array_depth == i,
                          "Tree %d: wrong depth at index %d, %d instead of %d",
                          k, j, tree_array_depth, i);
                j++;
            }
        }
    }
}

// Verify that each node's timeframe is partitionned into its children's
// timeframes
Test(merkle_tree, time_coherence) {
    LB_ALLOC_ROOT_MD(root_md);
    LB_INIT_ROOT_MD(root_md);
    LB_ALLOC_MERKLE_TREE(root, slot_pool);
    LB_INIT_MERKLE_TREE(root);
    lb_init_merkle_tree(&root, &root_md, &slot_pool, LB_MT_OFFSET);

    for (int k = 0; k < LB_MT_NB_CHILDREN; k++) {
        int node_count = root_md.node_count[k];
        lb_merkle_tree *tree_array = root.p_children[k];

        for (int i = 0; i < node_count; i++) {
            lb_merkle_tree tree_node = tree_array[i];
            if (tree_node.p_children[0] != NULL) {
                cr_expect(tree_node.start == tree_node.p_children[0]->start);

                for (int j = 1; j < LB_MT_NB_CHILDREN - 1; j++) {
                    cr_expect(
                        tree_node.p_children[j]->end ==
                            tree_node.p_children[j + 1]->start,
                        "Tree %d: end is %d and start is %d when they should "
                        "be equal",
                        k, tree_node.p_children[j]->end,
                        tree_node.p_children[j + 1]->start);
                }

                cr_expect(tree_node.end ==
                          tree_node.p_children[LB_MT_NB_CHILDREN - 1]->end);
            }
        }
    }
}
