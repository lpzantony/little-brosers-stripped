# List of source folders
CORE_DIR   = core
SIM_DIR    = sim
HW_DIR 	   = hw
TEST_DIR  = tests

# C hardware independent source code
CORE_SRC  = utils.c \
			message.c \
			merkle_tree.c \
			slot_pool.c \
			merkle_merge.c \


# C++ simulation source code
SIM_SRC   = sha256.cpp \
			device.cpp \
			benchmark.cpp \
			shell.cpp \
			net_tools.cpp \
			command.cpp \
			merkle_merge.c \

# Hardware dependant source code
HW_SRC    = power_management.c \
			pm.c \
			gap.c \
			conn_params.c \
			gatt.c \
			advertising.c \
			log.c \
			bsp_timers.c \
			ble_stack.c \
			test_hw.c \
			services.c \
			authentication_service.c \
			authentication_service_backend.c \
			lb_crypto.c \
			cert.c \
			flash.c \
			dfu.c \
			drop_data.c \
			diff_service.c \
			drop.c \
			connection_timer.c \

TEST_SRC  = merkle_tree_tests.c \
			slot_pool_tests.c \
			restoration_tests.c \
			task_stack_tests.c \
			index_stack_tests.c \
			diff_tests.c \
			shift_tests.c \
			merge_tests.c \

HW_MAIN   = main.c
SIM_MAIN  = main.cpp

# Required include directories
CORE_INC  = $(CORE_DIR)
SIM_INC   = $(SIM_DIR)
HW_INC	  = $(HW_DIR)
TEST_INC  = $(TEST_DIR)


# Allow to apply the linter to source files
# WARNING : The following targets expect that this file will be included from a Makefile in a
#           neighboring directory and that a SRC_DIR variable will be defined in this
#           Makefile, refering to the folder where this file (src.mk) is located.
lint:
	find $(PROJ_DIR)/$(SRC_DIR)/ -name '*.[ch]' -o -name '*.cpp' | xargs clang-format -style=file -i

# Allow to check linter without modifying the source files
lintck:
	@find $(PROJ_DIR)/$(SRC_DIR)/ -name '*.[ch]' -o -name '*.cpp' \
	| xargs clang-format -style=file -output-replacements-xml \
	| grep -c "<replacement " | grep '^0$$' > /dev/null

$(PROJ_DIR)/$(SRC_DIR)/$(CORE_DIR)/%.lbf: $(PROJ_DIR)/$(SRC_DIR)/$(CORE_DIR)/merkle_tree.h
	python $(PROJ_DIR)/$(SRC_DIR)/$(CORE_DIR)/merkle_constants.py

size:
	@stat -c '(size in bytes) %-25n %10s' $(OUTPUT_DIRECTORY)/*
