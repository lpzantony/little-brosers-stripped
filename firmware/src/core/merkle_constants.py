import math
import os
#################################################################
#   Defining init variables
#################################################################
'''File from which we will get the init constants'''
to_parse = "merkle_tree.h"
'''File we will generate at the end of the script'''
to_gen = "merkle_tree_constants.lbf"
'''Declaring the constants name in the to_parse file'''
largest_subtree_size = "LB_MT_LARGEST_SIZE"
bucket_size_const_name = "LB_MT_BUCKET_SIZE"
nb_children_const_name = "LB_MT_NB_CHILDREN"
start_const_name = "LB_MT_START"
end_1_const_name = "LB_MT_END1"
end_2_const_name = "LB_MT_END2"
end_3_const_name = "LB_MT_END3"
end_4_const_name = "LB_MT_END4"
end_5_const_name = "LB_MT_END5"
end_6_const_name = "LB_MT_END6"
end_7_const_name = "LB_MT_END7"
end_const_name = "LB_MT_END"


#####################################################################
#   Useful function to compute the size and depth of a merkle tree
#####################################################################
def compute_size(nb_children, timeframe, bucket_size):
    depth = int(1 + math.floor(math.log(timeframe / bucket_size, nb_children)))
    # In case leaf width is between 1 and 2, shrink the tree's depth by one
    if int(timeframe / math.pow(nb_children, depth-1)) <= bucket_size :
       depth = depth -1
    nb_nodes = int((1 - math.pow(nb_children, depth)) / (1 - nb_children))
    first_leaf = int((1 - math.pow(nb_children, depth-1)) / (1 - nb_children))
    return [depth, nb_nodes, first_leaf]


#################################################################
#   Actual script
#################################################################
'''Looking for the merkle_tree.h file'''
os.system('find .. -name ' + to_parse + ' > tmp_output.txt')
find_output = open('tmp_output.txt', 'r').read()
os.remove('tmp_output.txt')
find_output = find_output.replace("\n", "")
srcfile = open(find_output, 'r')
in_comment = 0
'''Parsing header to find constants'''
timestamps = []
for line in srcfile:
    if "/*" in line:
        in_comment = 1
        if "*/" in line:
            in_comment = 0
        continue
    if "*/" in line:
        in_comment = 0
        if "/*" in line:
            in_comment = 1
        continue
    if in_comment == 1:
        continue
    if "//#define " in line:
        continue
    if "#define " + nb_children_const_name in line:
        nb_children = int(line.split()[2])
    elif "#define " + bucket_size_const_name in line:
        bucket_size = int(line.split()[2])
    elif "#define " + start_const_name in line:
        timestamps.append(int(line.split()[2]))
    elif "#define " + end_1_const_name in line:
        timestamps.append(int(line.split()[2]))
    elif "#define " + end_2_const_name in line:
        timestamps.append(int(line.split()[2]))
    elif "#define " + end_3_const_name in line:
        timestamps.append(int(line.split()[2]))
    elif "#define " + end_4_const_name in line:
        timestamps.append(int(line.split()[2]))
    elif "#define " + end_5_const_name in line:
        timestamps.append(int(line.split()[2]))
    elif "#define " + end_6_const_name in line:
        timestamps.append(int(line.split()[2]))
    elif "#define " + end_7_const_name in line:
        timestamps.append(int(line.split()[2]))
    elif "#define " + end_const_name in line:
        timestamps.append(int(line.split()[2]))
srcfile.close()
'''Computing sub-merkle tree sizes'''
depth = [None] * nb_children
nb_nodes = [None] * nb_children
first_leaf = [None] * nb_children
for i in range(0, nb_children):
    [depth[i], nb_nodes[i], first_leaf[i]] = compute_size(
        nb_children, timestamps[i + 1] - timestamps[i], bucket_size)
'''Generating the file to be include'''
genf = open(find_output.replace(to_parse, to_gen), 'w')

genf.write("#ifndef " + to_gen.upper().replace(".", "_") + "\n")
genf.write("#define " + to_gen.upper().replace(".", "_") + "\n\n")

genf.write("#include \"" + to_parse + "\"\n\n")

genf.write("#define " + largest_subtree_size + " "+ str(max(nb_nodes)) + "\n\n")

for i in range(0, nb_children):
    genf.write("#define MERKLE_TREE_" + str(i) + "_DEPTH      " +
               str(depth[i]) + "\n")
    genf.write("#define MERKLE_TREE_" + str(i) + "_NODE_COUNT " +
               str(nb_nodes[i]) + "\n")
    genf.write("#define MERKLE_TREE_" + str(i) + "_FIRST_LEAF_ID " +
               str(first_leaf[i]) + "\n\n")

genf.write("\n#endif" + "\n")
