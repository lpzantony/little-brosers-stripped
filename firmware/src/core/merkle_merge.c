#include "merkle_merge.h"
#include "slot_pool.h"

#define DEBUG_MERGE

// Fill the payload root's hash
err_t lb_fill_payload_ROOT(lb_task_stack *p_st, lb_merkle_tree *p_node) {
    if (p_st == NULL || p_node == NULL) {
        return LB_WRONG_ARGS_VALUE;
    }
    p_st->buff.payload[0] = TASK_ROOT;
    p_st->buff.used++;
    lb_stack_push(p_st, TASK_RX, p_node);
    memcpy(p_st->buff.payload + p_st->buff.used, p_node->p_hash,
           LB_SLOT_SIGNATURE_SIZE);
    p_st->buff.used += LB_SLOT_SIGNATURE_SIZE;
    return LB_OK;
}

// Fill the payload with the p_node's children's p_hash
// Add one reception to the stak for each child of p_node
err_t lb_fill_payload_C(lb_task_stack *p_st, lb_merkle_tree *p_node) {
    if (p_st == NULL || p_node == NULL) {
        return LB_WRONG_ARGS_VALUE;
    }
    p_st->buff.payload[0] = TASK_CHILD;
    p_st->buff.used++;
    for (int i = 0; i < LB_MT_NB_CHILDREN; i++) {
        lb_stack_push(p_st, TASK_RX, p_node->p_children[i]);
        memcpy(p_st->buff.payload + p_st->buff.used,
               p_node->p_children[i]->p_hash, LB_SLOT_SIGNATURE_SIZE);
        p_st->buff.used += LB_SLOT_SIGNATURE_SIZE;
    }
    return LB_OK;
}

// Fill the payload with count of slot to send
// Add a reception associated with p_node
err_t lb_fill_payload_B(lb_task_stack *p_st, lb_merkle_tree *p_node) {
    if (p_st == NULL || p_node == NULL) {
        return LB_WRONG_ARGS_VALUE;
    }
    p_st->buff.payload[0] = TASK_BCKT;
    p_st->buff.used++;
    lb_stack_push(p_st, TASK_RX, p_node);

    // Compute bucket_size
    int32_t nb_slots;
    ssize_t i_last;
    lb_get_bucket_size(p_node, p_st->p_pool, &i_last, &nb_slots);

    // register how many slots we need to send
    memcpy(p_st->buff.payload + p_st->buff.used, &nb_slots, sizeof(int32_t));
    p_st->buff.used += sizeof(int32_t);
    p_st->slot_left = nb_slots;
    p_st->i_lb = p_node->i_bucket_slot;
#ifdef DEBUG_MERGE
    LBLOG("Fill payload B: %d\n", p_st->slot_left);
#endif
    return LB_OK;
}

// Fill the payload with count of slot to send
// Fill the payload with requests of slots identified
// by their position in the burst they were received in.
err_t lb_fill_payload_BR(lb_task_stack *p_st, lb_merkle_tree *p_node) {
    if (p_st == NULL || p_node == NULL) {
        return LB_WRONG_ARGS_VALUE;
    }
    p_st->buff.payload[0] = TASK_BCKT_RESP;
    p_st->buff.used++;

    // register how many slots we need to require
    p_st->slot_left = lb_istack_size(&p_st->to_recv);
    memcpy(&p_st->buff.payload[p_st->buff.used], &p_st->slot_left,
           sizeof(int32_t));
#ifdef DEBUG_MERGE
    LBLOG("lb_fill_payload_BR with to_recv size =  %d : ",
          lb_istack_size(&p_st->to_recv));
#endif
    p_st->buff.used += sizeof(int32_t);
    p_st->i_lb = 0;
    return LB_OK;
}

err_t lb_fill_payload_E(lb_task_stack *p_st) {
    if (p_st == NULL) {
        return LB_WRONG_ARGS_VALUE;
    }
    p_st->buff.payload[0] = TASK_EMPTY;
    p_st->buff.used++;
    return LB_OK;
}

// Send slots of this leaf's bucket starting from the begining
err_t lb_burst_TX_B(lb_task_stack *p_st, lb_merkle_tree *p_node) {
    if (p_st == NULL || p_node == NULL) {
        return LB_WRONG_ARGS_VALUE;
    }

    p_st->buff.used = 1;
    int i = 0;

    lb_slot *curr_slot;
    while (i < p_st->slot_left && i < LB_MT_NB_CHILDREN) {
        curr_slot = &p_st->p_pool->slots[p_st->i_lb];
        // Copy the slot hash to send buff
        memcpy(p_st->buff.payload + p_st->buff.used, curr_slot->p_hash,
               LB_SLOT_SIGNATURE_SIZE);

        // Update the send buff's used bytes count
        p_st->buff.used += LB_SLOT_SIGNATURE_SIZE;

        // Prepare p_st->i_lb for next call
        p_st->i_lb = curr_slot->i_next;

        // Cout the number of hash written in payload during this loop
        i++;
    }
    p_st->slot_left -= i;
    return LB_OK;
}

// Fill the payload with the positon of missing slots
// withing the RX_B burst that happened juste before the call
// to this function
err_t lb_burst_TX_BR(lb_task_stack *p_st) {
    if (p_st == NULL) {
        return LB_WRONG_ARGS_VALUE;
    }

    p_st->buff.used = 1;
    int32_t nb_slots;
    int i = 0;

    while (i < p_st->slot_left && i < LB_MT_NB_CHILDREN) {
        nb_slots = p_st->i_lb;
        // Copy the slot hash to send buff and update cursor
        memcpy(&p_st->buff.payload[p_st->buff.used],
               &p_st->to_recv.indexs[nb_slots], sizeof(int32_t));
        p_st->buff.used += sizeof(int32_t);

        // Prepare p_st->i_lb for next call
        p_st->i_lb++;

        // Cout the number of order written in payload during this loop
        i++;
    }
    p_st->slot_left -= i;
    return LB_OK;
}

err_t lb_transmission(lb_task_stack *p_st, lb_task *p_task) {
    if (p_st == NULL || p_task == NULL) {
        return LB_WRONG_ARGS_VALUE;
    }

    p_st->buff.used = 0;

    switch (p_task->type) {
    case TASK_TX | TASK_ROOT:
        lb_fill_payload_ROOT(p_st, p_task->p_node);
#ifdef DEBUG_MERGE
        LBLOG("fill ROOT\n");
#endif
        break;
    case TASK_TX | TASK_CHILD:
        lb_fill_payload_C(p_st, p_task->p_node);
#ifdef DEBUG_MERGE
        LBLOG("fill C\n");
#endif
        break;
    case TASK_TX | TASK_BCKT:
        lb_fill_payload_B(p_st, p_task->p_node);
#ifdef DEBUG_MERGE
        LBLOG("fill B\n");
#endif
        break;
    case TASK_TX | TASK_BCKT_RESP:
        lb_fill_payload_BR(p_st, p_task->p_node);
#ifdef DEBUG_MERGE
        LBLOG("fill BR\n");
#endif
        break;
    case TASK_TX | TASK_EMPTY:
        lb_fill_payload_E(p_st);
#ifdef DEBUG_MERGE
        LBLOG("fill E\n");
#endif
        break;
    default:
        return LB_ERR;
    }
    return LB_OK;
}

// Compare other device's root to ours
err_t lb_handle_ROOT(lb_task_stack *p_st, lb_merkle_tree *p_node) {
    if (p_st == NULL || p_node == NULL) {
        return LB_WRONG_ARGS_VALUE;
    }

    int pyld_index = 1;
    // If the hashes are equal, add a TASK_EMPTY send
    if (lb_compare_hash(p_node->p_hash, p_st->buff.payload + pyld_index) == 0) {
        lb_stack_push(p_st, TASK_TX | TASK_EMPTY, p_node);
        // The hashes are not equal, test if we are handling a leaf or a
        // node
    } else if (p_node->p_children[0] != NULL) {
        // We are handling a node, adding a TASK_CHILD send
        lb_stack_push(p_st, TASK_TX | TASK_CHILD, p_node);
    } else {
        // We are handling a leaf, adding a TASK_BCKT send
        lb_stack_push(p_st, TASK_TX | TASK_BCKT, p_node);
    }
    return LB_OK;
}

// For each child of the associated node, if the corresponding hash
// in the payload is equal, add an E-transmission to the stack.
// Else, add a transmission to the stack associated with the
// child of type C if the child has children, and of type B if it does not
err_t lb_handle_C(lb_task_stack *p_st, lb_merkle_tree *p_node) {
    if (p_st == NULL || p_node == NULL) {
        return LB_WRONG_ARGS_VALUE;
    }

    int pyld_index = 1;
    for (int i = 0; i < LB_MT_NB_CHILDREN; i++) {
        lb_merkle_tree *p_child = p_node->p_children[i];
        // If the hashes are equal, add a TASK_EMPTY send
        if (lb_compare_hash(p_child->p_hash, p_st->buff.payload + pyld_index) ==
            0) {
            lb_stack_push(p_st, TASK_TX | TASK_EMPTY, p_child);
            // The hashes are not equal, test if we are handling a leaf or a
            // node
        } else if (p_child->p_children[0] != NULL) {
            // We are handling a node, adding a TASK_CHILD send
            lb_stack_push(p_st, TASK_TX | TASK_CHILD, p_child);
        } else {
            // We are handling a leaf, adding a TASK_BCKT send
            lb_stack_push(p_st, TASK_TX | TASK_BCKT, p_child);
        }
        // Move across the payload
        pyld_index += LB_SLOT_SIGNATURE_SIZE;
    }
    return LB_OK;
}

err_t lb_handle_B(lb_task_stack *p_st, lb_merkle_tree *p_node) {
    if (p_st == NULL || p_node == NULL) {
        return LB_WRONG_ARGS_VALUE;
    }
    lb_stack_push(p_st, TASK_TX | TASK_BCKT_RESP, p_node);
    lb_unset_shared(p_node, p_st->p_pool);
    memcpy(&p_st->slot_left, &p_st->buff.payload[1], sizeof(int32_t));
    p_st->i_lb = 0;
#ifdef DEBUG_MERGE
    LBLOG("Handle B value: %d\n", p_st->slot_left);
#endif
    lb_istack_clear(&p_st->to_recv);
    return LB_OK;
}

err_t lb_handle_BR(lb_task_stack *p_st, lb_merkle_tree *p_node) {
    if (p_st == NULL || p_node == NULL) {
        return LB_WRONG_ARGS_VALUE;
    }

    memcpy(&p_st->slot_left, &p_st->buff.payload[1], sizeof(int32_t));
    if (p_st->slot_left != 0) {
        lb_unset_shared(p_node, p_st->p_pool);
    }
#ifdef DEBUG_MERGE
    LBLOG("lb_handle_BR : slot_left value %d\n", p_st->slot_left);
#endif
    p_st->i_lb = p_node->i_bucket_slot;
    return LB_OK;
}

// For each slot received, find it in the leaf's bucket
// If it couldn't be found, add it to to_receive
// Mark slots not present in the received burst with shared = 0
err_t lb_burst_RX_B(lb_task_stack *p_st, lb_merkle_tree *p_node) {
    if (p_st == NULL || p_node == NULL) {
        return LB_WRONG_ARGS_VALUE;
    }
    int i = 0;
    int pyld_index = 1;
    while (i < p_st->slot_left && i < LB_MT_NB_CHILDREN) {
        ssize_t i_tmp = p_node->i_bucket_slot;
        lb_slot *curr_slot = &p_st->p_pool->slots[i_tmp];

        // Retrieve the hash of a slot
        uint8_t *dist_hash = p_st->buff.payload + pyld_index;

        // Cycle through leaf's bucket to find the requested slot
        while (i_tmp != -1 &&
               p_st->p_pool->slots[i_tmp].timestamp < p_node->end) {
            curr_slot = &p_st->p_pool->slots[i_tmp];
            if (lb_compare_hash(dist_hash, curr_slot->p_hash) == 0) {
#ifdef DEBUG_MERGE
                LBLOG("Slot marked shared in RX_B: %ld\n", p_st->i_lb);
#endif
                curr_slot->shared = 1;
                break;
            }
            i_tmp = curr_slot->i_next;
        }

        // If we left the while loop because we could
        // not find a slot with the same hash
        if (i_tmp == -1 || curr_slot->shared == 0) {
            // Push the index of the requested slot in to_recv
            lb_istack_push(&p_st->to_recv, p_st->i_lb);
#ifdef DEBUG_MERGE
            LBLOG("Slot marked not shared in RX_B: %ld\n", p_st->i_lb);
#endif
            p_st->to_recv_count++;
        }

        pyld_index +=
            LB_SLOT_SIGNATURE_SIZE; // update cursor position in payload

        // Increment the number of slot received
        p_st->i_lb++;
        i++;
    }
    p_st->slot_left -= i;
    return LB_OK;
}

// For each slot order in the payload, find it in the leaf's bucket
err_t lb_burst_RX_BR(lb_task_stack *p_st, lb_merkle_tree *p_node) {
    if (p_st == NULL || p_node == NULL) {
        return LB_WRONG_ARGS_VALUE;
    }
    int i = 0;
    lb_slot_pool *p_pool = p_st->p_pool;
    lb_slot *curr_slot = &p_pool->slots[p_st->i_lb];
    static int32_t to_jump = 0;
    int pyld_index = 1;
    while (i < p_st->slot_left && i < LB_MT_NB_CHILDREN) {
        curr_slot = &p_pool->slots[p_st->i_lb];

        // Retrieve the position of the requested slot in the leaf's bucket
        int32_t old_to_jump = to_jump;
        memcpy(&to_jump, &p_st->buff.payload[pyld_index], sizeof(int32_t));
#ifdef DEBUG_MERGE
        LBLOG("Value read from payload to_jump: %d\n", to_jump);
#endif

        // Cycle through leaf's bucket to find the requested slot
        for (int j = old_to_jump; j < to_jump; j++) {
            p_pool->slots[p_st->i_lb].shared = 1;
#ifdef DEBUG_MERGE
            LBLOG("Slot (ts = %d) marked shared: %ld\n", curr_slot->timestamp,
                  curr_slot - p_st->p_pool->slots);
#endif
            p_st->i_lb = curr_slot->i_next;
            curr_slot = &p_pool->slots[p_st->i_lb];
        }
        to_jump++;
#ifdef DEBUG_MERGE
        LBLOG("Slot (ts = %d)  marked not shared: %ld\n", curr_slot->timestamp,
              curr_slot - p_st->p_pool->slots);
#endif
        p_st->i_lb = curr_slot->i_next;

        pyld_index += sizeof(int32_t);
        i++;
    }
    p_st->slot_left -= i;

    // If we are done with asked bucket, set the remainings as shared
    if (p_st->slot_left <= 0) {
        to_jump = 0;
        while (p_st->i_lb != -1 &&
               p_pool->slots[p_st->i_lb].timestamp < p_node->end) {
            p_pool->slots[p_st->i_lb].shared = 1;
            p_st->i_lb = curr_slot->i_next;
            curr_slot = &p_pool->slots[p_st->i_lb];
        }
    }
    return LB_OK;
}

err_t lb_reception(lb_task_stack *p_st, lb_task *p_task) {
    if (p_st == NULL || p_task == NULL) {
        return LB_WRONG_ARGS_VALUE;
    }

    p_st->buff.used = 0;

    switch (p_st->buff.payload[0]) {
    case TASK_ROOT:
        lb_handle_ROOT(p_st, p_task->p_node);
#ifdef DEBUG_MERGE
        LBLOG("handle ROOT\n");
#endif
        break;
    case TASK_CHILD:
        lb_handle_C(p_st, p_task->p_node);
#ifdef DEBUG_MERGE
        LBLOG("handle C\n");
#endif
        break;
    case TASK_BCKT:
        lb_handle_B(p_st, p_task->p_node);
#ifdef DEBUG_MERGE
        LBLOG("handle B\n");
#endif
        break;
    case TASK_BCKT_RESP:
        lb_handle_BR(p_st, p_task->p_node);
#ifdef DEBUG_MERGE
        LBLOG("handle BR\n");
#endif
        break;
    case TASK_EMPTY:
#ifdef DEBUG_MERGE
        LBLOG("handle E\n");
#endif
        break;
    default:
        return LB_ERR;
    }
    return LB_OK;
}

err_t lb_diff(lb_task_stack *p_st, lb_merkle_tree *p_root, int start,
              lb_diff_RX_TX lb_send, void *lb_send_args, lb_diff_RX_TX lb_recv,
              void *lb_recv_args) {
    if (p_st == NULL || p_root == NULL || p_st->p_pool == NULL ||
        lb_send == NULL || lb_recv == NULL)
        return LB_WRONG_ARGS_VALUE;

    // Make sure every slot is marked as non-shared
    lb_set_shared_flag(p_st->p_pool);
    lb_istack_clear(&p_st->to_recv);
    p_st->to_recv_count = 0;

    if (start) {
        lb_stack_push(p_st, TASK_TX | TASK_ROOT, p_root);
    } else {
        lb_stack_push(p_st, TASK_RX, p_root);
    }
    err_t retval;
#ifdef DEBUG_MERGE
    int counter = 1;
#endif
    lb_task task;
    // Loop while we have transactions to do
    while (lb_stack_size(p_st) > 0) {
#ifdef DEBUG_MERGE
        LBLOG("Step %d\n", counter++);
        LBLOG("Stack size: %ld ", p_st->front);
#endif
        lb_stack_peek(p_st, &task.type, &task.p_node);
        lb_stack_pop(p_st);
        if (task.type & TASK_RX) {
            retval = lb_recv(p_st, lb_recv_args);
            if (retval != LB_OK) {
                return LB_ERR;
            }
            lb_reception(p_st, &task);
            while (p_st->slot_left > 0) {
#ifdef DEBUG_MERGE
                LBLOG("RX : slot_left : %d\n", p_st->slot_left);
#endif
                retval = lb_recv(p_st, lb_recv_args);
                if (retval != LB_OK) {
                    return LB_ERR;
                }
                if (p_st->buff.payload[0] & TASK_BCKT) {
                    lb_burst_RX_B(p_st, task.p_node);
                } else if (p_st->buff.payload[0] & TASK_BCKT_RESP) {
                    lb_burst_RX_BR(p_st, task.p_node);
                }
            }
        } else if (task.type & TASK_TX) {
            lb_transmission(p_st, &task);
            retval = lb_send(p_st, lb_send_args);
            if (retval != LB_OK) {
                return LB_ERR;
            }
            while (p_st->slot_left > 0) {
#ifdef DEBUG_MERGE
                LBLOG("TX : slot_left : %d\n", p_st->slot_left);
#endif
                if (task.type & TASK_BCKT) {
                    lb_burst_TX_B(p_st, task.p_node);
                } else if (task.type & TASK_BCKT_RESP) {
                    lb_burst_TX_BR(p_st);
                }
                retval = lb_send(p_st, lb_send_args);
                if (retval != LB_OK) {
                    return LB_ERR;
                }
            }
        } else {
            return LB_ERR;
        }
    }
    lb_print_diff_result(p_st);
    return LB_OK;
}

#define WAITING_TO_START 3
#define DOING 2
#define DONE 1
#if !defined(__ANDROID__)
err_t lb_next_msg_to_send(lb_storage_func lb_load_msg, void *lb_load_msg_arg,
                          lb_slot_pool *p_pool, uint8_t *p_buff,
                          size_t *p_written) {
    static char done = WAITING_TO_START;
    static ssize_t i_curr = 0;

    if (p_pool == NULL || lb_load_msg == NULL || p_buff == NULL ||
        p_written == NULL) {
        done = WAITING_TO_START;
        return LB_WRONG_ARGS_VALUE;
    }
    ssize_t i_tmp = p_pool->i_begin_busy;
    if (done == DONE) {
        done = WAITING_TO_START;
        *p_written = 0;
        return LB_OK;
    } else if (done == DOING) {
        i_tmp = i_curr;
    } else if (done == WAITING_TO_START) {
        i_curr = p_pool->i_begin_busy;
        i_tmp = i_curr;
    } else {
        // Should never happen
        return LB_ERR;
    }

    message_t msg;
    err_t retval = LB_OK;
    while (i_tmp != -1) {
        lb_slot *p_slot = &p_pool->slots[i_tmp];
        if (p_slot->shared == 0) {
            // load it from the mass storage
            retval = lb_load_msg(lb_load_msg_arg, p_slot->addr, &msg);
            if (retval != LB_OK || msg.size > MESSAGE_MAX_SIZE) {
                done = WAITING_TO_START;
                *p_written = 0;
                return LB_ERR;
            }
            LBLOG("Loaded message from flash");
            for (int i = 0; i < 6; i++) {
                LBLOG("%d", msg.signature[i]);
            }
            message_to_raw(p_buff, &msg);
            *p_written = msg.size;
            i_curr = p_slot->i_next;
            if (p_slot->i_next == -1) {
                done = DONE;
                return LB_OK;
            }
            done = DOING;
            return LB_OK;
        }
        i_tmp = p_slot->i_next;
    }
    // Reset for next merge
    i_curr = -1;
    *p_written = 0;
    return LB_OK;
}
#else
err_t lb_next_msg_to_send(lb_slot_pool *p_pool, int32_t *p_db_addr) {
    static char done = WAITING_TO_START;
    static ssize_t i_curr = 0;

    if (p_pool == NULL || p_db_addr == NULL) {
        done = WAITING_TO_START;
        return LB_WRONG_ARGS_VALUE;
    }
    ssize_t i_tmp = p_pool->i_begin_busy;
    if (done == DONE) {
        done = WAITING_TO_START;
        *p_db_addr = -1;
        return LB_OK;
    } else if (done == DOING) {
        i_tmp = i_curr;
    } else if (done == WAITING_TO_START) {
        i_curr = p_pool->i_begin_busy;
        i_tmp = i_curr;
    } else {
        // Should never happen
        return LB_ERR;
    }

    message_t msg;
    err_t retval = LB_OK;
    while (i_tmp != -1) {
        lb_slot *p_slot = &p_pool->slots[i_tmp];
        if (p_slot->shared == 0) {
            // load it from the mass storage
            *p_db_addr = p_pool->slots[i_tmp].addr;
            i_curr = p_slot->i_next;
            if (p_slot->i_next == -1) {
                done = DONE;
                return LB_OK;
            }
            done = DOING;
            return LB_OK;
        }
        i_tmp = p_slot->i_next;
    }
    // Reset for next merge
    i_curr = -1;
    *p_db_addr = -1;
    done = WAITING_TO_START;
    return LB_OK;
}
#endif
