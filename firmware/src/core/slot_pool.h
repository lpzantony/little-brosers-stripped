#ifndef SLOT_POOL_H
#define SLOT_POOL_H
#include "message.h"
#include <unistd.h>

#define LB_NB_MAX_MSGS 700

#define LB_FREE_SLOT_SIZE 1 // Can't be more than LB_NB_MAX_MSGS

#define LB_SLOT_SIGNATURE_SIZE 6
#define LB_ADDR_FIRST_STORAGE 128

// WARNING : LB_SLOT_SIZE_BYTE must be a divider of 528 ! (extflash)
// LB_SLOT_SIZE_BYTE Shoule be 16 + LB_SLOT_SIGNATURE_SIZE
#define LB_SLOT_SIZE_BYTE                                                      \
    (sizeof(ssize_t) + sizeof(ssize_t) + sizeof(uint32_t) + sizeof(uint32_t) + \
     sizeof(uint8_t) * LB_SLOT_SIGNATURE_SIZE)

#define LB_SLOT_POOL_MD_SIZE_BYTE                                              \
    (sizeof(ssize_t) + sizeof(ssize_t) + sizeof(size_t) + sizeof(size_t) +     \
     sizeof(uint32_t))

#define LB_SLOT_POOL_SIZE_BYTE                                                 \
    (LB_SLOT_SIZE_BYTE + LB_SLOT_POOL_MD_SIZE_BYTE) * (LB_NB_MAX_MSGS + 1)

typedef struct __attribute__((__packed__)) {
    ssize_t i_previous;
    ssize_t i_next;
    uint32_t timestamp;
    uint32_t addr;
    uint8_t p_hash[LB_SLOT_SIGNATURE_SIZE];
    // 'shared' is used during diff tells if this slot is present in the other
    // device
    uint8_t shared;
} lb_slot;

typedef struct __attribute__((__packed__)) {
    // The + 1 is used to ease the memory management when all slots are full
    lb_slot slots[LB_NB_MAX_MSGS + 1];
    ssize_t i_begin_busy;
    ssize_t i_begin_free;
    size_t nb_free;
    size_t nb_slots;
    uint32_t certif_date;
} lb_slot_pool;

typedef err_t (*lb_slot_storage_func)(lb_slot_pool *, void *);

#define LB_ALLOC_SLOT_POOL(slot_pool) lb_slot_pool slot_pool

static inline int lb_is_free_pool_empty(lb_slot_pool *slot_pool) {
    return (slot_pool->nb_free <= 1);
}
static inline err_t lb_set_shared_flag(lb_slot_pool *slot_pool) {
    if (slot_pool == NULL) {
        return LB_WRONG_ARGS_VALUE;
    }
    for (int i = 0; i < LB_NB_MAX_MSGS + 1; i++) {
        slot_pool->slots[i].shared = 1;
    }
    return LB_OK;
}

err_t lb_init_slots(lb_slot_pool *slot_pool, uint32_t certif_date);

err_t lb_empty_slot_pool(lb_slot_pool *slot_pool);

ssize_t lb_get_free_slot(lb_slot_pool *slot_pool);
err_t lb_slot_to_raw(uint8_t *buff, lb_slot *p_slot);
err_t lb_slot_from_raw(uint8_t *buff, lb_slot *p_slot);
err_t lb_slot_md_to_raw(uint8_t *buff, lb_slot_pool *p_pool);
err_t lb_slot_md_from_raw(uint8_t *buff, lb_slot_pool *p_pool);

static inline uint32_t lb_convert_absolute_to_relative_timestamp(
    uint32_t timestamp, uint32_t certif_date, uint32_t offset) {
    uint32_t diff = MAX(certif_date, timestamp) - MIN(certif_date, timestamp);
    return MAX(diff, offset) - MIN(diff, offset);
}
#ifndef __ANDROID__
static inline void lb_fill_slot(lb_slot *p_slot, message_t *p_msg,
                                uint32_t certif_date, uint32_t offset) {
#else
static inline void lb_fill_slot(lb_slot *p_slot, message_t *p_msg,
                                uint32_t certif_date, uint32_t offset,
                                uint32_t db_addr) {
#endif
    // Fill the freshly reset slot
    p_slot->timestamp = lb_convert_absolute_to_relative_timestamp(
        p_msg->header.timestamp, certif_date, offset);

#ifdef __ANDROID__
    p_slot->addr = db_addr;
#endif

    // The offset is to avoid collisions due to the first four bytes of an ECDSA
    // signature are very similar
    // No offset for Android since the data passed through the JNI already takes
    // the offset into account
#ifndef __ANDROID__
    memcpy(p_slot->p_hash, p_msg->signature, LB_SLOT_SIGNATURE_SIZE);
#else
    memcpy(p_slot->p_hash, p_msg->signature, LB_SLOT_SIGNATURE_SIZE);
#endif
}

void lb_free_a_slot(lb_slot_pool *slot_pool, ssize_t i_to_free);

void lb_clear_slot(lb_slot_pool *p_slot_pool, ssize_t i_to_clear);

void lb_insert_slot_after(lb_slot_pool *slot_pool, ssize_t i_stock,
                          ssize_t i_to_insert);
void lb_insert_slot_before(lb_slot_pool *slot_pool, ssize_t i_stock,
                           ssize_t i_to_insert);

int lb_compare_slot(lb_slot *to_compare, lb_slot *base);

// Be careful, this function only compares hash-order, not the timestamp
int lb_compare_slot_hash(lb_slot *to_compare, lb_slot *base);

err_t lb_get_send_count(lb_slot_pool *p_slot_pool, uint32_t *p_send_count);

static inline int lb_compare_hash(const uint8_t *const to_compare,
                                  const uint8_t *const base) {
    if (base == NULL || to_compare == NULL)
        return LB_ERR;

    for (int i = 0; i < LB_SLOT_SIGNATURE_SIZE; i++) {
        if (to_compare[i] < base[i])
            return -1;
        else if (to_compare[i] > base[i])
            return 1;
    }
    return 0;
}

#if defined(CPPSIM) || defined(__ANDROID__)
err_t lb_save_slot_pool(lb_slot_pool *p_slot_pool, void *path);
int lb_does_file_exists(char *path);
err_t lb_load_slot_pool_size(size_t *p_pool_size, void *path);
err_t lb_load_slot_pool(lb_slot_pool *p_slot_pool, void *path);
#endif
err_t lb_slot_pool_size(lb_slot_pool *p_slot_pool, unsigned int *pool_size);
#endif /* SLOT_POOL_H */
