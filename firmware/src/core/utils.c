#include "utils.h"

// Temporary solution
#define COMPANY_ID 0xDEAD
#define NETWORK_ID 0xBEAF
#define DROP_ID 0x6790

// Byte swap unsigned short
uint16_t swap_uint16(uint16_t val) { return (val << 8) | (val >> 8); }

// Byte swap unsigned int
uint32_t swap_uint32(uint32_t val) {
    val = ((val << 8) & 0xFF00FF00) | ((val >> 8) & 0xFF00FF);
    return (val << 16) | (val >> 16);
}

// 4 uint8_t to uint32_t
uint32_t bytes_to_uint32(const uint8_t bytes[]) {
    return (bytes[0] << 24) + (bytes[1] << 16) + (bytes[2] << 8) + (bytes[3]);
}

// 4 uint8_t to int32_t
int32_t bytes_to_int32(const uint8_t bytes[]) {
    return (bytes[0] << 24) + (bytes[1] << 16) + (bytes[2] << 8) + (bytes[3]);
}

// 2 uint8_t to uint16_t
uint16_t bytes_to_uint16(const uint8_t bytes[]) {
    return (bytes[0] << 8) + (bytes[1]);
}

// uint32_t to 4 uint8_t
int uint32_to_bytes(const uint32_t val, uint8_t bytes[]) {
    bytes[0] = (val >> 24) & 0xFF;
    bytes[1] = (val >> 16) & 0xFF;
    bytes[2] = (val >> 8) & 0xFF;
    bytes[3] = val & 0xFF;
    return 4;
}

// int32_t to 4 uint8_t
int int32_to_bytes(const int32_t val, uint8_t bytes[]) {
    bytes[0] = (val >> 24) & 0xFF;
    bytes[1] = (val >> 16) & 0xFF;
    bytes[2] = (val >> 8) & 0xFF;
    bytes[3] = val & 0xFF;
    return 4;
}

// uint16_t to 2 uint8_t
int uint16_to_bytes(const uint16_t val, uint8_t bytes[]) {
    bytes[0] = (val >> 8) & 0xFF;
    bytes[1] = val & 0xFF;
    return 2;
}
