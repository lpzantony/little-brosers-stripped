#ifndef MERKLE_MERGE_H
#define MERKLE_MERGE_H
#include "merkle_tree.h"
#include "message.h"
#include "task_stack.h"
#include "utils.h"

// A lb_diff_RX_TX function can be a sending or a receiving function
// As a sending function, it is supposed to send the content of
// stack->buff->payload with a size of stack->buff->used
// And only return when it is sent
// As a receiving function, it is supposed to received bytes
// and put it from the begining in stack->buff->payload
// And only return when something has been received
typedef err_t (*lb_diff_RX_TX)(lb_task_stack *, void *);

err_t lb_diff(lb_task_stack *p_st, lb_merkle_tree *p_root, int start,
              lb_diff_RX_TX lb_send, void *lb_send_args, lb_diff_RX_TX lb_recv,
              void *lb_recv_args);

static inline err_t lb_print_diff_result(lb_task_stack *p_st) {
    if (p_st == NULL) {
        return LB_WRONG_ARGS_VALUE;
    }
    LBLOG("To send : \n");
    ssize_t i_curr = p_st->p_pool->i_begin_busy;
    lb_slot *p_slot = &p_st->p_pool->slots[i_curr];
    int i = 0;
    while (i_curr != -1) {
        p_slot = &p_st->p_pool->slots[i_curr];
        if (p_slot->shared == 0) {
            LBLOG("%2d : h =", i);
            for (int j = 0; j < LB_SLOT_SIGNATURE_SIZE; j++) {
                LBLOG(" %2d", p_slot->p_hash[j]);
            }
            LBLOG(" t = %6d\n", p_slot->timestamp);
            i++;
        }
        i_curr = p_slot->i_next;
    }
    LBLOG("\nNeed to send    %4d\n", i);
    LBLOG("Need to receive %4d\n", LU(p_st->to_recv_count));
    return LB_OK;
}
#if !defined(__ANDROID__)
err_t lb_next_msg_to_send(lb_storage_func lb_load_msg, void *lb_load_msg_arg,
                          lb_slot_pool *p_pool, uint8_t *p_buff,
                          size_t *p_written);
#else
err_t lb_next_msg_to_send(lb_slot_pool *p_pool, int32_t *p_db_addr);
#endif

// Start : Exported functions for test

err_t lb_fill_payload_ROOT(lb_task_stack *p_st, lb_merkle_tree *p_node);
err_t lb_fill_payload_C(lb_task_stack *p_st, lb_merkle_tree *p_node);
err_t lb_fill_payload_B(lb_task_stack *p_st, lb_merkle_tree *p_node);
err_t lb_fill_payload_BR(lb_task_stack *p_st, lb_merkle_tree *p_node);
err_t lb_fill_payload_E(lb_task_stack *p_st);
err_t lb_burst_TX_B(lb_task_stack *p_st, lb_merkle_tree *p_node);
err_t lb_burst_TX_BR(lb_task_stack *p_st);
err_t lb_transmission(lb_task_stack *p_st, lb_task *p_task);
err_t lb_handle_ROOT(lb_task_stack *p_st, lb_merkle_tree *p_node);
err_t lb_handle_C(lb_task_stack *p_st, lb_merkle_tree *p_node);
err_t lb_handle_B(lb_task_stack *p_st, lb_merkle_tree *p_node);
err_t lb_handle_BR(lb_task_stack *p_st, lb_merkle_tree *p_node);
err_t lb_burst_RX_B(lb_task_stack *p_st, lb_merkle_tree *p_node);
err_t lb_burst_RX_BR(lb_task_stack *p_st, lb_merkle_tree *p_node);
err_t lb_reception(lb_task_stack *p_st, lb_task *p_task);

// End   : Exported functiosn for test

#endif
