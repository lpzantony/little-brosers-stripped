#ifndef TASK_STACK_H
#define TASK_STACK_H

#include "index_stack.h"
#include "merkle_tree_constants.lbf"
#include "utils.h"
#include <stddef.h>

#define TASK_TX (1 << 7)
#define TASK_RX (1 << 6)
#define TASK_CHILD (1 << 5)
#define TASK_EMPTY (1 << 4)
#define TASK_BCKT (1 << 3)
#define TASK_BCKT_RESP (1 << 2)
#define TASK_ROOT (1 << 1)

#define LB_STACK_SIZE LB_MT_LARGEST_SIZE + LB_MT_NB_CHILDREN
#define LB_STACK_PLD_SIZE 1 + LB_MT_NB_CHILDREN *LB_SLOT_SIGNATURE_SIZE
#define IS_TX()
typedef struct {
    uint8_t type;
    lb_merkle_tree *p_node;
} lb_task;
typedef struct {
    uint8_t payload[1 + LB_STACK_PLD_SIZE];
    size_t used;
} lb_diff_buff;

typedef struct {
    lb_diff_buff buff;
    lb_task tasks[LB_STACK_SIZE];
    lb_index_stack to_recv;
    int32_t slot_left;
    size_t front;
    size_t to_recv_count;
    lb_slot_pool *p_pool;
    // Use of i_lb :
    // During burst RX BR :
    //     Index to the bucket's slot we need
    //     to start the burst on
    // During burst RX B :
    //     Count the number of slots we received
    // During burst TX B :
    //     Keep track of slot's index at which
    //     we stopped during the previous burst packet
    // During burst TX BR :
    //     Keep track of slot's index at which
    //     we stopped during the previous burst packet
    //     within the p_st->to_recv stack
    ssize_t i_lb;
} lb_task_stack;

static inline err_t lb_stack_init(lb_task_stack *st, lb_slot_pool *p_pool) {
    if (st == NULL || p_pool == NULL)
        return LB_WRONG_ARGS_VALUE;
    st->front = 0;
    for (int i = 0; i < LB_STACK_SIZE; i++) {
        st->tasks[i].type = 0;
        st->tasks[i].p_node = NULL;
    }
    memset(st->buff.payload, 0, LB_STACK_PLD_SIZE);
    st->buff.used = 0;
    st->slot_left = 0;
    st->p_pool = p_pool;
    lb_istack_init(&st->to_recv);
    st->to_recv_count = 0;
    return LB_OK;
}

static inline int lb_stack_size(lb_task_stack *st) {
    if (st == NULL)
        return LB_WRONG_ARGS_VALUE;
    return st->front;
}

static inline err_t lb_stack_push(lb_task_stack *st, uint8_t type,
                                  lb_merkle_tree *p_node) {
    if (st == NULL || p_node == NULL)
        return LB_WRONG_ARGS_VALUE;
    if (st->front >= LB_STACK_SIZE) {
        return LB_STACK_FULL;
    }
    st->tasks[st->front].type = type;
    st->tasks[st->front].p_node = p_node;
    st->front++;
    return LB_OK;
}

static inline err_t lb_stack_peek(lb_task_stack *st, uint8_t *type,
                                  lb_merkle_tree **p_node) {
    if (st == NULL || type == NULL || p_node == NULL) {
        return LB_WRONG_ARGS_VALUE;
    }
    if (st->front == 0) {
        *type = 0;
        *p_node = NULL;
        return LB_STACK_EMPTY;
    }
    *type = st->tasks[st->front - 1].type;
    *p_node = st->tasks[st->front - 1].p_node;
    return LB_OK;
}

static inline err_t lb_stack_peek_node(lb_task_stack *st,
                                       lb_merkle_tree **p_node) {
    if (st == NULL || p_node == NULL)
        return LB_WRONG_ARGS_VALUE;
    if (st->front == 0) {
        *p_node = NULL;
        return LB_STACK_EMPTY;
    }
    *p_node = st->tasks[st->front - 1].p_node;
    return LB_OK;
}

static inline err_t lb_stack_pop(lb_task_stack *st) {
    if (st == NULL)
        return LB_WRONG_ARGS_VALUE;
    if (st->front == 0) {
        return LB_STACK_EMPTY;
    }
    st->front--;
    return LB_OK;
}

static inline err_t lb_stack_dump(lb_task_stack *st) {
    if (st == NULL)
        return LB_WRONG_ARGS_VALUE;
    for (int i = st->front - 1; i >= 0; i--) {
        LBLOG("task %d of type %02x linked to node %p\n", i, st->tasks[i].type,
              st->tasks[i].p_node);
    }
    return LB_OK;
}

static inline void lb_dump_payload(lb_task_stack *p_st) {
    LBLOG("Payload dump ");
    for (int i = 0; i < LB_STACK_PLD_SIZE; i++) {
        LBLOG("%02x ", p_st->buff.payload[i]);
    }
    LBLOG("\n");
}

static inline err_t lb_task_size_size(lb_task_stack *p_st,
                                      unsigned int *stack_size) {
    if (p_st == NULL || stack_size == NULL) {
        return LB_ERR;
    }
    // buff->payload
    *stack_size = sizeof(uint8_t) * 1 + LB_STACK_PLD_SIZE;
    // buff->used
    *stack_size += sizeof(size_t);
    // tasks->type
    *stack_size += sizeof(uint8_t) * LB_STACK_SIZE;
    // task->p_node
    *stack_size += sizeof(lb_merkle_tree *) * LB_STACK_SIZE;
    // to_recv->indexs
    *stack_size += sizeof(int32_t *) * LB_NB_MAX_MSGS;
    // to_recv->front
    *stack_size += sizeof(size_t);
    // front
    *stack_size += sizeof(size_t);
    // to_recv_count
    *stack_size += sizeof(size_t);
    // p_pool
    *stack_size += sizeof(lb_slot_pool *);
    // i_lb
    *stack_size += sizeof(ssize_t);
    return LB_OK;
}

#endif
