#ifndef INDEX_STACK_H
#define INDEX_STACK_H
#include "slot_pool.h"
#include "utils.h"
#include <unistd.h>
typedef struct {
    int32_t indexs[LB_NB_MAX_MSGS];
    size_t front;
} lb_index_stack;

static inline err_t lb_istack_init(lb_index_stack *st) {
    if (st == NULL)
        return LB_WRONG_ARGS_VALUE;
    st->front = 0;
    memset(st->indexs, 0, LB_NB_MAX_MSGS * sizeof(int32_t));
    return LB_OK;
}

static inline err_t lb_istack_clear(lb_index_stack *st) {
    if (st == NULL) {
        return LB_WRONG_ARGS_VALUE;
    }
    st->front = 0;
    return LB_OK;
}

static inline int lb_istack_size(lb_index_stack *st) {
    if (st == NULL)
        return LB_WRONG_ARGS_VALUE;
    return st->front;
}

static inline err_t lb_istack_push(lb_index_stack *st, int32_t index) {
    if (st == NULL)
        return LB_WRONG_ARGS_VALUE;
    if (st->front >= LB_NB_MAX_MSGS) {
        return LB_STACK_FULL;
    }
    st->indexs[st->front] = index;
    st->front++;
    return LB_OK;
}

static inline err_t lb_istack_peek(lb_index_stack *st, int32_t *index) {
    if (st == NULL)
        return LB_WRONG_ARGS_VALUE;
    if (st->front == 0) {
        return LB_STACK_EMPTY;
    }
    *index = st->indexs[st->front - 1];
    return LB_OK;
}

static inline err_t lb_istack_pop(lb_index_stack *st) {
    if (st == NULL)
        return LB_WRONG_ARGS_VALUE;
    if (st->front == 0) {
        return LB_STACK_EMPTY;
    }
    st->front--;
    return LB_OK;
}

static inline err_t lb_istack_dump(lb_index_stack *st) {
    if (st == NULL)
        return LB_WRONG_ARGS_VALUE;
    for (int i = st->front - 1; i >= 0; i--) {
        LBLOG("Index %d is %d\n", i, st->indexs[i]);
    }
    return LB_OK;
}

#endif