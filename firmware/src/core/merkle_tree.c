#include "merkle_tree.h"
#include "utils.h"
#include <stdlib.h>
#include <string.h>
#if defined(CPPSIM) || defined(__ANDROID__)
#include "sha256.h"
#else
#include "nrf_log.h"
#endif

void sub_print_merkle_tree(lb_merkle_tree *mk, lb_slot_pool *slot_pool) {
    if (mk == NULL) {
        return;
    }
    uint16_t mk_depth = lb_get_node_depth(mk);
    if (mk->p_children[0] == NULL) {
        LBLOG("Leaf, level %i, node n° %i (%d to %d)\n", mk_depth, mk->array_id,
              mk->start, mk->end);
        ssize_t tmp_i = mk->i_bucket_slot;

        // For debugging purposes we print the whole list to verify it is
        // correctly linked
        while (tmp_i != -1 && (slot_pool->slots + tmp_i)->timestamp < mk->end) {
            lb_slot *bucket_slot = &slot_pool->slots[tmp_i];

            LBLOG("Leaf %i (%d to %d) has a slot in its bucket (timestamp : "
                  "%i, h[0] = %d)\n",
                  mk->array_id, mk->start, mk->end, bucket_slot->timestamp,
                  bucket_slot->p_hash[0]);

            if (tmp_i == bucket_slot->i_next) {
                exit(LB_ERR);
            }

            tmp_i = bucket_slot->i_next;
        }
    } else {
        LBLOG("Subtree, level %i, node n° %i\n", mk_depth, mk->array_id);
        for (unsigned int i = 0; i < LB_MT_NB_CHILDREN; i++) {
            sub_print_merkle_tree(mk->p_children[i], slot_pool);
        }
    }
}

void lb_print_merkle_tree(lb_merkle_tree *mk, lb_slot_pool *slot_pool) {
    LBLOG("=====> Begining of the tree ");
    LBLOG("from %d to %d\n", mk[0].start, mk[0].end);
    sub_print_merkle_tree(mk, slot_pool);
    LBLOG("=====> End of the tree\n");
}
err_t lb_init_merkle_tree(lb_merkle_tree *p_root, lb_merkle_root_md *p_root_md,
                          lb_slot_pool *p_slot_pool, uint32_t certif_date) {
    lb_init_slots(p_slot_pool, certif_date);
    err_t retval = lb_init_merkle_tree_only(p_root, p_root_md);
    lb_update_nodes_hash(p_slot_pool, p_root);
    return retval;
}

err_t lb_init_merkle_tree_only(lb_merkle_tree *p_root,
                               lb_merkle_root_md *p_root_md) {

    p_root->start = LB_MT_START;
    p_root->end = LB_MT_END;
    p_root->p_parent =
        NULL; // p_root has no parents, it's a strong independant tree
    p_root->array_id = 0; // this variable has no sense for the p_root

    // Populating the subtrees
    err_t retval = LB_ERR;
    for (int k = 0; k < LB_MT_NB_CHILDREN; k++) {
        p_root->p_children[k]->p_parent = p_root;
        retval =
            lb_build_merkle_tree(p_root->p_children[k], p_root_md->timeframe[k],
                                 p_root_md->timeframe[k + 1], k, 0, 0, p_root);
    }
    return (retval != LB_OK) ? retval : LB_OK;
}

err_t lb_build_merkle_tree(lb_merkle_tree *p_root, uint32_t start, uint32_t end,
                           uint16_t parent_depth, uint16_t depth_id,
                           uint16_t array_id, lb_merkle_tree *p_parent) {

    // Check that arguments are coherent
    if (start > end || LB_MT_BUCKET_SIZE < 1 || p_parent == NULL) {
        return LB_WRONG_ARGS_VALUE;
    }

    // Filling the elements of p_root
    uint32_t children_width = (end - start) / LB_MT_NB_CHILDREN;
    p_root->start = start;
    p_root->end = end;

    // If we are at the begining of the sub-tree
    uint16_t p_root_depth = (p_parent->p_parent == NULL) ? 0 : parent_depth + 1;

    p_root->array_id = array_id;
    p_root->p_parent = p_parent;

    p_root->i_bucket_slot = -1;
    for (unsigned int i = 0; i < LB_SLOT_SIGNATURE_SIZE; i++)
        p_root->p_hash[i] = 0;
    for (unsigned int i = 0; i < LB_MT_NB_CHILDREN; i++)
        p_root->p_children[i] = NULL;

    // Is p_root a leaf or a subtree ?
    if (children_width > LB_MT_BUCKET_SIZE) {
        // We are in a subtree

        for (int i = 0; i < LB_MT_NB_CHILDREN; i++) {
            // Register a pointer to the child i
            uint16_t left_nodes_in_line =
                (pow(LB_MT_NB_CHILDREN, p_root_depth) - depth_id);
            uint16_t shift_to_child =
                left_nodes_in_line + depth_id * LB_MT_NB_CHILDREN + i;
            p_root->p_children[i] = shift_to_child + p_root;

            uint16_t child_array_id = shift_to_child + p_root->array_id;

            // Computing start and end for the child i
            uint32_t child_start = start + i * children_width;

            // Ternary assignement extend the last child width if the width
            // computation result is not an integer
            uint32_t child_end = (i == LB_MT_NB_CHILDREN - 1)
                                     ? end
                                     : child_start + children_width;

            // Init the child i
            lb_build_merkle_tree(p_root->p_children[i], child_start, child_end,
                                 p_root_depth, depth_id * LB_MT_NB_CHILDREN + i,
                                 child_array_id, p_root);
        }
    } else {
        // We are in a leaf, do nothing
    }
    return LB_OK;
}

err_t lb_update_branch_hashes(lb_merkle_tree *p_node, lb_slot_pool *p_pool) {
    if (p_node == NULL || p_pool == NULL) {
        return LB_WRONG_ARGS_VALUE;
    }
    if (p_node->p_parent == NULL) {
        if (p_node->p_children[0] != NULL) {
            lb_compute_node_hash(p_node);
        }
        // Manage if the root is also a leaf. usefull for testing
        else {
            lb_update_leaf_hash(p_pool, p_node);
        }
        return LB_OK;
    }
    if (p_node->p_children[0] != NULL) {
        lb_compute_node_hash(p_node);
        return lb_update_branch_hashes(p_node->p_parent, p_pool);
    } else {
        // We are in a leaf, compute its hash and inform the caller
        lb_update_leaf_hash(p_pool, p_node);
        return lb_update_branch_hashes(p_node->p_parent, p_pool);
    }
}

err_t lb_detach_slot(lb_merkle_tree *p_root, ssize_t i_slot,
                     lb_slot_pool *p_pool) {
    if (p_root == NULL || p_pool == NULL) {
        return LB_WRONG_ARGS_VALUE;
    }

    // Get a pointer to the leaf containing slot at i_slot
    lb_merkle_tree *slot_leaf =
        lb_get_leaf_for_timestamp(p_root, p_pool->slots[i_slot].timestamp);

    // Let's make sure to notify any leaf pointing to the slot
    // that this slot will no more be available
    // Two possible cases here, NS being the slot after i_slot
    // 1 : NS belongs to another leaf or doesn't exist, we need to
    //     make i_slot's leaf's bucket point to NULL, that's it
    // 2 : NS belongs to the same leaf.
    //     This is the else part of the if() bellow. We simply
    //     attach this leaf's bucket to NS before freeing the
    //     i_slot
    if (p_pool->slots[i_slot].i_next == -1 ||
        p_pool->slots[p_pool->slots[i_slot].i_next].timestamp >= slot_leaf->end)
        slot_leaf->i_bucket_slot = -1;
    else {
        slot_leaf->i_bucket_slot = p_pool->slots[i_slot].i_next;
    }
    lb_update_branch_hashes(slot_leaf, p_pool);
    return LB_OK;
}

err_t lb_attach_leafs_to_slots(lb_merkle_tree *p_root, lb_slot_pool *p_pool) {
    if (p_root == NULL || p_pool == NULL) {
        return LB_WRONG_ARGS_VALUE;
    }
    ssize_t i_tmp = p_pool->i_begin_busy;
    for (unsigned i = 0; i < LB_MT_NB_CHILDREN; i++) {
        lb_merkle_tree *tmp_leaf =
            lb_get_leaf_for_timestamp(p_root, p_root->p_children[i]->start);
        // From first to last leaf of this subtree
        while (1) {
            tmp_leaf->i_bucket_slot = i_tmp;
            while (i_tmp != -1) {
                // If we moved too far in the linked list and we are out of
                // the tmp_leaf timeframe
                if (p_pool->slots[i_tmp].timestamp >= tmp_leaf->end) {
                    tmp_leaf->i_bucket_slot = -1;
                    break;
                }
                // if the timestamp of slot at i_tmp is greater than or equal
                // to tmp_leaf's start
                if (p_pool->slots[i_tmp].timestamp >= tmp_leaf->start) {
                    tmp_leaf->i_bucket_slot = i_tmp;
                    i_tmp = p_pool->slots[i_tmp].i_next;
                    while (i_tmp != -1 &&
                           p_pool->slots[i_tmp].timestamp < tmp_leaf->end) {
                        i_tmp = p_pool->slots[i_tmp].i_next;
                    }
                    break;
                }
                // i_tmp is not in the leaf bucket, go to next one
                i_tmp = p_pool->slots[i_tmp].i_next;
            }
            // Update leaf's hash and parent's
            lb_update_branch_hashes(tmp_leaf, p_pool);

            // Break because end of the subtree leafs
            if (tmp_leaf->end >= p_root->p_children[i]->end) {
                break;
            }
            tmp_leaf += 1;
        }
    }
    return LB_OK;
}

err_t lb_shift_tree(lb_merkle_tree *p_root, lb_slot_pool *p_pool,
                    uint32_t certif_date, lb_slot_storage_func stor_func,
                    void *stor_func_arg) {
    // Just in case the authentication process to call this function whereas
    // our certificate is not older than the distant one
    if (p_root == NULL || certif_date <= p_pool->certif_date ||
        p_pool == NULL) {
        return LB_WRONG_ARGS_VALUE;
    }
    uint32_t delta = certif_date - p_pool->certif_date;
    ssize_t i_tmp = p_pool->i_begin_busy;

    // Shift timestamp of all used slots
    while (i_tmp != -1) {
        // If the shift pushs the slot's timestamp to underflow, detache then
        // free the slot
        if (delta > p_pool->slots[i_tmp].timestamp) {
            ssize_t i_to_delete = i_tmp;
            i_tmp = p_pool->slots[i_tmp].i_next;
            lb_free_a_slot(p_pool, i_to_delete);
        } else {
            // Otherwise, shift the slot's timestamp
            p_pool->slots[i_tmp].timestamp =
                p_pool->slots[i_tmp].timestamp - delta;
            i_tmp = p_pool->slots[i_tmp].i_next;
        }
    }
    // Linked list of slots is updated, detach all leaf bucket and re-attach it
    lb_attach_leafs_to_slots(p_root, p_pool);
    LBLOG("Shifted, timestamp was %d, will be %d\n", p_pool->certif_date,
          certif_date);
    p_pool->certif_date = certif_date;
    if (stor_func != NULL) {
        err_t retval = stor_func(p_pool, stor_func_arg);
        if (retval != LB_OK) {
            return retval;
        }
    }
    return LB_OK;
}

// This function creates a small linked list for each leaf
// which contains msgs in its timeframe and at the same time
// takes care to attach all those linked lists together
// to form a long sorted linked list of slots
err_t lb_attach_msgs_to_tree(lb_merkle_tree *p_root, message_pool_t *p_msg_pool,
                             lb_slot_pool *p_slot_pool,
                             lb_storage_func lb_store_msg, void *arg2) {
    if (p_root == NULL || p_msg_pool == NULL || p_msg_pool->p_msgs == NULL ||
        p_msg_pool->size == 0) {
        return LB_WRONG_ARGS_VALUE;
    }

#ifdef __ANDROID__
    if (arg2 == NULL)
        return LB_WRONG_ARGS_VALUE;
    uint32_t *p_db_indexs = (uint32_t *)arg2;
#endif

    message_t *p_msgs = p_msg_pool->p_msgs;
    ssize_t nb_msgs = p_msg_pool->size;

    // Each loop insert a msg into the leafs's bucket
    for (unsigned int i = 0; i < nb_msgs; i++) {

        // If the msg is out of the tree boundaries, just ignore it
        if (lb_is_out_of_tree(p_slot_pool->certif_date, p_root,
                              p_msgs[i].header.timestamp)) {
            continue;
        }

        if (lb_is_free_pool_empty(p_slot_pool)) {
            // There is no more available slot, we need to free the oldest one

            // If demanding message is older than our oldest, continue,
            // we don't need to free anything, this msg is outdated
            lb_slot tmp_slot;
            lb_slot *p_begin_busy_slot =
                &p_slot_pool->slots[p_slot_pool->i_begin_busy];
#ifndef __ANDROID__
            lb_fill_slot(&tmp_slot, &p_msgs[i], p_slot_pool->certif_date,
                         LB_MT_OFFSET);
#else
            lb_fill_slot(&tmp_slot, &p_msgs[i], p_slot_pool->certif_date,
                         LB_MT_OFFSET, 0);
#endif
            if (lb_compare_slot(&tmp_slot, p_begin_busy_slot) == -1) {
                continue;
            }

            // We know for sure that we will free the oldest

            lb_detach_slot(p_root, p_slot_pool->i_begin_busy, p_slot_pool);

            // Finally freeing the oldest slot
            lb_free_a_slot(p_slot_pool, p_slot_pool->i_begin_busy);
        }

        // Get the leaf in which this msg will take place
        lb_merkle_tree *message_node = lb_get_leaf_for_timestamp(
            p_root, lb_convert_absolute_to_relative_timestamp(
                        p_msgs[i].header.timestamp, p_slot_pool->certif_date,
                        LB_MT_OFFSET));

        if (message_node == NULL) {
            continue; // Should never happen, ignore this message
        }

        ssize_t i_free = lb_get_free_slot(p_slot_pool);
        lb_slot *p_free = &p_slot_pool->slots[i_free];
#ifndef __ANDROID__
        lb_fill_slot(p_free, &p_msgs[i], p_slot_pool->certif_date,
                     LB_MT_OFFSET);
#else
        lb_fill_slot(p_free, &p_msgs[i], p_slot_pool->certif_date, LB_MT_OFFSET,
                     p_db_indexs[i]);
#endif

        // If this leaf has no bucket yet, start it by adding
        // our freshly new freed slot
        if (message_node->i_bucket_slot == -1) {
            // If first ever ever ever ever node we have met, register it
            if (p_slot_pool->i_begin_busy == -1) {
                p_slot_pool->i_begin_busy = i_free;
            } else {
                // Else insert it in the "busy slots" linked list
                int i_used = p_slot_pool->i_begin_busy;
                lb_slot *p_used = &p_slot_pool->slots[i_used];
                while (i_used != -1) {
                    if (lb_compare_slot(p_free, p_used) == -1) {
                        lb_insert_slot_before(p_slot_pool, i_used, i_free);
                        break;
                    }
                    // End of the linked list, no place could be found
                    // for this slot. Let's place it at the end
                    if (p_used->i_next == -1) {
                        lb_insert_slot_after(p_slot_pool, i_used, i_free);
                        break;
                    }
                    // Move forward into the list
                    i_used = p_used->i_next;
                    p_used = &p_slot_pool->slots[i_used];
                }
            }
            // Attach leaf's bucket to free_slot
            message_node->i_bucket_slot = i_free;
            lb_update_branch_hashes(message_node, p_slot_pool);
#ifndef __ANDROID__
            // Write in flash
            if (lb_store_msg != NULL) {
                lb_store_msg(arg2, p_slot_pool->slots[i_free].addr, &p_msgs[i]);
            }
#endif
            continue;
        }

        // From this point, we know this leaf has already a bucket at least of
        // size 1
        int i_node_slot = message_node->i_bucket_slot;
        while (i_node_slot != -1) {
            lb_slot *p_node_slot = &p_slot_pool->slots[i_node_slot];
            err_t retval = lb_compare_slot(p_free, p_node_slot);
            if (retval == -1) {
                // If we are inserting free_slot before the first slot of this
                // leaf's bucket, update the leaf's bucket
                if (message_node->i_bucket_slot == i_node_slot) {
                    message_node->i_bucket_slot = i_free;
                }
                lb_insert_slot_before(p_slot_pool, i_node_slot, i_free);
#ifndef __ANDROID__
                // Write in flash
                if (lb_store_msg != NULL) {
                    lb_store_msg(arg2, p_slot_pool->slots[i_free].addr,
                                 &p_msgs[i]);
                }
#endif
                break;
            }
            if (retval == 0) {
                // If free_slot represents exactly the same message than the one
                // represented by tmp_slot_of_node, ignore it
                lb_free_a_slot(p_slot_pool, i_free);
                break;
            }
            // End of the linked list, no place could be found
            // for this slot. Let's place it at the end
            if (p_node_slot->i_next == -1) {
                lb_insert_slot_after(p_slot_pool, i_node_slot, i_free);
#ifndef __ANDROID__
                // write in flash
                if (lb_store_msg != NULL) {
                    lb_store_msg(arg2, p_slot_pool->slots[i_free].addr,
                                 &p_msgs[i]);
                }
#endif
                break;
            }
            // Move forward into the list
            i_node_slot = p_node_slot->i_next;
        }
        lb_update_branch_hashes(message_node, p_slot_pool);
    }
    return LB_OK;
}

void lb_update_leaf_hash(lb_slot_pool *slot_pool, lb_merkle_tree *p_root) {
    if (p_root == NULL || p_root->i_bucket_slot == -1) {
        return;
    }
    static sha256_context_t ctx;
    sha256_init(&ctx);

    // We take the hash of the first message of the bucket as the leaf's hash
    ssize_t i_bucket_slot = p_root->i_bucket_slot;

    while (i_bucket_slot != -1 &&
           slot_pool->slots[i_bucket_slot].timestamp < p_root->end) {
        sha256_update(&ctx, slot_pool->slots[i_bucket_slot].p_hash,
                      LB_SLOT_SIGNATURE_SIZE);
        i_bucket_slot = slot_pool->slots[i_bucket_slot].i_next;
    }
    sha256_final(&ctx, ctx.data, 0);
    memcpy(p_root->p_hash, ctx.data, LB_SLOT_SIGNATURE_SIZE);
}

void lb_update_nodes_hash(lb_slot_pool *slot_pool, lb_merkle_tree *p_root) {

    if (p_root == NULL)
        return;
    if (p_root->p_children[0] == NULL) {
        // We are in a leaf, compute its hash and inform the caller
        lb_update_leaf_hash(slot_pool, p_root);
        return;
    }

    // Ok, we are not in a leaf but a subtree, thus p_root must have children.
    // Let's treat theme then we'll see for the current p_root
    for (int i = 0; i < LB_MT_NB_CHILDREN; i++) {
        lb_update_nodes_hash(slot_pool, p_root->p_children[i]);
    }

    // All children hashs have been updated.
    lb_compute_node_hash(p_root);
    return;
}

void lb_compute_node_hash(lb_merkle_tree *p_root) {
    if (p_root == NULL)
        return;

    static sha256_context_t ctx;
    sha256_init(&ctx);

    for (int i = 0; i < LB_MT_NB_CHILDREN; i++) {
        sha256_update(&ctx, p_root->p_children[i]->p_hash,
                      LB_SLOT_SIGNATURE_SIZE);
    }
    sha256_final(&ctx, ctx.data, 0);
    memcpy(p_root->p_hash, ctx.data, LB_SLOT_SIGNATURE_SIZE);
}

// This function returns a pointer to the leaf which has the given
// timestamp into its timeframe
lb_merkle_tree *lb_get_leaf_for_timestamp(lb_merkle_tree *p_root,
                                          uint32_t timestamp) {
    if (p_root == NULL)
        return NULL;
    unsigned int i = 0;

    // Message is out of merkle tree boundaries

    if (timestamp < p_root->start || timestamp >= p_root->end)
        return NULL;

    // While the timeframe is too large or while we are at the very first root
    while (lb_get_node_children_width(p_root) > LB_MT_BUCKET_SIZE ||
           p_root->p_parent == NULL) {

        if (p_root->p_children[0] == NULL) {
            return p_root;
        }

        // Try to find which leaf include the timestamp in its timeframe
        for (i = 0; i < LB_MT_NB_CHILDREN; i++) {

            if (p_root->p_children[i]->start <= timestamp &&
                timestamp < p_root->p_children[i]->end) {
                p_root = p_root->p_children[i];
                break;
            }
        }
    }
    return p_root;
}

// This function goes from a given node and travels all the way to the
// root node. Then returns a pointer to the root's child which containt the
// given node.
lb_merkle_tree *lb_get_sub_tree_root(lb_merkle_tree *p_node) {
    if (p_node == NULL)
        return NULL;
    lb_merkle_tree *old_node = p_node;
    // climb up the tree
    while (p_node->p_parent != NULL) {
        old_node = p_node;
        // register the last node
        p_node = p_node->p_parent;
    }
    return old_node;
}

#if defined(CPPSIM) || defined(__ANDROID__)
err_t lb_save_merkle_tree(lb_merkle_tree *p_root, lb_merkle_root_md *p_root_md,
                          const char *path) {
    if (p_root == NULL || p_root_md == NULL || path == NULL) {
        return LB_ERR;
    }

    FILE *f = fopen(path, "w+");
    if (f == NULL) {
        LBLOG("Could not open file at path %s\n", path);
    }

    int nb_node_saved = 0;
    for (int k = 0; k < LB_MT_NB_CHILDREN; k++) {
        lb_merkle_tree *p_tree = p_root->p_children[k];
        size_t node_count = p_root_md->node_count[k];

        for (unsigned int i = 0; i < node_count; i++) {
            size_t ret = fwrite(p_tree[i].p_hash, sizeof(uint8_t),
                                LB_SLOT_SIGNATURE_SIZE, f);
            nb_node_saved++;
            if (ret != LB_SLOT_SIGNATURE_SIZE) {
                goto error;
            }
            // If the node i a child node, save its bucket index
            if (p_tree[i].p_children[0] == NULL) {
                ret = fwrite(&p_tree[i].i_bucket_slot, sizeof(ssize_t), 1, f);
                if (ret != 1) {
                    goto error;
                }
            }
        }
    }
    LBLOG("Total nodes : %d\n", nb_node_saved);

    fclose(f);
    return LB_OK;

error:
    fclose(f);
    return LB_ERR;
}

err_t lb_load_merkle_tree(lb_merkle_tree *p_root, lb_merkle_root_md *p_root_md,
                          const char *path) {
    if (p_root == NULL || p_root_md == NULL || path == NULL) {
        return LB_ERR;
    }

    FILE *f = fopen(path, "r+");
    if (f == NULL) {
        LBLOG("Could not open file at path %s\n", path);
    }

    for (int k = 0; k < LB_MT_NB_CHILDREN; k++) {
        lb_merkle_tree *p_tree = p_root->p_children[k];
        size_t node_count = p_root_md->node_count[k];

        for (unsigned int i = 0; i < node_count; i++) {
            size_t ret = fread(p_tree[i].p_hash, sizeof(uint8_t),
                               LB_SLOT_SIGNATURE_SIZE, f);
            if (ret != LB_SLOT_SIGNATURE_SIZE) {
                goto error;
            }
            // If the node i a child node, save its bucket index
            if (p_tree[i].p_children[0] == NULL) {
                ret = fread(&p_tree[i].i_bucket_slot, sizeof(ssize_t), 1, f);
                if (ret != 1) {
                    goto error;
                }
            }
        }
    }

    fclose(f);
    return LB_OK;

error:
    fclose(f);
    return LB_ERR;
}
#endif

// Write the number of slots in nb_slots and the index of the last slot in
// i_last
err_t lb_get_bucket_size(const lb_merkle_tree *const p_node,
                         const lb_slot_pool *const p_pool, ssize_t *i_last,
                         int32_t *const nb_slots) {
    if (p_node == NULL || p_pool == NULL)
        return LB_WRONG_ARGS_VALUE;

    ssize_t i_tmp;
    *nb_slots = 0;
    i_tmp = p_node->i_bucket_slot;
    while (i_tmp != -1 && p_pool->slots[i_tmp].timestamp < p_node->end) {
        *i_last = i_tmp;
        i_tmp = p_pool->slots[i_tmp].i_next;
        (*nb_slots)++;
    }
    return LB_OK;
}

err_t lb_unset_shared(const lb_merkle_tree *const p_node,
                      lb_slot_pool *const p_pool) {
    if (p_node == NULL || p_pool == NULL)
        return LB_WRONG_ARGS_VALUE;

    ssize_t i_tmp = p_node->i_bucket_slot;
    while (i_tmp != -1 && p_pool->slots[i_tmp].timestamp < p_node->end) {
        p_pool->slots[i_tmp].shared = 0;
        i_tmp = p_pool->slots[i_tmp].i_next;
    }

    return LB_OK;
}
// Here timestamp is supposed to be relative !
// Not absolute, not frm the msg timestamp but from the slot !
err_t lb_get_timestamp_index(lb_merkle_tree *p_node, uint32_t timestamp,
                             const lb_slot_pool *const p_pool,
                             ssize_t *i_slot) {
    if (p_node == NULL || p_pool == NULL) {
        return LB_WRONG_ARGS_VALUE;
    }
    lb_merkle_tree *p_tmp_node = lb_get_leaf_for_timestamp(p_node, timestamp);
    if (p_tmp_node == NULL) {
        return LB_ERR;
    }

    ssize_t i_tmp = p_tmp_node->i_bucket_slot;
    while (i_tmp != -1 && p_pool->slots[i_tmp].timestamp < p_tmp_node->end) {
        if (p_pool->slots[i_tmp].timestamp == timestamp) {
            *i_slot = i_tmp;
            return LB_OK;
        }
        i_tmp = p_pool->slots[i_tmp].i_next;
    }
    return LB_ERR;
}

err_t lb_merkle_tree_size(lb_merkle_tree *p_root, lb_merkle_root_md *p_root_md,
                          unsigned int *tree_size, unsigned int *nb_nodes) {
    if (p_root == NULL || p_root_md == NULL || tree_size == NULL ||
        nb_nodes == NULL) {
        return LB_ERR;
    }

    *tree_size = sizeof(*p_root);
    *tree_size += sizeof(*p_root_md);
    *nb_nodes = 1;
    for (int k = 0; k < LB_MT_NB_CHILDREN; k++) {
        *tree_size += p_root_md->node_count[k] * sizeof(*p_root);
        *nb_nodes += p_root_md->node_count[k];
        LBLOG("subtree %2d has %3lu nodes and weight %5lu\n", k,
              p_root_md->node_count[k],
              p_root_md->node_count[k] * sizeof(*p_root));
    }
    return LB_OK;
}
