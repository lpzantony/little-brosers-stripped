#ifndef MERKLE_TREE_H
#define MERKLE_TREE_H

#include "merkle_tree_constants.lbf"
#include "message.h"
#include "slot_pool.h"
#include <math.h>

#include "sha256.h"

#define LB_NOW 1515401216
#define LB_TWO_WEEK 1209600
#define LB_ONE_WEEK 604800
#define LB_ONE_DAY 86400
#define LB_ONE_HOUR 3600
#define LB_ONE_MINUTE 3600

#define LB_MT_NB_CHILDREN 7

// Supposed to be final values
#define LB_MT_START 0
#define LB_MT_END1 432000  //+5 days
#define LB_MT_END2 518400  //+6 days
#define LB_MT_END3 604800  //+7 days
#define LB_MT_END4 691200  //+8 days
#define LB_MT_END5 777600  //+9 days
#define LB_MT_END6 1036800 //+12 days
#define LB_MT_END 1209600  // + 14 days

#define LB_MT_OFFSET LB_MT_END3

#define LB_MT_BUCKET_SIZE 1800 // 1800 = 30min

typedef err_t (*lb_storage_func)(void *, const uint16_t, message_t *);

// THESE MACROS HAVE TO BE EDITED IF THE NUMBER OF CHILDREN CHANGES

#define LB_ALLOC_MERKLE_TREE(root, slot_pool)                                  \
    LB_ALLOC_SLOT_POOL(slot_pool);                                             \
    LB_ALLOC_MERKLE_TREE_ONLY(root)

#define LB_ALLOC_MERKLE_TREE_ONLY(root)                                        \
    lb_merkle_tree root;                                                       \
    lb_merkle_tree root##_merkle_trees0[MERKLE_TREE_0_NODE_COUNT];             \
    lb_merkle_tree root##_merkle_trees1[MERKLE_TREE_1_NODE_COUNT];             \
    lb_merkle_tree root##_merkle_trees2[MERKLE_TREE_2_NODE_COUNT];             \
    lb_merkle_tree root##_merkle_trees3[MERKLE_TREE_3_NODE_COUNT];             \
    lb_merkle_tree root##_merkle_trees4[MERKLE_TREE_4_NODE_COUNT];             \
    lb_merkle_tree root##_merkle_trees5[MERKLE_TREE_5_NODE_COUNT];             \
    lb_merkle_tree root##_merkle_trees6[MERKLE_TREE_6_NODE_COUNT]

#define LB_ALLOC_ROOT_MD(root_md) lb_merkle_root_md root_md

#define LB_INIT_MERKLE_TREE(root)                                              \
    root.p_children[0] = root##_merkle_trees0;                                 \
    root.p_children[1] = root##_merkle_trees1;                                 \
    root.p_children[2] = root##_merkle_trees2;                                 \
    root.p_children[3] = root##_merkle_trees3;                                 \
    root.p_children[4] = root##_merkle_trees4;                                 \
    root.p_children[5] = root##_merkle_trees5;                                 \
    root.p_children[6] = root##_merkle_trees6

#define LB_INIT_ROOT_MD(root_md)                                               \
    root_md.depth[0] = MERKLE_TREE_0_DEPTH;                                    \
    root_md.depth[1] = MERKLE_TREE_1_DEPTH;                                    \
    root_md.depth[2] = MERKLE_TREE_2_DEPTH;                                    \
    root_md.depth[3] = MERKLE_TREE_3_DEPTH;                                    \
    root_md.depth[4] = MERKLE_TREE_4_DEPTH;                                    \
    root_md.depth[5] = MERKLE_TREE_5_DEPTH;                                    \
    root_md.depth[6] = MERKLE_TREE_6_DEPTH;                                    \
    root_md.node_count[0] = MERKLE_TREE_0_NODE_COUNT;                          \
    root_md.node_count[1] = MERKLE_TREE_1_NODE_COUNT;                          \
    root_md.node_count[2] = MERKLE_TREE_2_NODE_COUNT;                          \
    root_md.node_count[3] = MERKLE_TREE_3_NODE_COUNT;                          \
    root_md.node_count[4] = MERKLE_TREE_4_NODE_COUNT;                          \
    root_md.node_count[6] = MERKLE_TREE_6_NODE_COUNT;                          \
    root_md.node_count[5] = MERKLE_TREE_5_NODE_COUNT;                          \
    root_md.timeframe[0] = LB_MT_START;                                        \
    root_md.timeframe[1] = LB_MT_END1;                                         \
    root_md.timeframe[2] = LB_MT_END2;                                         \
    root_md.timeframe[3] = LB_MT_END3;                                         \
    root_md.timeframe[4] = LB_MT_END4;                                         \
    root_md.timeframe[5] = LB_MT_END5;                                         \
    root_md.timeframe[6] = LB_MT_END6;                                         \
    root_md.timeframe[LB_MT_NB_CHILDREN] = LB_MT_END

// This is the recursive merkle tree structure we'll use
// to represent a merkle tree "object"
typedef struct lb_merkle_tree lb_merkle_tree;
struct __attribute__((__packed__)) lb_merkle_tree {
    uint32_t start;
    uint32_t end;
    lb_merkle_tree *p_parent;
    lb_merkle_tree *p_children[LB_MT_NB_CHILDREN];
    uint16_t array_id;

    ssize_t i_bucket_slot;
    uint8_t p_hash[LB_SLOT_SIGNATURE_SIZE];
};

typedef struct {
    size_t depth[LB_MT_NB_CHILDREN];
    size_t node_count[LB_MT_NB_CHILDREN];
    size_t timeframe[LB_MT_NB_CHILDREN + 1];
} lb_merkle_root_md;

static inline uint32_t
lb_get_node_children_width(const lb_merkle_tree *const p_root) {
    if (p_root == NULL)
        return 0;
    return (p_root->end - p_root->start) / LB_MT_NB_CHILDREN;
}

static inline uint16_t lb_get_node_depth(lb_merkle_tree *p_root) {
    // Compute p_root depth
    unsigned int tmp_depth = 0;
    unsigned int first_array_id = 0;
    for (tmp_depth = 0; first_array_id <= p_root->array_id; tmp_depth++) {
        first_array_id =
            ((1 - pow(LB_MT_NB_CHILDREN, tmp_depth)) / (1 - LB_MT_NB_CHILDREN));
    }
    return tmp_depth - 2;
}

/*!
 * \brief   Attach the nodes of a merkle tree together
 *
 * \param   p_root      Pointer to the node we want to attach
 * \param   start       Date of the begining of this node's timeframe
 * \param   end         Date of the end of this node's timeframe
 * \param   child_id    Parent of this node has LB_MT_NB_CHILDREN children,
 *                      this parameter is the number of this node within its
 *                      parent's children list
 * \param   array_id    The whole tree is stored in a array. array_id is the
 *                      number of the this node within this array
 * \param   p_parent    Pointer to this node's parent
 *
 * \return  Whenever there is an error during the tree build process,
 *          this function will return LB_ERR. LB_OK otherwise.
 */
err_t lb_build_merkle_tree(lb_merkle_tree *p_root, uint32_t start, uint32_t end,
                           uint16_t parent_depth, uint16_t depth_id,
                           uint16_t array_id, lb_merkle_tree *p_parent);

/*!
 * \brief   Drops have LB_MT_NB_CHILDREN time consecutive merkle trees,
 *          this function attachs them all manually to a root node
 *
 * \param   p_root      Pointer to the very first node, the absolute
 *                      root node. This pointer should be created with
 *                      the LB_ALLOC_MERKLE_TREE(root) macro
 * \param   slot_pool   Slot pool array used to represent the messages
 *                      stored in the external flash memory. This pointer should
 *                      be created with the LB_ALLOC_MERKLE_TREE(root) macro
 *
 * \return  Whenever there is an error during the tree build process,
 *          this function will return LB_ERR. LB_OK otherwise.
 */
err_t lb_init_merkle_tree(lb_merkle_tree *p_root, lb_merkle_root_md *p_root_md,
                          lb_slot_pool *p_slot_pool, uint32_t certif_date);

err_t lb_init_merkle_tree_only(lb_merkle_tree *p_root,
                               lb_merkle_root_md *p_root_md);

/*!
 * \brief   Returns the leaf where should be placed a message
 *          having the given timestamp
 *
 * \param   p_root      Pointer to the very first node, the absolute
 *                      root node.
 * \param   timestamp   Timestamp we want to locate in the tree's leafs
 *
 * \return  A pointer the the corresponding leaf for the given timestamp. A NULL
 *          pointer is returned if no leaf could be found for this timestamp
 */
lb_merkle_tree *lb_get_leaf_for_timestamp(lb_merkle_tree *p_root,
                                          uint32_t timestamp);

static inline int lb_is_out_of_tree(uint32_t certif_date,
                                    lb_merkle_tree *p_root,
                                    uint32_t timestamp) {
    return timestamp < certif_date - LB_MT_OFFSET - p_root->start ||
           certif_date + p_root->end - LB_MT_OFFSET <= timestamp;
}

void lb_update_nodes_hash(lb_slot_pool *slot_pool, lb_merkle_tree *p_root);

void lb_update_leaf_hash(lb_slot_pool *slot_pool, lb_merkle_tree *p_root);
void lb_compute_node_hash(lb_merkle_tree *p_root);

err_t lb_shift_tree(lb_merkle_tree *p_root, lb_slot_pool *p_pool,
                    uint32_t certif_date, lb_slot_storage_func stor_func,
                    void *stor_func_arg);

// If lb_store_msg function pointer is null, this function will avoid storing
// the message For Android used, it expects a uint32_t array of size
// p_msg_pool.size
err_t lb_attach_msgs_to_tree(lb_merkle_tree *p_root, message_pool_t *p_msg_pool,
                             lb_slot_pool *slot_pool,
                             lb_storage_func lb_store_msg, void *arg2);

err_t lb_attach_leafs_to_slots(lb_merkle_tree *p_root, lb_slot_pool *p_pool);

void lb_print_merkle_tree(lb_merkle_tree *mk, lb_slot_pool *slot_pool);

lb_merkle_tree *lb_get_sub_tree_root(lb_merkle_tree *p_node);

#if defined(CPPSIM) || defined(__ANDROID__)
err_t lb_save_merkle_tree(lb_merkle_tree *p_root, lb_merkle_root_md *p_root_md,
                          const char *path);

err_t lb_merkle_tree_size(lb_merkle_tree *p_root, lb_merkle_root_md *p_root_md,
                          unsigned int *tree_size, unsigned int *nb_nodes);

err_t lb_load_merkle_tree(lb_merkle_tree *p_root, lb_merkle_root_md *p_root_md,
                          const char *path);
#endif

err_t lb_detach_slot(lb_merkle_tree *p_root, ssize_t i_slot,
                     lb_slot_pool *p_pool);

err_t lb_get_bucket_size(const lb_merkle_tree *const p_node,
                         const lb_slot_pool *const p_pool, ssize_t *i_last,
                         int32_t *const nb_slots);

err_t lb_unset_shared(const lb_merkle_tree *const p_node,
                      lb_slot_pool *const p_pool);

err_t lb_get_timestamp_index(lb_merkle_tree *p_node, uint32_t timestamp,
                             const lb_slot_pool *const p_pool, ssize_t *i_slot);

#endif
