#include "slot_pool.h"
#include <stdio.h>
#ifdef __ANDROID__
#include <android/log.h>
#endif
#if defined(CPPSIM) || defined(__ANDROID__)
#include "unistd.h" //access()
#endif

#include "utils.h"

err_t lb_init_slots(lb_slot_pool *slot_pool, uint32_t certif_date) {
    // Init the slots with default values
    if (slot_pool == NULL)
        return LB_ERR;
    slot_pool->i_begin_free = 0;
    slot_pool->i_begin_busy = -1;
    slot_pool->nb_free = LB_NB_MAX_MSGS + 1;
    slot_pool->certif_date = certif_date;
    slot_pool->nb_slots = LB_NB_MAX_MSGS + 1;
    for (int i = 0; i < LB_NB_MAX_MSGS + 1; i++) {
#ifndef __ANDROID__
        slot_pool->slots[i].addr = i + LB_ADDR_FIRST_STORAGE;
#endif
        slot_pool->slots[i].i_previous = i - 1;
        slot_pool->slots[i].i_next = (i == LB_NB_MAX_MSGS + 1 - 1) ? -1 : i + 1;
        slot_pool->slots[i].timestamp = 0;
        slot_pool->slots[i].shared = 1;
        memset(slot_pool->slots[i].p_hash, 0, LB_SLOT_SIGNATURE_SIZE);
    }
    return LB_OK;
}

err_t lb_empty_slot_pool(lb_slot_pool *slot_pool) {
    if (slot_pool == NULL)
        return LB_ERR;
    slot_pool->nb_free = LB_NB_MAX_MSGS + 1;
    slot_pool->i_begin_free = 0;
    slot_pool->i_begin_busy = -1;
    return LB_OK;
}

ssize_t lb_get_free_slot(lb_slot_pool *p_slot_pool) {
    if (p_slot_pool == NULL) {
        return -1;
    }
    ssize_t i_to_return = p_slot_pool->i_begin_free;
    p_slot_pool->i_begin_free =
        p_slot_pool->slots[p_slot_pool->i_begin_free].i_next;
    lb_clear_slot(p_slot_pool, i_to_return);
    p_slot_pool->nb_free -= 1;
    return i_to_return;
}

void lb_free_a_slot(lb_slot_pool *p_slot_pool, ssize_t i_to_free) {
    lb_slot *p_to_free = &p_slot_pool->slots[i_to_free];

    // Atach previous slot to next and vice-versa
    if (p_to_free->i_previous != -1) {
        p_slot_pool->slots[p_to_free->i_previous].i_next = p_to_free->i_next;
    }
    if (p_to_free->i_next != -1) {
        p_slot_pool->slots[p_to_free->i_next].i_previous =
            p_to_free->i_previous;
    }

    // If we are freeing the oldest slot, update the oldest slot pointer
    if (p_slot_pool->i_begin_busy == i_to_free) {
        p_slot_pool->i_begin_busy = p_to_free->i_next;
    }

    // Make to_free the head of the free slots and return
    if (lb_is_free_pool_empty(p_slot_pool)) {
        lb_clear_slot(p_slot_pool, i_to_free);
    } else {
        // Some free slots are still there. Attach to it
        p_to_free->i_previous = -1;
        p_to_free->i_next = p_slot_pool->i_begin_free;
        p_slot_pool->slots[p_slot_pool->i_begin_free].i_previous = i_to_free;
    }

    p_slot_pool->nb_free += 1;
    p_slot_pool->i_begin_free = i_to_free;
}

void lb_clear_slot(lb_slot_pool *p_slot_pool, ssize_t i_to_clear) {
    lb_slot *p_to_clear = &p_slot_pool->slots[i_to_clear];
    p_to_clear->i_previous = -1;
    p_to_clear->i_next = -1;
    p_to_clear->timestamp = 0;
    p_to_clear->shared = 1;
    memset(p_to_clear->p_hash, 0, LB_SLOT_SIGNATURE_SIZE);
}

void lb_insert_slot_after(lb_slot_pool *p_slot_pool, ssize_t i_stock,
                          ssize_t i_to_insert) {
    lb_slot *p_to_insert = &p_slot_pool->slots[i_to_insert];
    lb_slot *p_stock = &p_slot_pool->slots[i_stock];
    if (p_stock->i_next != -1) {
        // If stock had a following slot, attach it to to_insert
        p_to_insert->i_next = p_stock->i_next;
        p_slot_pool->slots[p_stock->i_next].i_previous = i_to_insert;
    }
    p_stock->i_next = i_to_insert;
    p_to_insert->i_previous = i_stock;
}

void lb_insert_slot_before(lb_slot_pool *p_slot_pool, ssize_t i_stock,
                           ssize_t i_to_insert) {
    lb_slot *p_to_insert = &p_slot_pool->slots[i_to_insert];
    lb_slot *p_stock = &p_slot_pool->slots[i_stock];
    // If stock is the oldest slot
    if (p_slot_pool->i_begin_busy == i_stock) {
        p_slot_pool->i_begin_busy = i_to_insert;
    }
    if (p_stock->i_previous != -1) {
        // If stock had a previous slot, attach it to to_insert
        p_to_insert->i_previous = p_stock->i_previous;
        p_slot_pool->slots[p_stock->i_previous].i_next = i_to_insert;
    }
    p_stock->i_previous = i_to_insert;
    p_to_insert->i_next = i_stock;
}

int lb_compare_slot(lb_slot *to_compare, lb_slot *base) {
    if (base == NULL || to_compare == NULL)
        return LB_ERR;

    if (to_compare->timestamp < base->timestamp) {
        return -1;
    }
    if (to_compare->timestamp > base->timestamp) {
        return 1;
    }
    return lb_compare_slot_hash(to_compare, base);
}

int lb_compare_slot_hash(lb_slot *to_compare, lb_slot *base) {
    if (base == NULL || to_compare == NULL)
        return LB_ERR;

    for (int i = 0; i < LB_SLOT_SIGNATURE_SIZE; i++) {
        if (to_compare->p_hash[i] < base->p_hash[i])
            return -1;
        else if (to_compare->p_hash[i] > base->p_hash[i])
            return 1;
    }
    return 0;
}

err_t lb_get_send_count(lb_slot_pool *p_slot_pool, uint32_t *send_count) {
    if (p_slot_pool == NULL || send_count == NULL) {
        return LB_WRONG_ARGS_VALUE;
    }

    ssize_t i_curr = p_slot_pool->i_begin_busy;
    lb_slot *p_slot = &p_slot_pool->slots[i_curr];
    uint32_t i = 0;
    while (i_curr != -1) {
        p_slot = &p_slot_pool->slots[i_curr];
        if (p_slot->shared == 0) {
            i++;
        }
        i_curr = p_slot->i_next;
    }
    *send_count = i;

    return LB_OK;
}

#if defined(CPPSIM) || defined(__ANDROID__)
err_t lb_save_slot_pool(lb_slot_pool *p_slot_pool, void *path) {
    if (path == NULL || p_slot_pool == NULL) {
        return LB_ERR;
    }
    char *_path = (char *)path;

    FILE *f = fopen(_path, "w+");
    if (f == NULL) {
        LBLOG("Could not open file at path %s\n", _path);
        return LB_ERR;
    }

    size_t ret = fwrite(&p_slot_pool->nb_slots, sizeof(size_t), 1, f);
    if (ret != 1) {
        goto error;
    }

    // Writing the slots array into file
    ret = fwrite(p_slot_pool->slots, sizeof(lb_slot), LB_NB_MAX_MSGS + 1, f);
    if (ret != LB_NB_MAX_MSGS + 1) {
        goto error;
    }

    // Writing i_begin_busy, i_begin_free, nb_free, nb_slots and certif_date
    // into file
    ret = fwrite(&p_slot_pool->i_begin_busy, sizeof(ssize_t), 1, f);
    if (ret != 1) {
        goto error;
    }
    ret = fwrite(&p_slot_pool->i_begin_free, sizeof(ssize_t), 1, f);
    if (ret != 1) {
        goto error;
    }
    ret = fwrite(&p_slot_pool->nb_free, sizeof(size_t), 1, f);
    if (ret != 1) {
        goto error;
    }
    ret = fwrite(&p_slot_pool->certif_date, sizeof(uint32_t), 1, f);
    if (ret != 1) {
        goto error;
    }

    fclose(f);
    return LB_OK;

error:
    fclose(f);
    return LB_ERR;
}
// Return true if file exists
int lb_does_file_exists(char *path) { return access(path, F_OK) != -1; }

FILE *lb_create_file(char *path) {
    FILE *file = fopen(path, "a");
    if (file != NULL) {
        fclose(file);
    }
    return file;
}

int lb_remove_file(char *path) { return remove(path); }

err_t lb_load_slot_pool_size(size_t *p_pool_size, void *path) {
    if (path == NULL || p_pool_size == NULL) {
        return LB_ERR;
    }
    char *_path = (char *)path;
    FILE *f = fopen(_path, "r+");
    if (f == NULL) {
        LBLOG("Could not open file at path %s\n", _path);
        return LB_ERR;
    }
    size_t ret = fread(p_pool_size, sizeof(size_t), 1, f);
    if (ret != 1) {
        fclose(f);
        return LB_ERR;
    }
    return LB_OK;
}

err_t lb_load_slot_pool(lb_slot_pool *p_slot_pool, void *path) {
    if (path == NULL || p_slot_pool == NULL) {
        return LB_ERR;
    }
    char *_path = (char *)path;

    FILE *f = fopen(_path, "r+");
    if (f == NULL) {
        LBLOG("Could not open file at path %s\n", _path);
        return LB_ERR;
    }

    size_t ret = fread(&p_slot_pool->nb_slots, sizeof(size_t), 1, f);
    if (ret != 1) {
        goto error;
    }

    // Writing the slots array into file
    ret = fread(p_slot_pool->slots, sizeof(lb_slot), LB_NB_MAX_MSGS + 1, f);
    if (ret != LB_NB_MAX_MSGS + 1) {
        goto error;
    }

    // Writing i_begin_busy, i_begin_free, and nb_free into file
    ret = fread(&p_slot_pool->i_begin_busy, sizeof(ssize_t), 1, f);
    if (ret != 1) {
        goto error;
    }

    ret = fread(&p_slot_pool->i_begin_free, sizeof(ssize_t), 1, f);
    if (ret != 1) {
        goto error;
    }

    ret = fread(&p_slot_pool->nb_free, sizeof(size_t), 1, f);
    if (ret != 1) {
        goto error;
    }
    ret = fread(&p_slot_pool->certif_date, sizeof(uint32_t), 1, f);
    if (ret != 1) {
        goto error;
    }

    fclose(f);
    return LB_OK;

error:
    fclose(f);
    return LB_ERR;
}
#endif

err_t lb_slot_pool_size(lb_slot_pool *p_slot_pool, unsigned int *pool_size) {
    if (p_slot_pool == NULL || pool_size == NULL) {
        return LB_ERR;
    }

    *pool_size = sizeof(lb_slot) * LB_NB_MAX_MSGS + 1;
    *pool_size += sizeof(ssize_t);
    *pool_size += sizeof(ssize_t);
    *pool_size += sizeof(size_t);
    *pool_size += sizeof(uint32_t);
    *pool_size += sizeof(size_t);
    return LB_OK;
}

err_t lb_slot_to_raw(uint8_t *buff, lb_slot *p_slot) {
    if (buff == NULL || p_slot == NULL) {
        return LB_WRONG_ARGS_VALUE;
    }
    unsigned int stored = 0;
    memcpy(buff, &p_slot->i_previous, sizeof(ssize_t));
    stored += sizeof(ssize_t);
    memcpy(buff + stored, &p_slot->i_next, sizeof(ssize_t));
    stored += sizeof(ssize_t);
    memcpy(buff + stored, &p_slot->timestamp, sizeof(uint32_t));
    stored += sizeof(uint32_t);
    memcpy(buff + stored, &p_slot->addr, sizeof(uint32_t));
    stored += sizeof(uint32_t);
    memcpy(buff + stored, p_slot->p_hash,
           sizeof(uint8_t) * LB_SLOT_SIGNATURE_SIZE);
    stored += sizeof(uint8_t) * LB_SLOT_SIGNATURE_SIZE;
    return stored;
}

err_t lb_slot_from_raw(uint8_t *buff, lb_slot *p_slot) {
    if (buff == NULL || p_slot == NULL) {
        return LB_WRONG_ARGS_VALUE;
    }
    unsigned int stored = 0;
    memcpy(&p_slot->i_previous, buff, sizeof(ssize_t));
    stored += sizeof(ssize_t);
    memcpy(&p_slot->i_next, buff + stored, sizeof(ssize_t));
    stored += sizeof(ssize_t);
    memcpy(&p_slot->timestamp, buff + stored, sizeof(uint32_t));
    stored += sizeof(uint32_t);
    memcpy(&p_slot->addr, buff + stored, sizeof(uint32_t));
    stored += sizeof(uint32_t);
    memcpy(p_slot->p_hash, buff + stored,
           sizeof(uint8_t) * LB_SLOT_SIGNATURE_SIZE);
    stored += sizeof(uint8_t) * LB_SLOT_SIGNATURE_SIZE;
    return stored;
}

err_t lb_slot_md_to_raw(uint8_t *buff, lb_slot_pool *p_pool) {
    if (buff == NULL || p_pool == NULL) {
        return LB_WRONG_ARGS_VALUE;
    }
    unsigned int stored = 0;
    memcpy(buff, &p_pool->i_begin_busy, sizeof(ssize_t));
    stored += sizeof(ssize_t);
    memcpy(buff + stored, &p_pool->i_begin_free, sizeof(ssize_t));
    stored += sizeof(ssize_t);
    memcpy(buff + stored, &p_pool->nb_free, sizeof(size_t));
    stored += sizeof(size_t);
    memcpy(buff + stored, &p_pool->certif_date, sizeof(uint32_t));
    stored += sizeof(size_t);
    memcpy(buff + stored, &p_pool->nb_slots, sizeof(size_t));
    stored += sizeof(size_t);
    return stored;
}

err_t lb_slot_md_from_raw(uint8_t *buff, lb_slot_pool *p_pool) {
    if (buff == NULL || p_pool == NULL) {
        return LB_WRONG_ARGS_VALUE;
    }
    unsigned int stored = 0;
    memcpy(&p_pool->i_begin_busy, buff, sizeof(ssize_t));
    stored += sizeof(ssize_t);
    memcpy(&p_pool->i_begin_free, buff + stored, sizeof(ssize_t));
    stored += sizeof(ssize_t);
    memcpy(&p_pool->nb_free, buff + stored, sizeof(size_t));
    stored += sizeof(size_t);
    memcpy(&p_pool->certif_date, buff + stored, sizeof(uint32_t));
    stored += sizeof(uint32_t);
    memcpy(&p_pool->nb_slots, buff + stored, sizeof(size_t));
    stored += sizeof(size_t);
    return stored;
}