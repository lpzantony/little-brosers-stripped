#include "message.h"
#if !defined(CPPSIM) && !defined(__ANDROID__)
#include "lb_crypto.h"
#endif
#include "sha256.h"

void header_to_raw(uint8_t *const raw_message,
                   const message_header_t *const header) {
    uint8_t *ptr = raw_message;
    uint32_to_bytes(header->timestamp, ptr);
    ptr += sizeof(uint32_t);
    uint32_to_bytes(header->from_id, ptr);
    ptr += sizeof(uint32_t);
    uint32_to_bytes(header->to_id, ptr);
    ptr += sizeof(uint32_t);
    uint16_to_bytes(header->type, ptr);
}

void header_from_raw(const uint8_t *const raw_message,
                     message_header_t *const header) {
    const uint8_t *ptr = raw_message;
    header->timestamp = bytes_to_uint32(ptr);
    ptr += sizeof(uint32_t);
    header->from_id = bytes_to_uint32(ptr);
    ptr += sizeof(uint32_t);
    header->to_id = bytes_to_uint32(ptr);
    ptr += sizeof(uint32_t);
    header->type = bytes_to_uint16(ptr);
}

void message_from_raw(const uint8_t *const raw_message,
                      message_t *const message) {

    // size
    message->size = bytes_to_uint16(raw_message);

    // Parse header
    header_from_raw(raw_message + 2, &message->header);

    // Parse payload
    memcpy(message->payload, raw_message + MESSAGE_HEADER_SIZE + 2,
           message->size - MESSAGE_HEADER_SIZE - MESSAGE_SIGNATURE_SIZE);

    // Parse signature
    memcpy(message->signature,
           raw_message + message->size - MESSAGE_SIGNATURE_SIZE,
           MESSAGE_SIGNATURE_SIZE);
}

uint16_t message_to_raw(uint8_t *const raw_message,
                        const message_t *const message) {

    // Size
    uint16_to_bytes(message->size, raw_message);

    // Header
    header_to_raw(raw_message + 2, &message->header);

    // Payload
    memcpy(raw_message + 2 + MESSAGE_HEADER_SIZE, message->payload,
           message->size - MESSAGE_HEADER_SIZE - MESSAGE_SIGNATURE_SIZE);

    // Signature
    memcpy(raw_message + message->size - MESSAGE_SIGNATURE_SIZE,
           message->signature, MESSAGE_SIGNATURE_SIZE);

    return message->size;
}

void compute_msg_hash(const message_t *const p_msg, uint8_t *p_dest,
                      int little_endian) {
    if (p_msg == NULL || p_dest == NULL)
        return;

    static sha256_context_t ctx;
    sha256_init(&ctx);

    static uint8_t buffer[4];
    uint32_to_bytes(p_msg->header.timestamp, buffer);
    sha256_update(&ctx, buffer, 4);
    uint32_to_bytes(p_msg->header.from_id, buffer);
    sha256_update(&ctx, buffer, 4);
    uint32_to_bytes(p_msg->header.to_id, buffer);
    sha256_update(&ctx, buffer, 4);
    uint16_to_bytes(p_msg->header.type, buffer);
    sha256_update(&ctx, buffer, 2);

    sha256_update(&ctx, p_msg->payload,
                  p_msg->size - sizeof(p_msg->size) - MESSAGE_HEADER_SIZE -
                      MESSAGE_SIGNATURE_SIZE);

    sha256_final(&ctx, p_dest, little_endian);

    return;
}

#if !defined(CPPSIM) && !defined(__ANDROID__)
bool message_verify_signature(const message_t *const message,
                              const nrf_value_length_t *const public_key) {

    // Init signature
    NRF_CRYPTO_ECDSA_SIGNATURE_CREATE(my_signature, SECP256R1);
    // Copy message signature into signature object
    memcpy(my_signature.p_value, message->signature, MESSAGE_SIGNATURE_SIZE);
    my_signature.length = MESSAGE_SIGNATURE_SIZE;

    // Init hash
    NRF_CRYPTO_HASH_CREATE(msg_hash, SHA256);

    // Hash message
    compute_msg_hash(message, msg_hash.p_value, LB_LITTLE_ENDIAN);

    return lb_crypto_verify_signature_from_hash(&msg_hash, &my_signature,
                                                public_key);
}

#endif /* CPPSIM */
