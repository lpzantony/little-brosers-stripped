#ifndef MESSAGE_H
#define MESSAGE_H

#if !defined(CPPSIM) && !defined(__ANDROID__)
#include "nrf_crypto.h"
#include "nrf_crypto_sw_hash.h"
#endif /* CPPSIM */

#include "utils.h"
#include <stdbool.h>
#include <stdint.h>
#include <string.h>

// Note : NRF is LE (0x00)

#define MESSAGE_HEADER_SIZE (4 + 4 + 4 + 2)
// For ciphertext + public key hash
#define MESSAGE_MAX_PAYLOAD_SIZE (256 + 32)
#define MESSAGE_SIGNATURE_SIZE 64
#define MESSAGE_MAX_SIZE                                                       \
    MESSAGE_HEADER_SIZE + 2 + MESSAGE_MAX_PAYLOAD_SIZE + MESSAGE_SIGNATURE_SIZE

typedef struct __attribute__((__packed__)) {
    uint32_t timestamp;
    uint32_t from_id;
    uint32_t to_id;
    uint16_t type;
} message_header_t;

typedef struct __attribute__((__packed__)) {
    message_header_t header;
    uint16_t size;
    uint8_t payload[MESSAGE_MAX_PAYLOAD_SIZE];
    uint8_t signature[MESSAGE_SIGNATURE_SIZE];
} message_t;

typedef struct {
    message_t *p_msgs;
    size_t size;
} message_pool_t;

void header_from_raw(const uint8_t *const raw_message,
                     message_header_t *const header);

void header_to_raw(uint8_t *const raw_message,
                   const message_header_t *const header);

void message_from_raw(const uint8_t *const raw_message,
                      message_t *const message);

// Returns the raw_message size
uint16_t message_to_raw(uint8_t *const raw_message,
                        const message_t *const message);

void compute_msg_hash(const message_t *const p_msg, uint8_t *p_dest,
                      int little_endian);

#ifndef CPPSIM
#ifndef __ANDROID__
bool message_verify_signature(const message_t *const message,
                              const nrf_value_length_t *const public_key);
#endif
#endif

#endif /* MESSAGE_H */
