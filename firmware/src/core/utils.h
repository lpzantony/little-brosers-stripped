#ifndef UTILS_H
#define UTILS_H
#include <stdint.h>

#define LU(x) ((uint32_t)x)

#ifdef CPPSIM
#include <stdio.h>
#define LBLOG(...) printf(__VA_ARGS__)
#else
#ifdef __ANDROID__
#include <android/log.h>
#define LBLOG(...) __android_log_print(ANDROID_LOG_INFO, "Native", __VA_ARGS__)
#else

// #define LB_DEBUG
#define LB_DELAY_MS 100
#include "nrf_delay.h"
#ifdef LB_DEBUG

#define LB_WAIT_MS(x) nrf_delay_ms(x)
#else
#define LB_WAIT_MS(x)
#endif

#include "nrf_log.h"
#ifdef LB_DEBUG
#define LBLOG(...)                                                             \
    NRF_LOG_INFO(__VA_ARGS__);                                                 \
    LB_WAIT_MS(LB_DELAY_MS)
#else
#define LBLOG(...) NRF_LOG_INFO(__VA_ARGS__)
#endif
#endif
#endif

#if defined(CPPSIM) || defined(__ANDROID__)
#define MIN(a, b) (((a) < (b)) ? (a) : (b))
#define MAX(a, b) (((a) > (b)) ? (a) : (b))
#endif

#ifndef SHA256_BLOCK_SIZE
#define SHA256_BLOCK_SIZE 32
#endif

#define LLU

#ifndef NULL
#define NULL ((void *)0)
#endif

#define LB_STACK_EMPTY -6
#define LB_STACK_FULL -5
#define LB_MASS_STORAGE_BUFFER_ERROR -4
#define LB_MASS_STORAGE_PAGE_ERROR -3
#define LB_WRONG_ARGS_VALUE -2
#define LB_ERR -1
#define LB_OK 0

typedef int8_t err_t;

static inline int is_LE() {
    unsigned int i = 1;
    char *c = (char *)&i;
    return (*c) ? 1 : 0;
}
/*
 * Source for swap functions
 * https://stackoverflow.com/questions/2182002/convert-big-endian-to-little-endian-in-c-without-using-provided-func
 */

// Byte swap unsigned short
uint16_t swap_uint16(uint16_t val);

// Byte swap unsigned int
uint32_t swap_uint32(uint32_t val);

/*
 * The following 4 functions use big endian
 */

// 4 uint8_t to uint32_t
uint32_t bytes_to_uint32(const uint8_t bytes[]);

// 4 uint8_t to int32_t
int32_t bytes_to_int32(const uint8_t bytes[]);

// 2 uint8_t to uint16_t
uint16_t bytes_to_uint16(const uint8_t bytes[]);

// uint32_t to 4 uint8_t
int uint32_to_bytes(uint32_t val, uint8_t bytes[]);

int int32_to_bytes(const int32_t val, uint8_t bytes[]);

// uint16_t to 2 uint8_t
int uint16_to_bytes(uint16_t val, uint8_t bytes[]);

#define LB_CHANGE_BIT(number, bit_index, value)                                \
    do {                                                                       \
        number ^= (-value ^ number) & (1 << bit_index);                        \
    } while (0)
#define LB_GET_BIT(number, bit_index) (((number >> bit_index) & 1))
#define LB_SET_BIT(number, bit_index)                                          \
    do {                                                                       \
        number |= 1 << bit_index;                                              \
    } while (0)
#define LB_CLEAR_BIT(number, bit_index)                                        \
    do {                                                                       \
        number &= ~(1 << bit_index);                                           \
    } while (0)
#define LB_GET_BITS(number, start, mask) (((number >> start) & mask))
#define LB_SET_BITS(number, mask)                                              \
    do {                                                                       \
        number |= mask;                                                        \
    } while (0)
#define LB_CLEAR_BITS(number, mask)                                            \
    do {                                                                       \
        number &= ~mask;                                                       \
    } while (0)
#define LB_CLEAR_AND_SET(number, to_clear, to_set)                             \
    do {                                                                       \
        number = (number & ~to_clear) | to_set;                                \
    } while (0)

#endif /* UTILS_H */
