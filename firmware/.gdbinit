target ext :2331
#mon endian little
mon halt

# User interface with asm, regs and cmd windows
define split
	layout split
	layout src
#	layout regs
	focus cmd
end

define flash
	mon halt
	make flash_settings
	load
	mon reset
end
