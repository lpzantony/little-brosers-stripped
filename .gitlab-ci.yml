# Read https://docs.gitlab.com/ee/ci/yaml/ for the documentation

# image: rfc1149/rose-dev
image: lpzantony/little-brosers

stages:
    - lint
    - build
    - post_build
    - test
    - deploy

lintcheck:
    stage : lint
    image: tuetuopay/clang
    before_script:
      - cd firmware/cppsim
    script :
      - make lintck

# C++ simulation build
build_cppsim:
    stage: build
    image: lpzantony/criterion
    script:
        - cd firmware/cppsim && make
    artifacts:
        paths:
        - firmware/cppsim/_build/simlb.elf
        expire_in: 1 week

# CA
lint_ca:
    stage: lint
    image: lpzantony/ca3
    before_script:
        - cd ca
    script:
        - pip3 install virtualenv && sh configure_env.sh
        - source .env/bin/activate && pycodestyle --exclude=.env/ .
    cache:
        paths:
            - ca/.env/
        key: "VIRTUALENV_CACHE"

# C code and SDK build
build_firmware:
    stage: build
    image: tuetuopay/arm-none-eabi
    script:
        - cd firmware && make
    artifacts:
        paths:
        - firmware/_build/nrf52832_xxaa.out
        - firmware/_build/nrf52832_xxaa.hex
        expire_in: 1 week

build_bootloader:
    stage: build
    image: tuetuopay/arm-none-eabi
    script:
        - cd firmware && make bootloader
    artifacts:
        paths:
        - firmware/_build/bootloader.hex
        - firmware/_build/settings.hex
        - firmware/_build/nrf52832_xxaa.hex
        expire_in: 1 week

# Pysim linting
lint_pysim:
  stage: lint
  image: lpzantony/alpython3
  before_script:
    - cd pysim
  script:
    - bash
    - source env.sh
    - pycodestyle *.py

# Android gradle lint
lint_android_gradle:
    stage: lint
    image: lpzantony/ndk
    tags:
        - android
    script:
        - export GRADLE_USER_HOME=$PWD/.gradle-cache
        - mkdir -p "$ANDROID_HOME/licenses" && cd Android/LittleBrosers && cp ../android-sdk-license "$ANDROID_HOME/licenses" && ./gradlew lint
    retry: 2
    cache:
        key: android
        paths:
            - .gradle-cache
            - Android/LittleBrosers/.gradle/


# Android gradle debug and release APK
build_android_gradle:
    stage: build
    image: lpzantony/little-brosers
    tags:
        - android
    script :
        - export GRADLE_USER_HOME=$PWD/.gradle-cache
        - mkdir -p "$ANDROID_HOME/licenses" && cd Android/LittleBrosers && cp ../android-sdk-license "$ANDROID_HOME/licenses" && ./gradlew assembleDebug && ./gradlew assembleRelease
    cache:
        key: android
        paths:
            - .gradle-cache
            - Android/LittleBrosers/.gradle/
    artifacts:
        paths:
        - Android/LittleBrosers/app/build/outputs/apk/debug/app-debug.apk
        - Android/LittleBrosers/app/build/outputs/apk/release/app-release-unsigned.apk
        expire_in: 1 week

test_ca:
    image: lpzantony/ca3
    stage: test
    services:
        - mongo
    before_script:
        - cd ca
        - pip3 install virtualenv && sh configure_env.sh
    script:
        - . .env/bin/activate && MONGO_HOST=mongo python -m unittest -v
        - MONGO_HOST=mongo coverage run -m unittest && coverage report --omit=".env/*"
    cache:
        paths:
            - ca/.env/
        key: "VIRTUALENV_CACHE"

test_android:
    stage: test
    image: lpzantony/ndk
    tags:
        - android
    cache:
        key: android
        paths:
            - .gradle-cache
            - Android/LittleBrosers/.gradle/
    script :
        - export GRADLE_USER_HOME=$PWD/.gradle-cache
        - mkdir -p "$ANDROID_HOME/licenses" && cd Android/LittleBrosers && cp ../android-sdk-license "$ANDROID_HOME/licenses" && ./gradlew test

test_lblib:
    stage: test
    image: lpzantony/criterion
    before_script:
        - cd firmware/cppsim
    script:
      - make test

output:
    stage: deploy
    image: xght/nrfutil
    before_script:
        - cd firmware
    script:
        - make _build/settings.hex
        - cp SDK/components/softdevice/s132/hex/s132_nrf52_5.0.0_softdevice.hex _build/s132_softdevice.hex
    artifacts:
        paths:
            - firmware/_build/nrf52832_xxaa.hex
            - firmware/_build/bootloader.hex
            - firmware/_build/settings.hex
            - firmware/_build/s132_softdevice.hex
        expire_in: 1 week
    only:
        - master

create_app_zip:
    stage: post_build
    image: xght/nrfutil
    before_script:
      - /usr/bin/printf -- "$PRIVATE_EC_KEY" > /tmp/key
    script:
        - cd firmware/_build && nrfutil pkg generate --application nrf52832_xxaa.hex --debug-mode --hw-version 52 --sd-req 0xFFFE --key-file /tmp/key nrf52832_xxaa.zip
    after_script:
        - rm /tmp/key
    dependencies:
        - build_firmware
    artifacts:
        paths:
            - firmware/_build/nrf52832_xxaa.zip
        expire_in: 1 week
    only:
        - master

deploy_ca:
    image: ruby
    stage: deploy
    tags:
        - ruby
    before_script:
        - cd ca
        - mkdir static
        - gem install dpl
    script:
        - cp ../firmware/_build/nrf52832_xxaa.zip static/app.zip
        - dpl --skip_cleanup --provider=heroku --app=little-brosers --api-key=$HEROKU_API_KEY
    environment:
        name: production
        url: https://little-brosers.herokuapp.com/
    only:
        - master
