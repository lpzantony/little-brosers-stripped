# Little Brosers

This repository is a stripped down version of the biggest project I worked on during my studies at Télécom ParisTech.
The course is called ROSE (Robotique et Systèmes Embarqués) and my project was called Little Brosers.  
(I know, it is spelled 'Brothers' but that's a pun referring to the 'ROSE' course).

This repo is stripped down because I could not publish the whole repo without being blocked by license issues.
Here is all the code I worked on, exepct the Android folder.

Most of my work is concentrated in the `firmware/` folder. Especially `firmware/src/sim` and `firmware/src/core` that is mostly filled with code I wrote.

## PCB
![Drop](https://scontent-cdt1-1.xx.fbcdn.net/v/t35.0-12/26695116_10156042550354438_1080603402_o.jpg?oh=17536b212febcca3f42c022534bd0327&oe=5A5936FE "Logo Title Text 1")
