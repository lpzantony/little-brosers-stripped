package com.littlebrosers;

import com.littlebrosers.messages.ClearTextMessage;
import com.littlebrosers.messages.EncryptedTextMessage;
import com.littlebrosers.users.ContactUser;
import com.littlebrosers.users.MainUser;
import com.littlebrosers.users.MainUserMap;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.util.Date;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(MockitoJUnitRunner.class)
public class MessageUnitTest {
    private final short mainUserCompanyId = (short) 0x42;
    private final short mainUserNetworkId = (short) 0x69;
    private final int mainUserUserId = 0xABCD;

    private MainUser mainUser = new MainUser(mainUserCompanyId, mainUserNetworkId, mainUserUserId,
            "TestMainUser");
    private MainUser spy = spy(mainUser);

    private ContactUser contactUser;

    // Mock the MainUser
    @Before
    public void setUp() {
        // Hardcode the RSA key
        try {
            String modulusString = "00d56047acf652298e3fcdbb8cecbc32214722aa1625f88480cf570cee373"
                    + "ada932b140c29b00dc44f6e59e7018dddca66b2f1c645dacb9d4a45459cfa8f7e33df";
            String exponentString = "18bc01730656bde47476f7cfbd3d8f9e15ede9c389814672dc161e349b08"
                    + "627fc885fe9d2442ae92f0214c7e97cf0b9a9fc876df4f53517ab63d710f997b2779";
            String publicExponentString = "65537";

            // Load the key into BigIntegers
            final int radix = 16;
            BigInteger modulus = new BigInteger(modulusString, radix);
            BigInteger exponent = new BigInteger(exponentString, radix);
            BigInteger pubExponent = new BigInteger(publicExponentString);

            // Create private and public key specs
            RSAPrivateKeySpec privateSpec = new RSAPrivateKeySpec(modulus, exponent);
            RSAPublicKeySpec publicSpec = new RSAPublicKeySpec(modulus, pubExponent);

            // Create a key factory
            KeyFactory factory = KeyFactory.getInstance("RSA");

            // Create the RSA private and public keys
            PrivateKey priv = factory.generatePrivate(privateSpec);
            PublicKey pub = factory.generatePublic(publicSpec);

            when(spy.getRsaPublicKey()).thenReturn(pub);
            when(spy.getRsaPrivateKey()).thenReturn(priv);

            String contactModulusString = "8a96279f5da3cfe816bc6bfc97eef08f366f364a021a59d326516b"
                    + "7deb68e3bbe9a832f7512a4ef3178c432729330f09c479c058000709e1938b1c89786db78f";
            String contactPubExponentString = "65537";

            BigInteger contactModulus = new BigInteger(contactModulusString, radix);
            BigInteger contactPubExponent = new BigInteger(contactPubExponentString, radix);
            RSAPublicKeySpec contactPublicSpec = new RSAPublicKeySpec(contactModulus,
                    contactPubExponent);

            PublicKey contactPub = factory.generatePublic(contactPublicSpec);

            contactUser = new ContactUser(mainUser, 0, "ContactTest",
                    contactPub, null);
        } catch (Exception e) {
            e.printStackTrace();
        }

        MainUserMap.getInstance().setMainUser(spy);
    }

    // Check if the toString function of Messages are working well
    @Test
    public void clearTextToString() {
        Date date = new Date();
        ClearTextMessage clrMsg = new ClearTextMessage(mainUserCompanyId, mainUserNetworkId,
                spy, spy, "Hello World!");

        assertEquals("Clear Text Message:\n"
                        + "-Timestamp: " + date.toString() + '\n'
                        + "-From: " + spy.toString() + "\n"
                        + "-To: " + spy.toString() + "\n"
                        + "-Text size: 12\n"
                        + "-Text: Hello World!\n",
                clrMsg.toString());
    }

    // Check that a message to oneself can be encrypted
    @Test
    public void clearTextEncrypt() {
        ClearTextMessage clearMessage = new ClearTextMessage(mainUserCompanyId, mainUserNetworkId,
                spy, spy, "Hello encryption");
        EncryptedTextMessage encryptedMessage = new EncryptedTextMessage(clearMessage);

        assertFalse(clearMessage.toString().equals(encryptedMessage.toString()));
    }

    // Check that a message to oneself can be encrypted then decrypted
    @Test
    public void clearTextEncryptDecrypt() {
        ClearTextMessage clearMessage = new ClearTextMessage(mainUserCompanyId, mainUserNetworkId,
                spy, spy, "Hello encryption decryption");
        EncryptedTextMessage encryptedMessage = new EncryptedTextMessage(clearMessage);
        ClearTextMessage decryptedMessage = new ClearTextMessage(encryptedMessage);

        assertEquals(clearMessage.toString(), decryptedMessage.toString());
    }

    // Add three mainUsers to the MainUserMap and verify its content afterwards
    @Test
    public void mainUsersAdd() {
        MainUser mainUser1 = mock(MainUser.class);
        MainUser mainUser2 = mock(MainUser.class);
        MainUser mainUser3 = mock(MainUser.class);

        final short mainUser1CompanyId = (short) 0x01;
        final short mainUser2CompanyId = (short) 0x02;
        final short mainUser3CompanyId = (short) 0x01;

        final short mainUser1NetworkId = (short) 0x11;
        final short mainUser2NetworkId = (short) 0x12;
        final short mainUser3NetworkId = (short) 0x13;

        when(mainUser1.getCompanyId()).thenReturn(mainUser1CompanyId);
        when(mainUser2.getCompanyId()).thenReturn(mainUser2CompanyId);
        when(mainUser3.getCompanyId()).thenReturn(mainUser3CompanyId);

        when(mainUser1.getNetworkId()).thenReturn(mainUser1NetworkId);
        when(mainUser2.getNetworkId()).thenReturn(mainUser2NetworkId);
        when(mainUser3.getNetworkId()).thenReturn(mainUser3NetworkId);

        MainUserMap mainUserMap = MainUserMap.getInstance();
        mainUserMap.setMainUser(mainUser1);
        mainUserMap.setMainUser(mainUser2);
        mainUserMap.setMainUser(mainUser3);

        MainUserMap mainUserMapTest = MainUserMap.getInstance();
        MainUser mainUserTest = mainUserMapTest.getMainUser(mainUserCompanyId, mainUserNetworkId);
        MainUser mainUser1Test = mainUserMapTest.getMainUser(mainUser1CompanyId, mainUser1NetworkId);
        MainUser mainUser2Test = mainUserMapTest.getMainUser(mainUser2CompanyId, mainUser2NetworkId);
        MainUser mainUser3Test = mainUserMapTest.getMainUser(mainUser3CompanyId, mainUser3NetworkId);

        assertEquals(mainUserTest.getUserId(), mainUserUserId);
        assertEquals(mainUser1, mainUser1Test);
        assertEquals(mainUser2, mainUser2Test);
        assertEquals(mainUser3, mainUser3Test);
    }

    // Add a contact to the main user from the same network
    @Test
    public void addContact() {
        ContactUser contactUser = new ContactUser(spy, 0x01,
                "Test", null, null);
        spy.addContact(contactUser);
        assertEquals(spy.getContacts().get(0x01), contactUser);
    }

    // Add a contact to the main user from another network, expects error
    @Test(expected = IllegalStateException.class)
    public void addWrongNetworkContact() {
        ContactUser contactUser = new ContactUser((short) 0, (short) 0, 1, "Test",
                null, null);
        spy.addContact(contactUser);
    }

    // Attempt to decrypt a message not addressed to the main user
    @Test(expected = IllegalArgumentException.class)
    public void failDecryptMessage() throws Exception {
        ClearTextMessage clearTextMessage = new ClearTextMessage(mainUserCompanyId,
                mainUserNetworkId, spy, contactUser, "Hello");
        EncryptedTextMessage encryptedTextMessage = new EncryptedTextMessage(clearTextMessage);
        @SuppressWarnings("unused")
        ClearTextMessage decryptedTextMessage = new ClearTextMessage(encryptedTextMessage);
    }

    // Attempt to encrypt a message which is not from the main user
    @Test(expected = IllegalStateException.class)
    public void failEncryptMessage() {
        ClearTextMessage clearTextMessage = new ClearTextMessage(mainUserCompanyId,
                mainUserNetworkId, contactUser, spy, "Hello");
        EncryptedTextMessage encryptedTextMessage = new EncryptedTextMessage(clearTextMessage);
    }
}
