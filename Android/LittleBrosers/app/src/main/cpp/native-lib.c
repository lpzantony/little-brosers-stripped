#include <jni.h>
#include <android/log.h>
#include <task_stack.h>
#include <message.h>
#include <slot_pool.h>
#include "merkle_tree.h"
#include "utils.h"
#include "merkle_merge.h"

const char * internalStoragePath;

// Struct used to keep track of the JNI environment during a succession of function
// It is invalidated as soon as the native function called from Java returns, so it has to be set
// at each call
typedef struct {
    JNIEnv *env;
    jobject this;
} jni_local_ref;

LB_ALLOC_MERKLE_TREE(root, slot_pool);
LB_ALLOC_ROOT_MD(root_md);


JNIEXPORT void JNICALL
Java_com_littlebrosers_gui_MainActivity_lbInit(JNIEnv *env, jobject this) {
    // TODO: Handle tree save and restore
    LB_INIT_ROOT_MD(root_md);
    LB_INIT_MERKLE_TREE(root);
    lb_init_merkle_tree_only(&root, &root_md);

    // Load the timestamp from the MainUser
    jclass cls = (*env)->GetObjectClass(env, this);
    jmethodID mid = (*env)->GetMethodID(env, cls, "getTimestamp", "()I");
    jint ret = (*env)->CallIntMethod(env, this, mid);
    uint32_t certif_date = (uint32_t) ret;
    if(lb_does_file_exists(internalStoragePath) == 0){
        //backup file doesn't exists, create and fill it
        LBLOG("Backup file does not exist");
        lb_init_slots(&slot_pool, certif_date);
        lb_create_file(internalStoragePath);
        lb_save_slot_pool(&slot_pool, internalStoragePath);
    } else{
        // It exist, let's check the size
        LBLOG("Backup file exists");
        size_t tmp_size;
        lb_load_slot_pool_size(&tmp_size, internalStoragePath);
        if(tmp_size != LB_NB_MAX_MSGS + 1){
            lb_init_slots(&slot_pool, certif_date);
            lb_remove_file(internalStoragePath);
            lb_create_file(internalStoragePath);
            lb_save_slot_pool(&slot_pool, internalStoragePath);
        }else{
            lb_load_slot_pool(&slot_pool, internalStoragePath);
        }
        lb_attach_leafs_to_slots(&root, &slot_pool);
    }
    LBLOG("Update the timestamp with latest value and save the slot pool");
    lb_shift_tree(&root, &slot_pool, certif_date, lb_save_slot_pool, internalStoragePath);
    LBLOG("Slot pool init done, timestamp is %d", slot_pool.certif_date);
    lb_print_merkle_tree(&root, &slot_pool);
}

#ifdef DEBUG_MERGE
int send_count = 0;
int recv_count = 0;
#endif

err_t lb_send_android(lb_task_stack *p_st, void *args) {
#ifdef DEBUG_MERGE
    send_count++;
    LBLOG("SEND JNI ANDROID %d\n", send_count);
#endif
    jni_local_ref *diff_args = (jni_local_ref *) args;
    JNIEnv *env = diff_args->env;
    jobject this = diff_args->this;

    jclass cls = (*env)->GetObjectClass(env, this);
    jmethodID mid = (*env)->GetMethodID(env, cls, "diffSend", "([B)Z");

    jbyteArray payload = (*env)->NewByteArray(env, (jsize) p_st->buff.used);
    (*env)->SetByteArrayRegion(env, payload, 0, (jsize)
            p_st->buff.used, (jbyte *) p_st->buff.payload);

    bool ret = (*env)->CallBooleanMethod(env, this, mid, payload);
    if (!ret) {
        LBLOG("ERROR\n");
    }
    return LB_OK;
}

err_t lb_recv_android(lb_task_stack *p_st, void *args) {
#ifdef DEBUG_MERGE
    recv_count++;
    LBLOG("RECV JNI ANDROID %d\n", recv_count);
#endif
    jni_local_ref *diff_args = (jni_local_ref *) args;
    JNIEnv *env = diff_args->env;
    jobject this = diff_args->this;
    jclass cls = (*env)->GetObjectClass(env, this);
    jmethodID mid = (*env)->GetMethodID(env, cls, "diffReceive", "()[B");

    jbyteArray payload = (*env)->CallObjectMethod(env, this, mid);
    jbyte *buff = (*env)->GetByteArrayElements(env, payload, NULL);
    memcpy(p_st->buff.payload, buff, (size_t) (*env)->GetArrayLength(env, payload));
    p_st->buff.used = (size_t) (*env)->GetArrayLength(env, payload);

    (*env)->ReleaseByteArrayElements(env, payload, buff, JNI_ABORT);

    return LB_OK;
}

JNIEXPORT void JNICALL
Java_com_littlebrosers_ble_BLEConnectionService_lbDiffWrapper(
        JNIEnv *env,
        jobject this) {
    // JNI reference to BLEConnectionService
    jni_local_ref args = {env, this};
    lb_task_stack task_stack;
    lb_stack_init(&task_stack, &slot_pool);

    lb_diff(&task_stack, &root, 1, lb_send_android, &args, lb_recv_android, &args);
}

// This function is useful to make the Java code independent from the signature size macro
JNIEXPORT jint JNICALL
Java_com_littlebrosers_utils_Util_getSlotSignatureSize(JNIEnv *env, jobject this) {
    return (jint) LB_SLOT_SIGNATURE_SIZE;
}

JNIEXPORT void JNICALL
Java_com_littlebrosers_utils_Util_lbAttachMessageToTreeWrapper(JNIEnv *env,
                                                               jobject this,
                                                               jbyteArray messageData) {
    uint8_t timestamp_buffer[sizeof(uint32_t)];
    uint8_t message_id_buffer[sizeof(uint32_t)];

    message_t message = {0};
    // The first 4 bytes are the timestamp
    (*env)->GetByteArrayRegion(env, messageData, 0, 4, (jbyte *) &timestamp_buffer);
    // The next 4 bytes are the message_id (or db_address)
    (*env)->GetByteArrayRegion(env, messageData, 4, 4, (jbyte *) &message_id_buffer);
    // The next LB_SLOT_SIGNATURE_SIZE bytes are the bytes of the signature we keep in the slot
    (*env)->GetByteArrayRegion(env, messageData, 8, LB_SLOT_SIGNATURE_SIZE,
                               (jbyte *) message.signature);

    // Correct endianness if needed
    uint32_t message_id;
    if (is_LE()) {
        message_id = (uint32_t) bytes_to_int32(message_id_buffer);
        uint32_t timestamp = (uint32_t) bytes_to_int32(timestamp_buffer);
        memcpy(&message.header.timestamp, &timestamp, sizeof(uint32_t));
    } else {
        memcpy(&message.header.timestamp, timestamp_buffer, sizeof(int32_t));
        memcpy(&message_id, message_id_buffer, sizeof(int32_t));
    }
    message_pool_t message_pool = {&message, 1};

    err_t ret = lb_attach_msgs_to_tree(&root, &message_pool, &slot_pool, NULL, &message_id);

    if (ret != LB_OK) {
        LBLOG("Something went wrong adding the message to the tree");
    } else {
        LBLOG("Message added to the Merkle tree");
        for (int i = 0; i < 6; i++) {
            LBLOG("%d", message.signature[i]);
        }
    }
}

JNIEXPORT jint JNICALL
Java_com_littlebrosers_ble_BLEConnectionService_lbNextMsgToSendWrapper(JNIEnv *env, jobject this) {
    int32_t message_id;
    lb_next_msg_to_send(&slot_pool, &message_id);
    LBLOG("Value of message_id: %d", message_id);
    return (jint) message_id;
}


JNIEXPORT void JNICALL
Java_com_littlebrosers_ble_BLEConnectionService_saveSlotPool(JNIEnv *env, jobject this) {
    lb_save_slot_pool(&slot_pool, internalStoragePath);
}

JNIEXPORT void JNICALL
Java_com_littlebrosers_gui_MainActivity_loadSlotPool(JNIEnv *env, jobject this) {
    lb_load_slot_pool(&slot_pool, internalStoragePath);
}

JNIEXPORT void JNICALL
Java_com_littlebrosers_gui_MainActivity_lbSetBckpPath(JNIEnv *env, jobject instance,
                                                      jstring backupPath_) {
    internalStoragePath = (*env)->GetStringUTFChars(env, backupPath_, 0);
    LBLOG("Storage path : %s", internalStoragePath);
}
