package com.littlebrosers.messages;


import android.support.annotation.NonNull;

import com.littlebrosers.users.MainUser;
import com.littlebrosers.users.User;

import java.util.ArrayList;

public class Conversation implements Comparable<Conversation> {
    private static int counter;

    private MainUser mainUser;
    private User contactUser;
    private int conversationId;
    private ArrayList<ClearTextMessage> messages;

    public Conversation(MainUser mainUser, User contactUser) {
        this.mainUser = mainUser;
        this.contactUser = contactUser;
        messages = new ArrayList<>();
        conversationId = ++counter;
    }

    public static int getCounter() {
        return counter;
    }

    // To be called after context restore from database
    public static void setCounter(int newCounter) {
        counter = newCounter;
    }

    public short getCompanyId() {
        return mainUser.getCompanyId();
    }

    public short getNetworkId() {
        return mainUser.getNetworkId();
    }

    public int getConversationId() {
        return conversationId;
    }

    public void setConversationId(int conversationId) {
        this.conversationId = conversationId;
    }

    public ArrayList<ClearTextMessage> getMessages() {
        return messages;
    }

    public void setMessages(ArrayList<ClearTextMessage> messages) {
        this.messages = messages;
    }

    public MainUser getMainUser() {
        return mainUser;
    }

    public User getContactUser() {
        return contactUser;
    }

    public void addMessage(ClearTextMessage message) {
        if (message.getFromUser().getUserId() == mainUser.getUserId() && message.getToUser().getUserId() == contactUser.getUserId()
                || message.getFromUser().getUserId() == contactUser.getUserId() && message.getToUser().getUserId() == mainUser.getUserId()) {
            messages.add(message);
            return;
        }
        throw new IllegalArgumentException("Incompatible message");
    }

    public ClearTextMessage getLastMessage() {
        if (messages.size() == 0) {
            return null;
        }

        return messages.get(messages.size() - 1);
    }

    @Override
    public int compareTo(@NonNull Conversation conversation) {
        if (this.getLastMessage() == null) {
            return -1;
        }

        if (conversation.getLastMessage() == null) {
            return 1;
        }
        return -this.getLastMessage().compareTo(conversation.getLastMessage());
    }
}
