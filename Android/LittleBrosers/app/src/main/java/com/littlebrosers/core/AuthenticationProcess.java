package com.littlebrosers.core;

import com.littlebrosers.drops.Drop;
import com.littlebrosers.users.MainUser;
import com.littlebrosers.utils.Util;

import java.security.SignatureException;
import java.util.Date;
import java.util.Random;

import static com.littlebrosers.utils.CryptoUtil.checkSignature;
import static com.littlebrosers.utils.CryptoUtil.compressSignature;
import static com.littlebrosers.utils.CryptoUtil.decompressSignature;
import static com.littlebrosers.utils.CryptoUtil.invertSignature;
import static com.littlebrosers.utils.CryptoUtil.sign;

public final class AuthenticationProcess {
    public static final int AUTHENTICATION_SERVICE_ID = 0x1000;

    public static final long AUTHENTICATION_SERVICE_UUID_MOST_SIG = 0x1000L << 32 | Util.UUID_MOST_SIG_CONST;
    public static final long SERVICE_STATUS_UUID_MOST_SIG = (0x01L & 0xFF) << 32 | AUTHENTICATION_SERVICE_UUID_MOST_SIG;
    public static final long TIME_CERTIFICATE_UUID_MOST_SIG = (0x02L & 0xFF) << 32 | AUTHENTICATION_SERVICE_UUID_MOST_SIG;
    public static final long USER_CERTIFICATE_UUID_MOST_SIG = (0x03L & 0xFF) << 32 | AUTHENTICATION_SERVICE_UUID_MOST_SIG;
    public static final long DROP_CHALLENGE_UUID_MOST_SIG = (0x04L & 0xFF) << 32 | AUTHENTICATION_SERVICE_UUID_MOST_SIG;
    public static final long DROP_RESPONSE_UUID_MOST_SIG = DROP_CHALLENGE_UUID_MOST_SIG;
    public static final long USER_CHALLENGE_UUID_MOST_SIG = (0x05L & 0xFF) << 32 | AUTHENTICATION_SERVICE_UUID_MOST_SIG;
    public static final long USER_RESPONSE_UUID_MOST_SIG = USER_CHALLENGE_UUID_MOST_SIG;

    public static final int AUTHENTICATION_COMPLETE_CODE = 0xFF;
    public static final int READ_TIME_CERTIFICATE_CODE = 0xF0;

    public static final int TIME_CERTIFICATE_JSON_LENGTH = 186;

    private static final int CHALLENGE_LENGTH = 64;
    private static byte[] dropChallenge;

    private AuthenticationProcess() {

    }

    public static byte[] generateDropChallenge() {
        dropChallenge = new byte[CHALLENGE_LENGTH];
        Random random = new Random((new Date()).getTime());
        random.nextBytes(dropChallenge);
        return dropChallenge;
    }

    public static boolean verifyDropResponse(byte[] response, Drop drop) {
        // Invert and decompress signature
        invertSignature(response);
        byte[] signature = decompressSignature(response);

        boolean result = false;
        try {
            result = checkSignature(dropChallenge,
                    signature, drop.getPublicKey());
        } catch (SignatureException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static byte[] answerChallenge(byte[] challenge, MainUser mainUser) {
        byte[] response = new byte[0];
        try {
            response = compressSignature(sign(challenge, mainUser));
            // Invert signature for the drop
            invertSignature(response);
        } catch (SignatureException e) {
            e.printStackTrace();
        }

        return response;
    }
}
