package com.littlebrosers.users;

import android.support.annotation.NonNull;

import com.littlebrosers.utils.Util;

import java.security.PublicKey;
import java.util.Locale;

public abstract class User implements Comparable<User> {
    private short companyId;
    private short networkId;
    private int userId;
    private String userName;
    private PublicKey rsaPublicKey;
    private PublicKey ecPublicKey;

    User(short companyId, short networkId, int userId, String userName) {
        this.companyId = companyId;
        this.networkId = networkId;
        this.userId = userId;

        this.userName = userName;
    }

    public PublicKey getRsaPublicKey() {
        return rsaPublicKey;
    }

    void setRsaPublicKey(PublicKey rsaPublicKey) {
        this.rsaPublicKey = rsaPublicKey;
    }

    public PublicKey getEcPublicKey() {
        return ecPublicKey;
    }

    void setEcPublicKey(PublicKey ecPublicKey) {
        this.ecPublicKey = ecPublicKey;
    }

    public String getUserName() {
        return userName;
    }

    public short getCompanyId() {
        return companyId;
    }

    public short getNetworkId() {
        return networkId;
    }

    public int getUserId() {
        return userId;
    }

    protected void setUserId(int userId) {
        this.userId = userId;
    }

    public String toString() {
        return String.format("UserName: %s - CompanyId : 0x%x - NetworkId: 0x%x - UserId: 0x%x",
                userName, companyId, networkId, userId);
    }

    public String getNetworkName() {
        return Util.getNetworks().get(companyId).get(networkId);
    }

    @Override
    public int compareTo(@NonNull User user) {
        return this.userName.toUpperCase(Locale.US).compareTo(user.userName.toUpperCase(Locale.US));
    }

}
