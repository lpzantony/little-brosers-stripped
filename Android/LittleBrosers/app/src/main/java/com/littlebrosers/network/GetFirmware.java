package com.littlebrosers.network;

import android.content.Context;
import android.os.AsyncTask;

import com.littlebrosers.R;

import org.apache.commons.io.IOUtils;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;

public class GetFirmware extends AsyncTask<Void, Void, byte[]> {
    private static final String URL = "https://little-brosers.herokuapp.com/static/app.zip";
    private WeakReference<Context> mContext;

    public GetFirmware(Context context) {
        mContext = new WeakReference<>(context);
    }

    @Override
    protected byte[] doInBackground(Void... data) throws IllegalArgumentException {
        HttpURLConnection client;
        try {
            URL url = new URL(URL);
            client = (HttpURLConnection) url.openConnection();
            client.setRequestMethod("GET");

            InputStream in = new BufferedInputStream(client.getInputStream());
            byte[] firmwareBytes = IOUtils.toByteArray(in);
            in.close();
            client.disconnect();
            return firmwareBytes;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /*
     * Write the firmware in a file
     */
    @Override
    protected void onPostExecute(byte[] firmwareBytes) {
        if (firmwareBytes == null) {
            return;
        }
        FileOutputStream outputStream;
        // Get the context and return if it doesn't exist anymore
        Context context = mContext.get();
        if (context == null) {
            return;
        }
        try {
            outputStream = context.openFileOutput(context.getString(R.string.firmware_filename),
                    Context.MODE_PRIVATE);
            outputStream.write(firmwareBytes);
            outputStream.close();
            System.out.println("Firmware downloaded");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
