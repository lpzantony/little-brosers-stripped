package com.littlebrosers.database;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Base64;
import android.util.Log;
import android.util.SparseArray;

import com.littlebrosers.drops.Drop;
import com.littlebrosers.drops.DropRepository;
import com.littlebrosers.messages.ClearTextMessage;
import com.littlebrosers.messages.Conversation;
import com.littlebrosers.users.ContactUser;
import com.littlebrosers.users.MainUser;
import com.littlebrosers.users.MainUserMap;
import com.littlebrosers.users.User;
import com.littlebrosers.utils.Util;

import java.io.ByteArrayInputStream;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.List;

import static android.database.sqlite.SQLiteDatabase.CONFLICT_REPLACE;

public class LBDatabaseHandler implements LBDatabaseInterface {
    private LBDatabaseHelper mDbHelper;

    public LBDatabaseHandler(Context context) {
        mDbHelper = new LBDatabaseHelper(context);
    }


    @Override
    public SparseArray<SparseArray<String>> getNetworks() {
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        // Wanted output
        String[] projection = {
                LBDatabaseContract.NetworkEntry.COLUMN_NAME_COMPANY,
                LBDatabaseContract.NetworkEntry.COLUMN_NAME_NETWORK,
                LBDatabaseContract.NetworkEntry.COLUMN_NAME_NETWORK_NAME
        };

        Cursor cursor = db.query(
                LBDatabaseContract.NetworkEntry.TABLE_NAME,
                projection,
                null,                                // The columns for the WHERE clause
                null,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                 // The sort order
        );

        if (cursor == null) {
            Log.i("Db", "Null cursor");
            db.close();
            return null;
        }

        SparseArray<SparseArray<String>> networks = new SparseArray<>();
        while (cursor.moveToNext()) {
            short companyId = cursor.getShort(cursor.getColumnIndex(LBDatabaseContract.NetworkEntry.COLUMN_NAME_COMPANY));
            SparseArray<String> company = networks.get(companyId, null);
            if (company == null) {
                company = new SparseArray<>();
                networks.put(companyId, company);
            }

            short networkId = cursor.getShort(cursor.getColumnIndex(LBDatabaseContract.NetworkEntry.COLUMN_NAME_NETWORK));
            String name = cursor.getString(cursor.getColumnIndex(LBDatabaseContract.NetworkEntry.COLUMN_NAME_NETWORK_NAME));
            company.put(networkId, name);
        }


        cursor.close();
        db.close();
        return networks;
    }

    @Override
    public SparseArray<SparseArray<String>> getServers() {
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        // Wanted output
        String[] projection = {
                LBDatabaseContract.NetworkEntry.COLUMN_NAME_COMPANY,
                LBDatabaseContract.NetworkEntry.COLUMN_NAME_NETWORK,
                LBDatabaseContract.NetworkEntry.COLUMN_NAME_SERVER_URL
        };

        Cursor cursor = db.query(
                LBDatabaseContract.NetworkEntry.TABLE_NAME,
                projection,
                null,                                // The columns for the WHERE clause
                null,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                 // The sort order
        );

        if (cursor == null) {
            Log.i("Db", "Null cursor");
            db.close();
            return null;
        }

        SparseArray<SparseArray<String>> servers = new SparseArray<>();
        while (cursor.moveToNext()) {
            short companyId = cursor.getShort(cursor.getColumnIndex(LBDatabaseContract.NetworkEntry.COLUMN_NAME_COMPANY));
            SparseArray<String> company = servers.get(companyId, null);
            if (company == null) {
                company = new SparseArray<>();
                servers.put(companyId, company);
            }

            short networkId = cursor.getShort(cursor.getColumnIndex(LBDatabaseContract.NetworkEntry.COLUMN_NAME_NETWORK));
            String url = cursor.getString(cursor.getColumnIndex(LBDatabaseContract.NetworkEntry.COLUMN_NAME_SERVER_URL));
            company.put(networkId, url);

        }

        cursor.close();
        db.close();
        return servers;
    }

    @Override
    public void getMainUsers(MainUserMap mainUserMap) {
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        // Wanted output
        String[] projection = {
                LBDatabaseContract.MainUserEntry.COLUMN_NAME_COMPANY,
                LBDatabaseContract.MainUserEntry.COLUMN_NAME_NETWORK,
                LBDatabaseContract.MainUserEntry.COLUMN_NAME_MAIN_USER_ID,
                LBDatabaseContract.MainUserEntry.COLUMN_NAME_MAIN_USER_NAME,
                LBDatabaseContract.MainUserEntry.COLUMN_NAME_MAIN_USER_TIME_CERTIFICATE,
                LBDatabaseContract.MainUserEntry.COLUMN_NAME_MAIN_USER_CERTIFICATE
        };

        Cursor cursor = db.query(
                LBDatabaseContract.MainUserEntry.TABLE_NAME,
                projection,
                null,                                // The columns for the WHERE clause
                null,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                 // The sort order
        );

        if (cursor == null) {
            Log.i("Db", "Null cursor");
            db.close();
            return;
        }

        while (cursor.moveToNext()) {
            short companyId = cursor.getShort(cursor.getColumnIndex(LBDatabaseContract.MainUserEntry.COLUMN_NAME_COMPANY));
            short networkId = cursor.getShort(cursor.getColumnIndex(LBDatabaseContract.MainUserEntry.COLUMN_NAME_NETWORK));
            int userId = cursor.getInt(cursor.getColumnIndex(LBDatabaseContract.MainUserEntry.COLUMN_NAME_MAIN_USER_ID));
            String username = cursor.getString(cursor.getColumnIndex(LBDatabaseContract.MainUserEntry.COLUMN_NAME_MAIN_USER_NAME));
            String timeCertificate = cursor.getString(cursor.getColumnIndex(LBDatabaseContract.MainUserEntry.COLUMN_NAME_MAIN_USER_TIME_CERTIFICATE));
            String certificate = cursor.getString(cursor.getColumnIndex(LBDatabaseContract.MainUserEntry.COLUMN_NAME_MAIN_USER_CERTIFICATE));

            MainUser mainUser = new MainUser(companyId, networkId, userId, username);
            mainUser.setTimeCertificate(Base64.decode(timeCertificate, Base64.DEFAULT));

            try {
                CertificateFactory cf = CertificateFactory.getInstance("x509");
                byte[] userCertBinary = Base64.decode(certificate, Base64.DEFAULT);
                ByteArrayInputStream in = new ByteArrayInputStream(userCertBinary);
                mainUser.setCertificate(cf.generateCertificate(in));
            } catch (CertificateException e) {
                e.printStackTrace();
            }
            mainUser.loadKeys();
            mainUserMap.setMainUser(mainUser);
        }

        cursor.close();
        db.close();
    }

    @Override
    public void getDrops(MainUserMap mainUserMap) {
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        // Wanted output
        String[] projection = {
                LBDatabaseContract.DropEntry.COLUMN_NAME_COMPANY,
                LBDatabaseContract.DropEntry.COLUMN_NAME_NETWORK,
                LBDatabaseContract.DropEntry.COLUMN_NAME_DROP_ID,
                LBDatabaseContract.DropEntry.COLUMN_NAME_DROP_COUNTER,
                LBDatabaseContract.DropEntry.COLUMN_NAME_DROP_UPTODATE,
                LBDatabaseContract.DropEntry.COLUMN_NAME_DROP_PUB_KEY
        };

        Cursor cursor = db.query(
                LBDatabaseContract.DropEntry.TABLE_NAME,
                projection,
                null,                                // The columns for the WHERE clause
                null,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                 // The sort order
        );

        if (cursor == null) {
            Log.i("Db", "Null cursor");
            db.close();
            return;
        }

        while (cursor.moveToNext()) {
            short companyId = cursor.getShort(cursor.getColumnIndex(LBDatabaseContract.DropEntry.COLUMN_NAME_COMPANY));
            short networkId = cursor.getShort(cursor.getColumnIndex(LBDatabaseContract.DropEntry.COLUMN_NAME_NETWORK));
            short dropId = cursor.getShort(cursor.getColumnIndex(LBDatabaseContract.DropEntry.COLUMN_NAME_DROP_ID));
            int dropCounter = cursor.getInt(cursor.getColumnIndex(LBDatabaseContract.DropEntry.COLUMN_NAME_DROP_COUNTER));
            int uptodate = cursor.getInt(cursor.getColumnIndex(LBDatabaseContract.DropEntry.COLUMN_NAME_DROP_UPTODATE));
            String pubkeyString = cursor.getString(cursor.getColumnIndex(LBDatabaseContract.DropEntry.COLUMN_NAME_DROP_PUB_KEY));

            byte[] encoded = Base64.decode(pubkeyString, Base64.DEFAULT);

            Drop drop = mainUserMap.getMainUser(companyId, networkId).getRepository().addDrop(dropId);
            drop.setCounter(dropCounter);
            try {
                drop.setPublicKey(KeyFactory.getInstance("EC").generatePublic(new X509EncodedKeySpec(encoded)));
            } catch (InvalidKeySpecException | NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
        }

        cursor.close();
        db.close();
    }

    @Override
    public void getContacts(MainUserMap mainUserMap) {
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        // Wanted output
        String[] projection = {
                LBDatabaseContract.UserEntry.COLUMN_NAME_COMPANY,
                LBDatabaseContract.UserEntry.COLUMN_NAME_NETWORK,
                LBDatabaseContract.UserEntry.COLUMN_NAME_CONTACT_USER_ID,
                LBDatabaseContract.UserEntry.COLUMN_NAME_CONTACT_USER_NAME,
                LBDatabaseContract.UserEntry.COLUMN_NAME_USER_RSA_PUB_KEY,
                LBDatabaseContract.UserEntry.COLUMN_NAME_USER_EC_PUB_KEY
        };

        Cursor cursor = db.query(
                LBDatabaseContract.UserEntry.TABLE_NAME,
                projection,
                null,                              // The columns for the WHERE clause
                null,                           // The values for the WHERE clause
                null,                               // don't group the rows
                null,                                // don't filter by row groups
                null                                // The sort order
        );

        if (cursor == null) {
            Log.i("Db", "Null cursor");
            db.close();
            return;
        }

        while (cursor.moveToNext()) {
            short companyId = cursor.getShort(cursor.getColumnIndex(LBDatabaseContract.UserEntry.COLUMN_NAME_COMPANY));
            short networkId = cursor.getShort(cursor.getColumnIndex(LBDatabaseContract.UserEntry.COLUMN_NAME_NETWORK));
            int userId = cursor.getInt(cursor.getColumnIndex(LBDatabaseContract.UserEntry.COLUMN_NAME_CONTACT_USER_ID));
            String username = cursor.getString(cursor.getColumnIndex(LBDatabaseContract.UserEntry.COLUMN_NAME_CONTACT_USER_NAME));
            String userRsaString = cursor.getString(cursor.getColumnIndex(LBDatabaseContract.UserEntry.COLUMN_NAME_USER_RSA_PUB_KEY));
            String userEcString = cursor.getString(cursor.getColumnIndex(LBDatabaseContract.UserEntry.COLUMN_NAME_USER_EC_PUB_KEY));

            byte[] rsaEncoded = Base64.decode(userRsaString, Base64.DEFAULT);
            byte[] ecEncoded = Base64.decode(userEcString, Base64.DEFAULT);


            try {
                PublicKey ecKey = KeyFactory.getInstance("EC").generatePublic(new X509EncodedKeySpec(ecEncoded));
                PublicKey rsaKey = KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(rsaEncoded));
                ContactUser contact = new ContactUser(companyId, networkId, userId, username, rsaKey, ecKey);
                mainUserMap.getMainUser(companyId, networkId).addContact(contact);
            } catch (InvalidKeySpecException | NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
        }

        cursor.close();
        db.close();
    }

    @Override
    public void getConversations(MainUserMap mainUserMap) {
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        // Wanted output
        String[] projection = {
                "conv." + LBDatabaseContract.ConversationEntry.COLUMN_NAME_COMPANY,
                "conv." + LBDatabaseContract.ConversationEntry.COLUMN_NAME_NETWORK,
                "conv." + LBDatabaseContract.ConversationEntry.COLUMN_NAME_CONTACT_USER_ID,
                "conv." + LBDatabaseContract.ConversationEntry.COLUMN_NAME_CONVERSATION_ID,
                "msg." + LBDatabaseContract.MessageEntry.COLUMN_NAME_MESSAGE_ID,
                "msg." + LBDatabaseContract.MessageEntry.COLUMN_NAME_TIMESTAMP,
                "msg." + LBDatabaseContract.MessageEntry.COLUMN_NAME_FROM_ID,
                "msg." + LBDatabaseContract.MessageEntry.COLUMN_NAME_TO_ID,
                "msg." + LBDatabaseContract.MessageEntry.COLUMN_NAME_TEXT
        };

        String selection = "conv." + LBDatabaseContract.ConversationEntry.COLUMN_NAME_CONVERSATION_ID
                + " = " + "map." + LBDatabaseContract.ConversationMapEntry.COLUMN_NAME_CONVERSATION_ID
                + " AND " + "map." + LBDatabaseContract.ConversationMapEntry.COLUMN_NAME_MESSAGE_ID
                + " = " + "msg." + LBDatabaseContract.MessageEntry.COLUMN_NAME_MESSAGE_ID;
        String[] selectionArgs = {};

        Cursor cursor = db.query(
                LBDatabaseContract.ConversationEntry.TABLE_NAME + " conv "
                        + " JOIN " + LBDatabaseContract.ConversationMapEntry.TABLE_NAME + " map "
                        + " JOIN " + LBDatabaseContract.MessageEntry.TABLE_NAME + " msg ",
                projection,
                selection,                                // The columns for the WHERE clause
                selectionArgs,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                 // The sort order
        );

        if (cursor == null) {
            Log.i("Db", "Null cursor");
            db.close();
            return;
        }

        ClearTextMessage msg;
        while (cursor.moveToNext()) {
            short companyId = cursor.getShort(cursor.getColumnIndex(LBDatabaseContract.ConversationEntry.COLUMN_NAME_COMPANY));
            short networkId = cursor.getShort(cursor.getColumnIndex(LBDatabaseContract.ConversationEntry.COLUMN_NAME_NETWORK));

            MainUser mainUser = mainUserMap.getMainUser(companyId, networkId);
            if (mainUser == null) {
                continue;
            }

            int messageId = cursor.getInt(cursor.getColumnIndex(LBDatabaseContract.MessageEntry.COLUMN_NAME_MESSAGE_ID));
            int timestamp = cursor.getInt(cursor.getColumnIndex(LBDatabaseContract.MessageEntry.COLUMN_NAME_TIMESTAMP));
            int fromId = cursor.getInt(cursor.getColumnIndex(LBDatabaseContract.MessageEntry.COLUMN_NAME_FROM_ID));
            int toId = cursor.getInt(cursor.getColumnIndex(LBDatabaseContract.MessageEntry.COLUMN_NAME_TO_ID));
            String text = cursor.getString(cursor.getColumnIndex(LBDatabaseContract.MessageEntry.COLUMN_NAME_TEXT));

            msg = new ClearTextMessage(companyId, networkId);
            msg.setFromId(fromId);
            msg.setToId(toId);
            msg.setSeconds(timestamp);
            msg.setCleartext(text);
            msg.setMessageId(messageId);

            int conversationId = cursor.getInt(cursor.getColumnIndex(LBDatabaseContract.ConversationEntry.COLUMN_NAME_CONVERSATION_ID));
            int contactId = cursor.getInt(cursor.getColumnIndex(LBDatabaseContract.ConversationEntry.COLUMN_NAME_CONTACT_USER_ID));

            Conversation conv;
            if (mainUser.getConversations() == null || mainUser.getConversations().get(contactId) == null) {
                conv = new Conversation(mainUser, mainUser.getContacts().get(contactId));
                conv.setConversationId(conversationId);
                mainUser.addConversation(conv);
            } else {
                conv = mainUser.getConversations().get(contactId);
            }

            try {
                conv.addMessage(msg);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        cursor.close();
        db.close();
    }

    @Override
    public byte[] getRawMessage(int index) {
        byte[] rawMessage = null;
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        // Wanted output
        String[] projection = {
                LBDatabaseContract.RawMessageEntry.COLUMN_NAME_RAW_MESSAGE_ID,
                LBDatabaseContract.RawMessageEntry.COLUMN_NAME_PAYLOAD
        };

        String selection = LBDatabaseContract.RawMessageEntry.COLUMN_NAME_RAW_MESSAGE_ID + " = ?";
        String[] selectionArgs = {String.valueOf(index)};

        Cursor cursor = db.query(
                LBDatabaseContract.RawMessageEntry.TABLE_NAME,
                projection,
                selection,                                // The columns for the WHERE clause
                selectionArgs,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                 // The sort order
        );

        if (cursor == null) {
            Log.i("Db", "Null cursor");
            db.close();
            return null;
        }

        while (cursor.moveToNext()) {
            rawMessage = cursor.getBlob(cursor.getColumnIndex(LBDatabaseContract.RawMessageEntry.COLUMN_NAME_PAYLOAD));
        }
        cursor.close();
        db.close();

        return rawMessage;
    }

    @Override
    public void saveNetworks(SparseArray<SparseArray<String>> networks) {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        for (int i = 0; i < networks.size(); i++) {
            short companyId = (short) networks.keyAt(i);
            SparseArray<String> company = networks.get(companyId);
            for (int j = 0; j < company.size(); j++) {
                short networkId = (short) company.keyAt(j);
                ContentValues values = new ContentValues();
                values.put(LBDatabaseContract.NetworkEntry.COLUMN_NAME_COMPANY, companyId);
                values.put(LBDatabaseContract.NetworkEntry.COLUMN_NAME_NETWORK, networkId);
                values.put(LBDatabaseContract.NetworkEntry.COLUMN_NAME_NETWORK_NAME, company.get(networkId));
                values.put(LBDatabaseContract.NetworkEntry.COLUMN_NAME_SERVER_URL, Util.getServerUrl(
                        companyId, networkId));
                db.insertWithOnConflict(LBDatabaseContract.NetworkEntry.TABLE_NAME, null, values, CONFLICT_REPLACE);
            }
        }
        db.close();
    }

    @Override
    public void saveMainUsers(MainUserMap mainUserMap) {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        for (MainUser mainUser : mainUserMap.getMainUsers()) {
            ContentValues values = new ContentValues();
            values.put(LBDatabaseContract.MainUserEntry.COLUMN_NAME_COMPANY, mainUser.getCompanyId());
            values.put(LBDatabaseContract.MainUserEntry.COLUMN_NAME_NETWORK, mainUser.getNetworkId());
            values.put(LBDatabaseContract.MainUserEntry.COLUMN_NAME_MAIN_USER_ID, mainUser.getUserId());
            values.put(LBDatabaseContract.MainUserEntry.COLUMN_NAME_MAIN_USER_NAME, mainUser.getUserName());
            try {
                values.put(LBDatabaseContract.MainUserEntry.COLUMN_NAME_MAIN_USER_CERTIFICATE, Base64.encodeToString(
                        mainUser.getCertificate().getEncoded(), Base64.DEFAULT));
            } catch (CertificateEncodingException e) {
                e.printStackTrace();
            }
            values.put(LBDatabaseContract.MainUserEntry.COLUMN_NAME_MAIN_USER_TIME_CERTIFICATE, Base64.encodeToString(
                    mainUser.getTimeCertificate(), Base64.DEFAULT));
            db.insertWithOnConflict(LBDatabaseContract.MainUserEntry.TABLE_NAME, null, values, CONFLICT_REPLACE);
        }
        db.close();
    }

    @Override
    public void saveDrops(MainUserMap mainUserMap) {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        for (MainUser mainUser : mainUserMap.getMainUsers()) {
            DropRepository repo = mainUser.getRepository();
            Drop drop;

            if (repo == null || repo.getDropList() == null) {
                return;
            }
            for (int i = 0; i < repo.getDropList().size(); i++) {
                ContentValues values = new ContentValues();
                drop = repo.getDropList().valueAt(i);
                values.put(LBDatabaseContract.DropEntry.COLUMN_NAME_COMPANY, mainUser.getCompanyId());
                values.put(LBDatabaseContract.DropEntry.COLUMN_NAME_NETWORK, mainUser.getNetworkId());
                values.put(LBDatabaseContract.DropEntry.COLUMN_NAME_DROP_ID, drop.getDropId());
                values.put(LBDatabaseContract.DropEntry.COLUMN_NAME_DROP_COUNTER, drop.getCounter());
                values.put(LBDatabaseContract.DropEntry.COLUMN_NAME_DROP_UPTODATE, drop.isUpToDate());
                values.put(LBDatabaseContract.DropEntry.COLUMN_NAME_DROP_PUB_KEY, Base64.encodeToString(
                        drop.getPublicKey().getEncoded(), Base64.DEFAULT));
                db.insertWithOnConflict(LBDatabaseContract.DropEntry.TABLE_NAME, null, values, CONFLICT_REPLACE);
            }
        }
        db.close();
    }

    @Override
    public void saveContacts(MainUserMap mainUserMap) {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        for (MainUser mainUser : mainUserMap.getMainUsers()) {
            List<User> contacts = mainUser.getContactList();
            for (User contact : contacts) {
                ContentValues values = new ContentValues();
                values.put(LBDatabaseContract.UserEntry.COLUMN_NAME_COMPANY, contact.getCompanyId());
                values.put(LBDatabaseContract.UserEntry.COLUMN_NAME_NETWORK, contact.getNetworkId());
                values.put(LBDatabaseContract.UserEntry.COLUMN_NAME_CONTACT_USER_ID, contact.getUserId());
                values.put(LBDatabaseContract.UserEntry.COLUMN_NAME_CONTACT_USER_NAME, contact.getUserName());
                values.put(LBDatabaseContract.UserEntry.COLUMN_NAME_USER_RSA_PUB_KEY, Base64.encodeToString(
                        contact.getRsaPublicKey().getEncoded(), Base64.DEFAULT));
                values.put(LBDatabaseContract.UserEntry.COLUMN_NAME_USER_EC_PUB_KEY, Base64.encodeToString(
                        contact.getEcPublicKey().getEncoded(), Base64.DEFAULT));
                db.insertWithOnConflict(LBDatabaseContract.UserEntry.TABLE_NAME, null, values, CONFLICT_REPLACE);
            }
        }
        db.close();
    }

    @Override
    public void saveConversations(MainUserMap mainUserMap) {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        ContentValues values;
        for (MainUser mainUser : mainUserMap.getMainUsers()) {
            SparseArray<Conversation> conversations = mainUser.getConversations();
            if (conversations == null) {
                return;
            }
            for (int i = 0; i < conversations.size(); i++) {
                Conversation conversation = conversations.valueAt(i);
                // Conversation entry
                values = new ContentValues();
                values.put(LBDatabaseContract.ConversationEntry.COLUMN_NAME_COMPANY, conversation.getCompanyId());
                values.put(LBDatabaseContract.ConversationEntry.COLUMN_NAME_NETWORK, conversation.getNetworkId());
                values.put(LBDatabaseContract.ConversationEntry.COLUMN_NAME_CONTACT_USER_ID, conversation.getContactUser().getUserId());
                values.put(LBDatabaseContract.ConversationEntry.COLUMN_NAME_CONVERSATION_ID, conversation.getConversationId());
                db.insertWithOnConflict(LBDatabaseContract.ConversationEntry.TABLE_NAME, null, values, CONFLICT_REPLACE);

                for (ClearTextMessage message : conversation.getMessages()) {
                    // Conversation map entry
                    values = new ContentValues();
                    values.put(LBDatabaseContract.ConversationMapEntry.COLUMN_NAME_CONVERSATION_ID, conversation.getConversationId());
                    values.put(LBDatabaseContract.ConversationMapEntry.COLUMN_NAME_MESSAGE_ID, message.getMessageId());
                    db.insertWithOnConflict(LBDatabaseContract.ConversationMapEntry.TABLE_NAME, null, values, CONFLICT_REPLACE);

                    // Message entry
                    values = new ContentValues();
                    values.put(LBDatabaseContract.MessageEntry.COLUMN_NAME_MESSAGE_ID, message.getMessageId());
                    values.put(LBDatabaseContract.MessageEntry.COLUMN_NAME_TIMESTAMP, message.getSeconds());
                    values.put(LBDatabaseContract.MessageEntry.COLUMN_NAME_FROM_ID, message.getFromId());
                    values.put(LBDatabaseContract.MessageEntry.COLUMN_NAME_TO_ID, message.getToId());
                    values.put(LBDatabaseContract.MessageEntry.COLUMN_NAME_TEXT, message.getCleartext());
                    db.insertWithOnConflict(LBDatabaseContract.MessageEntry.TABLE_NAME, null, values, CONFLICT_REPLACE);
                }
            }
        }
        db.close();
    }

    @Override
    public void saveMessage(ClearTextMessage message, Conversation conversation) {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        // Save conversation
        values.put(LBDatabaseContract.ConversationEntry.COLUMN_NAME_COMPANY, conversation.getCompanyId());
        values.put(LBDatabaseContract.ConversationEntry.COLUMN_NAME_NETWORK, conversation.getNetworkId());
        values.put(LBDatabaseContract.ConversationEntry.COLUMN_NAME_CONTACT_USER_ID, conversation.getContactUser().getUserId());
        values.put(LBDatabaseContract.ConversationEntry.COLUMN_NAME_CONVERSATION_ID, conversation.getConversationId());
        db.insertWithOnConflict(LBDatabaseContract.ConversationEntry.TABLE_NAME, null, values, CONFLICT_REPLACE);

        // Save conversation map
        values = new ContentValues();
        values.put(LBDatabaseContract.ConversationMapEntry.COLUMN_NAME_CONVERSATION_ID, conversation.getConversationId());
        values.put(LBDatabaseContract.ConversationMapEntry.COLUMN_NAME_MESSAGE_ID, message.getMessageId());
        db.insertWithOnConflict(LBDatabaseContract.ConversationMapEntry.TABLE_NAME, null, values, CONFLICT_REPLACE);

        // Save message
        values = new ContentValues();
        values.put(LBDatabaseContract.MessageEntry.COLUMN_NAME_MESSAGE_ID, message.getMessageId());
        values.put(LBDatabaseContract.MessageEntry.COLUMN_NAME_TIMESTAMP, message.getSeconds());
        values.put(LBDatabaseContract.MessageEntry.COLUMN_NAME_FROM_ID, message.getFromId());
        values.put(LBDatabaseContract.MessageEntry.COLUMN_NAME_TO_ID, message.getToId());
        values.put(LBDatabaseContract.MessageEntry.COLUMN_NAME_TEXT, message.getCleartext());
        db.insertWithOnConflict(LBDatabaseContract.MessageEntry.TABLE_NAME, null, values, CONFLICT_REPLACE);

        // Save contact
        if (!(conversation.getContactUser() instanceof MainUser)) {
            values = new ContentValues();
            ContactUser contact = (ContactUser) conversation.getContactUser();
            values.put(LBDatabaseContract.UserEntry.COLUMN_NAME_COMPANY, contact.getCompanyId());
            values.put(LBDatabaseContract.UserEntry.COLUMN_NAME_NETWORK, contact.getNetworkId());
            values.put(LBDatabaseContract.UserEntry.COLUMN_NAME_CONTACT_USER_ID, contact.getUserId());
            values.put(LBDatabaseContract.UserEntry.COLUMN_NAME_CONTACT_USER_NAME, contact.getUserName());
            values.put(LBDatabaseContract.UserEntry.COLUMN_NAME_USER_RSA_PUB_KEY, Base64.encodeToString(
                    contact.getRsaPublicKey().getEncoded(), Base64.DEFAULT));
            values.put(LBDatabaseContract.UserEntry.COLUMN_NAME_USER_EC_PUB_KEY, Base64.encodeToString(
                    contact.getEcPublicKey().getEncoded(), Base64.DEFAULT));
            db.insertWithOnConflict(LBDatabaseContract.UserEntry.TABLE_NAME, null, values, CONFLICT_REPLACE);
        }

        // Save main user
        values = new ContentValues();
        MainUser mainUser = conversation.getMainUser();
        values.put(LBDatabaseContract.MainUserEntry.COLUMN_NAME_COMPANY, mainUser.getCompanyId());
        values.put(LBDatabaseContract.MainUserEntry.COLUMN_NAME_NETWORK, mainUser.getNetworkId());
        values.put(LBDatabaseContract.MainUserEntry.COLUMN_NAME_MAIN_USER_ID, mainUser.getUserId());
        values.put(LBDatabaseContract.MainUserEntry.COLUMN_NAME_MAIN_USER_NAME, mainUser.getUserName());
        try {
            values.put(LBDatabaseContract.MainUserEntry.COLUMN_NAME_MAIN_USER_CERTIFICATE, Base64.encodeToString(
                    mainUser.getCertificate().getEncoded(), Base64.DEFAULT));
        } catch (CertificateEncodingException e) {
            e.printStackTrace();
        }
        values.put(LBDatabaseContract.MainUserEntry.COLUMN_NAME_MAIN_USER_TIME_CERTIFICATE, Base64.encodeToString(
                mainUser.getTimeCertificate(), Base64.DEFAULT));
        db.insertWithOnConflict(LBDatabaseContract.MainUserEntry.TABLE_NAME, null, values, CONFLICT_REPLACE);

        db.close();
    }

    @Override
    public void saveRawMessage(int index, byte[] rawMessage) {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(LBDatabaseContract.RawMessageEntry.COLUMN_NAME_RAW_MESSAGE_ID, index);
        values.put(LBDatabaseContract.RawMessageEntry.COLUMN_NAME_PAYLOAD, rawMessage);
        db.insertWithOnConflict(LBDatabaseContract.RawMessageEntry.TABLE_NAME, null, values, CONFLICT_REPLACE);
        db.close();
    }

    @Override
    public void clearDatabase() {
        Log.i("DB", "Clear DB");
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        mDbHelper.clearDatabase(db);
        db.close();
    }

    @Override
    public void clearRawMessages() {
        Log.i("DB", "Clear raw messages");
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        mDbHelper.clearRawMessages(db);
        db.close();
    }
}
