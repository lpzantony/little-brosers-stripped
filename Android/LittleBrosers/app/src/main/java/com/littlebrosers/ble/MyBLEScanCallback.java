package com.littlebrosers.ble;

import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.littlebrosers.R;
import com.littlebrosers.drops.Drop;
import com.littlebrosers.drops.DropRepository;
import com.littlebrosers.utils.Util;

import java.nio.ByteBuffer;
import java.util.List;

import no.nordicsemi.android.dfu.DfuServiceInitiator;
import no.nordicsemi.android.support.v18.scanner.ScanCallback;
import no.nordicsemi.android.support.v18.scanner.ScanResult;

class MyBLEScanCallback extends ScanCallback {

    private final BLEScanService service;

    MyBLEScanCallback(BLEScanService service) {
        super();
        this.service = service;
    }

    @Override
    public void onBatchScanResults(List<ScanResult> results) {
        for (ScanResult result : results) {
            BluetoothDevice dev = result.getDevice();
            if (service.inDfuMode()) {
                Toast.makeText(service, String.format("Launching DFU on %s", dev.getName()),
                    Toast.LENGTH_LONG).show();
                final DfuServiceInitiator starter = new DfuServiceInitiator(dev.getAddress())
                        .setKeepBond(false);
                String firmwarePath = service.getFileStreamPath(
                        service.getString(R.string.firmware_filename)
                ).getAbsolutePath();
                starter.setZip(null, firmwarePath);
                starter.start(service, DfuService.class);
                service.stopSelf();
                return;
            }
            Log.i("BLE Batch", dev.toString());
            // Looking for LB manufacturer specific data
            int manufacturerId = 0xFFFF;
            if (result.getScanRecord() == null) {
                return;
            }
            byte[] manufacturerData = result.getScanRecord().getManufacturerSpecificData(manufacturerId);

            // Manufacturer data should be 14 bytes long
            int bufferLength = 14;
            if (manufacturerData == null || manufacturerData.length != bufferLength) {
                Log.i("BLE Service", "Incorrect length of advertising data, ignoring");
                results.remove(result);
                continue;
            }

            // Get companyId, networkId, dropId and counter
            final int companyIdOffset = 4;
            final int companyIdLength = 2;
            short companyId = ByteBuffer.wrap(manufacturerData, companyIdOffset, companyIdLength).getShort();
            final int networkIdOffset = 6;
            final int networkIdLength = 2;
            short networkId = ByteBuffer.wrap(manufacturerData, networkIdOffset, networkIdLength).getShort();
            final int dropIdOffset = 8;
            final int dropIdLength = 2;
            short dropId = ByteBuffer.wrap(manufacturerData, dropIdOffset, dropIdLength).getShort();
            final int counterOffset = 10;
            final int counterLength = 4;
            int counter = ByteBuffer.wrap(manufacturerData, counterOffset, counterLength).getInt();

            DropRepository repo = Util.getDropRepository(companyId, networkId);
            if (repo == null) {
                continue;
            }

            Drop drop = repo.getDrop(dropId);
            if (drop == null) {
                Log.i("Scan Callback", String.format("Unrecognized drop %d", dropId));
                continue;
            }
            // Select the drops that should be connected to
            if (drop.shouldConnect(counter)) {
                Log.i("Should Connect", drop.toString());
                sendDropForProcessing(drop, counter, dev);
            }
        }
    }

    // Send drop data to BLE connection service for connection and exchange
    private void sendDropForProcessing(Drop dropObject, int counter, BluetoothDevice dropDevice) {
        Bundle dropBundle = new Bundle();
        dropBundle.putShort(service.getString(R.string.ble_scan_result_company_id),
                dropObject.getCompanyId());
        dropBundle.putShort(service.getString(R.string.ble_scan_result_network_id),
                dropObject.getNetworkId());
        dropBundle.putShort(service.getString(R.string.ble_scan_result_drop_id),
                dropObject.getDropId());
        dropBundle.putInt(service.getString(R.string.ble_scan_result_counter),
                counter);
        dropBundle.putParcelable(service.getString(R.string.ble_scan_result_drop_device), dropDevice);
        Intent intent = new Intent(service, BLEConnectionService.class);
        intent.putExtra(service.getString(R.string.ble_scan_result_bundle), dropBundle);
        service.startService(intent);
    }
}
