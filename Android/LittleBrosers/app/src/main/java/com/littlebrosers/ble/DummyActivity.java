package com.littlebrosers.ble;

import android.app.Activity;
import android.os.Bundle;

/**
 * Workaround from https://issuetracker.google.com/issues/36967794#c1
 */

public class DummyActivity extends Activity {
    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        finish();
    }
}
