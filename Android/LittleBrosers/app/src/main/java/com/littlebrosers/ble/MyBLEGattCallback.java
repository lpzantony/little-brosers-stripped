package com.littlebrosers.ble;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothProfile;
import android.util.Log;

import java.util.concurrent.CountDownLatch;

import static android.bluetooth.BluetoothGatt.GATT_SUCCESS;
import static com.littlebrosers.core.AuthenticationProcess.AUTHENTICATION_COMPLETE_CODE;
import static com.littlebrosers.core.AuthenticationProcess.AUTHENTICATION_SERVICE_ID;
import static com.littlebrosers.core.AuthenticationProcess.READ_TIME_CERTIFICATE_CODE;
import static com.littlebrosers.core.MergeProcess.MERGE_END_CODE;
import static com.littlebrosers.core.MergeProcess.MERGE_NONE_CODE;
import static com.littlebrosers.core.MergeProcess.MERGE_WRITTEN_CODE;
import static com.littlebrosers.core.MerkleTreeDiffProcess.DIFF_END_ACK_CODE;
import static com.littlebrosers.core.MerkleTreeDiffProcess.DIFF_MID_ACK_CODE;
import static com.littlebrosers.core.MerkleTreeDiffProcess.DIFF_SERVICE_ID;
import static com.littlebrosers.core.MerkleTreeDiffProcess.DIFF_START_ACK_CODE;


public class MyBLEGattCallback extends BluetoothGattCallback {
    public static final int DISCONNECTED_FLAG = -1;
    public static final int DEFAULT_FLAG = 0;
    public static final int READ_DATA_FLAG = 1;
    public static final int AUTHENTICATION_COMPLETE_FLAG = 2;
    public static final int READ_TIME_CERTIFICATE_FLAG = 3;
    public static final int DIFF_START_ACK_FLAG = 4;
    public static final int DIFF_MID_ACK_FLAG = 5;
    public static final int DIFF_END_ACK_FLAG = 6;
    public static final int MERGE_WRITTEN_FLAG = 7;
    public static final int MERGE_NONE_FLAG = 8;
    public static final int MERGE_END_FLAG = 9;

    private static final String TAG = "BLE Gatt Callback";

    private int runningService = 0;

    private CountDownLatch countDownLatch = new CountDownLatch(1);
    private int flag = 0;
    private byte[] readData;

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public void setRunningService(int service) {
        runningService = service;
    }

    public byte[] getReadData() {
        return readData;
    }

    public CountDownLatch getCountDownLatch() {
        return countDownLatch;
    }

    @Override
    public void onConnectionStateChange(BluetoothGatt gatt, int status,
                                        int newState) {
        if (newState == BluetoothProfile.STATE_CONNECTED) {
            Log.i(TAG, "Connected to GATT server.");
            Log.i(TAG, "Attempting to start service discovery:"
                    + gatt.discoverServices());

        } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
            Log.i(TAG, "Disconnected from GATT server.");
            flag = DISCONNECTED_FLAG;
            release();
        }
    }

    @Override
    public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
        if (status == GATT_SUCCESS) {
            readData = characteristic.getValue();
            flag = READ_DATA_FLAG;
            release();
        }
    }

    @Override
    public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
        if (status == GATT_SUCCESS) {
            release();
        }
    }

    @Override
    public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
        switch (runningService) {
            case AUTHENTICATION_SERVICE_ID:
                switch (characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0)) {
                    case AUTHENTICATION_COMPLETE_CODE:
                        flag = AUTHENTICATION_COMPLETE_FLAG;
                        break;
                    case READ_TIME_CERTIFICATE_CODE:
                        flag = READ_TIME_CERTIFICATE_FLAG;
                        break;
                    default:
                        flag = DEFAULT_FLAG;
                        break;
                }
                break;

            // This case is for either the diff or the merge
            // Duplicate case is forbidden so while it is the same IDit has to be kept this way
            case DIFF_SERVICE_ID:
                switch (characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0)) {
                    case DIFF_START_ACK_CODE:
                        flag = DIFF_START_ACK_FLAG;
                        break;
                    case DIFF_MID_ACK_CODE:
                        flag = DIFF_MID_ACK_FLAG;
                        break;
                    case DIFF_END_ACK_CODE:
                        flag = DIFF_END_ACK_FLAG;
                        break;
                    case MERGE_WRITTEN_CODE:
                        Log.i(TAG, "Received written");
                        flag = MERGE_WRITTEN_FLAG;
                        break;
                    case MERGE_NONE_CODE:
                        Log.i(TAG, "Received none");
                        flag = MERGE_NONE_FLAG;
                        break;
                    case MERGE_END_CODE:
                        Log.i(TAG, "Received end");
                        flag = MERGE_END_FLAG;
                        break;
                    default:
                        flag = DEFAULT_FLAG;
                        break;
                }
                break;

            default:
                flag = DEFAULT_FLAG;
                break;
        }
        release();
    }

    @Override
    public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
        Log.i(TAG, "Service status notification subscription.");
        release();
    }

    @Override
    public void onServicesDiscovered(BluetoothGatt gatt, int status) {
        Log.i(TAG, "Services discovered.");
        release();
    }

    @Override
    public void onMtuChanged(BluetoothGatt gatt, int mtu, int status) {
        if (status == GATT_SUCCESS) {
            Log.i(TAG, "MTU: " + String.valueOf(mtu));
            release();
        }
    }

    private void release() {
        countDownLatch.countDown();
        countDownLatch = new CountDownLatch(1);
    }
}
