package com.littlebrosers.gui;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.littlebrosers.R;
import com.littlebrosers.users.MainUser;

public class ConversationListFragment extends Fragment {
    private RecyclerView conversationRecycler;
    private ConversationListAdapter conversationListAdapter;

    private MainUser mainUser;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ((MainActivity) getActivity()).setActionBarTitle(mainUser.getNetworkName());
        ((MainActivity) getActivity()).showHamburgerIcon();
        View rootView = inflater.inflate(R.layout.fragment_conversation_list, container, false);
        rootView.findViewById(R.id.newConvButton).setVisibility(View.VISIBLE);

        conversationRecycler = rootView.findViewById(R.id.reyclerview_conversation_list);

        conversationRecycler.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(),
                conversationRecycler, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                MainActivity context = (MainActivity) view.getContext();
                context.setSelectedConversation(mainUser.getConversationsList().get(position).getContactUser().getUserId());
                context.loadMessageListFragment(false);
            }

            @Override
            public void onItemLongClick(View view, int position) {
                Log.i("Debug", "Long click " + position);
            }
        }));

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(conversationRecycler.getContext(), DividerItemDecoration.VERTICAL);
        conversationRecycler.addItemDecoration(dividerItemDecoration);

        conversationListAdapter = new ConversationListAdapter(mainUser.getConversationsList());
        conversationRecycler.setAdapter(conversationListAdapter);
        conversationRecycler.setLayoutManager(new LinearLayoutManager(rootView.getContext()));
        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mainUser = ((MainActivity) context).getSelectedMainUser();
    }
}
