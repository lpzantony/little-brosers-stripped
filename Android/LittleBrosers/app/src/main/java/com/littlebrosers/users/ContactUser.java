package com.littlebrosers.users;

import java.security.PublicKey;

public class ContactUser extends User {
    public ContactUser(short companyId, short networkId, int userId, String userName,
                       PublicKey rsaPublicKey, PublicKey ecPublicKey) {
        super(companyId, networkId, userId, userName);
        setRsaPublicKey(rsaPublicKey);
        setEcPublicKey(ecPublicKey);
    }

    public ContactUser(MainUser mainUser, int userId, String userName, PublicKey rsaPublicKey,
                       PublicKey ecPublicKey) {
        super(mainUser.getCompanyId(), mainUser.getNetworkId(), userId, userName);
        setRsaPublicKey(rsaPublicKey);
        setEcPublicKey(ecPublicKey);
    }

    public String toString() {
        return "Contact User: " + super.toString();
    }
}
