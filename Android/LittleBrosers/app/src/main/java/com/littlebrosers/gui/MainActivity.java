package com.littlebrosers.gui;

import android.Manifest;
import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.SparseArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.littlebrosers.R;
import com.littlebrosers.ble.BLEScanService;
import com.littlebrosers.database.LBDatabaseHandler;
import com.littlebrosers.messages.ClearTextMessage;
import com.littlebrosers.messages.Conversation;
import com.littlebrosers.messages.EncryptedTextMessage;
import com.littlebrosers.messages.GenericMessage;
import com.littlebrosers.network.GetFirmware;
import com.littlebrosers.users.MainUser;
import com.littlebrosers.users.MainUserMap;
import com.littlebrosers.users.User;
import com.littlebrosers.utils.PRNGFixes;
import com.littlebrosers.utils.Util;

import no.nordicsemi.android.dfu.DfuProgressListener;
import no.nordicsemi.android.dfu.DfuProgressListenerAdapter;
import no.nordicsemi.android.dfu.DfuServiceListenerHelper;

import static com.littlebrosers.utils.Util.getHexValue;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static String backupFileName = "lb_bckp";

    static {
        System.loadLibrary("lb-lib");
    }

    private ActionBarDrawerToggle toggle;
    private Intent intent;
    private NavigationView navigationView;
    private int selectedMainUserIndex;
    private int selectedConversationId;
    private int selectedContactId;

    private boolean scanState = false;
    private boolean dfuState = false;
    private MenuItem scanItem;
    private MenuItem dfuItem;
    private final DfuProgressListener mDfuProgressListener = new DfuProgressListenerAdapter() {
        @Override
        public void onEnablingDfuMode(final String deviceAddress) {
            // Update scan icon
            stopScan();
        }

        @Override
        public void onDfuAborted(final String deviceAddress) {
            dfuState = false;
            updateDfuIcon();
        }

        @Override
        public void onDfuCompleted(final String deviceAddress) {
            dfuState = false;
            updateDfuIcon();
        }

        @Override
        public void onError(final String deviceAddress, final int error, final int errorType, final String message) {
            dfuState = false;
            updateDfuIcon();
        }
    };

    /**
     * Check for Bluetooth.
     *
     * @return True if Bluetooth is available.
     */
    private static boolean isBluetoothAvailable() {
        final BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        return (bluetoothAdapter != null && bluetoothAdapter.isEnabled());
    }

    public native void lbSetBckpPath(String backupPath);

    public native void lbInit();

    private void saveState() {
        saveCounters();

        // DB
        LBDatabaseHandler db = new LBDatabaseHandler(this);
        // Save networks and servers
        Log.i("Networks", String.valueOf(Util.getNetworks()));
        Log.i("Servers", String.valueOf(Util.getServers()));
        db.saveNetworks(Util.getNetworks());
        // Save MainUsers
        if (MainUserMap.getInstance().getMainUsers().size() == 0) {
            Log.i("Save state", "No main users");
            return;
        }
        db.saveMainUsers(MainUserMap.getInstance());

        // Save contacts
        db.saveContacts(MainUserMap.getInstance());
        // Save drops
        db.saveDrops(MainUserMap.getInstance());
        // Save Conversations
        db.saveConversations(MainUserMap.getInstance());
    }

    private void loadState() {
        // DB
        LBDatabaseHandler db = new LBDatabaseHandler(this);
        // Get networks
        Util.setNetworks(db.getNetworks());
        Log.i("Networks", String.valueOf(Util.getNetworks()));
        // Get servers
        Util.setServers(db.getServers());
        Log.i("Servers", String.valueOf(Util.getServers()));
        // Get MainUsers
        db.getMainUsers(MainUserMap.getInstance());
        if (MainUserMap.getInstance().getMainUsers().size() == 0) {
            Log.i("Load state", "No main users");
            return;
        }

        // Get contacts
        db.getContacts(MainUserMap.getInstance());
        // Get drops
        db.getDrops(MainUserMap.getInstance());
        // Get Conversations
        db.getConversations(MainUserMap.getInstance());

        // Load saved clear message counter
        loadCounters();
    }

    @SuppressLint("DefaultLocale")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Apply Android security fix
        PRNGFixes.apply();
        loadState();

        /*
         * Get the latest firmware zip, we can do it many times because it is only 64 KO
         */
        downloadFirmware();

        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        intent = new Intent(this, BLEScanService.class);

        for (int i = 0; i < Util.getNetworks().size(); i++) {
            short companyId = (short) Util.getNetworks().keyAt(i);
            SparseArray<String> company = Util.getNetworks().get(companyId);
            for (int j = 0; j < company.size(); j++) {
                short networkId = (short) company.keyAt(j);
                Log.i("Main Activity", String.format("0x%x-0x%x", companyId, networkId));
                MainUser mainUser = MainUserMap.getInstance().getMainUser(companyId, networkId);
                if (mainUser != null) {
                    Log.i("Main Activity", mainUser.getUserName());
                    Log.i("Main user", String.format("%d contacts, %d conversations",
                            mainUser.getContacts().size(), mainUser.getConversations().size()));
                }
            }
        }

        if (MainUserMap.getInstance().getMainUsers().size() != 0) {
            MainUserMap.getInstance().updateWithServer();
        }


        resetDrawerMenu();
        selectedMainUserIndex = 0;

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.content_fragment, new MainFragment()).commit();

        // Init the Merkle tree
        lbSetBckpPath(this.getFilesDir().getAbsolutePath() + backupFileName);
        lbInit();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (BLEScanService.isRunning()) {
            if (scanItem != null) {
                scanItem.setIcon(R.drawable.ic_sync_white_24dp);
            }
            scanState = true;
        }
    }

    public void signUp() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

        fragmentManager.beginTransaction()
                .replace(R.id.content_fragment, new SignupFragment())
                .addToBackStack(null)
                .commit();

        fragmentManager.executePendingTransactions();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            // Switch to previous fragment if available
            int count = getSupportFragmentManager().getBackStackEntryCount();
            Log.i("debug", Integer.toString(count));
            if (count != 0) {
                getSupportFragmentManager().popBackStack();
            } else {
                super.onBackPressed();
            }
        }
    }

    public void setActionBarTitle(String title) {
        getSupportActionBar().setTitle(title);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        scanItem = menu.findItem(R.id.action_scan_toggle);
        dfuItem = menu.findItem(R.id.action_dfu_toggle);
        return true;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        // Notification clicked
        if (intent.getBooleanExtra(getString(R.string.new_message_notification_click_extra), false)) {
            Log.i("Main Activity", "onNewIntent");
            short companyId = intent.getShortExtra(getString(R.string.new_message_company_id_extra), (short) 0);
            short networkId = intent.getShortExtra(getString(R.string.new_message_network_id_extra), (short) 0);
            int userId = intent.getIntExtra(getString(R.string.new_message_user_id_extra), 0);

            if (companyId * networkId * userId == 0) {
                return;
            }

            MainUser mainUser = MainUserMap.getInstance().getMainUser(companyId, networkId);
            if (mainUser == null) {
                return;
            }
            selectedMainUserIndex = MainUserMap.getInstance().getMainUsers().indexOf(mainUser);

            Conversation conv = mainUser.getConversation(mainUser.getContacts().get(userId));
            if (conv == null) {
                return;
            }

            Log.i("Main Activity", "Launching fragment");
            selectedConversationId = mainUser.getConversationsList().indexOf(conv);

            loadMessageListFragment(false);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_scan_toggle) {
            if (scanState) {
                stopScan();
                BLEScanService.resetRunning();
            } else {
                startScan(false);
            }
            return true;
        } else if (id == R.id.action_dfu_toggle) {
            if (dfuState) {
                if (scanState) {
                    dfuState = false;
                }
                // If we stop the DFU service with an intent, the service will crash
                stopScan();
            } else {
                // First stop the current scan, because we need to create the scan
                stopScan();
                startScan(true);
                dfuState = true;
            }
            updateDfuIcon();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == navigationView.getMenu().size()) {
            signUp();
            DrawerLayout drawer = findViewById(R.id.drawer_layout);
            drawer.closeDrawer(GravityCompat.START);
            return true;
        }

        setSelectedMainUserIndex(id);

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.content_fragment, new ConversationListFragment())
                .addToBackStack(null)
                .commit();

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        fragmentManager.executePendingTransactions();
        return true;
    }

    public void setSelectedMainUserIndex(int mainUserIndex) {
        this.selectedMainUserIndex = mainUserIndex;
    }

    public MainUser getSelectedMainUser() {
        return MainUserMap.getInstance().getMainUsers().get(selectedMainUserIndex);
    }

    public Conversation getSelectedConversation() {
        return getSelectedMainUser().getConversations().get(selectedConversationId);
    }

    public void setSelectedConversation(int conversationId) {
        this.selectedConversationId = conversationId;
    }

    public void resetDrawerMenu() {
        Menu menu = navigationView.getMenu();
        menu.clear();

        int i = 0;
        for (MainUser user : MainUserMap.getInstance().getMainUsers()) {
            MenuItem item = menu.add(0, i, i, user.getUserName());
            item.setIcon(R.drawable.ic_account_circle_black_24dp);
            ++i;
        }
        MenuItem item = menu.add(0, i + 1, i + 1, "Sign Up");
        item.setIcon(android.R.drawable.ic_input_add);
    }

    public void newConversation(View v) {
        FragmentManager fragmentManager = getSupportFragmentManager();

        fragmentManager.beginTransaction()
                .replace(R.id.content_fragment, new NewConversationListFragment())
                .addToBackStack(null)
                .commit();

        fragmentManager.executePendingTransactions();
    }

    public boolean startScan(Boolean dfuMode) {
        // Check if Bluetooth is on
        if (!isBluetoothAvailable()) {
            Toast.makeText(this, getString(R.string.BLE_off_toast),
                    Toast.LENGTH_LONG).show();
            Log.e("BLE", "Bluetooth Off");
            return false;
        }

        // Ask for the location permission and check if it's been given
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 1);

        int permissionCheck = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION);

        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            Log.e("BLE", "Location permission needed");
            return false;
        }
        /* Let the service know if it should launch DFU or launch the diff
          This is not the best way to do it, but a good abstraction would be too long to implement
          now */
        intent.putExtra("dfuMode", dfuMode);
        startService(intent);
        scanItem.setIcon(R.drawable.ic_sync_white_24dp);
        scanState = true;
        return true;
    }

    public void stopScan() {
        scanItem.setIcon(R.drawable.ic_sync_disabled_white_24dp);
        scanState = false;
        stopService(intent);
    }

    private void saveGenericMessage(GenericMessage genericMessage) throws Exception {
        LBDatabaseHandler db = new LBDatabaseHandler(this);
        db.saveRawMessage(genericMessage.getMessageId(), genericMessage.toRawMessage());
    }

    private GenericMessage loadGenericMessage(short companyId, short networkId, int messageId) {
        GenericMessage genericMessage = null;
        LBDatabaseHandler db = new LBDatabaseHandler(this);
        byte[] raw = db.getRawMessage(messageId);
        if (raw != null) {
            genericMessage = new GenericMessage(companyId, networkId, messageId).fromRawMessage(raw);
        }
        return genericMessage;
    }

    public void testMessage(View v) {
            Log.i("Message", "TestMessage");
            short companyId = (short) 0xDEAD;
            short networkId = (short) 0xBEEF;
            MainUser mainUser = MainUserMap.getInstance().getMainUser(companyId, networkId);
            ClearTextMessage clrMsg = new ClearTextMessage(companyId, networkId, mainUser, mainUser,
                    "Hello World!");

            Log.i("Message", "Sent clear message");
            Log.i("Message", clrMsg.toString());

            EncryptedTextMessage enc = new EncryptedTextMessage(clrMsg);
            Log.i("Message", "Sent encrypted message");
            Log.i("Message", enc.toString());

            try {
                GenericMessage genericMessage = enc.toGenericMessage();
                Log.i("Message", genericMessage.toString());
                byte[] raw = genericMessage.toRawMessage();
                saveGenericMessage(genericMessage);
                Log.i("Message", "Sent raw message");
                Log.i("Message", String.valueOf(getHexValue(raw)));

                GenericMessage recGen = loadGenericMessage(companyId, networkId, genericMessage.getMessageId());
                if (recGen == null) {
                    Log.i("Message", "Message not in database");
                    return;
                }
                Log.i("Message", "Received generic message");
                Log.i("Message", recGen.toString());

                if (recGen.isClear()) {
                    Log.i("Message", "Is clear");
                    if (recGen.getType() == EncryptedTextMessage.MESSAGE_TYPE) {

                        Log.i("Message", "Valid type");
                        EncryptedTextMessage recEnc = new EncryptedTextMessage(recGen);

                        Log.i("Message", "Received encrypted message");
                        Log.i("Message", recEnc.toString());

                        ClearTextMessage recClr = new ClearTextMessage(recEnc);
                        Log.i("Message", "Received clear message");
                        Log.i("Message", recClr.toString());
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                throw new IllegalStateException("FOUND YOU");
            }
        }

    // newConversation indicates if the method was called when starting a new conversation
    public void loadMessageListFragment(boolean newConversation) {
        if (newConversation) {
            Conversation conversation = getSelectedMainUser().getConversation(getSelectedContact());
            if (conversation == null) {
                conversation = new Conversation(getSelectedMainUser(), getSelectedContact());
                getSelectedMainUser().addConversation(conversation);
            }
            setSelectedConversation(selectedContactId);
            getSupportFragmentManager().popBackStack();
        }

        FragmentManager fragmentManager = getSupportFragmentManager();

        fragmentManager.beginTransaction()
                .replace(R.id.content_fragment, new MessageListFragment())
                .addToBackStack(null)
                .commit();

        fragmentManager.executePendingTransactions();
    }

    public void showHamburgerIcon() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        toggle.setDrawerIndicatorEnabled(true);

    }

    public void showBackIcon() {
        toggle.setDrawerIndicatorEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }

    @Override
    protected void onStop() {
        super.onStop();
        BLEScanService.resetRunning();
        saveState();
    }

    private void loadCounters() {
        // Read from shared preferences
        SharedPreferences prefs = this.getSharedPreferences(this.getString(R.string.shared_preferences_index),
                Context.MODE_PRIVATE);
        ClearTextMessage.setCounter(prefs.getInt(
                this.getString(R.string.shared_preferences_clear_message_counter), 0));
        Conversation.setCounter(prefs.getInt(
                this.getString(R.string.shared_preferences_conversation_counter), 0));
        GenericMessage.setCounter(prefs.getInt(
                this.getString(R.string.shared_preferences_generic_message_counter), 0));
    }

    private void saveCounters() {
        // Write to shared preferences
        SharedPreferences prefs = this.getSharedPreferences(this.getString(R.string.shared_preferences_index),
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(
                this.getString(R.string.shared_preferences_clear_message_counter), ClearTextMessage.getCounter());
        editor.putInt(
                this.getString(R.string.shared_preferences_conversation_counter), Conversation.getCounter());
        editor.putInt(
                this.getString(R.string.shared_preferences_generic_message_counter), GenericMessage.getCounter());
        editor.apply();
    }

    private User getSelectedContact() {
        return getSelectedMainUser().getContacts().get(selectedContactId);
    }

    public void setSelectedContact(int selectedContactId) {
        this.selectedContactId = selectedContactId;
    }

    // Function called from native on init to get the latest timestamp
    private int getTimestamp() {
        // Hardcoded companyId networkId for the moment
        short companyId = (short) 0xDEAD;
        short networkId = (short) 0xBEEF;
        MainUser mainUser = MainUserMap.getInstance().getMainUser(companyId, networkId);
        if (mainUser == null) {
            return 604800;
        }
        return mainUser.getTimeReference();
    }

    /*
     * Download the latest signed firmware from the server and store it in internal memory
     */
    private void downloadFirmware() {
        new GetFirmware(this).execute();
    }

    private void updateDfuIcon() {
        if (dfuState) {
            dfuItem.setIcon(R.drawable.ic_file_upload_disabled_24dp);
        } else {
            dfuItem.setIcon(R.drawable.ic_file_upload_white_24dp);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        DfuServiceListenerHelper.registerProgressListener(this, mDfuProgressListener);
    }

    @Override
    protected void onPause() {
        super.onPause();

        DfuServiceListenerHelper.unregisterProgressListener(this, mDfuProgressListener);
    }
}
