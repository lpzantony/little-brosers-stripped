package com.littlebrosers.messages;

import android.util.SparseArray;

import com.littlebrosers.users.MainUser;
import com.littlebrosers.users.MainUserMap;
import com.littlebrosers.users.User;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

/**
 * Created by chris on 11/30/17.
 * Represents an AbstractMessage object, the parent of other message types.
 */

public abstract class AbstractMessage {
    static final int HEADER_SIZE = 14;
    static final int MAX_PAYLOAD_SIZE = 256 + 32; // For the EncryptedMessage
    static final int SIGNATURE_SIZE = 64;
    // + 2 at the end is to account for the two size bytes needed by the drop
    static final int MAX_MESSAGE_SIZE = HEADER_SIZE + MAX_PAYLOAD_SIZE + SIGNATURE_SIZE + 2;

    // Temporary way to store companyId and networkId, hardcoded
    private short companyId = 0x42;
    private short networkId = 0x69;
    private User fromUser;
    private User toUser;

    private Calendar timestamp = new GregorianCalendar(TimeZone.getTimeZone("GMT"));
    private byte[] signature;
    // Indicate if the message can be read by the user
    private boolean toClear = false;

    AbstractMessage(short companyId, short networkId) {
        this.fromUser = null;
        this.toUser = null;
        this.companyId = companyId;
        this.networkId = networkId;
    }

    AbstractMessage(AbstractMessage msg) {
        timestamp = msg.timestamp;
        fromUser = msg.fromUser;
        toUser = msg.toUser;
        companyId = msg.companyId;
        networkId = msg.networkId;
        setToClear();
    }

    AbstractMessage(short companyId, short networkId, User fromUser, User toUser) {
        this.fromUser = fromUser;
        this.toUser = toUser;
        this.companyId = companyId;
        this.networkId = networkId;
        setToClear();
    }

    public Calendar getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Calendar timestamp) {
        this.timestamp = timestamp;
    }

    // Transform Date object to a 32-bit int
    public int getSeconds() {
        final int tmp = 1000;
        return (int) (timestamp.getTimeInMillis() / tmp);
    }

    public void setSeconds(int seconds) {
        final int tmp = 1000;
        timestamp.setTimeInMillis((long) seconds * tmp);
    }

    public User getFromUser() {
        return fromUser;
    }

    public void setFromUser(User fromUser) {
        this.fromUser = fromUser;
    }

    public User getToUser() {
        return toUser;
    }

    public void setToUser(User toUser) {
        this.toUser = toUser;
    }

    public int getFromId() {
        return this.fromUser.getUserId();
    }

    public void setFromId(int id) {
        MainUser mainUser = MainUserMap.getInstance().getMainUser(companyId, networkId);
        if (mainUser == null) {
            throw new IllegalArgumentException("Main user doesn't exist");
        }
        if (id == mainUser.getUserId()) {
            fromUser = mainUser;
        } else {
            SparseArray<User> contacts = mainUser.getContacts();
            fromUser = contacts.get(id);
        }
    }

    public int getToId() {
        return this.toUser.getUserId();
    }

    public void setToId(int id) {
        MainUser mainUser = MainUserMap.getInstance().getMainUser(companyId, networkId);
        if (mainUser == null) {
            throw new IllegalArgumentException("Main user doesn't exist");
        }
        if (id == mainUser.getUserId()) {
            toUser = mainUser;
        } else {
            SparseArray<User> contacts = mainUser.getContacts();
            toUser = contacts.get(id);
        }
    }

    public short getCompanyId() {
        return companyId;
    }

    public short getNetworkId() {
        return networkId;
    }

    public void setToClear() {
        toClear = toUser instanceof MainUser;
    }

    public boolean isClear() {
        return toClear;
    }

    public byte[] getSignature() {
        return signature;
    }

    public void setSignature(byte[] signature) {
        this.signature = signature;
    }

    public String toString() {
        return "-Timestamp: "
                + timestamp.getTime().toString() + '\n'
                + "-From: " + fromUser.toString() + '\n'
                + "-To: " + toUser.toString() + '\n';
    }

    // Enum for message types
    public enum Type {
        ENCRYPTED_TEXT(0x01);

        private final short typeId;

        Type(int type) {
            typeId = (short) type;
        }

        public static Type getType(short type) {
            for (Type t : Type.values()) {
                if (t.typeId == type) {
                    return t;
                }
            }
            throw new IllegalArgumentException("Type " + String.valueOf(type) + " doesn't exist");
        }

        public short getTypeShort() {
            return typeId;
        }
    }

}
