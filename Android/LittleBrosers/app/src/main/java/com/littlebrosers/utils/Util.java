package com.littlebrosers.utils;


import android.util.Log;
import android.util.SparseArray;

import com.littlebrosers.drops.DropRepository;
import com.littlebrosers.messages.GenericMessage;
import com.littlebrosers.users.MainUser;
import com.littlebrosers.users.MainUserMap;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;

public final class Util {
    public static final int LITTLE_BROSERS_ID = 0x47414358;
    public static final long UUID_MOST_SIG_CONST = 0xC6524FEFL;
    public static final String SERVER_URL = "https://little-brosers.herokuapp.com/";
    public static final Object LOCK = new Object();
    //    private static SparseArray<ArrayList<Short>> networks;
    private static SparseArray<SparseArray<String>> networks;
    private static SparseArray<SparseArray<String>> servers;

    private Util() {
    }

    public static native int getSlotSignatureSize();

    public static native int getSlotSignatureOffset();

    public static native void lbAttachMessageToTreeWrapper(byte[] messageData);

    // http://www.itcsolutions.eu/2011/08/22/how-to-convert-a-byte-array-to-a-hex-string-in-java/
    public static char[] getHexValue(byte[] array) {
        char[] symbols = "0123456789ABCDEF".toCharArray();
        char[] hexValue = new char[array.length * 2];

        for (int i = 0; i < array.length; i++) {
            //convert the byte to an int
            int current = array[i] & 0xFF;
            //determine the Hex symbol for the last 4 bits
            hexValue[i * 2 + 1] = symbols[current & 0x0F];
            //determine the Hex symbol for the first 4 bits
            hexValue[i * 2] = symbols[current >> 4];
        }
        return hexValue;
    }

    // https://stackoverflow.com/questions/140131/convert-a-string-representation-of-a-hex-dump-to-a-byte-array-using-java
    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }

    public static DropRepository getDropRepository(short companyId, short networkId) {
        try {
            return MainUserMap.getInstance().getMainUser(companyId, networkId).getRepository();
        } catch (NullPointerException e) {
            return null;
        }
    }

    public static SparseArray<SparseArray<String>> getNetworks() {
        return networks;
    }

    public static void setNetworks(SparseArray<SparseArray<String>> networks) {
        Util.networks = networks;
    }

    public static ArrayList<Integer> getNetworkList() {
        ArrayList<Integer> list = new ArrayList<>();
        for (int i = 0; i < networks.size(); i++) {
            short companyId = (short) networks.keyAt(i);
            SparseArray<String> company = networks.get(companyId);
            for (int j = 0; j < company.size(); j++) {
                short networkId = (short) company.keyAt(j);
                int net = (companyId << 16) & 0xFFFF0000 | networkId & 0x0000FFFF;
                list.add(net);
            }
        }
        return list;
    }

    public static SparseArray<SparseArray<String>> getServers() {
        return servers;
    }

    public static void setServers(SparseArray<SparseArray<String>> servers) {
        Util.servers = servers;
    }

    public static MainUser getMainUser(short companyId, short networkId) {
        return MainUserMap.getInstance().getMainUser(companyId, networkId);
    }

    public static String getServerUrl(short companyId, short networkId) {
        SparseArray<String> company = servers.get(companyId);
        if (company == null) {
            return null;
        }
        return company.get(networkId);
    }

    // Take a generic message and transfer its useful data to the native code:
    // Timestamp, the message id and the SLOT_SIGNATURE_SIZE first bytes of the signature
    public static void attachMessageToTree(GenericMessage genericMessage) {
        ByteBuffer byteBuffer = ByteBuffer.allocate(4 + 4 + getSlotSignatureSize());
        byteBuffer.putInt(genericMessage.getSeconds());
        byteBuffer.putInt(genericMessage.getMessageId());
        // Compress the signature to send it to the drop
        Log.i("Debug", String.valueOf(Util.getHexValue(genericMessage.getSignature())));
        byteBuffer.put(CryptoUtil.compressSignature(genericMessage.getSignature()), 0,
                getSlotSignatureSize());
        Log.i("Java", "Adding message to tree");
        Log.i("Java", Arrays.toString(genericMessage.getSignature()));
        lbAttachMessageToTreeWrapper(byteBuffer.array());
    }

    public static ArrayList<String> getNetworkNamesList() {
        ArrayList<String> networkNamesList = new ArrayList<>();
        for (int i = 0; i < networks.size(); i++) {
            short companyId = (short) networks.keyAt(i);
            SparseArray<String> company = networks.get(companyId);
            for (int j = 0; j < company.size(); j++) {
                short networkId = (short) company.keyAt(j);
                networkNamesList.add(company.get(networkId));
            }
        }
        return networkNamesList;
    }
}
