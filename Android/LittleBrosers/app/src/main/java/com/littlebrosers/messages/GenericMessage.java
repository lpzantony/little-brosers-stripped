package com.littlebrosers.messages;

import com.littlebrosers.users.MainUser;
import com.littlebrosers.users.User;
import com.littlebrosers.utils.CryptoUtil;
import com.littlebrosers.utils.Util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.security.SignatureException;
import java.util.Arrays;

import static java.lang.System.arraycopy;

/**
 * Created by chris on 12/1/17.
 * Define a generic message, which is a direct correspondence with a raw message.
 */

public class GenericMessage extends AbstractMessage {

    private static int counter;

    private Type type;
    private byte[] header;
    private byte[] payload;
    private int messageId;
    private boolean signatureVerified = false;

    public GenericMessage(short companyId, short networkId) {
        super(companyId, networkId);
        this.type = null;
        this.payload = null;
        messageId = ++counter;
    }

    public GenericMessage(short companyId, short networkId, int messageId) {
        super(companyId, networkId);
        this.type = null;
        this.payload = null;
        this.messageId = messageId;
    }

    public GenericMessage(short companyId, short networkId, User fromUser, User toUser, Type type, byte[] payload)
            throws IllegalArgumentException {
        super(companyId, networkId, fromUser, toUser);
        this.type = type;
        messageId = ++counter;

        // Check if this user is the receiver (unicast or broadcast)
        setToClear();

        setPayload(payload);
    }

    public GenericMessage(AbstractMessage msg, Type type, byte[] payload) throws IllegalArgumentException {
        super(msg);
        this.type = type;
        messageId = ++counter;


        // Check if this user is the receiver (unicast or broadcast)
        setToClear();

        setPayload(payload);
    }

    public static int getCounter() {
        return counter;
    }

    // To be called after context restore from database
    public static void setCounter(int newCounter) {
        counter = newCounter;
    }

    private void generateHeader() throws IllegalArgumentException {
        ByteArrayOutputStream msg = new ByteArrayOutputStream();

        // Concatenate fields in a byte[]
        try {
            final int timestampLength = 4;
            // Timestamp
            msg.write(ByteBuffer.allocate(timestampLength).putInt(getSeconds()).array());

            final int idLength = 4;
            // From ID
            msg.write(ByteBuffer.allocate(idLength).putInt(getFromId()).array());

            // To ID
            msg.write(ByteBuffer.allocate(idLength).putInt(getToId()).array());

            // Type
            msg.write(ByteBuffer.allocate(2).putShort(type.getTypeShort()).array());
        } catch (IOException e) {
            throw new IllegalStateException("Unexpected exception in ByteArrayOutputStream write");
        }

        // Test payload size
        if (msg.size() > HEADER_SIZE) {
            throw new IllegalArgumentException("Header is larger than max size");
        }

        header = msg.toByteArray();
    }

    public byte[] getPayload() {
        return payload;
    }

    private void setPayload(byte[] payload) throws IllegalArgumentException {
        // Test payload size
        if (payload.length > MAX_PAYLOAD_SIZE) {
            throw new IllegalArgumentException("Payload is larger than max size");
        }

        this.payload = payload;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public boolean isSignatureVerified() {
        return signatureVerified;
    }

    public int getMessageId() {
        return messageId;
    }

    private void sign() throws SignatureException {
        byte[] message = new byte[header.length + payload.length];
        arraycopy(header, 0, message, 0, header.length);
        arraycopy(payload, 0, message, header.length, payload.length);
        setSignature(CryptoUtil.sign(message, (MainUser) getFromUser()));
    }

    // Transform object into the proper message to send
    public byte[] toRawMessage() throws Exception {
        ByteArrayOutputStream msg = new ByteArrayOutputStream();
        IllegalStateException exception =
                new IllegalStateException("Unexpected exception in ByteArrayOutputStream write");

        generateHeader();
        short rawSize = (short) (2 + AbstractMessage.HEADER_SIZE + payload.length
                + AbstractMessage.SIGNATURE_SIZE);

        msg.write(ByteBuffer.allocate(2).putShort(rawSize).array());

        // Test header size
        if (header.length > HEADER_SIZE) {
            throw new IllegalArgumentException("Header is larger than max size");
        }
        // Add header
        try {
            msg.write(header);
        } catch (IOException e) {
            throw exception;
        }

        // Test payload size
        if (payload.length > MAX_PAYLOAD_SIZE) {
            throw new IllegalArgumentException("Payload is larger than max size");
        }
        // Add payload
        try {
            msg.write(payload);
        } catch (IOException e) {
            throw exception;
        }

        // Sign and compress and add signature and return the entire byte[], only if it has not been
        // signed previously
        byte[] signature;
        if (getSignature() == null) {
            sign();
        }
        signature = CryptoUtil.compressSignature(getSignature());
        try {
            msg.write(signature);
        } catch (IOException e) {
            throw exception;
        }

        return msg.toByteArray();
    }

    /**
     * Build an EncryptedMessage object from a byte[] raw message.
     * <p>
     * Return true if the message can be read.
     */
    public GenericMessage fromRawMessage(byte[] raw) throws IllegalArgumentException {
        if (raw.length > MAX_MESSAGE_SIZE) {
            throw new IllegalArgumentException("Invalid input");
        }

        final int bufferLength = 4;
        ByteArrayInputStream msg = new ByteArrayInputStream(raw);
        byte[] temp = new byte[bufferLength];
        int status;

        // Discard the two size bytes
        status = msg.read(temp, 0, 2);
        if (status != 2) {
            throw new IllegalArgumentException("Error discarding size bytes");
        }
        // Read time
        status = msg.read(temp, 0, bufferLength);
        // Check if valid input
        if (status != bufferLength) {
            throw new IllegalArgumentException("Invalid input");
        }
        setSeconds(ByteBuffer.wrap(temp).getInt());

        // Read from_id
        status = msg.read(temp, 0, bufferLength);
        // Check if valid input
        if (status != bufferLength) {
            throw new IllegalArgumentException("Invalid input");
        }
        setFromId(ByteBuffer.wrap(temp).getInt());

        // Read to_id
        status = msg.read(temp, 0, bufferLength);
        // Check if valid input
        if (status != bufferLength) {
            throw new IllegalArgumentException("Invalid input");
        }
        setToId(ByteBuffer.wrap(temp).getInt());

        // Read message type
        status = msg.read(temp, 0, 2);
        // Check if valid input
        if (status != 2) {
            throw new IllegalArgumentException("Invalid input");
        }
        setType(Type.getType(ByteBuffer.wrap(temp).getShort()));

        // Read what's left of the message as the payload, considering the fixed signature size
        int load = msg.available() - SIGNATURE_SIZE;
        payload = new byte[load];
        status = msg.read(payload, 0, load);
        // Check if valid input
        if (status != load) {
            throw new IllegalArgumentException("Invalid input");
        }

        byte[] tempSignature = new byte[SIGNATURE_SIZE];
        // Read signature
        status = msg.read(tempSignature, 0, SIGNATURE_SIZE);
        // Check if valid input
        if (status != SIGNATURE_SIZE) {
            throw new IllegalArgumentException("Invalid input");
        }
        setSignature(CryptoUtil.decompressSignature(tempSignature));

        byte[] body = Arrays.copyOfRange(raw, 2, raw.length - SIGNATURE_SIZE);

        // Check the signature
        try {
            signatureVerified = CryptoUtil.checkSignature(body, getSignature(), getFromUser());
        } catch (SignatureException e) {
            e.printStackTrace();
        }

        // Check if this user is the receiver (unicast or broadcast)
        setToClear();

        return this;
    }

    public String toString() {
        StringBuilder builder = new StringBuilder("Generic Message:\n");
        builder.append(super.toString());

        builder.append("-Type: ");
        builder.append(type.getTypeShort());
        builder.append('\n');

        builder.append("-Payload: ");
        builder.append(Util.getHexValue(payload));
        builder.append('\n');

        builder.append("-Signature: ");
        if (getSignature() != null) {
            builder.append(Util.getHexValue(getSignature()));
        } else {
            builder.append("null");
        }
        builder.append('\n');
        return builder.toString();
    }

}
