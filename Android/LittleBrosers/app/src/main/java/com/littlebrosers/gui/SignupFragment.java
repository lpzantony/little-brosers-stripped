package com.littlebrosers.gui;

import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.littlebrosers.R;
import com.littlebrosers.users.MainUser;
import com.littlebrosers.users.MainUserMap;
import com.littlebrosers.utils.Util;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class SignupFragment extends Fragment implements View.OnClickListener {

    public static final int MAX_USERNAME_LENGTH = 20;
    public static final int MIN_USERNAME_LENGTH = 3;
    private Spinner networkSpinner;
    private ArrayList<Integer> networks;

    public SignupFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ((MainActivity) getActivity()).setActionBarTitle("Sign Up");
        ((MainActivity) getActivity()).showBackIcon();
        View rootView = inflater.inflate(R.layout.fragment_signup, container, false);
        // Populate the spinner with the networks list
        networks = Util.getNetworkList();
        ArrayList<String> networkNames = Util.getNetworkNamesList();
        if (networks == null) {
            return rootView;
        }

        Button signupButton = rootView.findViewById(R.id.signUpButton);
        signupButton.setOnClickListener(this);

        networkSpinner = rootView.findViewById(R.id.networkSpinner);
        List<String> spinnerArray = new ArrayList<>();

        spinnerArray.addAll(networkNames);
        spinnerArray.add("Select a network");

        // Inspired by https://stackoverflow.com/questions/3574827/how-to-disable-an-item-in-a-spinner
        // And https://stackoverflow.com/questions/867518/how-to-make-an-android-spinner-with-initial-text-select-one
        ArrayAdapter adapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_spinner_dropdown_item, spinnerArray) {
            @Override
            public boolean isEnabled(int position) {
                int networkEntry = networks.get(position);
                short companyId = (short) (networkEntry >> 16);
                short networkId = (short) (networkEntry & 0xFFFF);
                return MainUserMap.getInstance().getMainUser(companyId, networkId) == null;
            }

            // Change color item
            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View mView = super.getDropDownView(position, convertView, parent);
                mView.setVisibility(View.VISIBLE);
                TextView mTextView = (TextView) mView;
                if (isEnabled(position)) {
                    mTextView.setTextColor(Color.BLACK);
                } else {
                    mTextView.setTextColor(Color.GRAY);
                }
                return mView;
            }

            @Override
            public int getCount() {
                return super.getCount() - 1;
            }
        };
        networkSpinner.setAdapter(adapter);
        networkSpinner.setSelection(adapter.getCount());
        // Inflate the layout for this fragment
        return rootView;
    }

    @Override
    public void onClick(View view) {
        Log.i("Signup fragment", "onClick");
        switch (view.getId()) {
            case R.id.signUpButton:
                signUp();
                break;
            default:
                break;
        }
    }

    public void signUp() {
        // Verify username
        String username = ((EditText) getView().findViewById(R.id.usernameEditText)).getText().toString();
        TextInputLayout textInputLayout = getView().findViewById(R.id.usernameTextInputLayout);
        if (username.length() > MAX_USERNAME_LENGTH || username.length() < MIN_USERNAME_LENGTH) {
            textInputLayout.setError(String.format(Locale.US,
                    "Username should consist of %d to %d characters",
                    MIN_USERNAME_LENGTH, MAX_USERNAME_LENGTH));
            return;
        }

        // Verify network selection
        int position = networkSpinner.getSelectedItemPosition();
        if (position >= networks.size()) {
            Toast.makeText(getActivity(), "Please select a network", Toast.LENGTH_SHORT).show();
            return;
        }
        int networkEntry = networks.get(position);

        short companyId = (short) (networkEntry >> 16);
        short networkId = (short) (networkEntry & 0xFFFF);
        // If main user already exists
        if (MainUserMap.getInstance().getMainUser(companyId, networkId) != null) {
            Log.i("Main Activity", "User already exists");
            return;
        }

        Log.i("Main Activity", "Creating user");
        int status = MainUserMap.getInstance().signUp(companyId, networkId, username);
        switch (status) {
            case MainUser.SIGNUP_USERNAME_TAKEN:
                // Display username taken
                textInputLayout.setError("Username taken");
                return;
            case MainUser.SIGNUP_SUCCESS:
                Toast.makeText(getActivity(), "Successfully signed up", Toast.LENGTH_SHORT).show();
                break;
            default:
                Toast.makeText(getActivity(), "An error has occurred", Toast.LENGTH_SHORT).show();
                return;
        }
        // Update side panel
        ((MainActivity) getActivity()).resetDrawerMenu();
        // Display success and return to mainActivity
        getFragmentManager().popBackStack();
    }
}
