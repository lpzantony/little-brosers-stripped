package com.littlebrosers.drops;

import android.util.SparseArray;


public class DropRepository {
    private short companyId;
    private short networkId;
    private SparseArray<Drop> dropList = new SparseArray<>();

    public DropRepository(short companyId, short networkId) {
        this.companyId = companyId;
        this.networkId = networkId;
    }

    public boolean putDrop(Drop drop) {
        if (drop.getCompanyId() != companyId || drop.getNetworkId() != networkId) {
            return false;
        }
        dropList.put(drop.getDropId(), drop);
        return true;
    }

    public short getCompanyId() {
        return companyId;
    }

    public short getNetworkId() {
        return networkId;
    }

    public SparseArray<Drop> getDropList() {
        return dropList;
    }

    // Create a new one, add it to the list and return it or return the already present one
    public Drop addDrop(short dropId) {
        Drop oldDrop = getDrop(dropId);
        if (oldDrop != null) {
            return oldDrop;
        }
        Drop newDrop = new Drop(companyId, networkId, dropId);
        dropList.put(dropId, newDrop);
        return newDrop;
    }

    // Return Drop with specified dropId or create a new one, add it to the list and return it
    public Drop getDrop(short dropId) {
        return dropList.get(dropId);
    }

    // When receiving or generating new messages, all drops become not upToDate
    public void deprecateAll() {
        for (int i = 0; i < dropList.size(); i++) {
            dropList.valueAt(i).deprecate();
        }
    }
}
