package com.littlebrosers.users;

import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;
import android.util.Base64;
import android.util.Log;
import android.util.SparseArray;

import com.littlebrosers.drops.DropRepository;
import com.littlebrosers.messages.Conversation;
import com.littlebrosers.network.DropList;
import com.littlebrosers.network.GetTimeCertificate;
import com.littlebrosers.network.SignUp;
import com.littlebrosers.network.UserList;
import com.littlebrosers.utils.Util;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.UnrecoverableEntryException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.security.spec.ECGenParameterSpec;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutionException;

import static com.littlebrosers.utils.Util.getServerUrl;
import static com.littlebrosers.utils.Util.hexStringToByteArray;

// Main user of the application, one instantiation by network the user belongs to
public class MainUser extends User {
    public static final int SIGNUP_SUCCESS = 0;
    public static final int SIGNUP_USERNAME_TAKEN = 403;
    public static final int SIGNUP_BAD_PUB_KEY = 400;
    public static final int SIGNUP_ERROR = -1;
    // Temporarily hardcoded username
    private String rsaKeyAlias;
    private String ecKeyAlias;
    private KeyStore keyStore;
    private PrivateKey rsaPrivateKey;
    private PrivateKey ecPrivateKey;
    // Sparse array to get User objects from their ids
    private SparseArray<User> contacts;
    private SparseArray<Conversation> conversations;
    // Drop repository associated with this network, and therefore the main user
    private DropRepository repository;
    private Certificate certificate = null;
    private byte[] timeCertificate;
    private int timeReference = 0;

    public MainUser(short companyId, short networkId, int userId, String userName) {
        super(companyId, networkId, userId, userName);
        this.repository = new DropRepository(companyId, networkId);
        this.contacts = new SparseArray<>();
        this.conversations = new SparseArray<>();
        this.rsaKeyAlias = userName + "_rsaKeyAlias";
        this.ecKeyAlias = userName + "_ecKeyAlias";
//        addContact(this);
    }

    public void loadKeys() {
        loadKeyStore();
        generateRsaKeys();
        generateEcKeys();
        loadRsaKeys();
        loadEcKeys();
    }

    // Load the Android KeyStore into the keyStore attribute
    private void loadKeyStore() {
        try {
            // Loading keystore
            keyStore = KeyStore.getInstance("AndroidKeyStore");
            keyStore.load(null);
        } catch (KeyStoreException | NoSuchAlgorithmException | IOException
                | CertificateException e) {
            e.printStackTrace();
            throw new IllegalStateException();
        }
    }

    // Load the rsa keys from the Android KeyStore into the privateKey and publicKey attributes
    private void loadRsaKeys() {
        try {
            KeyStore.PrivateKeyEntry entry =
                    (KeyStore.PrivateKeyEntry) keyStore.getEntry(rsaKeyAlias, null);
            rsaPrivateKey = entry.getPrivateKey();
            setRsaPublicKey(entry.getCertificate().getPublicKey());
        } catch (NoSuchAlgorithmException | UnrecoverableEntryException | KeyStoreException e) {
            e.printStackTrace();
            throw new IllegalStateException();
        }
    }

    private void loadEcKeys() {
        try {
            KeyStore.PrivateKeyEntry entry =
                    (KeyStore.PrivateKeyEntry) keyStore.getEntry(ecKeyAlias, null);
            ecPrivateKey = entry.getPrivateKey();
            setEcPublicKey(entry.getCertificate().getPublicKey());
        } catch (Exception e) {
            e.printStackTrace();
            throw new IllegalStateException("Error getting EC key in KeyStore");
        }
    }

    private void generateRsaKeys() {
        try {
            if (!keyStore.containsAlias(rsaKeyAlias)) {
                KeyPairGenerator keyGen = KeyPairGenerator.getInstance(
                        KeyProperties.KEY_ALGORITHM_RSA, "AndroidKeyStore");

                KeyGenParameterSpec spec = new KeyGenParameterSpec.Builder(rsaKeyAlias,
                        KeyProperties.PURPOSE_DECRYPT | KeyProperties.PURPOSE_ENCRYPT)
                        .setKeySize(2048)
                        .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_RSA_PKCS1)
                        .build();

                keyGen.initialize(spec);
                keyGen.generateKeyPair();
                Log.i("KeyStore", "RSA key generated");
            } else {
                Log.i("KeyStore", "RSA key already exists");
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new IllegalStateException("Error generating RSA keys");
        }
    }

    private void generateEcKeys() {
        try {
            if (!keyStore.containsAlias(ecKeyAlias)) {
                KeyPairGenerator keyGen = KeyPairGenerator.getInstance(KeyProperties.KEY_ALGORITHM_EC, "AndroidKeyStore");

                KeyGenParameterSpec spec = new KeyGenParameterSpec.Builder(ecKeyAlias,
                        KeyProperties.PURPOSE_SIGN | KeyProperties.PURPOSE_VERIFY)
                        .setAlgorithmParameterSpec(new ECGenParameterSpec("secp256r1"))
                        .setDigests(KeyProperties.DIGEST_SHA256)
                        .build();

                keyGen.initialize(spec);
                keyGen.generateKeyPair();
                Log.i("KeyStore", "EC key generated");
            } else {
                Log.i("KeyStore", "EC Key already exists");
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new IllegalStateException("Error generating EC keys");
        }
    }

    public void addContact(User user) {
        if (getCompanyId() == user.getCompanyId()
                && getNetworkId() == user.getNetworkId()) {
            this.contacts.put(user.getUserId(), user);
        } else {
            throw new IllegalStateException("Incompatible contact");
        }
    }

    public SparseArray<User> getContacts() {
        return contacts;
    }

    public void addConversation(Conversation conversation) {
        if (conversation.getMainUser() == this) {
            this.conversations.put(conversation.getContactUser().getUserId(), conversation);
        } else {
            throw new IllegalArgumentException("Incompatible conversation");
        }
    }

    public Conversation getConversation(User user) {
        if (user == null) {
            return null;
        }
        return conversations.get(user.getUserId());
    }

    public SparseArray<Conversation> getConversations() {
        return conversations;
    }

    public ArrayList<Conversation> getConversationsList() {
        ArrayList<Conversation> conversationsList = new ArrayList<>();
        for (int i = 0; i < conversations.size(); i++) {
            if (conversations.valueAt(i).getLastMessage() == null) {
                continue;
            }
            conversationsList.add(conversations.valueAt(i));
        }
        Collections.sort(conversationsList);
        return conversationsList;
    }

    public PrivateKey getRsaPrivateKey() {
        return rsaPrivateKey;
    }

    public PrivateKey getEcPrivateKey() {
        return ecPrivateKey;
    }

    public String toString() {
        return "Main User: " + super.toString();
    }

    public DropRepository getRepository() {
        return repository;
    }

    public void setRepository(DropRepository repository) {
        this.repository = repository;
    }

    public List<User> getContactList() {
        if (contacts == null) {
            return null;
        }
        List<User> arrayList = new ArrayList<>(contacts.size());
        for (int i = 0; i < contacts.size(); i++) {
            arrayList.add(contacts.valueAt(i));
        }
        Collections.sort(arrayList);
        return arrayList;
    }

    public Certificate getCertificate() {
        return certificate;
    }

    public void setCertificate(Certificate certificate) {
        this.certificate = certificate;
    }

    public byte[] getTimeCertificate() {
        return timeCertificate;
    }

    public void setTimeCertificate(byte[] timeCertificate) {
        final int bufferLength = 4;
        ByteArrayInputStream cert = new ByteArrayInputStream(timeCertificate);
        byte[] temp = new byte[bufferLength];
        int status;

        // Read time
        status = cert.read(temp, 0, bufferLength);
        // Check if valid input
        if (status != bufferLength) {
            throw new IllegalArgumentException("Invalid input");
        }
        int newTimeReference = ByteBuffer.wrap(temp).getInt();
        if (newTimeReference < timeReference) {
            return;
        }

        this.timeReference = newTimeReference;
        this.timeCertificate = timeCertificate;
    }

    public int getTimeReference() {
        return timeReference;
    }

    public int signUp() {
        // Encode EC public key as PEM
        String ecKey = "-----BEGIN PUBLIC KEY-----\n" + Base64.encodeToString(
                getEcPublicKey().getEncoded(), Base64.DEFAULT) + "-----END PUBLIC KEY-----";
        // Encode RSA publickey as PEM
        String rsaKey = "-----BEGIN PUBLIC KEY-----\n" + Base64.encodeToString(
                getRsaPublicKey().getEncoded(), Base64.DEFAULT) + "-----END PUBLIC KEY-----";
        // Send data to http post request
        try {
            StringBuffer error = new StringBuffer();
            Certificate cert = new SignUp().execute(Util.getServerUrl(getCompanyId(), getNetworkId()),
                    ecKey, rsaKey, getUserName(), error).get();
            if (cert == null) {
                Log.i("Signup", error.toString());
                return Integer.parseInt(error.toString());
            }
            // Save certificate
            certificate = cert;
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            return -1;
        }
        // If user already registered
        if (getUserId() != 0) {
            return 0;
        }


        // Parse userId from certificate
        String name = ((X509Certificate) certificate).getSubjectX500Principal().getName();
        String[] parts = name.split(",");
        for (String part : parts) {
            String[] subParts = part.split("=");
            if (Objects.equals(subParts[0], "UID")) {
                setUserId(Integer.parseInt(subParts[1]));
            }
        }
        return 0;
    }

    public void loadTimeCertificate() {
        try {
            String timeCert = new GetTimeCertificate().execute(getServerUrl(getCompanyId(),
                    getNetworkId())).get();
            if (timeCert != null) {
                JSONObject obj = new JSONObject(timeCert);
                final int timestampLength = 4;
                byte[] timestampArray = hexStringToByteArray(obj.getString("timestamp"));
                byte[] signatureArray = hexStringToByteArray(obj.getString("signature"));
                timeCertificate = new byte[timestampArray.length + signatureArray.length];
                System.arraycopy(timestampArray, 0, timeCertificate, 0, timestampLength);
                System.arraycopy(signatureArray, 0, timeCertificate, timestampLength,
                        signatureArray.length);

                timeReference = ByteBuffer.wrap(timestampArray).getInt();
            }
        } catch (InterruptedException | ExecutionException | JSONException e) {
            e.printStackTrace();
        }
    }

    public void loadContacts() {
        try {
            new UserList(this).execute(getServerUrl(getCompanyId(), getNetworkId())).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }

    public void loadDrops() {
        try {
            new DropList(repository).execute(getServerUrl(getCompanyId(), getNetworkId())).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }
}
