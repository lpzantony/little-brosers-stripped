package com.littlebrosers.messages;

import com.littlebrosers.users.MainUser;
import com.littlebrosers.utils.CryptoUtil;
import com.littlebrosers.utils.Util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * Created by chris on 11/27/17.
 * Class to define encrypted messages
 */

public class EncryptedTextMessage extends ImplementedMessage {
    public static final AbstractMessage.Type MESSAGE_TYPE = AbstractMessage.Type.ENCRYPTED_TEXT;

    private static final int CIPHERTEXT_SIZE = 256;
    private static final int KEY_HASH_SIZE = 32;

    private byte[] ciphertext;
    private byte[] publicEncryptionKeyHash;

    EncryptedTextMessage(short companyId, short networkId) {
        super(companyId, networkId);
    }

    public EncryptedTextMessage(GenericMessage msg) throws Exception {
        super(msg, MESSAGE_TYPE);
    }

    // Generate EncryptedMessage from ClearMessage
    public EncryptedTextMessage(ClearTextMessage msg) throws IllegalArgumentException {
        super(msg, MESSAGE_TYPE);

        // Encrypt from_id + clear_text
        ByteArrayOutputStream toEncrypt = new ByteArrayOutputStream();

        try {
            final int idLength = 4;
            toEncrypt.write(ByteBuffer.allocate(idLength).putInt(getFromId()).array());
            toEncrypt.write(msg.getCleartext().getBytes());
        } catch (IOException e) {
            throw new IllegalStateException("Unexpected exception in ByteArrayOutputStream write");
        }

        encrypt(toEncrypt.toByteArray());

        // Hash public encryption key
        hashKey();
    }

    private void encrypt(byte[] msg) throws IllegalArgumentException {
        if (!(getFromUser() instanceof MainUser)) {
            throw new IllegalStateException("Trying to encrypt a message not from a MainUser");
        }
        ciphertext = CryptoUtil.encrypt(msg, getToUser());
    }

    private void hashKey() {
        publicEncryptionKeyHash = CryptoUtil.getKeyHash(getToUser());
    }

    // Transform payload into a byte[] raw payload
    @Override
    protected byte[] toRawPayload() throws IllegalStateException {
        ByteArrayOutputStream msg = new ByteArrayOutputStream();

        // Add ciphertext and public encryption key hash
        try {
            msg.write(ciphertext);
            msg.write(publicEncryptionKeyHash);
        } catch (IOException e) {
            throw new IllegalStateException("Unexpected exception in ByteArrayOutputStream write");
        }

        return msg.toByteArray();
    }

    // Build an EncryptedMessage payload from a byte[] raw payload
    @Override
    protected void fromRawPayload(byte[] raw) throws IllegalArgumentException {
        ByteArrayInputStream msg = new ByteArrayInputStream(raw);
        int status;

        // Read ciphertext
        ciphertext = new byte[CIPHERTEXT_SIZE];
        status = msg.read(ciphertext, 0, CIPHERTEXT_SIZE);
        // Check if valid input
        if (status != CIPHERTEXT_SIZE) {
            throw new IllegalArgumentException("Invalid input");
        }

        // Read encryption key hash
        publicEncryptionKeyHash = new byte[KEY_HASH_SIZE];
        status = msg.read(publicEncryptionKeyHash, 0, KEY_HASH_SIZE);

        // Check if valid input
        if (status != KEY_HASH_SIZE) {
            throw new IllegalArgumentException("Invalid input");
        }
    }

    byte[] getCiphertext() {
        return ciphertext;
    }

    byte[] getKeyHash() {
        return publicEncryptionKeyHash;
    }

    @Override
    public String toString() {
        return "Encrypted Text Message\n" + super.toString()
                + "-Ciphertext: "
                + String.valueOf(Util.getHexValue(ciphertext)) + '\n'
                + "-Public key hash: "
                + String.valueOf(Util.getHexValue(publicEncryptionKeyHash)) + '\n';
    }
}
