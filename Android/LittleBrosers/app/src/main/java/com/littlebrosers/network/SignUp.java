package com.littlebrosers.network;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;

import static java.net.HttpURLConnection.HTTP_OK;

public class SignUp extends AsyncTask<Object, Void, Certificate> {
    private static final String SIGNUP_ENDPOINT = "signUp";

    @SuppressLint("DefaultLocale")
    @Override
    protected Certificate doInBackground(Object... data) throws IllegalArgumentException {
        if (data.length != 5) {
            throw new IllegalArgumentException(String.format(
                    "Expecting 5 arguments, %d were given", data.length));
        }
        String urlString = data[0] + SIGNUP_ENDPOINT;
        HttpURLConnection client;
        Certificate response = null;
        ((StringBuffer) data[data.length - 1]).append(-1);
        try {
            URL url = new URL(urlString);
            client = (HttpURLConnection) url.openConnection();
            client.setRequestMethod("POST");
            client.setDoOutput(true);

            OutputStream out = new BufferedOutputStream(client.getOutputStream());
            writeStream(out, data);
            if (client.getResponseCode() != HTTP_OK) {
                ((StringBuffer) data[data.length - 1]).delete(0, 2);
                ((StringBuffer) data[data.length - 1]).append(client.getResponseCode());
                return null;
            }
            CertificateFactory cf = CertificateFactory.getInstance("x509");
            InputStream in = new BufferedInputStream(client.getInputStream());
            response = cf.generateCertificate(in);

            client.disconnect();

        } catch (IOException | CertificateException e) {
            e.printStackTrace();
        }
        return response;
    }

    private void writeStream(OutputStream out, Object[] data) {
        StringBuilder builder = new StringBuilder();
        try {
            builder.append("pubkey=");
            builder.append(URLEncoder.encode((String) data[1], "UTF-8"));
            builder.append("&enc_pubkey=");
            builder.append(URLEncoder.encode((String) data[2], "UTF-8"));
            builder.append("&username=");
            builder.append((String) data[3]);
            out.write(builder.toString().getBytes());
            out.flush();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPostExecute(Certificate result) {
        if (result == null) {
            Log.i("Signup", "Certificate is null");
        }
    }
}
