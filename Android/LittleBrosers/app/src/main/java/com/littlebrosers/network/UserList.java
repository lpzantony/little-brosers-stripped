package com.littlebrosers.network;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.util.Base64;

import com.littlebrosers.users.ContactUser;
import com.littlebrosers.users.MainUser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;

public class UserList extends AsyncTask<String, Void, String> {
    private static final String USER_LIST_ENDPOINT = "users";
    private MainUser mainUser;

    public UserList(MainUser mainUser) {
        this.mainUser = mainUser;
    }

    @SuppressLint("DefaultLocale")
    @Override
    protected String doInBackground(String... data) throws IllegalArgumentException {
        if (data.length != 1) {
            throw new IllegalArgumentException(String.format(
                    "Expecting 1 argument, %d were given", data.length));
        }
        String urlString = data[0] + USER_LIST_ENDPOINT;
        HttpURLConnection client;
        String response = null;
        try {
            URL url = new URL(urlString);
            client = (HttpURLConnection) url.openConnection();
            client.setRequestMethod("GET");


            StringBuilder stringBuilder = new StringBuilder();
            InputStream in = new BufferedInputStream(client.getInputStream());
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            String inputStr;
            while ((inputStr = reader.readLine()) != null) {
                stringBuilder.append(inputStr);
            }
            in.close();
            client.disconnect();
            response = stringBuilder.toString();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    @Override
    protected void onPostExecute(String s) {
        if (s == null) {
            return;
        }

        JSONArray json;
        try {
            json = new JSONArray(s);
            for (int i = 0; i < json.length(); i++) {
                JSONObject userEntry = json.getJSONObject(i);
                int userId = userEntry.getInt("id");

                if (userId == mainUser.getUserId()) {
//                    mainUser.addContact(mainUser);
                    continue;
                }
                String username = userEntry.getString("username");
                String rsaKeyString = userEntry.getString("enc_pubkey");
                String ecKeyString = userEntry.getString("pubkey");

                ecKeyString = ecKeyString.replace("-----BEGIN PUBLIC KEY-----\n", "");
                ecKeyString = ecKeyString.replace("-----END PUBLIC KEY-----", "");
                byte[] ecEncoded = Base64.decode(ecKeyString, Base64.DEFAULT);

                rsaKeyString = rsaKeyString.replace("-----BEGIN PUBLIC KEY-----\n", "");
                rsaKeyString = rsaKeyString.replace("-----END PUBLIC KEY-----", "");
                byte[] rsaEncoded = Base64.decode(rsaKeyString, Base64.DEFAULT);

                PublicKey ecKey = KeyFactory.getInstance("EC").generatePublic(new X509EncodedKeySpec(ecEncoded));
                PublicKey rsaKey = KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(rsaEncoded));
                ContactUser contact = new ContactUser(mainUser, userId, username, rsaKey, ecKey);
                mainUser.addContact(contact);
            }
        } catch (JSONException | NoSuchAlgorithmException | InvalidKeySpecException e) {
            e.printStackTrace();
        }


    }
}
