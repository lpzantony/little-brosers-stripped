package com.littlebrosers.gui;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.littlebrosers.R;
import com.littlebrosers.messages.ClearTextMessage;
import com.littlebrosers.users.MainUser;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

// This has been built based on the tutorial found here:
// https://blog.sendbird.com/android-chat-tutorial-building-a-messaging-ui
public class MessageListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int VIEW_TYPE_MESSAGE_SENT = 1;
    private static final int VIEW_TYPE_MESSAGE_RECEIVED = 2;

    private List<ClearTextMessage> messageList;

    MessageListAdapter(List<ClearTextMessage> messageList) {
        this.messageList = messageList;
    }

    @Override
    public int getItemCount() {
        return messageList.size();
    }

    // Determines the appropriate ViewType according to the sender of the message.
    @Override
    public int getItemViewType(int position) {
        ClearTextMessage message = messageList.get(position);

        if (message.getFromUser() instanceof MainUser) {
            // If the current user is the sender of the message
            return VIEW_TYPE_MESSAGE_SENT;
        } else {
            // If some other user sent the message
            return VIEW_TYPE_MESSAGE_RECEIVED;
        }
    }

    // Inflates the appropriate layout according to the ViewType.
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;

        if (viewType == VIEW_TYPE_MESSAGE_SENT) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_message_sent, parent, false);
            return new SentMessageHolder(view);
        } else if (viewType == VIEW_TYPE_MESSAGE_RECEIVED) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_message_received, parent, false);
            return new ReceivedMessageHolder(view);
        }

        return null;
    }

    // Passes the message object to a ViewHolder so that the contents can be bound to UI.
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ClearTextMessage message = messageList.get(position);

        switch (holder.getItemViewType()) {
            case VIEW_TYPE_MESSAGE_SENT:
                ((SentMessageHolder) holder).bind(message);
                break;
            case VIEW_TYPE_MESSAGE_RECEIVED:
                ((ReceivedMessageHolder) holder).bind(message);
                break;
            default:
                throw new IllegalStateException("Trying to bind unknown message type to holder");
        }
    }

    private class SentMessageHolder extends RecyclerView.ViewHolder {
        private TextView messageText, timeText;

        SentMessageHolder(View itemView) {
            super(itemView);

            messageText = itemView.findViewById(R.id.text_message_body);
            timeText = itemView.findViewById(R.id.text_message_time);
        }

        void bind(ClearTextMessage message) {
            messageText.setText(message.getCleartext());
            // Convert time from GMT to local timezone
            Calendar calendar = new GregorianCalendar();
            calendar.setTimeInMillis(message.getTimestamp().getTimeInMillis());

            String time = String.format(Locale.US, "%02d", calendar.get(Calendar.HOUR_OF_DAY))
                    + ":" + String.format(Locale.US, "%02d", calendar.get(Calendar.MINUTE));

            // Format the stored timestamp into a readable String using method.
            timeText.setText(time);
        }
    }

    private class ReceivedMessageHolder extends RecyclerView.ViewHolder {
        private TextView messageText, timeText, nameText;

        ReceivedMessageHolder(View itemView) {
            super(itemView);

            messageText = itemView.findViewById(R.id.text_message_body);
            timeText = itemView.findViewById(R.id.text_message_time);
            nameText = itemView.findViewById(R.id.text_message_name);
        }

        void bind(ClearTextMessage message) {
            messageText.setText(message.getCleartext());
            // Convert time from GMT to local timezone
            Calendar calendar = new GregorianCalendar();
            calendar.setTimeInMillis(message.getTimestamp().getTimeInMillis());

            String time = String.format(Locale.US, "%02d", calendar.get(Calendar.HOUR_OF_DAY))
                    + ":" + String.format(Locale.US, "%02d", calendar.get(Calendar.MINUTE));

            // Format the stored timestamp into a readable String using method.
            timeText.setText(time);

            nameText.setText(message.getFromUser().getUserName());
        }
    }
}
