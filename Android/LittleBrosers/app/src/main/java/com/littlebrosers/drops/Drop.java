package com.littlebrosers.drops;

// TODO drop public key

import android.annotation.SuppressLint;

import com.littlebrosers.utils.Util;

import java.io.Serializable;
import java.security.PublicKey;

public class Drop implements Serializable {
    private short dropId;
    private short networkId;
    private short companyId;
    private int counter = 0;
    private boolean upToDate = false;
    private int tempCounter = 0;
    private PublicKey publicKey;

    public Drop(short companyId, short networkId, short dropId) {
        this.companyId = companyId;
        this.networkId = networkId;
        this.dropId = dropId;
    }

    public boolean shouldConnect(int counter) {
        tempCounter = counter;
        return (!upToDate || (counter != this.counter));
    }

    // newGiven should be set to true if the drop received new messages from the us
    public void update(boolean newGiven) {
        if (newGiven) {
            ++tempCounter;
        }
        this.counter = tempCounter;
        upToDate = true;
    }

    public short getDropId() {
        return dropId;
    }

    public short getNetworkId() {
        return networkId;
    }

    public short getCompanyId() {
        return companyId;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public boolean isUpToDate() {
        return upToDate;
    }

    public void setUpToDate(boolean upToDate) {
        this.upToDate = upToDate;
    }

    public void deprecate() {
        this.upToDate = false;
    }

    public long getUuidLeastSigBits() {
        long result = ((long) networkId & 0xFFFF) << 48;
        result |= ((long) companyId & 0xFFFF) << 32;
        result |= Util.LITTLE_BROSERS_ID;
        return result;
    }

    public PublicKey getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(PublicKey publicKey) {
        this.publicKey = publicKey;
    }

    @SuppressLint("DefaultLocale")
    @Override
    public String toString() {
        return String.format("%s%s", String.format("Drop\nCompany ID: 0x%x\nNetwork ID: 0x%x\n"
                        + "Drop ID: 0x%x\n", companyId, networkId, dropId),
                String.format("Counter: %d\nUp to Date: %b", counter, upToDate));
    }
}
