package com.littlebrosers.utils;


import com.littlebrosers.users.MainUser;
import com.littlebrosers.users.MainUserMap;
import com.littlebrosers.users.User;

import java.io.ByteArrayInputStream;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.util.Arrays;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import static java.lang.System.arraycopy;


/**
 * Created by chris on 11/29/17.
 * Class to handle all crypto and ID related functions, giving the least amount of access
 * from the outside
 */

public final class CryptoUtil {
    private static final String ENCRYPTION_ALGORITHM = "RSA";
    private static final String ENCRYPTION_ALGORITHM_2 = ENCRYPTION_ALGORITHM + "/ECB/PKCS1Padding";
    private static final String SIGNATURE_ALGORITHM = "ECDSA";
    private static final String SIGNATURE_ALGORITHM_2 = "SHA256with" + SIGNATURE_ALGORITHM;
    private static final String SIGNATURE_CURVE = "secp256r1";
    private static final byte SIGNATURE_LENGTH = 64;
    private static final String PROVIDER = "AndroidKeyStoreBCWorkaround";

    private CryptoUtil() {
        // Never called;
    }

    private static byte[] hash(byte[] msg) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            return digest.digest(msg);
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalStateException("Algorithm not found. Global error");
        }
    }

    // Check if the message signature is valid
    public static boolean checkSignature(byte[] data, byte[] signature, PublicKey publicKey)
            throws SignatureException {
        try {
            Signature sig = Signature.getInstance(SIGNATURE_ALGORITHM_2);
            sig.initVerify(publicKey);
            sig.update(data);
            return sig.verify(signature);
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalStateException("Algorithm not found. Global error");
        } catch (InvalidKeyException e) {
            throw new IllegalArgumentException("Invalid Key");
        }
    }

    // Generate the signature of the message
    private static byte[] sign(byte[] data, PrivateKey privateKey) throws SignatureException {
        try {
            Signature sig = Signature.getInstance(SIGNATURE_ALGORITHM_2);
            sig.initSign(privateKey);
            sig.update(data);
            return sig.sign();
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalStateException("Algorithm not found. Global error");
        } catch (Exception e) {
            e.printStackTrace();
            throw new IllegalArgumentException("Invalid Key");
        }
    }

    // Encrypt clear_data
    private static byte[] encrypt(byte[] clearData, PublicKey publicKey)
            throws IllegalArgumentException {
        try {
            Cipher cipher = Cipher.getInstance(ENCRYPTION_ALGORITHM_2);
            cipher.init(Cipher.ENCRYPT_MODE, publicKey);
            return cipher.doFinal(clearData);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            throw new IllegalStateException("Algorithm not found. Global error");
        } catch (InvalidKeyException e) {
            throw new IllegalArgumentException("Invalid Key");
        } catch (BadPaddingException | IllegalBlockSizeException e) {
            throw new IllegalArgumentException("Invalid input");
        }
    }

    // Decrypt cipherData
    private static byte[] decrypt(byte[] cipherData, PrivateKey privateKey)
            throws IllegalArgumentException {
        try {
            Cipher cipher = Cipher.getInstance(ENCRYPTION_ALGORITHM_2);
            cipher.init(Cipher.DECRYPT_MODE, privateKey);
            return cipher.doFinal(cipherData);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException
                e) {
            throw new IllegalStateException("Algorithm not found. Global error");
        } catch (InvalidKeyException e) {
            e.printStackTrace();
            throw new IllegalArgumentException("Invalid Key");
        } catch (BadPaddingException | IllegalBlockSizeException e) {
            e.printStackTrace();
            throw new IllegalArgumentException("Invalid input");
        }
    }

    public static boolean checkSignature(byte[] data, byte[] signature, User user)
            throws SignatureException {
        return checkSignature(data, signature, user.getEcPublicKey());
    }

    public static byte[] sign(byte[] data, MainUser mainUser) throws SignatureException {
        return sign(data, mainUser.getEcPrivateKey());
    }

    public static byte[] encrypt(byte[] clearData, User toUser) throws IllegalArgumentException {
        return encrypt(clearData, toUser.getRsaPublicKey());
    }

    public static byte[] decrypt(byte[] cipherData, byte[] keyHash, short companyId, short networkId)
            throws IllegalArgumentException {
        // Check key hash before decrypting
        MainUser mainUser = MainUserMap.getInstance().getMainUser(companyId, networkId);
        if (mainUser == null) {
            throw new IllegalArgumentException("No MainUser for this network");
        }
        if (!Arrays.equals(hash(mainUser.getRsaPublicKey().getEncoded()), keyHash)) {
            throw new IllegalArgumentException("Keys do not match");
        }
        return decrypt(cipherData, mainUser.getRsaPrivateKey());
    }

    public static byte[] getKeyHash(User user) {
        return hash(user.getRsaPublicKey().getEncoded());
    }

    public static byte[] compressSignature(byte[] signature) {
        byte size = SIGNATURE_LENGTH;
        byte[] result = new byte[size];
        ByteArrayInputStream sig = new ByteArrayInputStream(signature);
        long status;

        // Construct proper r and s integers
        int b2 = signature[3];
        if (b2 == size / 2) {
            // Skip the header
            status = sig.skip(4);
            // Check if valid input
            if (status != 4) {
                throw new IllegalArgumentException("Invalid input");
            }

        } else if (b2 == size / 2 + 1) {
            // Skip the header + first 0 byte
            status = sig.skip(5);
            // Check if valid input
            if (status != 5) {
                throw new IllegalArgumentException("Invalid input");
            }
        }

        // Parse size / 2 bytes for r
        status = sig.read(result, 0, size / 2);
        // Check if valid input
        if (status != size / 2) {
            throw new IllegalArgumentException("Invalid input");
        }

        int b3 = signature[4 + b2 + 1];
        if (b3 == size / 2) {
            // Skip 0x02 + b3
            status = sig.skip(2);
            // Check if valid input
            if (status != 2) {
                throw new IllegalArgumentException("Invalid input");
            }

        } else if (b3 == size / 2 + 1) {
            // Skip 0x02 + b3 + first 0 byte
            status = sig.skip(3);
            // Check if valid input
            if (status != 3) {
                throw new IllegalArgumentException("Invalid input");
            }
        }

        // Parse size / 2 bytes for s
        status = sig.read(result, size / 2, size / 2);
        // Check if valid input
        if (status != size / 2) {
            throw new IllegalArgumentException("Invalid input");
        }

        return result;
    }

    public static byte[] decompressSignature(byte[] compressed) {
        byte size = SIGNATURE_LENGTH;
        byte b2 = (byte) (size / 2);
        byte b3 = (byte) (size / 2);

        // Test if leading 0x00 is needed for r and s
        if (compressed[0] < 0) {
            b2++;
        }
        if (compressed[size / 2] < 0) {
            b3++;
        }

        byte b1 = (byte) (4 + b2 + b3);

        byte[] result = new byte[b1 + 2];

        // Fill resulting signature
        result[0] = 0x30;
        result[1] = b1;
        result[2] = 0x02;
        result[3] = b2;
        result[4 + b2] = 0x02;
        result[4 + b2 + 1] = b3;

        int cursor = 4;

        // If needed add leading 0x00 and move to the next byte
        if (b2 > size / 2) {
            result[cursor++] = 0x00;
        }

        // Copy r
        arraycopy(compressed, 0, result, cursor, size / 2);

        cursor = 4 + b2 + 2;

        // If needed add leading 0x00 and move to the next byte
        if (b3 > size / 2) {
            result[cursor++] = 0x00;
        }

        // Copy r
        arraycopy(compressed, size / 2, result, cursor, size / 2);


        return result;
    }

    public static void invertSignature(byte[] signature) {
        if (signature.length != SIGNATURE_LENGTH) {
            throw new IllegalArgumentException("Invalid Signature");
        }
        byte temp;
        int halfLength = SIGNATURE_LENGTH / 2;
        for (int i = 0; i < halfLength / 2; i++) {
            temp = signature[i];
            signature[i] = signature[halfLength - 1 - i];
            signature[halfLength - 1 - i] = temp;

            temp = signature[i + halfLength];
            signature[i + halfLength] = signature[halfLength - 1 - i + halfLength];
            signature[halfLength - 1 - i + halfLength] = temp;
        }
    }
}
