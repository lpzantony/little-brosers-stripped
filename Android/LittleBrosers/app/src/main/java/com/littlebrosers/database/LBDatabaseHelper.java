package com.littlebrosers.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.littlebrosers.utils.Util;

public class LBDatabaseHelper extends SQLiteOpenHelper {
    // If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 7;
    public static final String DATABASE_NAME = "LB.db";

    private static final String SQL_CREATE_NETWORK_ENTRIES =
            "CREATE TABLE IF NOT EXISTS " + LBDatabaseContract.NetworkEntry.TABLE_NAME + " ("
                    + LBDatabaseContract.NetworkEntry.COLUMN_NAME_COMPANY + " INTEGER,"
                    + LBDatabaseContract.NetworkEntry.COLUMN_NAME_NETWORK + " INTEGER,"
                    + LBDatabaseContract.NetworkEntry.COLUMN_NAME_NETWORK_NAME + " TEXT, "
                    + LBDatabaseContract.NetworkEntry.COLUMN_NAME_SERVER_URL + " TEXT, "
                    + "PRIMARY KEY ("
                    + LBDatabaseContract.NetworkEntry.COLUMN_NAME_COMPANY + ", "
                    + LBDatabaseContract.NetworkEntry.COLUMN_NAME_NETWORK
                    + ")"
                    + ")";

    private static final String SQL_DELETE_NETWORK_ENTRIES =
            "DROP TABLE IF EXISTS " + LBDatabaseContract.NetworkEntry.TABLE_NAME;

    private static final String SQL_CREATE_MAIN_USER_ENTRIES =
            "CREATE TABLE IF NOT EXISTS " + LBDatabaseContract.MainUserEntry.TABLE_NAME + " ("
                    + LBDatabaseContract.MainUserEntry.COLUMN_NAME_COMPANY + " INTEGER,"
                    + LBDatabaseContract.MainUserEntry.COLUMN_NAME_NETWORK + " INTEGER,"
                    + LBDatabaseContract.MainUserEntry.COLUMN_NAME_MAIN_USER_ID + " INTEGER,"
                    + LBDatabaseContract.MainUserEntry.COLUMN_NAME_MAIN_USER_NAME + " TEXT,"
                    + LBDatabaseContract.MainUserEntry.COLUMN_NAME_MAIN_USER_TIME_CERTIFICATE + " TEXT,"
                    + LBDatabaseContract.MainUserEntry.COLUMN_NAME_MAIN_USER_CERTIFICATE + " TEXT,"
                    + "PRIMARY KEY ("
                    + LBDatabaseContract.MainUserEntry.COLUMN_NAME_COMPANY + ", "
                    + LBDatabaseContract.MainUserEntry.COLUMN_NAME_NETWORK
                    + ")"
                    + ")";

    private static final String SQL_DELETE_MAIN_USER_ENTRIES =
            "DROP TABLE IF EXISTS " + LBDatabaseContract.MainUserEntry.TABLE_NAME;

    private static final String SQL_CREATE_DROP_ENTRIES =
            "CREATE TABLE IF NOT EXISTS " + LBDatabaseContract.DropEntry.TABLE_NAME + " ("
                    + LBDatabaseContract.DropEntry.COLUMN_NAME_COMPANY + " INTEGER,"
                    + LBDatabaseContract.DropEntry.COLUMN_NAME_NETWORK + " INTEGER,"
                    + LBDatabaseContract.DropEntry.COLUMN_NAME_DROP_ID + " INTEGER,"
                    + LBDatabaseContract.DropEntry.COLUMN_NAME_DROP_COUNTER + " INTEGER,"
                    + LBDatabaseContract.DropEntry.COLUMN_NAME_DROP_UPTODATE + " BOOLEAN,"
                    + LBDatabaseContract.DropEntry.COLUMN_NAME_DROP_PUB_KEY + " TEXT, "
                    + "PRIMARY KEY ("
                    + LBDatabaseContract.DropEntry.COLUMN_NAME_COMPANY + ", "
                    + LBDatabaseContract.DropEntry.COLUMN_NAME_NETWORK + ", "
                    + LBDatabaseContract.DropEntry.COLUMN_NAME_DROP_ID
                    + ")"
                    + ")";

    private static final String SQL_DELETE_DROP_ENTRIES =
            "DROP TABLE IF EXISTS " + LBDatabaseContract.DropEntry.TABLE_NAME;

    private static final String SQL_CREATE_USER_ENTRIES =
            "CREATE TABLE IF NOT EXISTS " + LBDatabaseContract.UserEntry.TABLE_NAME + " ("
                    + LBDatabaseContract.UserEntry.COLUMN_NAME_COMPANY + " INTEGER,"
                    + LBDatabaseContract.UserEntry.COLUMN_NAME_NETWORK + " INTEGER,"
                    + LBDatabaseContract.UserEntry.COLUMN_NAME_CONTACT_USER_ID + " INTEGER,"
                    + LBDatabaseContract.UserEntry.COLUMN_NAME_CONTACT_USER_NAME + " TEXT,"
                    + LBDatabaseContract.UserEntry.COLUMN_NAME_USER_RSA_PUB_KEY + " TEXT,"
                    + LBDatabaseContract.UserEntry.COLUMN_NAME_USER_EC_PUB_KEY + " TEXT, "
                    + "PRIMARY KEY ("
                    + LBDatabaseContract.UserEntry.COLUMN_NAME_COMPANY + ", "
                    + LBDatabaseContract.UserEntry.COLUMN_NAME_NETWORK + ", "
                    + LBDatabaseContract.UserEntry.COLUMN_NAME_CONTACT_USER_ID
                    + ")"
                    + ")";

    private static final String SQL_DELETE_USER_ENTRIES =
            "DROP TABLE IF EXISTS " + LBDatabaseContract.UserEntry.TABLE_NAME;

    private static final String SQL_CREATE_MESSAGE_ENTRIES =
            "CREATE TABLE IF NOT EXISTS " + LBDatabaseContract.MessageEntry.TABLE_NAME + " ("
                    + LBDatabaseContract.MessageEntry.COLUMN_NAME_MESSAGE_ID + " INTEGER PRIMARY KEY,"
                    + LBDatabaseContract.MessageEntry.COLUMN_NAME_TIMESTAMP + " INTEGER,"
                    + LBDatabaseContract.MessageEntry.COLUMN_NAME_FROM_ID + " INTEGER,"
                    + LBDatabaseContract.MessageEntry.COLUMN_NAME_TO_ID + " INTEGER,"
                    + LBDatabaseContract.MessageEntry.COLUMN_NAME_TEXT + " TEXT"
                    + ")";

    private static final String SQL_DELETE_MESSAGE_ENTRIES =
            "DROP TABLE IF EXISTS " + LBDatabaseContract.MessageEntry.TABLE_NAME;

    private static final String SQL_CREATE_CONVERSATION_ENTRIES =
            "CREATE TABLE IF NOT EXISTS " + LBDatabaseContract.ConversationEntry.TABLE_NAME + " ("
                    + LBDatabaseContract.ConversationEntry.COLUMN_NAME_COMPANY + " INTEGER,"
                    + LBDatabaseContract.ConversationEntry.COLUMN_NAME_NETWORK + " INTEGER,"
                    + LBDatabaseContract.ConversationEntry.COLUMN_NAME_CONTACT_USER_ID + " INTEGER,"
                    + LBDatabaseContract.ConversationEntry.COLUMN_NAME_CONVERSATION_ID + " INTEGER,"
                    + "PRIMARY KEY ("
                    + LBDatabaseContract.ConversationEntry.COLUMN_NAME_COMPANY + ", "
                    + LBDatabaseContract.ConversationEntry.COLUMN_NAME_NETWORK + ", "
                    + LBDatabaseContract.ConversationEntry.COLUMN_NAME_CONTACT_USER_ID
                    + ")"
                    + ")";

    private static final String SQL_DELETE_CONVERSATION_ENTRIES =
            "DROP TABLE IF EXISTS " + LBDatabaseContract.ConversationEntry.TABLE_NAME;

    private static final String SQL_CREATE_CONVERSATION_MAP_ENTRIES =
            "CREATE TABLE IF NOT EXISTS " + LBDatabaseContract.ConversationMapEntry.TABLE_NAME + " ("
                    + LBDatabaseContract.ConversationMapEntry.COLUMN_NAME_MESSAGE_ID + " INTEGER PRIMARY KEY,"
                    + LBDatabaseContract.ConversationMapEntry.COLUMN_NAME_CONVERSATION_ID + " INTEGER"
                    + ")";

    private static final String SQL_DELETE_CONVERSATION_MAP_ENTRIES =
            "DROP TABLE IF EXISTS " + LBDatabaseContract.ConversationMapEntry.TABLE_NAME;

    private static final String SQL_CREATE_RAW_MESSAGE_ENTRIES =
            "CREATE TABLE IF NOT EXISTS " + LBDatabaseContract.RawMessageEntry.TABLE_NAME + " ("
                    + LBDatabaseContract.RawMessageEntry.COLUMN_NAME_RAW_MESSAGE_ID + " INTEGER PRIMARY KEY,"
                    + LBDatabaseContract.RawMessageEntry.COLUMN_NAME_PAYLOAD + " BLOB"
                    + ")";

    private static final String SQL_DELETE_RAW_MESSAGE_ENTRIES =
            "DROP TABLE IF EXISTS " + LBDatabaseContract.RawMessageEntry.TABLE_NAME;


    public LBDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        Log.i("Db", "onCreate");
        clearDatabase(sqLiteDatabase);
        initDatabase(sqLiteDatabase);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        Log.i("Db", "onUpgrade");
        clearDatabase(sqLiteDatabase);
        onCreate(sqLiteDatabase);
    }

    public void clearDatabase(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SQL_DELETE_NETWORK_ENTRIES);
        sqLiteDatabase.execSQL(SQL_DELETE_MAIN_USER_ENTRIES);
        sqLiteDatabase.execSQL(SQL_DELETE_DROP_ENTRIES);
        sqLiteDatabase.execSQL(SQL_DELETE_USER_ENTRIES);
        sqLiteDatabase.execSQL(SQL_DELETE_MESSAGE_ENTRIES);
        sqLiteDatabase.execSQL(SQL_DELETE_CONVERSATION_ENTRIES);
        sqLiteDatabase.execSQL(SQL_DELETE_CONVERSATION_MAP_ENTRIES);

        sqLiteDatabase.execSQL(SQL_CREATE_NETWORK_ENTRIES);
        sqLiteDatabase.execSQL(SQL_CREATE_MAIN_USER_ENTRIES);
        sqLiteDatabase.execSQL(SQL_CREATE_DROP_ENTRIES);
        sqLiteDatabase.execSQL(SQL_CREATE_USER_ENTRIES);
        sqLiteDatabase.execSQL(SQL_CREATE_MESSAGE_ENTRIES);
        sqLiteDatabase.execSQL(SQL_CREATE_CONVERSATION_ENTRIES);
        sqLiteDatabase.execSQL(SQL_CREATE_CONVERSATION_MAP_ENTRIES);
        sqLiteDatabase.execSQL(SQL_CREATE_RAW_MESSAGE_ENTRIES);
    }

    public void initDatabase(SQLiteDatabase sqLiteDatabase) {
        ContentValues values = new ContentValues();
        values.put(LBDatabaseContract.NetworkEntry.COLUMN_NAME_COMPANY, 0xDEAD);
        values.put(LBDatabaseContract.NetworkEntry.COLUMN_NAME_NETWORK, 0xBEEF);
        values.put(LBDatabaseContract.NetworkEntry.COLUMN_NAME_NETWORK_NAME, "Télécom ParisTech - ROSE 2018");
        values.put(LBDatabaseContract.NetworkEntry.COLUMN_NAME_SERVER_URL, Util.SERVER_URL);
        long err = sqLiteDatabase.insert(LBDatabaseContract.NetworkEntry.TABLE_NAME, null, values);
        Log.i("Insert", String.valueOf(err));
    }

    public void clearRawMessages(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SQL_DELETE_RAW_MESSAGE_ENTRIES);
    }
}
