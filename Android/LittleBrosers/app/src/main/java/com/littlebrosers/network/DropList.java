package com.littlebrosers.network;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.util.Base64;

import com.littlebrosers.drops.Drop;
import com.littlebrosers.drops.DropRepository;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;

public class DropList extends AsyncTask<String, Void, String> {
    private static final String DROP_LIST_ENDPOINT = "drops";
    private DropRepository dropRepository;

    public DropList(DropRepository dropRepository) {
        this.dropRepository = dropRepository;
    }

    @SuppressLint("DefaultLocale")
    @Override
    protected String doInBackground(String... data) throws IllegalArgumentException {
        if (data.length != 1) {
            throw new IllegalArgumentException(String.format(
                    "Expecting 1 argument, %d were given", data.length));
        }
        String urlString = data[0] + DROP_LIST_ENDPOINT;
        HttpURLConnection client;
        String response = null;
        try {
            URL url = new URL(urlString);
            client = (HttpURLConnection) url.openConnection();
            client.setRequestMethod("GET");


            StringBuilder stringBuilder = new StringBuilder();
            InputStream in = new BufferedInputStream(client.getInputStream());
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            String inputStr;
            while ((inputStr = reader.readLine()) != null) {
                stringBuilder.append(inputStr);
            }
            in.close();
            client.disconnect();
            response = stringBuilder.toString();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    @Override
    protected void onPostExecute(String s) {
        if (s == null) {
            return;
        }

        JSONArray json;
        try {
            json = new JSONArray(s);
            for (int i = 0; i < json.length(); i++) {
                JSONObject dropEntry = json.getJSONObject(i);
                short dropId = (short) dropEntry.getInt("id");
                String dropPub = dropEntry.getString("pub_key");
                Drop drop = dropRepository.addDrop(dropId);

                dropPub = dropPub.replace("-----BEGIN PUBLIC KEY-----\n", "");
                dropPub = dropPub.replace("-----END PUBLIC KEY-----", "");
                byte[] encoded = Base64.decode(dropPub, Base64.DEFAULT);
                drop.setPublicKey(KeyFactory.getInstance("EC").generatePublic(new X509EncodedKeySpec(encoded)));
            }
        } catch (JSONException | NoSuchAlgorithmException | InvalidKeySpecException e) {
            e.printStackTrace();
        }


    }
}
