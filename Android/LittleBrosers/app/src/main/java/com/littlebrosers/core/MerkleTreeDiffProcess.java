package com.littlebrosers.core;

import com.littlebrosers.utils.Util;

public final class MerkleTreeDiffProcess {
    public static final int DIFF_SERVICE_ID = 0x2000;

    public static final long DIFF_SERVICE_UUID_MOST_SIG = 0x2000L << 32 | Util.UUID_MOST_SIG_CONST;
    public static final long DIFF_STATUS_UUID_MOST_SIG = (0x01L & 0xFF) << 32 | DIFF_SERVICE_UUID_MOST_SIG;
    public static final long DIFF_PAYLOAD_UUID_MOST_SIG = (0x02L & 0xFF) << 32 | DIFF_SERVICE_UUID_MOST_SIG;

    public static final int DIFF_START_ACK_CODE = 0x04;
    public static final int DIFF_MID_ACK_CODE = 0x05;
    public static final int DIFF_END_ACK_CODE = 0x06;

    private MerkleTreeDiffProcess() {

    }
}
