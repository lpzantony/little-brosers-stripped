package com.littlebrosers.gui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.littlebrosers.R;

public class MainFragment extends Fragment {

    public MainFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ((MainActivity) getActivity()).setActionBarTitle("Little Brosers");
        ((MainActivity) getActivity()).showHamburgerIcon();
        return inflater.inflate(R.layout.frament_main, container, false);
    }
}
