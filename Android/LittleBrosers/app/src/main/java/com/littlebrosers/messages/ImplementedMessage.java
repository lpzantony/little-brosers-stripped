package com.littlebrosers.messages;

/**
 * Created by chris on 12/2/17.
 * Base class for all the implemented message types
 */

abstract class ImplementedMessage extends AbstractMessage {
    private Type type = null;

    ImplementedMessage(short companyId, short networkId) {
        super(companyId, networkId);
    }

    ImplementedMessage(AbstractMessage msg, Type type) {
        super(msg);
        this.type = type;
    }

    ImplementedMessage(GenericMessage msg, Type type) throws Exception {
        super(msg);
        this.type = type;

        if (msg.getType() != type) {
            throw new IllegalArgumentException("Invalid GenericMessage type");
        }

        if (!msg.isSignatureVerified()) {
            throw new IllegalArgumentException("Message signature invalid");
        }
        fromRawPayload(msg.getPayload());
    }

    protected Type getType() {
        return type;
    }

    // Transform payload into a byte[] raw payload
    protected abstract byte[] toRawPayload();

    // Build an EncryptedMessage payload from a byte[] raw payload
    protected abstract void fromRawPayload(byte[] raw);

    public GenericMessage toGenericMessage() {
        return new GenericMessage(this, type, toRawPayload());
    }
}
