package com.littlebrosers.users;

import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;

// HashMap extension to efficiently match the MainUser objects with the companyIds and networkIds
public final class MainUserMap extends HashMap<Short, HashMap<Short, MainUser>> {
    private MainUserMap() {
    }

    public static MainUserMap getInstance() {
        return MainUserMapHolder.instance;
    }

    public int signUp(short companyId, short networkId, String username) {
        MainUser mainUser = new MainUser(companyId, networkId, 0, username);
        mainUser.loadKeys();
        int status = mainUser.signUp();
        if (status == MainUser.SIGNUP_SUCCESS) {
            setMainUser(mainUser);
            updateWithServer(companyId, networkId);
            Log.i("User Signup", mainUser.toString());
        }
        return status;
    }

    public void updateWithServer(short companyId, short networkId) {
        MainUser mainUser = getMainUser(companyId, networkId);
        if (mainUser == null) {
            return;
        }

        mainUser.loadTimeCertificate();
        mainUser.loadContacts();
        mainUser.loadDrops();
    }

    public void updateWithServer() {
        for (Short companyId : this.keySet()) {
            for (Short networkId : this.get(companyId).keySet()) {
                updateWithServer(companyId, networkId);
            }
        }
    }

    public void setMainUser(MainUser mainUser) {
        // Look for the company map
        HashMap<Short, MainUser> companyMap = this.get(mainUser.getCompanyId());
        if (companyMap == null) {
            // Add the company to the network map
            companyMap = new HashMap<>();
            this.put(mainUser.getCompanyId(), companyMap);
        }
        companyMap.put(mainUser.getNetworkId(), mainUser);
    }

    // Returns null if there is no userId matching the companyId and/or networkId
    public MainUser getMainUser(short companyId, short networkId) {
        HashMap<Short, MainUser> companyMap = this.get(companyId);
        if (companyMap == null) {
            return null;
        }
        return companyMap.get(networkId);
    }

    public void deleteMainUser(short companyId, short networkId) {
        HashMap<Short, MainUser> companyMap = this.get(companyId);
        if (companyMap == null) {
            return;
        }
        companyMap.remove(networkId);
    }

    public ArrayList<MainUser> getMainUsers() {
        ArrayList<MainUser> mainUsers = new ArrayList<>();
        for (Short companyId : this.keySet()) {
            for (Short networkId : this.get(companyId).keySet()) {
                mainUsers.add(this.get(companyId).get(networkId));
            }
        }
        return mainUsers;
    }

    private static class MainUserMapHolder {
        private static MainUserMap instance = new MainUserMap();
    }
}
