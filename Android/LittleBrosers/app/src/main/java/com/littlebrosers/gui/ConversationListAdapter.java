package com.littlebrosers.gui;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.littlebrosers.R;
import com.littlebrosers.messages.Conversation;
import com.littlebrosers.users.User;

import java.util.List;

public class ConversationListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<Conversation> convList;

    ConversationListAdapter(List<Conversation> convList) {
        this.convList = convList;
    }

    @Override
    public int getItemCount() {
        return convList.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;

        view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_conversation, parent, false);

        return new ConversationHolder(view);
    }

    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        User contactUser = convList.get(position).getContactUser();

        ((ConversationHolder) holder).bind(contactUser);
    }

    private class ConversationHolder extends RecyclerView.ViewHolder {
        private ImageView imageView;
        private TextView textView;

        ConversationHolder(View itemView) {
            super(itemView);

            textView = itemView.findViewById(R.id.conversation_title);
        }

        void bind(User user) {
            String userName = user.getUserName();

            textView.setText(userName);

            ColorGenerator generator = ColorGenerator.MATERIAL;

            int color = generator.getColor(userName);

            TextDrawable textDrawable = TextDrawable.builder()
                    .buildRound(userName.substring(0, 1), color);
            imageView = itemView.findViewById(R.id.conversation_image_view);
            imageView.setImageDrawable(textDrawable);
        }
    }
}