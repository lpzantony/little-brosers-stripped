package com.littlebrosers.gui;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.littlebrosers.R;
import com.littlebrosers.database.LBDatabaseHandler;
import com.littlebrosers.messages.ClearTextMessage;
import com.littlebrosers.messages.Conversation;
import com.littlebrosers.messages.EncryptedTextMessage;
import com.littlebrosers.messages.GenericMessage;
import com.littlebrosers.users.MainUser;
import com.littlebrosers.utils.Util;

import java.util.Arrays;

public class MessageListFragment extends Fragment implements View.OnClickListener {
    public static final int MAX_MESSAGE_SIZE = ClearTextMessage.MAX_TEXT_SIZE;
    private Context context;
    private RecyclerView conversationRecycler;
    private MessageListAdapter messageListAdapter;
    private TextInputEditText messageField;
    private MainUser mainUser;
    private Conversation conversation;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ((MainActivity) getActivity()).setActionBarTitle(conversation.getContactUser().getUserName());
        ((MainActivity) getActivity()).showBackIcon();
        View rootView = inflater.inflate(R.layout.fragment_message_list, container, false);

        conversationRecycler = rootView.findViewById(R.id.reyclerview_message_list);
        messageListAdapter = new MessageListAdapter(conversation.getMessages());
        conversationRecycler.setAdapter(messageListAdapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(rootView.getContext());
        layoutManager.setStackFromEnd(true);
        conversationRecycler.setLayoutManager(layoutManager);
        conversationRecycler.scrollToPosition(conversation.getMessages().size() - 1);

        messageField = rootView.findViewById(R.id.edittext_chatbox);
        messageField.requestFocus();
        Button sendButton = rootView.findViewById(R.id.button_chatbox_send);
        sendButton.setOnClickListener(this);

        TextInputLayout textInputLayout = rootView.findViewById(R.id.til_chatbox);
        textInputLayout.setErrorEnabled(true);
        textInputLayout.setCounterEnabled(true);
        textInputLayout.setCounterMaxLength(MAX_MESSAGE_SIZE);

        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        this.context = context;
        mainUser = ((MainActivity) context).getSelectedMainUser();
        conversation = ((MainActivity) context).getSelectedConversation();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_chatbox_send:
                addMessage();
                break;
            default:
                break;
        }
    }

    public void addMessage() {
        String messageText = messageField.getText().toString();

        if (messageText.length() > 0 && messageText.length() <= MAX_MESSAGE_SIZE) {
            // Create ClearTextMessage object
            ClearTextMessage clearTextMessage = new ClearTextMessage(mainUser.getCompanyId(),
                    mainUser.getNetworkId(), mainUser, conversation.getContactUser(), messageText);
            // Add message to conversation
            conversation.addMessage(clearTextMessage);
            // Load database
            LBDatabaseHandler db = new LBDatabaseHandler(getContext());
            // Save message in database
            db.saveMessage(clearTextMessage, conversation);
            refreshFragment();

            // Convert message to raw and save it in the database
            GenericMessage genericMessage = new EncryptedTextMessage(clearTextMessage).toGenericMessage();
            try {
                Log.i("Before save", Arrays.toString(genericMessage.getSignature()));
                Log.i("Before save", Arrays.toString(genericMessage.fromRawMessage(genericMessage.toRawMessage()).getSignature()));
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                db.saveRawMessage(genericMessage.getMessageId(), genericMessage.toRawMessage());
            } catch (Exception e) {
                e.printStackTrace();
            }

            final class Attach implements Runnable {
                private GenericMessage genericMessage;

                private Attach(GenericMessage genericMessage) {
                    this.genericMessage = genericMessage;
                }

                public void run() {
                    synchronized (Util.LOCK) {
                        // Attach to tree
                        Util.attachMessageToTree(genericMessage);
                    }
                }
            }
            new Thread(new Attach(genericMessage)).start();

            // Deprecate all drops since a new message was generated
            mainUser.getRepository().deprecateAll();
        }
    }

    public void refreshFragment() {
        messageField.setText("");

        conversationRecycler.scrollToPosition(conversation.getMessages().size() - 1);

        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                .detach(this)
                .attach(this)
                .commit();
    }
}
