package com.littlebrosers.messages;

import android.support.annotation.NonNull;

import com.littlebrosers.users.User;
import com.littlebrosers.utils.CryptoUtil;

import java.io.ByteArrayInputStream;
import java.nio.ByteBuffer;
import java.security.InvalidParameterException;

import static java.lang.Math.min;

/**
 * Created by chris on 11/29/17.
 * Class to define clear messages that can be read
 */


public class ClearTextMessage extends AbstractMessage implements Comparable<ClearTextMessage> {
    // Max clear payload size = 190, it includes a 4-byte integer as well as the text
    public static final int MAX_TEXT_SIZE = 190 - 4;
    private static int counter;
    private String cleartext;
    private int textSize;
    private int messageId;

    public ClearTextMessage(short companyId, short networkId) {
        super(companyId, networkId);
    }

    public ClearTextMessage(short companyId, short networkId, User fromUser, User toUser, String text) {
        super(companyId, networkId, fromUser, toUser);

        if (text == null) {
            return;
        }

        if (text.length() > MAX_TEXT_SIZE) {
            throw new InvalidParameterException("Text size " + text.length()
                    + " is greater than the maximum text size " + MAX_TEXT_SIZE);
        }

        messageId = ++counter;

        cleartext = text;
        setTextSize();
    }

    public ClearTextMessage(EncryptedTextMessage msg) {
        super(msg);
        // Check if the message is destined for the user
        if (!msg.isClear()) {
            throw new IllegalArgumentException("Encrypted message cannot be read");
        }
        byte[] payload = CryptoUtil.decrypt(msg.getCiphertext(), msg.getKeyHash(), getCompanyId(),
                getNetworkId());
        // Parse from_id and clear text
        final int idLength = 4;
        ByteArrayInputStream buf = new ByteArrayInputStream(payload);
        textSize = min(MAX_TEXT_SIZE, payload.length - idLength);
        byte[] temp = new byte[idLength];

        int status = buf.read(temp, 0, idLength);
        if (status != idLength) {
            throw new IllegalArgumentException("Invalid input");
        }

        int innerFromId = ByteBuffer.wrap(temp).getInt();

        if (innerFromId != getFromId()) {
            // Disregard message
            throw new IllegalArgumentException("Invalid input");
        }

        messageId = ++counter;

        temp = new byte[textSize];

        status = buf.read(temp, 0, textSize);
        if (status == -1) {
            throw new IllegalArgumentException("Invalid input");
        }

        cleartext = new String(temp);
    }

    public static int getCounter() {
        return counter;
    }

    // To be called after context restore from database
    public static void setCounter(int newCounter) {
        counter = newCounter;
    }

    public String getCleartext() {
        return cleartext;
    }

    public void setCleartext(String cleartext) {
        if (cleartext.length() > MAX_TEXT_SIZE) {
            throw new InvalidParameterException("Text size " + cleartext.length()
                    + " is greater than the maximum text size " + MAX_TEXT_SIZE);
        }
        this.cleartext = cleartext;
        setTextSize();
    }

    public int getTextSize() {
        return textSize;
    }

    private void setTextSize() {
        textSize = cleartext.length();
    }

    public int getMessageId() {
        return messageId;
    }

    public void setMessageId(int messageId) {
        this.messageId = messageId;
    }

    public String toString() {
        return "Clear Text Message:\n"
                + super.toString()
                + "-Text size: "
                + textSize + '\n'
                + "-Text: "
                + cleartext + '\n';
    }

    @Override
    public int compareTo(@NonNull ClearTextMessage clearTextMessage) {
        return this.getTimestamp().compareTo(clearTextMessage.getTimestamp());
    }
}
