package com.littlebrosers.database;

import android.util.SparseArray;

import com.littlebrosers.messages.ClearTextMessage;
import com.littlebrosers.messages.Conversation;
import com.littlebrosers.users.MainUserMap;

public interface LBDatabaseInterface {
    SparseArray<SparseArray<String>> getNetworks();

    SparseArray<SparseArray<String>> getServers();

    void getMainUsers(MainUserMap mainUserMap);

    void getDrops(MainUserMap mainUserMap);

    void getContacts(MainUserMap mainUserMap);

    void getConversations(MainUserMap mainUserMap);

    byte[] getRawMessage(int index);

    void saveNetworks(SparseArray<SparseArray<String>> networks);

    void saveMainUsers(MainUserMap mainUserMap);

    void saveDrops(MainUserMap mainUserMap);

    void saveContacts(MainUserMap mainUserMap);

    void saveConversations(MainUserMap mainUserMap);

    void saveMessage(ClearTextMessage message, Conversation conversation);

    void saveRawMessage(int index, byte[] rawMessage);

    void clearDatabase();

    void clearRawMessages();
}
