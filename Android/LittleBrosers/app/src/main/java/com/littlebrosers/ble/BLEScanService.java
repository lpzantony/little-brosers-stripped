package com.littlebrosers.ble;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.util.SparseArray;

import com.littlebrosers.R;
import com.littlebrosers.gui.MainActivity;
import com.littlebrosers.utils.Util;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;

import no.nordicsemi.android.support.v18.scanner.BluetoothLeScannerCompat;
import no.nordicsemi.android.support.v18.scanner.ScanFilter;
import no.nordicsemi.android.support.v18.scanner.ScanSettings;

import static no.nordicsemi.android.support.v18.scanner.ScanSettings.CALLBACK_TYPE_ALL_MATCHES;

public class BLEScanService extends Service {

    private static boolean running = false;
    private final BluetoothLeScannerCompat scanner;
    private final MyBLEScanCallback scanCallback;
    private final ScanSettings settings;
    private final ArrayList<ScanFilter> filters;
    private final Handler scanHandler;
    // in ms
    @SuppressWarnings("FieldCanBeLocal")
    private final int reportDelay = 750;
    // in ms
    @SuppressWarnings("FieldCanBeLocal")
    private final int scanningTime = 2000;
    @SuppressWarnings("FieldCanBeLocal")
    private final int idleTime = 3000;
    private Boolean scanning = false;
    private final Runnable stopScanRunnable = new Runnable() {
        @Override
        public void run() {
//            Log.i("BLE Scan Service", "Stopped scanning");
            if (scanning) {
                scanner.stopScan(scanCallback);
            }
            scanning = false;
            startBLEScan();
        }
    };
    private Boolean dfuMode;
    private PendingIntent pendingIntent;
    private final Runnable startScanRunnable = new Runnable() {
        @Override
        public void run() {
            if (!isBluetoothAvailable()) {
                // Alert user
                Notification notification =
                        new NotificationCompat.Builder(getApplicationContext(),
                                getString(R.string.default_notification_channel_id))
                                .setContentTitle("BLE scan")
                                .setContentIntent(pendingIntent)
                                .setContentText("Bluetooth is OFF. Turn it back on to continue scanning")
                                .setSmallIcon(android.R.drawable.stat_sys_data_bluetooth)
                                .setOngoing(true)
                                .build();

                NotificationManager nManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                if (nManager != null) {
                    nManager.notify(R.string.default_notification_channel_id, notification);
                }

                startBLEScan();
            } else {
                scanning = true;
                scanner.startScan(filters, settings, scanCallback);
//                Log.i("BLE Scan Service", "Started scanning");
                startBLEScan();
            }
        }
    };
    private Notification scanNotification;

    public BLEScanService() {
        super();
        scanHandler = new Handler();

        // Initialize Bluetooth adapter.
        scanner = BluetoothLeScannerCompat.getScanner();
        scanCallback = new MyBLEScanCallback(this);

        settings = new ScanSettings.Builder()
                .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY).setReportDelay(reportDelay)
                .setUseHardwareBatchingIfSupported(false).setCallbackType(CALLBACK_TYPE_ALL_MATCHES)
                .build();

        // Generate filters from the networks list

        ByteBuffer buff;
        ScanFilter filter;
        filters = new ArrayList<>();

        // Data bitmask (only the first 8 bytes are useful for filtering)
        int bufferLength = 14;
        final byte[] dataMask = new byte[bufferLength];
        final int filterLength = 8;
        final byte maskValue = (byte) 0xFF;
        Arrays.fill(dataMask, 0, filterLength, maskValue);

        SparseArray<SparseArray<String>> networks = Util.getNetworks();
        if (networks == null) {
            Log.w("ERR", "No networks");
            return;
        }
        for (int i = 0; i < networks.size(); i++) {
            short companyId = (short) networks.keyAt(i);
            SparseArray<String> company = networks.get(companyId);
            for (int j = 0; j < company.size(); j++) {
                short networkId = (short) company.keyAt(j);

                // Byte array with expected advertising manufacturer specific data
                buff = ByteBuffer.allocate(bufferLength);

                buff.putInt(Util.LITTLE_BROSERS_ID);
                buff.putShort(companyId);
                buff.putShort(networkId);
                buff.array();

                int manufacturerId = 0xFFFF;
                filter = new ScanFilter.Builder()
                        .setManufacturerData(manufacturerId, buff.array(), dataMask)
                        .build();

                // Add the filter to the filters list
                filters.add(filter);
            }
        }
    }

    public static boolean isRunning() {
        return running;
    }

    public static void resetRunning() {
        running = false;
    }

    /**
     * Check for Bluetooth.
     *
     * @return True if Bluetooth is available.
     */
    private static boolean isBluetoothAvailable() {
        final BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        return (bluetoothAdapter != null && bluetoothAdapter.isEnabled());
    }

    @Override
    public void onCreate() {
        super.onCreate();

        Intent notificationIntent = new Intent(this, MainActivity.class);
        pendingIntent =
                PendingIntent.getActivity(this, 0, notificationIntent, 0);

        // TODO May need to create a notification channel for android O
        scanNotification =
                new NotificationCompat.Builder(this, getString(R.string.default_notification_channel_id))
                        .setContentTitle("BLE scan")
                        .setContentText("Scanning...")
                        .setContentIntent(pendingIntent)
                        .setSmallIcon(android.R.drawable.stat_sys_data_bluetooth)
                        .setOngoing(true)
                        .build();
        scanNotification.flags = Notification.FLAG_ONGOING_EVENT | Notification.FLAG_NO_CLEAR;
    }

    // Scan for BLE devices
    private void startBLEScan() {
        running = true;

        if (scanning) {
            scanHandler.postDelayed(stopScanRunnable, scanningTime);
        } else {
            scanHandler.postDelayed(startScanRunnable, idleTime);
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public int onStartCommand(Intent intent, int flags, int startId) {
//        Log.i("BLE Scan Service", "onStartCommand");

        if (!isBluetoothAvailable()) {
            running = true;
            return START_NOT_STICKY;
        }

        if (scanning) {
            running = true;
            return START_NOT_STICKY;
        }

        dfuMode = intent.getBooleanExtra("dfuMode", false);

        stopProcess();
        startForeground(R.string.app_name, scanNotification);

        // Start scan
        startBLEScan();
        running = true;
        return START_NOT_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not implemented");
    }

    private void stopProcess() {
//        Log.i("BLE Scan Service", "stopProcess");
        scanHandler.removeCallbacks(startScanRunnable);
        scanHandler.removeCallbacks(stopScanRunnable);
        scanner.stopScan(scanCallback);
        running = false;
    }

    @Override
    public void onDestroy() {
        Log.i("BLE Scan Service", "onDestroy");
        stopProcess();
        super.onDestroy();
    }

    // Workaround from https://issuetracker.google.com/issues/36967794#c1
    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
        Log.i("BLE Scan Service", "onTaskRemoved");
        Intent intent = new Intent(this, DummyActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    public Boolean inDfuMode() {
        return dfuMode;
    }
}
