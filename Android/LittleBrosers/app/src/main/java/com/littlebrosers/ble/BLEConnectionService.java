package com.littlebrosers.ble;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.littlebrosers.R;
import com.littlebrosers.database.LBDatabaseHandler;
import com.littlebrosers.drops.Drop;
import com.littlebrosers.gui.MainActivity;
import com.littlebrosers.messages.ClearTextMessage;
import com.littlebrosers.messages.Conversation;
import com.littlebrosers.messages.EncryptedTextMessage;
import com.littlebrosers.messages.GenericMessage;
import com.littlebrosers.users.MainUser;
import com.littlebrosers.utils.Util;

import java.security.cert.CertificateEncodingException;
import java.util.ArrayList;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import static com.littlebrosers.ble.MyBLEGattCallback.AUTHENTICATION_COMPLETE_FLAG;
import static com.littlebrosers.ble.MyBLEGattCallback.DIFF_END_ACK_FLAG;
import static com.littlebrosers.ble.MyBLEGattCallback.DIFF_MID_ACK_FLAG;
import static com.littlebrosers.ble.MyBLEGattCallback.DIFF_START_ACK_FLAG;
import static com.littlebrosers.ble.MyBLEGattCallback.MERGE_END_FLAG;
import static com.littlebrosers.ble.MyBLEGattCallback.MERGE_WRITTEN_FLAG;
import static com.littlebrosers.ble.MyBLEGattCallback.READ_DATA_FLAG;
import static com.littlebrosers.ble.MyBLEGattCallback.READ_TIME_CERTIFICATE_FLAG;
import static com.littlebrosers.core.AuthenticationProcess.AUTHENTICATION_SERVICE_ID;
import static com.littlebrosers.core.AuthenticationProcess.AUTHENTICATION_SERVICE_UUID_MOST_SIG;
import static com.littlebrosers.core.AuthenticationProcess.DROP_CHALLENGE_UUID_MOST_SIG;
import static com.littlebrosers.core.AuthenticationProcess.DROP_RESPONSE_UUID_MOST_SIG;
import static com.littlebrosers.core.AuthenticationProcess.SERVICE_STATUS_UUID_MOST_SIG;
import static com.littlebrosers.core.AuthenticationProcess.TIME_CERTIFICATE_UUID_MOST_SIG;
import static com.littlebrosers.core.AuthenticationProcess.USER_CERTIFICATE_UUID_MOST_SIG;
import static com.littlebrosers.core.AuthenticationProcess.USER_CHALLENGE_UUID_MOST_SIG;
import static com.littlebrosers.core.AuthenticationProcess.USER_RESPONSE_UUID_MOST_SIG;
import static com.littlebrosers.core.AuthenticationProcess.answerChallenge;
import static com.littlebrosers.core.AuthenticationProcess.generateDropChallenge;
import static com.littlebrosers.core.AuthenticationProcess.verifyDropResponse;
import static com.littlebrosers.core.MergeProcess.MERGE_NONE_CODE;
import static com.littlebrosers.core.MergeProcess.MERGE_PAYLOAD_UUID_MOST_SIG;
import static com.littlebrosers.core.MergeProcess.MERGE_SERVICE_ID;
import static com.littlebrosers.core.MergeProcess.MERGE_SERVICE_UUID_MOST_SIG;
import static com.littlebrosers.core.MergeProcess.MERGE_STATUS_UUID_MOST_SIG;
import static com.littlebrosers.core.MergeProcess.MERGE_WRITTEN_CODE;
import static com.littlebrosers.core.MerkleTreeDiffProcess.DIFF_END_ACK_CODE;
import static com.littlebrosers.core.MerkleTreeDiffProcess.DIFF_MID_ACK_CODE;
import static com.littlebrosers.core.MerkleTreeDiffProcess.DIFF_PAYLOAD_UUID_MOST_SIG;
import static com.littlebrosers.core.MerkleTreeDiffProcess.DIFF_SERVICE_ID;
import static com.littlebrosers.core.MerkleTreeDiffProcess.DIFF_SERVICE_UUID_MOST_SIG;
import static com.littlebrosers.core.MerkleTreeDiffProcess.DIFF_START_ACK_CODE;
import static com.littlebrosers.core.MerkleTreeDiffProcess.DIFF_STATUS_UUID_MOST_SIG;


public class BLEConnectionService extends IntentService {
    private static final String TAG = "BLE Connection Service";
    private static final long TIMEOUT = 5000L;
    private final MyBLEGattCallback gattCallback = new MyBLEGattCallback();
    private BluetoothGatt bluetoothGatt;
    private BluetoothGattService diffService;
    private BluetoothGattService mergeService;

    @SuppressWarnings("FieldCanBeLocal")
    private Drop drop;
    private MainUser mainUser;
    private ArrayList<GenericMessage> receivedMessages;
    private long uuidLeastSigBits = 0;
    @SuppressWarnings("FieldCanBeLocal")
    private boolean newSent = false;
    @SuppressWarnings("FieldCanBeLocal")
    private boolean newReceived = false;

    public BLEConnectionService(String name) {
        super(name);
    }

    public BLEConnectionService() {
        super("BLEConnectionService");
    }

    private native void lbDiffWrapper();
    private native void saveSlotPool();
    private native int lbNextMsgToSendWrapper();

    private boolean writeCharacteristic(BluetoothGattService service, long uuidMostSig, byte[] data) {
        BluetoothGattCharacteristic characteristic =
                service.getCharacteristic(new UUID(uuidMostSig, uuidLeastSigBits));

        characteristic.setValue(data);
        bluetoothGatt.writeCharacteristic(characteristic);
        return await();
    }

    private byte[] readCharacteristic(BluetoothGattService service, long uuidMostSig) {
        BluetoothGattCharacteristic characteristic =
                service.getCharacteristic(new UUID(uuidMostSig, uuidLeastSigBits));
        bluetoothGatt.readCharacteristic(characteristic);
        if (!await() && gattCallback.getFlag() == READ_DATA_FLAG) {
            return null;
        }
        return gattCallback.getReadData();
    }

    // Authentication process
    private boolean authenticationProcess() {
        UUID serviceUuid = new UUID(AUTHENTICATION_SERVICE_UUID_MOST_SIG, uuidLeastSigBits);
        BluetoothGattService authService = bluetoothGatt.getService(serviceUuid);
        gattCallback.setRunningService(AUTHENTICATION_SERVICE_ID);

        // Enable notification
        UUID charUuid = new UUID(SERVICE_STATUS_UUID_MOST_SIG, uuidLeastSigBits);
        BluetoothGattCharacteristic characteristic = authService.getCharacteristic(charUuid);

        BluetoothGattDescriptor descriptor;
        try {
            bluetoothGatt.setCharacteristicNotification(characteristic, true);
            descriptor = characteristic.getDescriptor(
                    UUID.fromString(getString(R.string.ble_connection_descriptor_uuid)));
            descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
            bluetoothGatt.writeDescriptor(descriptor);
        } catch (Exception e) {
            return false;
        }
        if (!await()) {
            return false;
        }

        // Write time certificate
        Log.i(TAG, "Write time certificate");
        writeCharacteristic(authService, TIME_CERTIFICATE_UUID_MOST_SIG, mainUser.getTimeCertificate());
        if (!await()) {
            return false;
        }
        // Check if need to update the time certificate
        if (gattCallback.getFlag() == READ_TIME_CERTIFICATE_FLAG) {
            Log.i(TAG, "Reading new time certificate");
            byte[] newCertificate = readCharacteristic(authService, TIME_CERTIFICATE_UUID_MOST_SIG);
            if (newCertificate == null) {
                return false;
            }
            mainUser.setTimeCertificate(newCertificate);
        }

        // Write user certificate
        Log.i(TAG, "Write user certificate");
        if (mainUser.getCertificate() == null) {
            Log.i(TAG, "No certificate");
            return false;
        }

        try {
            writeCharacteristic(authService, USER_CERTIFICATE_UUID_MOST_SIG, mainUser.getCertificate().getEncoded());
            if (!await()) {
                return false;
            }
        } catch (CertificateEncodingException e) {
            e.printStackTrace();
        }


        // Read user challenge
        Log.i(TAG, "Read user challenge");
        byte[] challenge = readCharacteristic(authService, USER_CHALLENGE_UUID_MOST_SIG);
        if (challenge == null) {
            return false;
        }

        // Write drop challenge
        Log.i(TAG, "Write drop challenge");
        writeCharacteristic(authService, DROP_CHALLENGE_UUID_MOST_SIG, generateDropChallenge());
        if (!await()) {
            return false;
        }

        // Verify drop response
        Log.i(TAG, "Read drop response");
        byte[] dropResponse = readCharacteristic(authService, DROP_RESPONSE_UUID_MOST_SIG);
        if (dropResponse == null) {
            return false;
        }

        boolean dropAuth = verifyDropResponse(dropResponse, drop);
        Log.i(TAG, String.format("Drop response verification: %b", dropAuth));
        if (!dropAuth) {
            return false;
        }

        // Write user response
        Log.i(TAG, "Write user response");
        byte[] userResponse = answerChallenge(challenge, mainUser);
        writeCharacteristic(authService, USER_RESPONSE_UUID_MOST_SIG, userResponse);
        if (!await()) {
            return false;
        }

        // Wait for authentication complete signal
        if (gattCallback.getFlag() != AUTHENTICATION_COMPLETE_FLAG) {
            Log.i(TAG, "Authentication failure");
            return false;
        }
        Log.i(TAG, "Authentication success");
        // Disable notification
        charUuid = new UUID(SERVICE_STATUS_UUID_MOST_SIG, uuidLeastSigBits);
        characteristic = authService.getCharacteristic(charUuid);

        bluetoothGatt.setCharacteristicNotification(characteristic, false);
        descriptor = characteristic.getDescriptor(
                UUID.fromString(getString(R.string.ble_connection_descriptor_uuid)));
        descriptor.setValue(BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE);
        bluetoothGatt.writeDescriptor(descriptor);
        await();

        return true;
    }

    private boolean treeDiffProcess() {
        Log.i(TAG, "In tree diff process");
        UUID serviceUuid = new UUID(DIFF_SERVICE_UUID_MOST_SIG, uuidLeastSigBits);
        diffService = bluetoothGatt.getService(serviceUuid);
        gattCallback.setRunningService(DIFF_SERVICE_ID);

        Log.i(TAG, "Enabling notifications");
        gattCallback.setFlag(DIFF_END_ACK_FLAG);

        // Enable notification
        UUID charUuid = new UUID(DIFF_STATUS_UUID_MOST_SIG, uuidLeastSigBits);
        BluetoothGattCharacteristic characteristic = diffService.getCharacteristic(charUuid);
        BluetoothGattDescriptor descriptor;
        try {
            bluetoothGatt.setCharacteristicNotification(characteristic, true);
            descriptor = characteristic.getDescriptor(
                    UUID.fromString(getString(R.string.ble_connection_descriptor_uuid)));
            descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
            bluetoothGatt.writeDescriptor(descriptor);
        } catch (Exception e) {
            return false;
        }

        if (!await()) {
            return false;
        }


        Log.i("DIFF", "Notification enabled");
        lbDiffWrapper();
        return true;
    }

    private boolean mergeProcess() {
        UUID serviceUuid = new UUID(MERGE_SERVICE_UUID_MOST_SIG, uuidLeastSigBits);
        mergeService = bluetoothGatt.getService(serviceUuid);
        gattCallback.setRunningService(MERGE_SERVICE_ID);
        newReceived = false;
        newSent = false;

        receivedMessages = new ArrayList<>();

        // Init the process
        // The phone always initiate the merge process
        while (gattCallback.getFlag() != MERGE_END_FLAG) {
            Log.i(TAG, "Merge step");
            if (gattCallback.getFlag() == MERGE_WRITTEN_FLAG) {
                mergeHandleMsgIn();
            }
            mergeHandleMsgOut();
            if (!await()) {
                return false;
            }
        }
        return true;
    }

    private void registerMessages() {
        for (GenericMessage genericMessage : receivedMessages) {
            ClearTextMessage message;
            try {
                message = new ClearTextMessage(new EncryptedTextMessage(genericMessage));

                int fromId = message.getFromId();
                Conversation conv;
                if (mainUser.getConversations() == null || mainUser.getConversations()
                        .get(fromId) == null) {
                    conv = new Conversation(mainUser, mainUser.getContacts().get(fromId));
                    mainUser.addConversation(conv);
                } else {
                    conv = mainUser.getConversations().get(fromId);
                }

                try {
                    conv.addMessage(message);
                    LBDatabaseHandler db = new LBDatabaseHandler(this);
                    // Save message in database
                    db.saveMessage(message, conv);
                } catch (Exception e) {
                    e.printStackTrace();
                    continue;
                }
            } catch (Exception e) {
                e.printStackTrace();
                continue;
            }

            if (message == null) {
                continue;
            }


            Intent notificationIntent = new Intent(this, MainActivity.class);
            notificationIntent.putExtra(getString(R.string.new_message_notification_click_extra), true);
            notificationIntent.putExtra(getString(R.string.new_message_company_id_extra), mainUser.getCompanyId());
            notificationIntent.putExtra(getString(R.string.new_message_network_id_extra), mainUser.getNetworkId());
            notificationIntent.putExtra(getString(R.string.new_message_user_id_extra), message.getFromId());
            PendingIntent pendingIntent =
                    PendingIntent.getActivity(this, 0, notificationIntent, Intent.FILL_IN_ACTION);

            Notification messageNotification =
                    new NotificationCompat.Builder(this, getString(R.string.default_notification_channel_id))
                            .setContentTitle("New Message")
                            .setContentText("Received new message from " + message.getFromUser().getUserName())
                            .setContentIntent(pendingIntent)
                            .setSmallIcon(android.R.drawable.ic_dialog_email)
                            .setAutoCancel(true)
                            .build();

            NotificationManager nManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            if (nManager != null) {
                nManager.notify(R.string.message_notification_channel_id, messageNotification);
            }
        }

    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        if (intent == null) {
            return;
        }

        Log.i(TAG, "In onHandleIntent");

        Bundle bundle = intent.getBundleExtra(getString(R.string.ble_scan_result_bundle));
        if (bundle == null) {
            return;
        }

        short companyId = bundle.getShort(getString(R.string.ble_scan_result_company_id));
        short networkId = bundle.getShort(getString(R.string.ble_scan_result_network_id));
        short dropId = bundle.getShort(getString(R.string.ble_scan_result_drop_id));
        int counter = bundle.getInt(getString(R.string.ble_scan_result_counter));
        drop = Util.getDropRepository(companyId, networkId).getDrop(dropId);
        mainUser = Util.getMainUser(companyId, networkId);
        BluetoothDevice dropDevice = bundle.getParcelable(getString(R.string.ble_scan_result_drop_device));
        if (drop == null || dropDevice == null) {
            return;
        }

        // Retest if should connect
        if (!drop.shouldConnect(counter)) {
            return;
        }
        Log.i(TAG, drop.toString());
        uuidLeastSigBits = drop.getUuidLeastSigBits();
        Log.i(TAG, "Result received");

        // Connect to the drop and discover characteristics
        bluetoothGatt = dropDevice.connectGatt(this, false, gattCallback);
        if (!await()) {
            return;
        }

        boolean mtuReq = bluetoothGatt.requestMtu(512);
        Log.i(TAG, "MTU request " + String.valueOf(mtuReq));
        if (!mtuReq) {
            return;
        }

        if (!await()) {
            return;
        }

        Log.i(TAG, "Authentication process start");
        // Authentication process
        if (!authenticationProcess()) {
            disconnect();
            return;
        }

        synchronized (Util.LOCK) {

            Log.i(TAG, "Diff process start");
            // Merkle tree diff process
            if (!treeDiffProcess()) {
                disconnect();
                return;
            }

            Log.i(TAG, "Message exchange process start");
            // Message exchange process
            if (!mergeProcess()) {
                disconnect();
                return;
            }
        }

        gattCallback.setRunningService(0);

        if (newReceived) {
            Util.getDropRepository(companyId, networkId).deprecateAll();
        }

        // Update drop
        drop.update(newSent);

        registerMessages();
        saveSlotPool();

        Log.i(TAG, "Disconnecting");
        disconnect();
    }

    private boolean await() {
        boolean status = true;
        try {
            status = gattCallback.getCountDownLatch().await(TIMEOUT, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return status && (gattCallback.getFlag() != MyBLEGattCallback.DISCONNECTED_FLAG);
    }

    private void disconnect() {
        if (bluetoothGatt != null) {
            bluetoothGatt.close();
            bluetoothGatt = null;
            Log.i(TAG, "Disconnect");
        }
    }

    @Override
    public void onDestroy() {
        Log.i(TAG, "onDestroy");
        disconnect();
        super.onDestroy();
    }

    private void saveGenericMessage(GenericMessage genericMessage) throws Exception {
        LBDatabaseHandler db = new LBDatabaseHandler(this);
        db.saveRawMessage(genericMessage.getMessageId(), genericMessage.toRawMessage());
    }

    private GenericMessage loadGenericMessage(int messageId) {
        LBDatabaseHandler db = new LBDatabaseHandler(this);
        GenericMessage genericMessage = null;
        byte[] raw = db.getRawMessage(messageId);
        if (raw != null) {
            genericMessage = new GenericMessage(mainUser.getCompanyId(), mainUser.getNetworkId(),
                    messageId)
                    .fromRawMessage(raw);
        }
        return genericMessage;
    }

    // A message was written to the payload, save it to the db and attach it to the tree
    private void mergeHandleMsgIn() {
        // Read the raw message from the db
        byte[] rawMessage = readCharacteristic(mergeService, MERGE_PAYLOAD_UUID_MOST_SIG);
        // Reset the flag to written since it has been set to read by the callback
        gattCallback.setFlag(MERGE_WRITTEN_FLAG);

        GenericMessage genericMessage = new GenericMessage(mainUser.getCompanyId(),
                mainUser.getNetworkId());
        if (rawMessage == null) {
            Log.i(TAG, "Error retrieving raw message from payload char");
            // TODO: Handle disconnect
            return;
        }
        newReceived = true;
        genericMessage = genericMessage.fromRawMessage(rawMessage);

        Log.i(TAG, "Received message");
        // Save the message to the db
        try {
            saveGenericMessage(genericMessage);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Log.i(TAG, genericMessage.toString());
        if (genericMessage.isClear()) {
            Log.i(TAG, "Found one message for us");
            receivedMessages.add(genericMessage);
        }

        // Attach to the Merkle tree
        Util.attachMessageToTree(genericMessage);
    }

    // If there is a message to send, load it and send it
    // Set ack accordingly
    private void mergeHandleMsgOut() {
        int nextIndex = lbNextMsgToSendWrapper();

        if (nextIndex != -1) {
            newSent = true;

            // There is a message to send
            GenericMessage genericMessage = loadGenericMessage(nextIndex);
            byte[] rawMessage = null;
            try {
                rawMessage = genericMessage.toRawMessage();
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (rawMessage == null) {
                // TODO: Handle clean disconnect
                return;
            }

            Log.i(TAG, "Sending message");
            if (!writeCharacteristic(mergeService, MERGE_PAYLOAD_UUID_MOST_SIG, rawMessage)) {
                Log.i(TAG, "Failed to write ");
            }

            Log.i(TAG, "Sending ");
            // Write the WRITTEN_CODE to the status char
            byte[] ack = {MERGE_WRITTEN_CODE};
            writeCharacteristic(mergeService, MERGE_STATUS_UUID_MOST_SIG, ack);
            Log.i(TAG, "Sent written");
        } else {
            Log.i(TAG, "No message to send");
            // No more message to send
            byte[] ack = {MERGE_NONE_CODE};
            writeCharacteristic(mergeService, MERGE_STATUS_UUID_MOST_SIG, ack);
            Log.i(TAG, "Sent none");
        }
    }

    private boolean diffSend(byte[] payload) {
        // Wait for START ACK
        while (gattCallback.getFlag() != DIFF_START_ACK_FLAG) {
            if (!await()) {
                Log.i(TAG, "Did not receive start ack");
                return false;
            }
        }

        // Write payload
        if (!writeCharacteristic(diffService, DIFF_PAYLOAD_UUID_MOST_SIG, payload)) {
            Log.i(TAG, "Error reading payload");
            return false;
        }

        // Send MID ACK
        gattCallback.setFlag(DIFF_MID_ACK_FLAG);
        byte[] ack = {DIFF_MID_ACK_CODE};
        if (!writeCharacteristic(diffService, DIFF_STATUS_UUID_MOST_SIG, ack)) {
            Log.i(TAG, "Error writing mid ack");
            return false;
        }

        // Wait for END ACK
        while (gattCallback.getFlag() == DIFF_MID_ACK_FLAG) {
            if (!await()) {
                Log.i(TAG, "Did not receive end ack");
                return false;
            }
        }
        return true;
    }

    private byte[] diffReceive() {
        // Send START ACK
        gattCallback.setFlag(DIFF_START_ACK_FLAG);
        byte[] ack = {DIFF_START_ACK_CODE};
        if (!writeCharacteristic(diffService, DIFF_STATUS_UUID_MOST_SIG, ack)) {
            Log.i(TAG, "Error writing start ack");
            return null;
        }

        // Wait for MID ACK
        while (gattCallback.getFlag() != DIFF_MID_ACK_FLAG) {
            if (!await()) {
                Log.i(TAG, "Did not receive mid ack");
                return null;
            }
        }
        // Read payload
        byte[] payload = readCharacteristic(diffService, DIFF_PAYLOAD_UUID_MOST_SIG);

        // Send END ACK
        gattCallback.setFlag(DIFF_END_ACK_FLAG);
        ack[0] = DIFF_END_ACK_CODE;
        if (!writeCharacteristic(diffService, DIFF_STATUS_UUID_MOST_SIG, ack)) {
            Log.i(TAG, "Error writing end ack");
            return null;
        }

        return payload;
    }
}
