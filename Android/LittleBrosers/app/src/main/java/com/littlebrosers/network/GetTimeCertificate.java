package com.littlebrosers.network;

import android.annotation.SuppressLint;
import android.os.AsyncTask;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import static com.littlebrosers.core.AuthenticationProcess.TIME_CERTIFICATE_JSON_LENGTH;

public class GetTimeCertificate extends AsyncTask<String, Void, String> {
    private static final String TIME_ENDPOINT = "timestamp";

    @SuppressLint("DefaultLocale")
    @Override
    protected String doInBackground(String... data) throws IllegalArgumentException {
        if (data.length != 1) {
            throw new IllegalArgumentException(String.format(
                    "Expecting 1 argument, %d were given", data.length));
        }
        String urlString = data[0] + TIME_ENDPOINT;
        HttpURLConnection client;
        String response = null;
        try {
            URL url = new URL(urlString);
            client = (HttpURLConnection) url.openConnection();
            client.setRequestMethod("GET");


            byte[] cert = new byte[TIME_CERTIFICATE_JSON_LENGTH];
            InputStream in = new BufferedInputStream(client.getInputStream());
            //noinspection ResultOfMethodCallIgnored
            in.read(cert);
            in.close();

            response = new String(cert);

            client.disconnect();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }
}
