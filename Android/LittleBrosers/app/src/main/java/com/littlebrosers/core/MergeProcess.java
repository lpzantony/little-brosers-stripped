package com.littlebrosers.core;

import com.littlebrosers.utils.Util;

// The UUID values are the same as the diff process ones, but we keep separate variables in case the
// merge has to be moved to a different service someday
public final class MergeProcess {
    public static final int MERGE_SERVICE_ID = 0x2000;

    public static final long MERGE_SERVICE_UUID_MOST_SIG = 0x2000L << 32 | Util.UUID_MOST_SIG_CONST;
    public static final long MERGE_STATUS_UUID_MOST_SIG = (0x01L & 0xFF) << 32 | MERGE_SERVICE_UUID_MOST_SIG;
    public static final long MERGE_PAYLOAD_UUID_MOST_SIG = (0x02L & 0xFF) << 32 | MERGE_SERVICE_UUID_MOST_SIG;

    public static final int MERGE_WRITTEN_CODE = 0x07;
    public static final int MERGE_NONE_CODE = 0x08;
    public static final int MERGE_END_CODE = 0x09;

    private MergeProcess() {
    }
}
