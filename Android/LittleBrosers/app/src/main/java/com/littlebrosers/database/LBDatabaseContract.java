package com.littlebrosers.database;

import android.provider.BaseColumns;

public final class LBDatabaseContract {
    private LBDatabaseContract() {

    }

    public static class NetworkEntry implements BaseColumns {
        public static final String TABLE_NAME = "network_table";
        public static final String COLUMN_NAME_COMPANY = "company_id";
        public static final String COLUMN_NAME_NETWORK = "network_id";
        public static final String COLUMN_NAME_NETWORK_NAME = "network_name";
        public static final String COLUMN_NAME_SERVER_URL = "server_url";
    }

    public static class MainUserEntry implements BaseColumns {
        public static final String TABLE_NAME = "main_user_table";
        public static final String COLUMN_NAME_COMPANY = "company_id";
        public static final String COLUMN_NAME_NETWORK = "network_id";
        public static final String COLUMN_NAME_MAIN_USER_ID = "user_id";
        public static final String COLUMN_NAME_MAIN_USER_NAME = "username";
        public static final String COLUMN_NAME_MAIN_USER_TIME_CERTIFICATE = "user_time_certificate";
        public static final String COLUMN_NAME_MAIN_USER_CERTIFICATE = "user_certificate";
    }

    public static class DropEntry implements BaseColumns {
        public static final String TABLE_NAME = "drop_table";
        public static final String COLUMN_NAME_COMPANY = "company_id";
        public static final String COLUMN_NAME_NETWORK = "network_id";
        public static final String COLUMN_NAME_DROP_ID = "drop_id";
        public static final String COLUMN_NAME_DROP_COUNTER = "drop_counter";
        public static final String COLUMN_NAME_DROP_UPTODATE = "drop_uptodate";
        public static final String COLUMN_NAME_DROP_PUB_KEY = "drop_pub_key";
    }

    public static class UserEntry implements BaseColumns {
        public static final String TABLE_NAME = "user_table";
        public static final String COLUMN_NAME_COMPANY = "company_id";
        public static final String COLUMN_NAME_NETWORK = "network_id";
        public static final String COLUMN_NAME_CONTACT_USER_ID = "user_id";
        public static final String COLUMN_NAME_CONTACT_USER_NAME = "username";
        public static final String COLUMN_NAME_USER_EC_PUB_KEY = "user_ec_pub_key";
        public static final String COLUMN_NAME_USER_RSA_PUB_KEY = "user_rsa_pub_key";
    }

    public static class MessageEntry implements BaseColumns {
        public static final String TABLE_NAME = "message_table";
        public static final String COLUMN_NAME_MESSAGE_ID = "message_id";
        public static final String COLUMN_NAME_TIMESTAMP = "message_timestamp";
        public static final String COLUMN_NAME_FROM_ID = "message_from_id";
        public static final String COLUMN_NAME_TO_ID = "message_to_id";
        public static final String COLUMN_NAME_TEXT = "message_text";
    }

    public static class ConversationEntry implements BaseColumns {
        public static final String TABLE_NAME = "conversation_table";
        public static final String COLUMN_NAME_COMPANY = "company_id";
        public static final String COLUMN_NAME_NETWORK = "network_id";
        public static final String COLUMN_NAME_CONTACT_USER_ID = "user_id";
        public static final String COLUMN_NAME_CONVERSATION_ID = "conversation_id";
    }

    public static class ConversationMapEntry implements BaseColumns {
        public static final String TABLE_NAME = "conversation_map_table";
        public static final String COLUMN_NAME_CONVERSATION_ID = "conversation_id";
        public static final String COLUMN_NAME_MESSAGE_ID = "message_id";
    }

    public static class RawMessageEntry implements BaseColumns {
        public static final String TABLE_NAME = "raw_message_table";
        public static final String COLUMN_NAME_COMPANY = "company_id";
        public static final String COLUMN_NAME_NETWORK = "network_id";
        public static final String COLUMN_NAME_RAW_MESSAGE_ID = "raw_message_id";
        public static final String COLUMN_NAME_PAYLOAD = "payload";
    }
}
